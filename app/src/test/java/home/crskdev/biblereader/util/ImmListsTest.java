package home.crskdev.biblereader.util;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by criskey on 12/7/2017.
 */
public class ImmListsTest {
    @Test
    public void reduce() throws Exception {
        int sum = ImmLists.reduce(Arrays.asList(1, 2, 3, 4), 0, (acc, elem, index) -> acc + elem);
        assertEquals(10, sum);
        int indexOf = ImmLists.reduce(Arrays.asList("a", "b", "c", "d"), -1,
                (acc, elem, index) -> elem.equals("c") ? index : acc);
        assertEquals(2, indexOf);
        int notFound = ImmLists.reduce(Arrays.asList("a", "b", "c", "d"), -1,
                (acc, elem, index) -> elem.equals("z") ? index : acc);
        assertEquals(-1, notFound);
    }

}