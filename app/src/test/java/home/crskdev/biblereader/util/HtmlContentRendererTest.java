package home.crskdev.biblereader.util;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;

import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.Chapter;
import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;

import static org.junit.Assert.*;

/**
 * Created by criskey on 6/6/2017.
 */
public class HtmlContentRendererTest {
    @Test
    public void render() throws Exception {

        List<Content> content = Arrays.asList(
                new Book("b1", "b1"),
                new Chapter(1, "b1", "ch1"),
                Verset.asDTO("b1", 1, 1, "v1", false),
                new Chapter(2, "b1", "ch2"),
                Verset.asDTO("b1", 2, 1, "v2", false)
        );

        String html = new HtmlContentRenderer().render(content);
        System.out.println(html);


        String b = "<h1>b1</h1>";
        String ch1 = "<p><h3>ch1</h3><p>v1</p></p>";
        String ch2 = "<p><h3>ch2</h3><p>v2</p></p>";
        assertEquals(b + ch1 + ch2, html);

    }

}