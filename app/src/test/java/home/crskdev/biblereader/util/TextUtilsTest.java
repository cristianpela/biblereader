package home.crskdev.biblereader.util;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * Created by criskey on 4/7/2017.
 */
public class TextUtilsTest {

    @Test
    public void equalsIgnoreDiacriticsAndCase() throws Exception {
        assertTrue(TextUtils.equalsIgnoreDiacriticsAndCase(
                "ăbcdțeî",
                "abcdtei"
        ));
    }

    @Test
    public void startsWithIgnoreDiacritics() throws Exception {
        assertTrue(TextUtils.startsWithIgnoreDiacritics(
                "ăbcdțeî",
                "abc"
        ));

    }

    @Test
    public void containsIgnoreDiacritics() throws Exception {
        assertTrue(TextUtils.containsIgnoreDiacritics(
                "pavel, întemniţat al lui isus hristos, şi fratele timotei, către preaiubitul filimon, tovarăşul nostru de lucru,",
                "isus"
        ));

    }

    @Test
    public void multipleIndexesOf() throws Exception {
        assertEquals(
                Arrays.asList(0, 8, 16, 20),
                TextUtils.multipleIndexOf("abc cfd abc xxx abc abc", "abc")
        );
    }

    @Test
    public void isEmpty() throws Exception {
        assertTrue(TextUtils.isEmpty(null));
        assertTrue(TextUtils.isEmpty(""));
        assertTrue(TextUtils.isEmpty("  "));
        assertFalse(TextUtils.isEmpty("A"));
    }

}