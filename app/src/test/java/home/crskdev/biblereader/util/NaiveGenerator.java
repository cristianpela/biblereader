package home.crskdev.biblereader.util;

import home.crskdev.biblereader.favorites.detail.Identifiable;

/**
 * Created by criskey on 7/7/2017.
 */

public class NaiveGenerator implements Identifiable {

    String id;

    public NaiveGenerator(String id) {
        this.id = id;
    }

    @Override
    public String getOwnerId() {
        return id;
    }

    @Override
    public Identifiable getSource() {
        return this;
    }
}
