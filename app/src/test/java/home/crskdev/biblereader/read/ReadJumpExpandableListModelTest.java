package home.crskdev.biblereader.read;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crskdev.biblereader.BasePresenterTest;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.read.ReadJumpExpandableListModel.Book;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.biblereader.read.ReadJumpExpandableListModel.*;
import static org.junit.Assert.*;

/**
 * Created by criskey on 29/5/2017.
 */
public class ReadJumpExpandableListModelTest extends BasePresenterTest {

    private ReadJumpExpandableListModel model;
    private BehaviorSubject<ReadJumpExpandableListModel> state;

    @Test
    public void equals() throws Exception {

        assertEquals(3, model.getBooks().size());
        assertEquals(2, model.getChapters().get(model.getBooks().get(0)).size());

        ReadJumpExpandableListModel collapsed = model.collapse(0);
        assertEquals(true, model.equals(collapsed));
        //when out of bounds -> safe fail to current
        ReadJumpExpandableListModel collapsed2 = collapsed.collapse(3);
        assertEquals(true, collapsed2.equals(collapsed));

        ReadJumpExpandableListModel expanded = collapsed.expand(0);
        assertEquals(false, expanded.equals(collapsed));
        assertEquals(true, expanded.getBooks().get(0).expanded);

        assertEquals(3, expanded.getBooks().size());
        assertEquals(2, expanded.getChapters().get(expanded.getBooks().get(0)).size());
    }

    @Test
    public void testingDistinctStates() throws Exception {
        TestObserver<ReadJumpExpandableListModel> test = state.distinctUntilChanged().test();

        ReadJumpExpandableListModel collapse = model.collapse(0);
        state.onNext(collapse);

        collapse = collapse.collapse(0);
        state.onNext(collapse);

        collapse = collapse.collapse(0);
        state.onNext(collapse);

        ReadJumpExpandableListModel expanded = collapse.expand(0);
        state.onNext(expanded);

        assertFalse(expanded.equals(collapse));

        test.assertValueAt(0, model::equals);
        test.assertValueAt(1, expanded::equals);

        assertEquals(2, test.valueCount());
    }

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        List<Book> books = new ArrayList();
        Map<Book, List<Chapter>> chapters = new HashMap<>();
        Book b1 = new Book("1", "title1", false);
        chapters.put(b1, Arrays.asList(new Chapter(1, "a"), new Chapter(2, "b")));
        Book b2 = new Book("2", "title1", false);
        chapters.put(b2, Arrays.asList(new Chapter(1, "a"), new Chapter(2, "b")));
        Book b3 = new Book("3", "title1", false);
        chapters.put(b3, Arrays.asList(new Chapter(1, "a"), new Chapter(2, "b")));
        books.add(b1);
        books.add(b2);
        books.add(b3);
        model = new ReadJumpExpandableListModel(books, chapters);
        stateContainer.subjectJumpListState.onNext(model);
    }

}