package home.crskdev.biblereader.read;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import home.crskdev.biblereader.core.BibleInfo;
import home.crskdev.biblereader.core.BibleInfoLoader;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static home.crskdev.biblereader.core.BibleInfo.*;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 15/5/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ReadStateServiceTest {

    List<BookInfo> bookInfos = Arrays.asList(
            new BookInfo("id1", "Book1", 2, new HashMap<>()),
            new BookInfo("id2", "Book1", 3, new HashMap<>())
    );


    @Mock
    BibleInfoLoader loader;

    ReadStateService svc;

    @Before
    public void setUp() throws Exception {
        svc = new ReadStateService(null, loader);
        when(loader.load()).thenReturn(Single.just(new BibleInfo(bookInfos)));
    }

    @Test
    public void shouldBeStartAndEndWhenSingleBooksInList() throws Exception {
        when(loader.load()).thenReturn(Single.just(new BibleInfo(
                Collections.singleton(new BookInfo("id1", "Book1", 1, new HashMap<>()))
        )));
        TestObserver<ReadState> test = svc.readFrom(new ReadState("id1", 1)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 1 && v.isStart && v.isEnd);
    }

    @Test
    public void shouldReadNextChapter() throws Exception {
        TestObserver<ReadState> test = svc.readNext(new ReadState("id1", 1)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 2);
    }

    @Test
    public void shouldReadNextBookAfterPrevBookLastChapter() throws Exception {
        TestObserver<ReadState> test = svc.readNext(new ReadState("id1", 2)).test();
        test.assertValue((v) -> v.bookId.equals("id2") && v.chapter == 1);
    }

    @Test
    public void shouldShowEndWhenWeReachEndOfBooks() throws Exception {
        TestObserver<ReadState> test = svc.readNext(new ReadState("id2", 2)).test();
        test.assertValue((v) -> v.bookId.equals("id2") && v.chapter == 3 && v.isEnd);
    }

    @Test
    public void shouldReadTheSameChapterWhenReachEndOfBooks() throws Exception {
        TestObserver<ReadState> test = svc.readNext(new ReadState("id2", 3)).test();
        test.assertValue((v) -> v.bookId.equals("id2") && v.chapter == 3 && v.isEnd);
    }

    @Test
    public void shouldReadPrevChapter() throws Exception {
        TestObserver<ReadState> test = svc.readPrev(new ReadState("id1", 2)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 1);
    }

    @Test
    public void shouldReadPrevBookAfterPrevBookFirtChapter() throws Exception {
        TestObserver<ReadState> test = svc.readPrev(new ReadState("id2", 1)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 2);
    }

    @Test
    public void shouldReadTheSameChapterWhenReachStartOfBooks() throws Exception {
        TestObserver<ReadState> test = svc.readPrev(new ReadState("id1", 1)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 1 && v.isStart);
    }


    @Test
    public void shouldShowStartWhenWeReachStartOfBooks() throws Exception {
        TestObserver<ReadState> test = svc.readPrev(new ReadState("id1", 2)).test();
        test.assertValue((v) -> v.bookId.equals("id1") && v.chapter == 1 && v.isStart);
    }

}