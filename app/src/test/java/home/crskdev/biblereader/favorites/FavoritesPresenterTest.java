package home.crskdev.biblereader.favorites;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.BasePresenterTest;
import home.crskdev.biblereader.TabEventDispatcher;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.WaitEventDispatcher;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.biblereader.favorites.FavoritesPresenter.SIZE;
import static home.crskdev.biblereader.favorites.FavoritesState.ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.DEFAULT_ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_TAG;
import static home.crskdev.biblereader.favorites.FavoritesState.INIT_DATE;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 8/6/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class FavoritesPresenterTest extends BasePresenterTest{

    @Mock
    FavoritesService svc;
    @Mock
    FavoritesView view;
    @Mock
    FavoriteColorPalette palette;

    private FavoritesPresenter presenter;
    private BehaviorSubject<FavoritesState> state;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        state = stateContainer.subjectFavoriteState;
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        presenter = new FavoritesPresenter(bus, stateContainer, Schedulers.trampoline(), svc,
                palette, new WaitEventDispatcher(bus), mock(TabEventDispatcher.class));
        when(palette.nextColor(anyString())).thenReturn(1);
    }

    @Test
    public void testLoadInitialData() throws Exception {
        List<Tag> tags = Collections.emptyList();
        List<Verset> versets = Arrays.asList(
                new Verset("b1", "bt1", 1, 1, "c1", true, 1, tags),
                new Verset("b1", "bt1", 1, 2, "c2", true, 2, tags),
                new Verset("b1", "bt1", 1, 3, "c3", true, 3, tags),
                new Verset("b1", "bt1", 1, 4, "c4", true, 4, tags));
        when(svc.getAllFavorites(SIZE, INIT_DATE)).thenReturn(
                Single.just(versets));
        TestObserver<Event<?>> busTest = bus.toObservable().test();
        TestObserver<FavoritesState> stateTest = state.test();

        presenter.attach(view);
        verify(view).display(presenter.toColored(versets));

        busTest.assertValueAt(0, (v) -> v.equals(new Event<Boolean>(EventID.WAIT, true)));
        busTest.assertValueAt(1, (v) -> v.equals(new Event<Boolean>(EventID.WAIT, false)));

        presenter.detach();

        assertEquals(2, stateTest.valueCount());
        stateTest.assertValueAt(0, v -> v.equals(FavoritesState.DEFAULT));
        stateTest.assertValueAt(1, v -> v.equals(new FavoritesState(FILTER_MODE_BOOK, FavoritesState.ALL_BOOKS, null, 4L, false)));
    }

    @Test
    public void testLoadInitialDataFavoritesWithTags() throws Exception {
        List<Tag> tags = Collections.emptyList();
        List<Verset> versets = Arrays.asList(
                new Verset("b1", "bt1", 1, 1, "c1", true, 1, tags),
                new Verset("b1", "bt1", 1, 2, "c2", true, 2, tags),
                new Verset("b1", "bt1", 1, 3, "c3", true, 3, tags),
                new Verset("b1", "bt1", 1, 4, "c4", true, 4, tags));
        state.onNext(state.getValue().filterMode(FILTER_MODE_TAG));
        doReturn(Single.just(versets)).when(svc).getAllFavoritesWithTag(eq(SIZE), anyLong());
        doReturn(Single.just(versets)).when(svc).getAllFavoritesWithTagUntil(eq(SIZE), anyLong());
        TestObserver<FavoritesState> stateTest = state.test();

        presenter.attach(view);
        presenter.detach();
        presenter.attach(view);
        presenter.nextPage();

        assertEquals(3, stateTest.valueCount());
        stateTest.assertValueAt(0, v -> v.equals(DEFAULT_ALL_TAGS));
        stateTest.assertValueAt(1, v -> v.equals(new FavoritesState(FILTER_MODE_TAG,
                null, ALL_TAGS, 4L, false)));
        stateTest.assertValueAt(2, v -> v.equals(new FavoritesState(FILTER_MODE_TAG,
                null, ALL_TAGS, 4L, true)));

        verify(view, times(2)).display(presenter.toColored(versets));
    }


    @Test
    public void nextBatchFavorites() throws Exception {
        List<Tag> tags = Collections.emptyList();
        List<Verset> versets = Arrays.asList(
                new Verset("b1", "bt1", 1, 1, "c1", true, 1, tags),
                new Verset("b1", "bt1", 1, 2, "c2", true, 2, tags),
                new Verset("b1", "bt1", 1, 3, "c3", true, 3, tags),
                new Verset("b1", "bt1", 1, 4, "c4", true, 4, tags));
        when(svc.getAllFavoritesUntil(anyLong())).thenReturn(
                Single.just(versets));
        when(svc.getAllFavorites(anyInt(), anyLong())).thenReturn(
                Single.just(versets));
        TestObserver<FavoritesState> stateTest = stateContainer.subjectFavoriteState.test();

        //mPresenter life cycle
        presenter.attach(view);
        presenter.nextPage();
        presenter.detach();
        presenter.attach(view);

        ArgumentCaptor<Long> dateCaptor = ArgumentCaptor.forClass(Long.class);

        verify(svc, times(2)).getAllFavorites(anyInt(), dateCaptor.capture());
        assertEquals(FavoritesState.INIT_DATE, dateCaptor.getAllValues().get(0).longValue());
        assertEquals(4L, dateCaptor.getAllValues().get(1).longValue());

        verify(svc).getAllFavoritesUntil(4L);
        verify(view, times(2)).display(presenter.toColored(versets));
        verify(view, times(1)).displayBatch(presenter.toColored(versets));

        assertEquals(3, stateTest.valueCount());
        stateTest.assertValueAt(0, v -> v.equals(FavoritesState.DEFAULT));
        stateTest.assertValueAt(1, v -> v.equals(new FavoritesState(FILTER_MODE_BOOK, FavoritesState.ALL_BOOKS, null, 4L, true)));
        stateTest.assertValueAt(2, v -> v.equals(new FavoritesState(FILTER_MODE_BOOK, FavoritesState.ALL_BOOKS, null, 4L, false)));
    }

}