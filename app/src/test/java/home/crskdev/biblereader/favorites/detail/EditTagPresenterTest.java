package home.crskdev.biblereader.favorites.detail;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crskdev.biblereader.BasePresenterTest;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.EditedTag;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.Owner;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagOwnershipManager;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import home.crskdev.biblereader.util.NaiveGenerator;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.favorites.detail.FavoriteDetailView.ARG_KEY_VERSET;
import static home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 27/6/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class EditTagPresenterTest extends BasePresenterTest {

    EditTagPresenter presenter;

    @Mock
    FavoritesService svc;

    @Mock
    ResourceFinder res;

    @Mock
    EditTagView view;

    TagOwnershipManager tagOwnershipManager;
    private Owner master;
    private Owner slave;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        tagOwnershipManager = new TagOwnershipManager();
        presenter = spy(new EditTagPresenter(bus, stateContainer,
                Schedulers.trampoline(), svc, tagOwnershipManager, mock(ResourceFinder.class)));
        doAnswer(invocation -> {
            EditedTag editedTag = invocation.getArgument(0);
            TagWrapper editTw = invocation.getArgument(1);
            return new TagWrapper(editTw.owner, editedTag);
        }).when(presenter).decorateEditedTag(any(EditedTag.class), any(TagWrapper.class));

        Map<String, Object> args = new HashMap<>();

        master = new Owner(true, new NaiveGenerator("1"));
        slave = new Owner(false, new NaiveGenerator("!"));
        args.put(ARG_KEY_OWNERSHIP_MANAGER_ID, Arrays.asList(
                master,
                slave
        ));
        args.put(ARG_KEY_VERSET, Verset.DEFAULT);
        doReturn(args).when(view).getArgs();
    }

    @Test
    public void commitCompletable() throws Exception {

        presenter.attach(view);
        when(svc.editTags(any(List.class))).thenReturn(Completable.complete());
        when(svc.existTag(any(Tag.class))).thenReturn(Single.just(false));

        //select tag
        select(master, new Tag(1, "foo"));
        presenter.addToHistory("foo1");

        select(master, new Tag(2, "bar"));
        presenter.addToHistory("bar1");

        select(master, new Tag(1, "foo1"));
        presenter.addToHistory("foo2");

        select(master, new Tag(2, "bar1"));
        presenter.addToHistory("bar2");

        String history = presenter.getHistory().stream().map(t -> t.tag.id + "." + t.tag.name)
                .reduce((acc, curr) -> acc + "|" + curr).get();
        System.out.println(history);

        presenter.commitCompletable().subscribe();

        ArgumentCaptor<List<Tag>> captor = ArgumentCaptor.forClass(List.class);
        verify(svc).editTags(captor.capture());
        String out = captor.getValue().stream().map(t -> t.id + "." + t.name)
                .reduce((acc, curr) -> acc + "|" + curr).get();

        assertEquals("2.bar2|1.foo2", out);

    }

    @NonNull
    private Tag select(Owner master, Tag tag) {
        TagWrapper tw = new TagWrapper(master.id, tag);
        bus.dispatch(new Event<>(GroupEventID.GROUP_TAG, EventID.TAG_SELECT, tw));
        return tag;
    }

}