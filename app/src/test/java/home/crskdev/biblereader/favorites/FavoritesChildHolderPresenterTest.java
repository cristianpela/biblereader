package home.crskdev.biblereader.favorites;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import home.crskdev.biblereader.BasePresenterTest;
import home.crskdev.biblereader.BaseTest;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailFragmentView;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailView;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.favorites.FavoritesChildHolderPresenter.FAVORITES_DETAIL_VIEW_FRAGMENT_TAG;
import static home.crskdev.biblereader.favorites.FavoritesChildHolderPresenter.FAVORITES_VIEW_FRAGMENT_TAG;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by criskey on 19/6/2017.
 */
public class FavoritesChildHolderPresenterTest extends BasePresenterTest {
    @Test
    public void checkChangeView() throws Exception {
        FavoritesChildHolderView view = mock(FavoritesChildHolderView.class);
        Verset verset = Verset.asDTO("", 1, 1, "", true);

        FavoritesChildHolderPresenter presenter = new FavoritesChildHolderPresenter(
                bus, new StateContainer(bus), Schedulers.trampoline());
        presenter.attach(view);
        bus.dispatch(new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_SELECT,
                verset));

        ArgumentCaptor<String> fragTag = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Class<? extends BaseFragment>> inst = ArgumentCaptor.forClass(Class.class);
        ArgumentCaptor<Map<String, ?>> args = ArgumentCaptor.forClass(Map.class);
        ArgumentCaptor<Boolean> backStack = ArgumentCaptor.forClass(Boolean.class);

        verify(view, times(2)).addOrReplaceView(fragTag.capture(),
                inst.capture(), args.capture(), backStack.capture());

        assertEquals(Arrays.asList(FAVORITES_VIEW_FRAGMENT_TAG, FAVORITES_DETAIL_VIEW_FRAGMENT_TAG),
                fragTag.getAllValues());
        assertEquals(Arrays.asList(FavoritesViewFragment.class, FavoriteDetailFragmentView.class),
                inst.getAllValues());
        Map<String, Object> map = new HashMap<>();
        map.put(FavoriteDetailView.ARG_KEY_VERSET, verset);
        assertEquals(Arrays.asList(Collections.emptyMap(), map), args.getAllValues());
        assertEquals(Arrays.asList(false, true), backStack.getAllValues());
    }

}