package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.favorites.detail.Identifiable;
import home.crskdev.biblereader.favorites.detail.Tag;

import static home.crskdev.biblereader.TestUtils.mapList;
import static org.junit.Assert.assertEquals;

/**
 * Created by criskey on 24/6/2017.
 */
public class TagOwnershipManagerTest {

    TagOwnershipManager manager;

    @Before
    public void setup() throws Exception {
        manager = new TagOwnershipManager();
    }

    @Test
    public void decideAddTagMasterRelevant() throws Exception {
        Owner o1 = createOwner(true);
        Owner o2 = createOwner(false);
        Owner o3 = createOwner(false);
        manager.addOwner(o1);
        manager.addOwner(o2);
        manager.addOwner(o3);

        //simulate o1 selects a tag;
        Map<Integer, List<TagWrapper>> orders = manager.decideAddTag(new TagWrapper(o1.id, createTag()),
                true);
        List<TagWrapper> tws = orders.get(EventID.TAG_ADD);
        assertEquals(Arrays.asList(o2.id, o3.id), mapList(tws, tw -> tw.owner));

    }

    @Test
    public void decideAddTagMasterIRelevant() throws Exception {
        Owner o1 = createOwner(true);
        Owner o2 = createOwner(false);
        Owner o3 = createOwner(false);
        manager.addOwner(o1);
        manager.addOwner(o2);
        manager.addOwner(o3);

        //simulate o1 selects a tag;
        Map<Integer, List<TagWrapper>> orders = manager.decideAddTag(new TagWrapper(o1.id, createTag()),
                false);
        List<TagWrapper> tws = orders.get(EventID.TAG_ADD);
        assertEquals(Arrays.asList(o1.id, o2.id, o3.id), mapList(tws, tw -> tw.owner));

    }

    @Test
    public void decideDelete() throws Exception {
        Owner o1 = createOwner(true);
        Owner o2 = createOwner(false);
        Owner o3 = createOwner(false);
        manager.addOwner(o1);
        manager.addOwner(o2);
        manager.addOwner(o3);
        Map<Integer, List<TagWrapper>> orders = manager.decideDeleteTag(new TagWrapper(o1.id, createTag()));
        List<TagWrapper> twsAdd = orders.get(EventID.TAG_ADD);
        List<TagWrapper> twsDelete = orders.get(EventID.TAG_DELETE);
        assertEquals(Arrays.asList(o2.id, o3.id), mapList(twsAdd, tw -> tw.owner));
        assertEquals(Collections.singletonList(o1.id), mapList(twsDelete, tw -> tw.owner));
    }

    @Test
    public void decideUndoDelete() throws Exception {
        Owner o1 = createOwner(true);
        Owner o2 = createOwner(false);
        Owner o3 = createOwner(false);
        manager.addOwner(o1);
        manager.addOwner(o2);
        manager.addOwner(o3);
        Map<Integer, List<TagWrapper>> orders = manager
                .decideUndoTag(new TagWrapper(o1.id, createTag()), EventID.TAG_DELETE);
        List<TagWrapper> twsMaster = orders.get(EventID.TAG_ADD);
        List<TagWrapper> twsSlave = orders.get(EventID.TAG_DELETE);
        assertEquals(Arrays.asList(o2.id, o3.id), mapList(twsSlave, tw -> tw.owner));
        assertEquals(Collections.singletonList(o1.id), mapList(twsMaster, tw -> tw.owner));
    }


    @Test
    public void decideEdit() throws Exception {
        Owner master = createOwner(true);
        Owner slave = createOwner(false);
        manager.addOwner(master);
        manager.addOwner(slave);
        //select
        Tag original = new Tag(1, "foo");
        //edit
        EditedTag edited = EditedTag.from(original, "bar");
        //decide
        Map<Integer, List<TagWrapper>> orders = manager.decideEditTag(new TagWrapper(master.id, edited));
        //result
        Tag masterTag  = orders.get(EventID.TAG_EDIT).get(0).tag;
        Tag slaveTag = orders.get(EventID.TAG_ADD).get(0).tag;
        assertEquals("bar", masterTag.name);
        assertEquals("foo->bar", slaveTag.name);
    }

    @Test
    public void decideUndoEdit() throws Exception {
        Owner o1 = createOwner(true);
        Owner o2 = createOwner(false);
        manager.addOwner(o1);
        manager.addOwner(o2);

        Tag original = new Tag(1, "foo");

        Map<Integer, List<TagWrapper>> orders = manager
                .decideUndoTag(new TagWrapper(o1.id, EditedTag.from(original, "bar")), EventID.TAG_EDIT);

        Tag masterTag  = orders.get(EventID.TAG_EDIT).get(0).tag;
        Tag slaveTag = orders.get(EventID.TAG_DELETE).get(0).tag;
        assertEquals("foo", masterTag.name);
        assertEquals("foo", slaveTag.name);

    }


    @NonNull
    private Owner createOwner(boolean master) {
        return new Owner(master, new Identifiable.Generate());
    }

    @NonNull
    private Tag createTag(){
        return Tag.asDTO(UUID.randomUUID().toString());
    }




}