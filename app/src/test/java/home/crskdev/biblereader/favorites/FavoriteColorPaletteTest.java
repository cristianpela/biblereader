package home.crskdev.biblereader.favorites;

import org.junit.Test;

import home.crskdev.biblereader.core.mvp.ResourceFinder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 12/6/2017.
 */

public class FavoriteColorPaletteTest {
    @Test
    public void nextColor() throws Exception {

        ResourceFinder res = mock(ResourceFinder.class);
        FavoriteColorPalette pallete = new FavoriteColorPalette(res, 3);

        String prefix = pallete.getColorPrefix();
        when(res.getColorId(prefix + 1)).thenReturn(1);
        when(res.getColorId(prefix + 2)).thenReturn(2);
        when(res.getColorId(prefix + 3)).thenReturn(3);


        assertEquals(1, pallete.nextColor("BOOK1"));
        assertEquals(2, pallete.nextColor("BOOK2"));
        assertEquals(1, pallete.nextColor("BOOK1"));
        assertEquals(2, pallete.nextColor("BOOK2"));
        assertEquals(3, pallete.nextColor("BOOK3"));
        assertEquals(1, pallete.nextColor("BOOK1"));


    }

}