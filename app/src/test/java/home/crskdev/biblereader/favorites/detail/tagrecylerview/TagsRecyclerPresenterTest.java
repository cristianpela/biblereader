package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crskdev.biblereader.BasePresenterTest;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailView;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.NaiveGenerator;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.core.bus.EventID.*;
import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_TAG;
import static home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by criskey on 23/6/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagsRecyclerPresenterTest extends BasePresenterTest {

    @Mock
    FavoritesService svc;
    @Mock
    ITagsRecyclerView view;

    Map<String, Object> args = new HashMap<>();

    Verset verset;

    TagsRecyclerPresenter presenter;

    ArgumentCaptor<Tag> tagCaptor;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = new TagsRecyclerPresenter(bus, stateContainer, Schedulers.trampoline(), svc);
        verset = Verset.DEFAULT;
        args.put(ARG_KEY_HAS_DEFAULT_DATA, false);
        args.put(ARG_KEY_INTERACTABLE, false);
        args.put(ARG_KEY_ACTION_FLAGS, ACTION_NONE);
        args.put(ARG_KEY_OWNERSHIP_MANAGER_ID, new Owner(false, new NaiveGenerator("1")));
        doReturn(args).when(view).getArgs();
        tagCaptor = ArgumentCaptor.forClass(Tag.class);
    }

    @Test
    public void testWhenIsDefaultDataAndNoByVerset() throws Exception {
        when(svc.getAllTags()).thenReturn(Single.just(Collections.emptyList()));
        args.put(ARG_KEY_HAS_DEFAULT_DATA, true);
        presenter.attach(view);
        verify(view).onDisplayTags(any(List.class));
    }

    @Test
    public void testWhenIsDefaultDataAndByVerset() throws Exception {
        args.put(ARG_KEY_HAS_DEFAULT_DATA, true);
        args.put(FavoriteDetailView.ARG_KEY_VERSET, verset);

        List<Tag> tags = Collections.singletonList(new Tag(1, "foo"));
        when(svc.getTags(verset)).thenReturn(Single.just(verset.tags(tags)));

        ArgumentCaptor<List<Tag>> captor = ArgumentCaptor.forClass(List.class);

        presenter.attach(view);

        verify(view).onDisplayTags(captor.capture());
        assertEquals(tags, captor.getValue());
    }

    @Test
    public void testEachActionsFromBus() throws Exception {
        args.put(ARG_KEY_ACTION_FLAGS,
                ACTION_DELETE | ACTION_UNDO | ACTION_EDIT | ACTION_ADD);
        presenter.attach(view);

        Tag tag = new Tag(1, "foo");
        TagWrapper tagWrapper = new TagWrapper(presenter.getId(), tag);
        bus.dispatch(new Event<>(GROUP_TAG, TAG_DELETE, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_UNDO, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_ADD, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_EDIT, tagWrapper));

        verify(view).onDeleteTag(tagCaptor.capture());
        verify(view).onAddTag(tagCaptor.capture());
        verify(view).onEditTag(tagCaptor.capture());

        assertEquals(tag, tagCaptor.getValue());
    }

    @Test
    public void testShouldDoNothingWhenHasNoneAction() throws Exception {
        args.put(ARG_KEY_ACTION_FLAGS,
                ACTION_NONE | ACTION_DELETE | ACTION_UNDO | ACTION_EDIT | ACTION_ADD);
        presenter.attach(view);

        Tag tag = new Tag(1, "foo");
        TagWrapper tagWrapper = new TagWrapper(presenter.getId(), tag);
        bus.dispatch(new Event<>(GROUP_TAG, TAG_DELETE, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_UNDO, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_ADD, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_EDIT, tagWrapper));

        verify(view, times(0)).onDeleteTag(any());
        verify(view, times(0)).onAddTag(any());
        verify(view, times(0)).onEditTag(any());

    }

    @Test
    public void testAllActionsFromBus() throws Exception {
        args.put(ARG_KEY_ACTION_FLAGS, ACTION_ALL);
        presenter.attach(view);

        Tag tag = new Tag(1, "foo");
        TagWrapper tagWrapper = new TagWrapper(presenter.getId(), tag);
        bus.dispatch(new Event<>(GROUP_TAG, TAG_DELETE, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_ADD, tagWrapper));
        bus.dispatch(new Event<>(GROUP_TAG, TAG_EDIT, tagWrapper));

        verify(view).onDeleteTag(tagCaptor.capture());
        verify(view).onAddTag(tagCaptor.capture());
        verify(view).onEditTag(tagCaptor.capture());

        assertEquals(tag, tagCaptor.getValue());
    }


}