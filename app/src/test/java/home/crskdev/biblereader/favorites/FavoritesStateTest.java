package home.crskdev.biblereader.favorites;

import org.junit.Test;

import home.crskdev.biblereader.favorites.detail.Tag;

import static home.crskdev.biblereader.favorites.FavoritesState.Case.*;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_TAG;
import static org.junit.Assert.*;

/**
 * Created by criskey on 17/6/2017.
 */
public class FavoritesStateTest {

    @Test
    public void testFavoriteStateCases() throws Exception {
        FavoritesState state = FavoritesState.DEFAULT;

        state = state.filterMode(FILTER_MODE_BOOK);
        assertNull(state.filterTag);
        assertNotNull(state.filterBookId);

        assertEquals(BATCH_FILTER_ALL_BOOKS, state.getStateCase());
        state = state.nextPage(1); //next page
        assertEquals(BATCH_FILTER_ALL_BOOKS, state.getStateCase());

        state = state.setRestorePoint(1); //save page
        assertEquals(RESUME_FILTER_ALL_BOOKS, state.getStateCase());
        assertEquals(true, state.isFresh());

        //change filter type;
        state = state.filterMode(FILTER_MODE_TAG);
        assertNotNull(state.filterTag);
        assertNull(state.filterBookId);

        assertEquals(BATCH_FILTER_ALL_TAGS, state.getStateCase());
        state = state.nextPage(1); //next page
        assertEquals(BATCH_FILTER_ALL_TAGS, state.getStateCase());

        state = state.setRestorePoint(1); //save page
        assertEquals(RESUME_FILTER_ALL_TAGS, state.getStateCase());
        assertEquals(true, state.isFresh());

        state = state.tag(new Tag(1,"tag"));
        assertEquals(BATCH_FILTER_TAG, state.getStateCase());
        assertEquals(true, state.isFresh());

        state = state.tag(new Tag(1,"tag2"));
        assertEquals(BATCH_FILTER_TAG, state.getStateCase());
        assertEquals(true, state.isFresh());

        state = state.nextPage(1);//next page
        assertEquals(BATCH_FILTER_TAG, state.getStateCase());
        assertEquals(false, state.isFresh());

        state = state.setRestorePoint(1); //save page
        assertEquals(RESUME_FILTER_TAG, state.getStateCase());
        assertEquals(true, state.isFresh());

        //change filter type;
        state = state.filterMode(FILTER_MODE_BOOK);
        assertNull(state.filterTag);
        assertNotNull(state.filterBookId);

        state = state.bookId("foo");
        assertEquals(BATCH_FILTER_BOOK, state.getStateCase());
        assertEquals(true, state.isFresh());

        state = state.nextPage(1);//next page
        assertEquals(BATCH_FILTER_BOOK, state.getStateCase());
        assertEquals(false, state.isFresh());

        state = state.bookId("bar");
        assertEquals(BATCH_FILTER_BOOK, state.getStateCase());
        assertEquals(true, state.isFresh());

        state = state.setRestorePoint(1); //save page
        assertEquals(RESUME_FILTER_BOOK, state.getStateCase());
        assertEquals(true, state.isFresh());
    }
}