package home.crskdev.biblereader;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest extends BaseTest {


    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void fizzBuzz() throws Exception {
     Observable.interval(300, TimeUnit.MILLISECONDS, Schedulers.trampoline())
                .skip(1)
                .take(10)
                .publish(src -> Observable.merge(
                        src.filter(i -> i % 3 != 0 && i % 5 != 0).map(i -> Long.toString(i)),
                        src.filter(i -> i % 3 == 0 && i % 5 != 0).map(__ -> "Fizz"),
                        src.filter(i -> i % 5 == 0 && i % 3 != 0).map(__ -> "Buzz"),
                        src.filter(i -> i % 3 == 0 && i % 5 == 0).map(__ -> "FizzBuzz")));
    }

    @Test
    public void bitFlagTest() throws Exception {
        int none = 0;
        int add = 1;
        int delete = 2;
        int undo = 4;
        int edit = 8;
        int all = 15;

        int flags = add | delete | undo | edit;

        assertEquals(none, flags & none);
        assertEquals(add, flags & add);
        assertEquals(delete, flags & delete);
        assertEquals(edit, flags & edit);
        assertEquals(undo, flags & undo);

        flags = all;

        assertEquals(add, flags & add);
        assertEquals(delete, flags & delete);
        assertEquals(edit, flags & edit);
        assertEquals(undo, flags & undo);

    }

    @Test
    public void _01PartionTest() throws Exception {
        float[] partition = partition(1, 0f, 1f);
        float delta = 0.01f;

        assertArrayEquals(new float[]{0f, 1f}, partition, delta);
        partition = partition(2, 0f, 1f);
        assertArrayEquals(new float[]{0f, 0.5f, 1f}, partition, delta);
        partition = partition(3, 0f, 1f);
        assertArrayEquals(new float[]{0f, 0.33f, 0.66f, 1f}, partition, delta);
        partition = partition(4, 0f, 1f);
        assertArrayEquals(new float[]{0f, 0.25f, 0.5f, 0.75f, 1f}, partition, delta);
        partition = partition(5, 0f, 1f);
        assertArrayEquals(new float[]{0f, 0.2f, 0.4f, 0.6f, 0.8f, 1f}, partition, delta);
    }


    private float[] partition(int size, float lowerBound, float upperBound) {
        if (size <= 0)
            throw new IllegalArgumentException("Size must be bigger than 0");
        float[] partition = new float[size + 1];
        partition[0] = lowerBound;
        partition[partition.length - 1] = upperBound;

        float step = upperBound / size;
        float acc = step; //accumulator value
        for (int i = 1; i < partition.length - 1; i++) {
            partition[i] = acc;
            acc += step;
        }
        return partition;
    }
}