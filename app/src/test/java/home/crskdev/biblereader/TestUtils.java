package home.crskdev.biblereader;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by criskey on 24/6/2017.
 */

public final class TestUtils {


    public static <T, F> List<T> mapList(@NonNull Collection<F> list, Function<F, T> func) {
        List<T> mapped = new ArrayList<T>(list.size());
        for (F f : list) {
            try {
                mapped.add(func.apply(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapped;
    }

}
