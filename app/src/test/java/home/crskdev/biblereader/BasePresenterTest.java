package home.crskdev.biblereader;

import android.support.annotation.CallSuper;

import org.junit.Before;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;

import static org.mockito.Mockito.mock;

/**
 * Created by criskey on 23/6/2017.
 */

public class BasePresenterTest extends BaseTest {


    protected EventBus bus;

    protected StateContainer stateContainer;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();
        bus = new EventBus(new MockMainThreadDispatcher());
        stateContainer = new StateContainer(bus);
    }

    private class MockMainThreadDispatcher implements MainThreadDispatcher{

        @Override
        public void dispatch(Runnable run) {
            run.run();
        }

        @Override
        public boolean isMainThread() {
            return true;
        }
    }

}
