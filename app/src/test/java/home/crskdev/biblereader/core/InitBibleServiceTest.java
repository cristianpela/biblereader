package home.crskdev.biblereader.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

/**
 * Created by criskey on 30/4/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class InitBibleServiceTest {

    public static final String TEST_BIBLE_INFO_BOOK_NAME = "BOOK";

    public static final int TEST_BIBLE_INFO_CHAPTERS = 2;

    public static final int TEST_BIBLE_INFO_C_1_VERSETS = 2;

    public static final int TEST_BIBLE_INFO_C_2_VERSETS = 3;

    public static final BibleInfo TEST_BIBLE_INFO = new BibleInfo(Arrays.asList(
            new BibleInfo.BookInfo.Builder()
                    .setId("id")
                    .setTitle(TEST_BIBLE_INFO_BOOK_NAME)
                    .setChapters(1)
                    .incCurrChapterVerset()
                    .incCurrChapterVerset()
                    .setChapters(2)
                    .incCurrChapterVersetBy(3)
                    .createBook()
    ));

    @Mock
    BibleInfoMapper mapper;

    @Mock
    BibleInfoLoader loader;

    @Mock
    BibleDownloader downloader;

    @Test
    public void check() throws Exception {
        //TODO fix test

//
//        when(loader.isInitialized()).thenReturn(Single.just(false));
//        when(loader.load()).thenReturn(Single.just(TEST_BIBLE_INFO));
//        when(loader.save(any(BibleInfo.class))).thenReturn(Completable.complete());
//
//        when(downloader.download()).thenReturn(Completable.complete());
//        when(mapper.toBibleInfo()).thenReturn(TEST_BIBLE_INFO);
//
//        InitBibleService svc = new InitBibleService(loader, downloader, mapper);
//
//        svc.check().test().assertValue(TEST_BIBLE_INFO);
    }


}