package home.crskdev.biblereader.core.bus;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.operators.single.SingleObserveOn;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import io.reactivex.subscribers.ResourceSubscriber;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by criskey on 14/7/2017.
 */
public class WaitEventDispatcherTest {

    String src;
    private CompositeDisposable disposables;
    private WaitEventDispatcher wait;
    private TestScheduler testScheduler;

    @Before
    public void setUp() throws Exception {
        EventBus bus = new EventBus(mock(MainThreadDispatcher.class));
        disposables = new CompositeDisposable();
        wait = new WaitEventDispatcher(bus);
        testScheduler = new TestScheduler();
    }

    @Test
    public void wrapObservable() throws Exception {

        src = "OBS";

        disposables.add(Observable.never().compose(wait.dispatchObservable(src)).subscribe());
        disposables.add(Observable.fromCallable(() -> {
            throw new Exception();
        }).compose(wait.dispatchObservable(src)).subscribeWith(new EmptyObserver<>()));

        disposables.add(Observable.interval(500, TimeUnit.MILLISECONDS, testScheduler)
                .compose(wait.dispatchObservable(src)).subscribe());
        disposables.add(Observable.interval(200, TimeUnit.MILLISECONDS, testScheduler).take(4)
                .compose(wait.dispatchObservable(src)).subscribe());
        disposables.add(Observable.interval(100, TimeUnit.MILLISECONDS, testScheduler)
                .compose(wait.dispatchObservable(src)).subscribe());
        disposables.add(Observable.interval(50, TimeUnit.MILLISECONDS, testScheduler)
                .compose(wait.dispatchObservable(src)).subscribe());
        disposables.add(Observable.interval(10, TimeUnit.MILLISECONDS, testScheduler).take(15)
                .compose(wait.dispatchObservable(src)).subscribe());

        testScheduler.advanceTimeBy(3000, TimeUnit.MILLISECONDS);

        disposables.dispose();

        assertTrue(wait.sharedWaitState.isEmpty());

    }


    @Test
    public void wrapFlowable() throws Exception {

        src = "FLW";

        disposables.add(Flowable.interval(100, TimeUnit.MILLISECONDS, testScheduler)
                .compose(wait.dispatchFlowable(src)).subscribe());
        disposables.add(Flowable.just(1)
                .compose(wait.dispatchFlowable(src)).subscribe());
        disposables.add(Flowable.error(new Error(""))
                .compose(wait.dispatchFlowable(src)).subscribeWith(new EmptySubscriber<>()));

        testScheduler.advanceTimeBy(3000, TimeUnit.MILLISECONDS);

        disposables.dispose();

        assertTrue(wait.sharedWaitState.isEmpty());
    }


    @Test
    public void wrapCompletable() throws Exception {

        src = "FLW";

        TestObserver<Void> test = Completable.fromObservable(Observable.just(1))
                .compose(wait.dispatchCompletable(src))
                .test();

        test.assertComplete();

        assertTrue(wait.sharedWaitState.isEmpty());
    }


    @Test
    public void wrapSingle() throws Exception {
        src = "SGL";

        disposables.add(Single.just(1)
                .compose(wait.dispatchSingle(src)).subscribe());
        //FIXME: getting error when compile:
//        Error:(98, 31) error: incompatible types: inference variable T has incompatible bounds
//        equality constraints: SingleObserver
//        upper bounds: Disposable,E,SingleObserver<? super Object>,Object
//        where T,E are type-variables:
//        T extends Object declared in method <T>mock(Class<T>)
//        E extends SingleObserver<? super Object> declared in method <E>subscribeWith(E)
//        disposables.add(Single.error(new Error(""))
//                .compose(wait.dispatchSingle(src)).subscribeWith(mock(SingleObserver.class)));
        assertTrue(wait.sharedWaitState.isEmpty());
    }

    @Test
    public void wrapSubject() throws Exception {
        src = "SUB";

        Subject<String> sub = BehaviorSubject.createDefault("FOO");


        Observable<Integer> delayed = Observable.just(1)
                .delay(1000, TimeUnit.MILLISECONDS, testScheduler);

        disposables.add(sub
                .compose(wait.dispatchSubject(src, (s) -> delayed.map(i -> s + i)))
                .subscribe()
        );

        assertTrue(!wait.sharedWaitState.isEmpty());

        testScheduler.advanceTimeBy(1100, TimeUnit.MILLISECONDS);

        assertTrue(wait.sharedWaitState.isEmpty());
    }

    private class EmptyObserver<T> extends DisposableObserver<T> {

        @Override
        public void onNext(@NonNull T t) {

        }

        @Override
        public void onError(@NonNull Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

    private class EmptySubscriber<T> extends ResourceSubscriber<T> {

        @Override
        public void onNext(T t) {

        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onComplete() {

        }
    }

    private class EmptySingleObserver implements SingleObserver<Object> {

        @Override
        public void onSubscribe(@NonNull Disposable d) {

        }

        @Override
        public void onSuccess(@NonNull Object t) {

        }

        @Override
        public void onError(@NonNull Throwable e) {

        }
    }


}