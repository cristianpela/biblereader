package home.crskdev.biblereader.core;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by criskey on 6/6/2017.
 */
public class ContentTest {
    @Test
    public void organize() throws Exception {

        List<Verset> vs = Arrays.asList(
                Verset.asDTO("b1", 2, 4, null, false),
                Verset.asDTO("b2", 1, 3, null, false),
                Verset.asDTO("b1", 2, 1, null, false),
                Verset.asDTO("b2", 1, 1, null, false),
                Verset.asDTO("b1", 1, 1, null, false),
                Verset.asDTO("b2", 3, 4, null, false),
                Verset.asDTO("b1", 4, 7, null, false)

        );

        List<Content> expect = Arrays.asList(
                new Book("b1", ""),
                new Chapter(1, "b1", "1"),
                Verset.asDTO("b1", 1, 1, null, false),
                new Chapter(2, "b1", "2"),
                Verset.asDTO("b1", 2, 1, null, false),
                Verset.asDTO("b1", 2, 4, null, false),
                new Chapter(4, "b1", "4"),
                Verset.asDTO("b1", 4, 7, null, false),
                new Book("b2", ""),
                new Chapter(1, "b2", "1"),
                Verset.asDTO("b2", 1, 1, null, false),
                Verset.asDTO("b2", 1, 3, null, false),
                new Chapter(3, "b2", "3"),
                Verset.asDTO("b2", 3, 4, null, false)
        );

        List<Content> organize = Content.Util.organize(vs);
       // print(organize);
        assertEquals(expect, organize);

    }

    @Test
    public void testJoinFavorites() throws Exception {
        List<Verset> base = Arrays.asList(
                Verset.asDTO("b1", 2, 4, null, false),
                Verset.asDTO("b1", 1, 3, null, false),
                Verset.asDTO("b1", 2, 1, null, false),
                Verset.asDTO("b1", 1, 1, null, false),
                Verset.asDTO("b1", 1, 5, null, false)

        );

        List<Verset> transf = Arrays.asList(
                Verset.asDTO("b1", 2, 4, null, true),
                Verset.asDTO("b1", 2, 1, null, true),
                Verset.asDTO("b1", 1, 1, null, true)
        );

        List<Verset> j = Content.Util.joinFavorites(base, transf);
        print(j);
        assertTrue(find(Verset.asDTO("b1", 2, 4, null, true), j).favorite);
        assertTrue(find(Verset.asDTO("b1", 2, 1, null, true), j).favorite);
        assertTrue(find(Verset.asDTO("b1", 1, 1, null, true), j).favorite);
    }

    private Verset find(Verset verset, List<Verset> list) {
        for (int i = 0; i < list.size(); i++) {
            if (verset.equals(list.get(i))) {
                return list.get(i);
            }
        }
        return new Verset(null, null, -1, -1, null, false, -1, Collections.emptyList());
    }

    private<T> void print(List<T> l) {
        for (T e : l ) {
            System.out.println(e);
        }
    }
}