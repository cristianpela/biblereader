package home.crskdev.biblereader.core;

import org.junit.Test;

import java.io.File;
import java.util.Iterator;

import home.crskdev.biblereader.core.di.XMLModule;

import static home.crskdev.biblereader.core.BibleInfo.*;
import static org.junit.Assert.*;

/**
 * Created by criskey on 30/4/2017.
 */
public class BibleInfoMapperTest {

    @Test
    public void toBibleInfo() throws Exception {

        XMLModule module = new XMLModule();
        BibleInfoMapper mapper = new BibleInfoMapper(module.provideXMLInputFactory2(),
                new File(getClass().getClassLoader().getResource(BibleDownloader.FILE_NAME).getPath()));
        BibleInfo bibleInfo = mapper.toBibleInfo();
        assertFalse(bibleInfo == null);

        assertEquals(65, bibleInfo.getBookInfos().size());

        Iterator<BookInfo> it = bibleInfo.getBookInfos().iterator();

        BookInfo genesis = it.next();
        assertEquals("Geneza", genesis.title);
        assertEquals("GEN", genesis.id);
        assertEquals(50, genesis.noOfChapters);

        assertEquals(31, genesis.getVersetsOf(1));
        assertEquals(25, genesis.getVersetsOf(2));

        BookInfo exodus = it.next();
        assertEquals("Exodul", exodus.title);
        assertEquals("EXO", exodus.id);
        assertEquals(40, exodus.noOfChapters);

    }

}