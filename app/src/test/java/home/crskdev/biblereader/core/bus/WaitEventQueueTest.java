package home.crskdev.biblereader.core.bus;

import org.junit.Test;

import home.crskdev.biblereader.BaseTest;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.TestObserver;

import static home.crskdev.biblereader.core.bus.WaitEventQueue.WAIT_SESSION_END;
import static home.crskdev.biblereader.core.bus.WaitEventQueue.WAIT_SESSION_INFO;
import static home.crskdev.biblereader.core.bus.WaitEventQueue.WAIT_SESSION_START;

/**
 * Created by criskey on 12/7/2017.
 */
public class WaitEventQueueTest extends BaseTest {

    WaitEventQueue manager;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        //no integration.
        manager = new WaitEventQueue(null);
    }

    @Test(expected = IllegalStateException.class)
    public void raiseExceptionWhenAddingDoneEventOnEmptyQueue() {
        manager.decide(new Event<>(EventID.WAIT, false));
    }

    @Test(expected = IllegalStateException.class)
    public void raiseExceptionWhenAddingDoneEventOnEmptyQueue2() {
        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, false));
        manager.decide(new Event<>(EventID.WAIT, false));
    }

    @Test
    public void waitEventSessionTest() throws Exception {
        TestObserver<WaitEventQueue.WaitState> test = manager.toObservable().test();

        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, false));
        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, false));
        manager.decide(new Event<>(EventID.WAIT, false));

        test.assertValueAt(0, (e) -> e.type == WAIT_SESSION_START);
        test.assertValueAt(1, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);
        test.assertValueAt(2, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 2);
        test.assertValueAt(3, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);
        test.assertValueAt(4, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 2);
        test.assertValueAt(5, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);
        test.assertValueAt(6, (e) -> e.type == WAIT_SESSION_END);
    }

    @Test
    public void replay() throws Exception {
        Disposable disposable = manager.toObservable().subscribe();

        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, false));
        manager.decide(new Event<>(EventID.WAIT, true));
        manager.decide(new Event<>(EventID.WAIT, false));

        disposable.dispose();

        TestObserver<WaitEventQueue.WaitState> test = manager.toObservable().test();

        test.assertValueAt(0, (e) -> e.type == WAIT_SESSION_START);
        test.assertValueAt(1, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);
        test.assertValueAt(2, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 2);
        test.assertValueAt(3, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);
        test.assertValueAt(4, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 2);
        test.assertValueAt(5, (e) -> e.type == WAIT_SESSION_INFO && (Integer) e.data == 1);

        manager.decide(new Event<>(EventID.WAIT, false)); //session ended
        test.dispose();

        //if session has ended this subscriber will not receive a session replay
        test = manager.toObservable().test();
        test.assertEmpty();
    }
}