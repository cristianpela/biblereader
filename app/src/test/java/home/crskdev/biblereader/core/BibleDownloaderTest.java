package home.crskdev.biblereader.core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by criskey on 30/4/2017.
 */
public class BibleDownloaderTest {

    private File dFile;

    private String refPath;

    @Before
    public void init() {
        refPath = getClass()
                .getClassLoader()
                .getResource(BibleDownloader.FILE_NAME)
                .getPath();
    }

    @After
    public void clear() {
        if (dFile != null && dFile.exists()) {
            dFile.delete();
        }
    }

    @Test
    public void download() throws Exception {

        //String remotLink = "https://github.com/churchio/open-bibles/raw/master/ron-rccv.usfx.xml";
        String localLink = "file://" + refPath;
        String outDir = new File(refPath).getParent();
        String fileName = UUID.randomUUID().toString() + ".xml";

        dFile = new File(outDir, fileName);

        BibleDownloader downloader = new BibleDownloader(localLink, dFile);
        downloader.download().test().assertComplete();

        assertTrue(dFile.exists());

        File ref = new File(refPath);
        assertEquals(ref.length(), dFile.length());
    }


}