package home.crskdev.biblereader.search;

import org.codehaus.stax2.XMLInputFactory2;

import java.io.File;

import home.crskdev.biblereader.BaseTest;
import home.crskdev.biblereader.core.BibleDownloader;
import home.crskdev.biblereader.core.di.XMLModule;

/**
 * Created by criskey on 29/7/2017.
 */

public class BaseXMLTest extends BaseTest {

    protected XMLInputFactory2 inputXMLFactory2 = new XMLModule().provideXMLInputFactory2();

}
