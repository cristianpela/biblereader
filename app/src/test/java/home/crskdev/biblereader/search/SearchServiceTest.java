package home.crskdev.biblereader.search;

import android.os.Build;
import android.support.annotation.RequiresApi;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import home.crskdev.biblereader.core.BibleDownloader;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.di.XMLModule;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.subscribers.TestSubscriber;

import static junit.framework.Assert.assertEquals;

/**
 * Created by criskey on 1/5/2017.
 */
public class SearchServiceTest extends BaseXMLTest {

    private SearchService svc;

    @Before
    public void setup() {
        svc = new SearchService(inputXMLFactory2, loadFileFromResources(BibleDownloader.FILE_NAME));
    }

    @Test
    public void findPhraseInBook() throws Exception {
        List<String> expected = Arrays.asList(
                "„Am văzut pe Satana căzând ca un fulger din cer.",
                "Deci, dacă Satana este dezbinat împotriva lui însuşi, cum va dăinui împărăţia lui, fiindcă ziceţi că Eu scot dracii cu Beelzebul?",
                "Dar femeia aceasta, care este o fiică a lui Avraam şi pe care Satana o ţinea legată de optsprezece ani, nu trebuia oare să fie dezlegată de legătura aceasta în ziua Sabatului?”",
                "Dar Satana a intrat în Iuda, zis şi Iscarioteanul, care era din numărul celor doisprezece.",
                "„Simone, Simone, Satana v-a cerut să vă cearnă ca grâul."
        );

        TestSubscriber<CharSequence> testLuk = svc.findPhrase("LUK", "satana")
                .map(highlight -> highlight.verset.content).test();

        assertEquals(expected, testLuk.values());
    }

    @Test
    public void findVersetsInChapter() throws Exception {

        String v13 = "Să spele cu apă măruntaiele şi picioarele, şi preotul să le aducă pe toate şi să le ardă pe altar." +
                " Aceasta este o ardere de tot, o jertfă mistuită de foc, de un miros plăcut Domnului. ";
        String v5 = "Să înjunghie viţelul înaintea Domnului; şi preoţii, fiii lui Aaron, să aducă sângele şi să-l stropească de jur împrejur pe altarul de la uşa Cortului întâlnirii. ";

        Flowable<Verset> flow = svc.find("LEV", new int[]{1}, new int[]{5, 13}).share();

        TestSubscriber<CharSequence> contentTest = flow.map((s) -> s.content).test();
        contentTest.assertNoErrors();
        assertEquals(2, contentTest.valueCount());
        contentTest.assertValueAt(0, s -> String.valueOf(s).trim().equals(v5.trim()));
        contentTest.assertValueAt(1, s -> String.valueOf(s).trim().equals(v13.trim()));

        flow.map((s) -> s.verset)
                .toList()
                .test()
                .assertValue(Arrays.asList(5, 13));

        flow.map((s) -> s.bookTitle)
                .toList()
                .test()
                .assertValue(Arrays.asList("Leviticul", "Leviticul"));

        flow.map((s) -> s.chapterId)
                .toList()
                .test()
                .assertValue(Arrays.asList(1, 1));
    }

    @Test
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void findAllVersetsInChapter1And2() throws Exception {
        Flowable<Verset> flow = svc.find("GEN", new int[]{1, 2}, new int[]{}).share();
        flow.groupBy(s -> s.chapterId)
                .map(g -> g.map(s -> s.verset).toList())
                .flatMap(Single::toFlowable)
                .toList()
                .test()
                .assertValue(Arrays.asList(seq(31), seq(25)));
    }

    @Test
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void findAllVersetsInChapter1() throws Exception {
        Flowable<Verset> flow = svc.find("GEN", new int[]{1}, new int[]{}).share();
        flow.groupBy(sr -> sr.chapterId)
                .flatMapSingle(g -> g.collectInto(new Chapter("Ch" + g.getKey(), new ArrayList<>()), (ch, sr) -> {
                    ch.versets.add(sr.verset + ". " + sr.content);
                }))
                .toList()
                .subscribe(System.out::println);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private List<Integer> seq(int endInclusive) {
        return IntStream.rangeClosed(1, endInclusive).boxed().collect(Collectors.toList());
    }

    private static class Chapter {
        String chapter;
        List<String> versets;

        public Chapter(String chapter, List<String> versets) {
            this.chapter = chapter;
            this.versets = versets;
        }

        @Override
        public String toString() {
            return "BookContent{" +
                    "chapterId='" + chapter + '\'' +
                    ", versets=" + versets +
                    '}';
        }
    }
}