package home.crskdev.biblereader.search.suggest;

import org.codehaus.stax2.XMLInputFactory2;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import home.crskdev.biblereader.search.BaseXMLTest;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.*;

/**
 * Created by criskey on 29/7/2017.
 */
public class SuggestionServiceTest extends BaseXMLTest {

    private SuggestionService svc;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        svc = new SuggestionService(inputXMLFactory2, q -> {
            try {
                return loadFileFromResources("suggest.xml").toURI().toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    @Test
    public void fetch() throws Exception {
        TestSubscriber<CharSequence> test = svc.fetch("foo-bar").test();
        test.assertNoErrors();
        List<CharSequence> expected = Arrays.asList(
                "timisoara",
                "timisoara bucuresti",
                "timisoara harta",
                "timisoara cluj",
                "timisoara oradea",
                "timisoara deva",
                "timisoara weather",
                "timisoara arad",
                "timisoara vremea",
                "timisoara sibiu");
        assertEquals(expected, test.values());
    }

    @Test
    public void debounceFetch() throws Exception {

    }

}