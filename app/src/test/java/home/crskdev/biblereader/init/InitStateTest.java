package home.crskdev.biblereader.init;

import org.junit.Test;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.*;

/**
 * Created by criskey on 13/5/2017.
 */
public class InitStateTest {

    @Test
    public void shouldMatchFromBuilder() throws Exception {
        InitState basic = new InitState.Builder()
                .checked(true)
                .wait(true)
                .info("Some info")
                .error("Error")
                .progInfo("Prog info")
                .stepMax(100)
                .stepTick(338)
                .subStepTick(384)
                .subStepMax(3949)
                .create();
        assertEquals(basic, new InitState.Builder(basic).create());
    }

    @Test
    public void shouldWait() throws Exception {
        InitState wait = InitState.waitFor(InitState.DEFAULT, new StreamedInitState.Wait(true, "Waiting..."));
        assertTrue(wait.wait & !wait.checked);
    }

    @Test
    public void shouldProgressAndChecked() throws Exception {
        InitState state = InitState.waitFor(InitState.DEFAULT, new StreamedInitState.Wait(true, "Waiting..."));
        assertEquals("Waiting...", state.info);

        state = InitState.progressSteps(state, new StreamedInitState.ProgressStepMax(2, "First Step"));
        state = InitState.progressSubStep(state, new StreamedInitState.ProgressSubStepMax(100, "First Substep"));

        assertEquals(true, state.wait);
        assertEquals("First Step", state.info);
        assertEquals("First Substep", state.progInfo);
        assertEquals(2, state.stepMax);
        assertEquals(100, state.subStepMax);

        state = InitState.progressTick(state, new StreamedInitState.Tick(1, "Progress 1"));
        state = InitState.progressSubTick(state, new StreamedInitState.SubTick(1, "Sub Progress 1"));

        assertEquals("Progress 1", state.info);
        assertEquals("Sub Progress 1", state.progInfo);
        assertEquals(1, state.subStepTick);
        assertEquals(1, state.stepTick);

        state = InitState.checked();
        assertEquals(false, state.wait);
        assertEquals(true, state.checked);

    }

    @Test
    public void testFloawable() {
        Flowable<StreamedInitState> flow = Flowable.concatArray(
                Flowable.just((StreamedInitState) new StreamedInitState.Wait(true, "Waiting...")),
                Flowable.create(s -> {
                    s.onNext(new StreamedInitState.ProgressStepMax(2, "First Step"));
                    s.onNext(new StreamedInitState.Tick(1, "First Step"));
                    s.onComplete();
                }, BackpressureStrategy.BUFFER),
                Flowable.create(s -> {
                    s.onNext(new StreamedInitState.ProgressSubStepMax(100, "First Substep"));
                    s.onNext(new StreamedInitState.SubTick(1, "Tick 1 of 100"));
                    s.onComplete();
                }, BackpressureStrategy.BUFFER));
        TestSubscriber<StreamedInitState> test = flow.test();

        assertEquals(5, test.valueCount());

        test.assertValueAt(0, sis -> ((StreamedInitState.Wait) sis).wait);
        test.assertValueAt(1, sis ->{
            StreamedInitState.ProgressStepMax step = (StreamedInitState.ProgressStepMax) sis;
            return step.max == 2 && step.message.equals("First Step");
        });
        test.assertValueAt(2, sis ->{
            StreamedInitState.Tick tick = (StreamedInitState.Tick) sis;
            return tick.tick == 1 && tick.message.equals("First Step");
        });
        test.assertValueAt(3, sis ->{
            StreamedInitState.ProgressSubStepMax step = (StreamedInitState.ProgressSubStepMax) sis;
            return step.max == 100 && step.message.equals("First Substep");
        });
        test.assertValueAt(4, sis ->{
            StreamedInitState.SubTick tick = (StreamedInitState.SubTick) sis;
            return tick.tick == 1 && tick.message.equals("Tick 1 of 100");
        });
    }


}