package home.crskdev.biblereader.backup;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Scanner;

import home.crskdev.biblereader.BaseTest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 31/7/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class LocalBackupServiceTest extends BaseTest {

    @Mock
    FileStorageInfo info;

    LocalBackupService svc;

    File backupDir = new File(getResourcesDir(), "backup");
    File localDir = new File(backupDir, "local");
    File extDir = new File(backupDir, "external");

    String dbFileContent = "db-file-content";
    String bookFileContent = "bible-file-content";

    String dbFileName = "db.txt";
    String bookFileName = "bible.txt";

    File dbFile = new File(localDir, dbFileName);
    File bookFile = new File(localDir, bookFileName);

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        prepareFilesLocal();

        svc = new LocalBackupService(dbFile, bookFile, info);

        when(info.getExternalStorageDirectory()).thenReturn(extDir);
        when(info.isExternalStorageWritable()).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
        deleteFiles(localDir);
        deleteFiles(extDir);
    }

    @Test
    public void backupAndRestore() throws Exception {
        final long beforeDbModif = dbFile.lastModified();
        final long beforeBookModif = bookFile.lastModified();
        svc.backup().andThen(svc.restore()).subscribe(() -> {
            assertTrue(dbFile.lastModified() > beforeDbModif
                    && bookFile.lastModified() > beforeBookModif);
        });
    }

    public void deleteFiles(File dir) {
        //lvl 1 delete - no recursive
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                f.delete();
            }
        }
    }

    void prepareFilesLocal() throws IOException {
        write(dbFile, dbFileContent);
        write(bookFile, bookFileContent);
    }

    private void write(File f, String content) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(f), "utf-8"));
        writer.write(content);
        writer.close();
    }


    String read(File f) throws FileNotFoundException {
        return new Scanner(f).useDelimiter("\\Z").next();
    }

}