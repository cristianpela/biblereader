package home.crskdev.biblereader;

import android.support.annotation.CallSuper;

import org.junit.Before;

import java.io.File;

import home.crskdev.biblereader.util.asserts.Asserts;
import home.crskdev.biblereader.util.asserts.ThreadingAsserts;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;

/**
 * Created by criskey on 8/7/2017.
 */

public class BaseTest {

    static {
        Asserts.threadingAsserts = new ThreadingAsserts() {
            @Override
            public boolean assertIsOnMainThread() {
                return true;
            }

            @Override
            public boolean assertIsBackgroundThread() {
                return true;
            }
        };
    }

    protected TestScheduler testScheduler = new TestScheduler();

    @Before
    @CallSuper
    public void setUp() throws Exception {
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxJavaPlugins.setNewThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    protected File loadFileFromResources(String fileName) {
        return new File(getClass().getClassLoader().getResource(fileName).getPath());
    }

    protected File getResourcesDir() {
        return loadFileFromResources("ron-rccv.usfx.xml").getParentFile();
    }
}
