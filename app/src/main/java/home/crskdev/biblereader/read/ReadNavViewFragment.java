package home.crskdev.biblereader.read;

import android.os.Build;
import android.support.design.widget.BottomSheetBehavior;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.util.bottomsheet.BottomSheetFragment;
import home.crskdev.biblereader.MainState;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.bottomsheet.BottomSheetExpandableListView;

/**
 * Created by criskey on 17/5/2017.
 */

public class ReadNavViewFragment extends BottomSheetFragment implements ReadNavView {

    @BindView(R.id.btn_read_nav_next)
    ImageButton btnNext;

    @BindView(R.id.btn_read_nav_prev)
    ImageButton btnPrev;

    @BindView(R.id.btn_expand_collapse)
    ImageButton btnExpCol;

    @BindView(R.id.exp_list_nav_jump)
    BottomSheetExpandableListView listExp;

    @BindView(R.id.edit_read_nav_search)
    EditText editSearch;

    @Inject
    ReadNavPresenter presenter;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.read_nav_layout;
    }

    @Override
    public void onPostAttachPresenter() {
        presenter.dispatchNext(RxView.clicks(btnNext));
        presenter.dispatchPrev(RxView.clicks(btnPrev));
        presenter.searchBooks(RxTextView.textChanges(editSearch));
//        editSearch.getBackground().setColorFilter(getResources()
//                .getColorId(R.colorResId.colorPrimaryShade1), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onProgress(boolean isProgress) {
        btnNext.setEnabled(!isProgress);
        btnPrev.setEnabled(!isProgress);
        listExp.setEnabled(!isProgress);
    }

    @Override
    public void onStartReached() {
        btnPrev.setEnabled(false);
    }

    @Override
    public void onEndReached() {
        btnNext.setEnabled(false);
    }

    @Override
    public void onNavigation() {
        btnNext.setEnabled(true);
        btnPrev.setEnabled(true);
    }

    @Override
    public void onInitExpandedJumpModel(ReadJumpExpandableListModel model) {
        listExp.setAdapter(new ReadJumpExpandebleListAdapter(model));
        listExp.setOnGroupExpandListener(groupPosition -> {
            presenter.expand(groupPosition);
        });
        listExp.setOnGroupCollapseListener(groupPosition -> {
            presenter.collapse(groupPosition);
        });
        listExp.setOnItemLongClickListener((parent, view, position, id) -> {
            long packedPosition = listExp.getExpandableListPosition(position);
            if (ExpandableListView.getPackedPositionType(packedPosition) ==
                    ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                int groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition);
                presenter.collapse(groupPosition);
                return true;
            }
            return false;
        });
        listExp.setOnChildClickListener((parent, v, groupPosition, childPosition, id) -> {
            ReadJumpExpandebleListAdapter adapter = (ReadJumpExpandebleListAdapter) parent
                    .getExpandableListAdapter();
            String bookId = adapter.getGroup(groupPosition).id;
            int chapter = adapter.getChild(groupPosition, childPosition).id;
            presenter.jumpRead(bookId, chapter);
            getBehavior().setState(BottomSheetBehavior.STATE_COLLAPSED);
            return true;
        });
        List<Integer> expandedPositions = model.getExpandedBookPositions();
        for (int position : expandedPositions) {
            listExp.expandGroup(position);
        }
    }

    @Override
    public void onSheetStateChanged(View bottomSheet, int newState) {
        super.onSheetStateChanged(bottomSheet, newState);
        if (newState == BottomSheetBehavior.STATE_EXPANDED) {
            expandedModeDisplay();
        } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
            collapsedModeDisplay();
        }
    }

    private void expandedModeDisplay() {
        btnNext.setVisibility(View.GONE);
        btnPrev.setVisibility(View.GONE);
        editSearch.setVisibility(View.VISIBLE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnExpCol.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            params.addRule(RelativeLayout.ALIGN_PARENT_END);
        }
        params.addRule(RelativeLayout.CENTER_HORIZONTAL, 0);
        btnExpCol.setLayoutParams(params);
    }

    private void collapsedModeDisplay() {
        btnNext.setVisibility(View.VISIBLE);
        btnPrev.setVisibility(View.VISIBLE);
        editSearch.setVisibility(View.GONE);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) btnExpCol.getLayoutParams();
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            params.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
        }
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        btnExpCol.setLayoutParams(params);
    }

}
