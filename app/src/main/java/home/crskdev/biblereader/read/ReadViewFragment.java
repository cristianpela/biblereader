package home.crskdev.biblereader.read;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesAlerts;
import home.crskdev.biblereader.util.recyclerview.SelectiveDividerItemDecoration;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;

/**
 * Created by criskey on 12/5/2017.
 */

public class ReadViewFragment extends BaseFragment implements ReadView {

    @Inject
    ReadPresenter presenter;

    @BindView(R.id.read_recycler_view)
    TapableRecyclerView recyclerView;

    private SelectiveDividerItemDecoration divider;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.read_layout;
    }


    @Override
    public void onScrollPosition(int scrollPosition) {
        ((LinearLayoutManager) recyclerView.getLayoutManager())
                .scrollToPositionWithOffset(scrollPosition, 0);
        //todo: hacky we offset by 2 because 0 is book title and 1 row is chapter number
        divider.decorateAt(scrollPosition + 2, recyclerView);
    }

    @Override
    public void onPreAttachPresenter() {
        divider = new SelectiveDividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.divider));
        recyclerView.addItemDecoration(divider);
        recyclerView.setAdapter(new ReadRecyclerAdapter());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                presenter.setScrollPosition(manager.findFirstVisibleItemPosition());
            }
        });
    }

    @Override
    public void onPostAttachPresenter() {
        presenter.processTap(recyclerView.tapObservable());
    }

    @Override
    public void showJumpDialog(int maxChapters) {
        final EditText input = new AppCompatEditText(new ContextThemeWrapper(getContext(), R.style.dialog_search_edit_style));
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
//        AppCompatTextView customTitleView = new AppCompatTextView(new ContextThemeWrapper(getContext(), R.style.dialog_title_style));
//        customTitleView.setText(getString(R.string.read_jmp_diag_title, maxChapters));
        new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AppTheme_Dialog))
                .setView(input)
                //.setCustomTitle(customTitleView)
                .setTitle(getString(R.string.read_jmp_diag_title, maxChapters))
                .setIcon(R.drawable.ic_book_black_24dp)
                .setCancelable(true)
                .setPositiveButton(R.string.read_jmp_diag_jump, (dialog, which) -> {
                    Editable text = input.getText();
                    if (!TextUtils.isEmpty(text)) {
                        presenter.jumpToChapter(maxChapters, text.toString());
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    @Override
    public void onConfirmFavoriteShow(Verset verset) {
        FavoritesAlerts.favoriteConfirmShow(getContext(), verset, (d, w) ->
                presenter.confirmFavoriteShow(verset)).create().show();
    }

    private ReadRecyclerAdapter getAdapter() {
        return (ReadRecyclerAdapter) recyclerView.getAdapter();
    }

    @Override
    public void onDisplay(List<Content> items) {
        getAdapter().setContents(items);
    }

    @Override
    public void onSelect(int index) {
        getAdapter().changeFavorite(index);
    }

    @Override
    public void onConfirmToggleFavorite(Content item, int index) {
        FavoritesAlerts.favoriteConfirmDialogBuilder(getContext(),
                (Verset) item, (dialog, which) -> {
                    dialog.dismiss();
                    presenter.confirmToggleFavorite(index);
                }, null).create().show();
    }
}
