package home.crskdev.biblereader.read;

import android.text.Spanned;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 1/6/2017.
 */

public interface FullScreenReadView extends IView {

    void onScale(float scale);

    void display(Spanned content);

}
