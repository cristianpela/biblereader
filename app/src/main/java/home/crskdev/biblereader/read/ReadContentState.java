package home.crskdev.biblereader.read;

import java.util.List;

import home.crskdev.biblereader.core.Content;

/**
 * Created by criskey on 6/6/2017.
 */

public class ReadContentState {

    public final List<Content> contents;


    public ReadContentState(List<Content> contents) {
        this.contents = contents;
    }

}
