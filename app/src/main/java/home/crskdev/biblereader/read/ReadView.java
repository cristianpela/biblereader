package home.crskdev.biblereader.read;

import java.util.List;

import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.IListDisplayView;

/**
 * Created by criskey on 12/5/2017.
 */

public interface ReadView extends IListDisplayView<Content> {

    void onScrollPosition(int scrollPosition);

    void showJumpDialog(int maxChapters);

    void onConfirmFavoriteShow(Verset verset);
}
