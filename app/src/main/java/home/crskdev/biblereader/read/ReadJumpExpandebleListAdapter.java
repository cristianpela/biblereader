package home.crskdev.biblereader.read;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import home.crskdev.biblereader.R;

import static home.crskdev.biblereader.read.ReadJumpExpandableListModel.*;

/**
 * Created by criskey on 19/5/2017.
 */

public class ReadJumpExpandebleListAdapter extends BaseExpandableListAdapter {

    private ReadJumpExpandableListModel model;

    public ReadJumpExpandebleListAdapter(ReadJumpExpandableListModel model) {
        this.model = model;
    }

    @Override
    public int getGroupCount() {
        return model.getBooks().size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return model.getChapters().get(model.getBooks().get(groupPosition)).size();
    }

    @Override
    public Book getGroup(int groupPosition) {
        return model.getBooks().get(groupPosition);
    }

    @Override
    public Chapter getChild(int groupPosition, int childPosition) {
        return model.getChapters().get(model.getBooks().get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getCombinedChildId(groupPosition, childPosition);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder vh;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.icon_list_item_custom, null);
            convertView.setTag(vh = new GroupViewHolder(convertView));
        } else {
            vh = (GroupViewHolder) convertView.getTag();
        }
        vh.txt.setText(getGroup(groupPosition).title);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder vh;
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.icon_list_item_custom, null);
            convertView.setTag(vh = new ChildViewHolder(convertView));
        } else {
            vh = (ChildViewHolder) convertView.getTag();
        }
        vh.txt.setText(getChild(groupPosition, childPosition).text);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
       // model = model.collapse(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
       // model = model.expand(groupPosition);
    }

    public static class GroupViewHolder {

        @BindView(R.id.icon_list_item_custom)
        ImageView img;

        @BindView(R.id.txt_list_item_custom)
        TextView txt;

        GroupViewHolder(View v) {
            ButterKnife.bind(this, v);
            // img.setImageResource(R.drawable.ic_book_black_24dp);
        }

    }

    public static class ChildViewHolder {

        @BindView(R.id.icon_list_item_custom)
        ImageView img;

        @BindView(R.id.txt_list_item_custom)
        TextView txt;

        ChildViewHolder(View v) {
            ButterKnife.bind(this, v);
            //img.setImageResource(R.drawable.ic_description_black_24dp);
        }

    }

}
