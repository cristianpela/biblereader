package home.crskdev.biblereader.read;

import android.text.Spanned;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.BaseActivity;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.MathExt;

/**
 * Created by criskey on 29/5/2017.
 */
public class FullScreenActivity extends BaseActivity implements FullScreenReadView {

    @Inject
    FullScreenReadPresenter presenter;

    @BindView(R.id.txt_full_screen)
    TextView txtFullScreen;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.read_fullscreen_layout;
    }

    @Override
    public void onPostAttachPresenter() {
        txtFullScreen.setOnTouchListener(new View.OnTouchListener() {

            ScaleGestureDetector detector = new ScaleGestureDetector(getApplicationContext(), new ScaleGestureDetector.OnScaleGestureListener() {

                @Override
                public boolean onScale(ScaleGestureDetector detector) {
                    return false;
                }

                @Override
                public boolean onScaleBegin(ScaleGestureDetector detector) {
                    return true;
                }

                @Override
                public void onScaleEnd(ScaleGestureDetector detector) {
                    presenter.scale(txtFullScreen.getTextSize(), detector.getScaleFactor());
                }
            });
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return detector.onTouchEvent(event);
            }
        });
    }

    @Override
    public void onScale(float scale) {
        txtFullScreen.setTextSize(TypedValue.COMPLEX_UNIT_PX, scale);
    }

    @Override
    public void display(Spanned content) {
        txtFullScreen.setText(content);
    }

}
