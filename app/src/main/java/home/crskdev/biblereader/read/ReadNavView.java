package home.crskdev.biblereader.read;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 17/5/2017.
 */

public interface ReadNavView extends IView {

    void onProgress(boolean isProgress);

    void onStartReached();

    void onEndReached();

    void onNavigation();

    void onInitExpandedJumpModel(ReadJumpExpandableListModel model);
}
