package home.crskdev.biblereader.read;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.TabEventDispatcher;
import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.bus.WaitEventDispatcher;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.favorites.FavoritesOpsMediator;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.search.SearchService;
import home.crskdev.biblereader.util.MathExt;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.RxUtils;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapEvent;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.util.RxUtils.scheduleObservable;
import static home.crskdev.biblereader.util.RxUtils.scheduleSingle;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.*;


/**
 * Created by criskey on 12/5/2017.
 */
@PerFragment
public final class ReadPresenter extends Presenter<ReadView> {

    private static final String TAG = ReadPresenter.class.getSimpleName();

    private static final String STRING_RES_READ_ERR_EMPTY = "read_err_empty";
    private static final String STRING_RES_READ_ERR_CHAPTER = "read_err_chapter";
    private static final String STRING_RES_READ_ERR_CHAPTER_RANGE = "read_err_chapter_range";

    private final ReadStateService readStateService;

    private final SearchService searchService;

    private final ResourceFinder res;

    private final FavoritesService favService;

    private final WaitEventDispatcher waitEventDispatcher;

    private final TabEventDispatcher tabEventDispatcher;

    private int scrollPosition;

    @Inject
    public ReadPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler,
                         ReadStateService readStateService, SearchService searchService, ResourceFinder res,
                         FavoritesService favService, WaitEventDispatcher waitEventDispatcher,
                         TabEventDispatcher tabEventDispatcher) {
        super(bus, stateContainer, uiScheduler);
        this.readStateService = readStateService;
        this.searchService = searchService;
        this.res = res;
        this.favService = favService;
        this.waitEventDispatcher = waitEventDispatcher;
        this.tabEventDispatcher = tabEventDispatcher;
    }

    private void subscribeToLoadFromPersistence() {
        disposables.add(stateContainer.subjectReadState.filter(rs -> !rs.equals(ReadState.DEFAULT))
                .subscribe(displayConsumer()));
        disposables.add(stateContainer.subjectReadState.filter(rs -> rs.equals(ReadState.DEFAULT))
                .flatMap(__ -> Observable.defer(() -> readStateService
                        .loadReadState().flatMap(rs -> readStateService
                                .readFrom(rs)
                                .compose(toDisplayableState())).toObservable()
                        .startWith(ReadState.DEFAULT.inProgress())
                        .compose(RxUtils.scheduleObservable(uiScheduler)))
                        .compose(waitEventDispatcher.dispatchObservable(TAG)))
                .subscribe(stateContainer.subjectReadState::onNext));
    }

    private void subscribeToJumpDialogOpenCommand() {
        disposables.add(bus.toObservable()
                .flatMapSingle(e -> e.check(GroupEventID.GROUP_READ, EventID.READ_NAV_JUMP_DIALOG)
                        ? readStateService.getMaxChapters(getValue())
                        : Single.never())
                .subscribe((max) -> {
                    view.showJumpDialog(max);
                }));
    }

    @Override
    public void accept(Event<?> e) throws Exception {
        if (e.checkGroup(GroupEventID.GROUP_READ)) {
            Single<ReadState> read = null;
            ReadState value = getValue();
            switch (e.id) {
                case EventID.READ_NAV_NEXT:
                    read = readStateService.readNext(value.scrollPosition(0));
                    break;
                case EventID.READ_NAV_PREV:
                    read = readStateService.readPrev(value.scrollPosition(0));
                    break;
                case EventID.READ_NAV_JUMP:
                    //scroll e.data should be already 0
                    read = readStateService.readFrom((ReadState) e.data);
                    break;
            }
            if (read != null) {
                Observable<ReadState> readStateObservable = startWithWait(value,
                        read.compose(toDisplayableState())
                                .compose(scheduleSingle(uiScheduler))).toObservable()
                        .compose(waitEventDispatcher.dispatchObservable(TAG));
                disposables.add(readStateObservable
                        .subscribe((t) -> {
                            stateContainer.subjectReadState.onNext(t);
                        }));
            }
        }
    }

    @NonNull
    private Consumer<ReadState> displayConsumer() {
        return (state) -> {
            if (!state.inProgress) {
                view.onDisplay(state.content);
                view.onScrollPosition(state.scrollPos);
                bus.dispatch(new Event<>(GroupEventID.GROUP_READ, EventID.READ_NAV_RESULT,
                        getValue().noContent()));
            }
        };
    }

    @Override
    protected void postAttach() {
        subscribeToLoadFromPersistence();
        subscribeToJumpDialogOpenCommand();
    }

    @Override
    protected void postDetach() {
        //store in memory the current state
        ReadState state = getValue().scrollPosition(scrollPosition).inIdle();
        stateContainer.subjectReadState.onNext(state);
        readStateService.saveReadState(state).subscribe();
    }

    public void setScrollPosition(int position) {
        scrollPosition = position;
    }

    @NonNull
    private ReadState getValue() {
        return stateContainer.subjectReadState.getValue();
    }

    void jumpToChapter(int maxChapters, String chapterText) {
        try {
            if (TextUtils.isEmpty(chapterText)) {
                bus.dispatch(new Event<>(EventID.ERROR, new ResError(STRING_RES_READ_ERR_EMPTY)));
                return;
            }
            int chapter = Integer.parseInt(chapterText);
            if (!MathExt.inRangeIncl(chapter, 1, maxChapters)) {
                bus.dispatch(new Event<>(EventID.ERROR,
                        new ResError(STRING_RES_READ_ERR_CHAPTER_RANGE, chapter, maxChapters)));
                return;
            }
            ReadState value = stateContainer.subjectReadState.getValue();
            Flowable<ReadState> readStateFlowable = startWithWait(value, readStateService
                    .readFrom(new ReadState(value.bookId, chapter))
                    .compose(toDisplayableState())
                    .compose(scheduleSingle(uiScheduler)))
                    .compose(waitEventDispatcher.dispatchFlowable(TAG));
            disposables.add(readStateFlowable
                    .subscribe(stateContainer.subjectReadState::onNext));
        } catch (NumberFormatException e) {
            bus.dispatch(new Event<>(EventID.ERROR, new ResError(STRING_RES_READ_ERR_CHAPTER, chapterText)));
        }
    }

    private SingleTransformer<ReadState, ReadState> toDisplayableState() {
        return up -> up
                .subscribeOn(Schedulers.io())
                .flatMap(rs -> searchService
                        .find(rs.bookId, new int[]{rs.chapter}, new int[]{})
                        .subscribeOn(Schedulers.io())
                        .toList()
                        .flatMap(l -> favService.getFavoritesFromChapter(rs.bookId, rs.chapter, true)
                                .map(f -> Content.Util.joinFavorites(l, f)))
                        .map(l -> rs.scrollPosition(findPositionFromVersetId(l, rs.scrollPos))
                                .content(Content.Util.organize(l, null, res.getString("read_chapter_prefix")))));
    }

    /**
     * basically we assume that when calling for a chapter jump. we send the verset id as
     * "scrollposition" all along, then we try to fint it in the result list.<br/>
     * It's pretty confusing, maybe we should introduce a variable member name for eg. "versetToPoint",
     * instead of piggybacking on scrollposition (TODO)
     *
     * @param list - result from search
     * @param id   - sent from other parts of application, smuggled as "scrollposition"
     * @return the verset position in list, 0 if not found
     */
    private int findPositionFromVersetId(List<Verset> list, int id) {
        if (id == 0) return id;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).verset == id) {
                return i;
            }
        }
        return 0;
    }


    private Flowable<ReadState> startWithWait(ReadState from, Single<ReadState> query) {
        return Single.just(from.inProgress()).concatWith(query);
    }

    void processTap(Observable<TapEvent> tapObs) {
        disposables.add(tapObs
                .filter(ev -> ev.viewType == ReadRecyclerAdapter.VERSET_VIEW_TYPE)
                .subscribe(ev -> {
                    Verset verset = (Verset) stateContainer.subjectReadState.getValue()
                            .content.get(ev.position);
                    switch (ev.kind) {
                        case DOUBLE_TAP:
                            view.onConfirmToggleFavorite(verset, ev.position);
                            break;
                        case LONG_TAP:
                            if (verset.favorite) {
                                view.onConfirmFavoriteShow(verset);
                            }
                            break;
                        case SINGLE_TAP:
                            break;
                    }
                }));
    }

    void confirmToggleFavorite(int index) {
        Disposable disposable = Observable.just(index)
                .compose(FavoritesOpsMediator.toggle(favService,
                        () -> stateContainer.subjectReadState.getValue().content))
                .compose(scheduleObservable(uiScheduler))
                .subscribe(pair -> {
                    bus.dispatch(new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_CHANGED,
                            pair.second));
                    view.onSelect(pair.first);
                    view.onScrollPosition(scrollPosition);
                });
        disposables.add(disposable);
    }

    public void confirmFavoriteShow(Verset verset) {
        tabEventDispatcher.openFavoriteTab(verset);
    }
}
