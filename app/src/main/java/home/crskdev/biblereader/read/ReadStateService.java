package home.crskdev.biblereader.read;

import android.content.SharedPreferences;

import java.util.Collection;
import java.util.Iterator;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.Completable;
import io.reactivex.Single;

import static home.crskdev.biblereader.core.BibleInfo.BookInfo;
import static home.crskdev.biblereader.read.ReadState.DEFAULT;

/**
 * Created by criskey on 15/5/2017.
 */
@Singleton
public class ReadStateService {

    private static final String READ_BOOK_ID = "read_book_id";

    private static final String READ_CHAPTER = "read_chapter";

    private static final String READ_IS_START = "read_is_start";

    private static final String READ_IS_END = "read_is_end";

    private static final String READ_SCROLL = "read_scroll";

    private static final int OUT_OF_BOUNDS = 0;

    private SharedPreferences prefs;

    private BibleInfoLoader loader;

    @Inject
    public ReadStateService(SharedPreferences prefs, BibleInfoLoader loader) {
        this.prefs = prefs;
        this.loader = loader;
    }

    public Single<ReadState> loadReadState() {
        return Single.create(s -> {
            Asserts.assertIsBackgroundThread();
            String bookId = prefs.getString(READ_BOOK_ID, DEFAULT.bookId);
            int chapter = prefs.getInt(READ_CHAPTER, DEFAULT.chapter);
            int scrollPosition = prefs.getInt(READ_SCROLL, DEFAULT.scrollPos);
            boolean isStart = prefs.getBoolean(READ_IS_START, DEFAULT.isStart);
            boolean isEnd = prefs.getBoolean(READ_IS_END, DEFAULT.isEnd);
            s.onSuccess(new ReadState(bookId, chapter, isStart, isEnd, scrollPosition));
        });
    }

    public Single<Integer> getMaxChapters(ReadState state) {
        return loader.load().map(bi -> bi.getBookById(state.bookId)).map(b -> b.noOfChapters);
    }

    public Single<ReadState> readNext(ReadState rs) {
        return loader.load().map(bi -> {
            BookInfo bookInfo = bi.getBookById(rs.bookId);
            int nextChapter = nextChapter(bookInfo, rs.chapter);
            boolean isEnd = false;
            if (nextChapter == OUT_OF_BOUNDS) {
                bookInfo = nextBook(bi.getBookInfos(), rs.bookId);
                if (bookInfo.equals(BookInfo.NO_BOOK)) { // we reach the end somehow
                    return rs.end();
                } else {
                    nextChapter = 1; // we have first chapterId
                }
            } else {
                isEnd = nextBook(bi.getBookInfos(), rs.bookId).equals(BookInfo.NO_BOOK) &&
                        nextChapter(bookInfo, nextChapter) == OUT_OF_BOUNDS;
            }
            return new ReadState(bookInfo.id, nextChapter, false, isEnd);
        });
    }

    public Single<ReadState> readPrev(ReadState rs) {
        return loader.load().map(bi -> {
            BookInfo bookInfo = bi.getBookById(rs.bookId);
            int prevChapter = prevChapter(rs.chapter);
            boolean isStart = false;
            if (prevChapter == OUT_OF_BOUNDS) {
                bookInfo = prevBook(bi.getBookInfos(), rs.bookId);
                if (bookInfo.equals(BookInfo.NO_BOOK)) { // we reach start somehow
                    return rs.start();
                } else {
                    prevChapter = bookInfo.noOfChapters; // we have last chapterId
                }
            } else {
                isStart = prevBook(bi.getBookInfos(), rs.bookId).equals(BookInfo.NO_BOOK) &&
                        prevChapter(prevChapter) == OUT_OF_BOUNDS;
            }
            return new ReadState(bookInfo.id, prevChapter, isStart, false);
        });
    }

    public Single<ReadState> readFrom(ReadState rs) {
        return Single.zip(readNext(rs), readPrev(rs),
                (next, prev) -> new ReadState(rs.bookId, rs.chapter, prev.isStart, next.isEnd, rs.scrollPos));
    }

    private BookInfo prevBook(Collection<BookInfo> bookInfos, String currId) {
        BookInfo prev = BookInfo.NO_BOOK;
        for (Iterator<BookInfo> it = bookInfos.iterator(); it.hasNext(); ) {
            BookInfo next = it.next();
            if (next.id.equals(currId)) {
                return prev;
            }
            prev = next;
        }
        return prev;
    }

    private int prevChapter(int currChapter) {
        int prev = currChapter - 1;
        return (prev >= 1) ? prev : OUT_OF_BOUNDS;
    }

    private BookInfo nextBook(Collection<BookInfo> bookInfos, String currId) {
        for (Iterator<BookInfo> it = bookInfos.iterator(); it.hasNext(); ) {
            if (it.next().id.equals(currId)) {
                return (it.hasNext()) ? it.next() : BookInfo.NO_BOOK;
            }
        }
        return BookInfo.NO_BOOK;
    }

    private int nextChapter(BookInfo bookInfo, int currChapter) {
        int next = currChapter + 1;
        return (next <= bookInfo.noOfChapters) ? next : OUT_OF_BOUNDS;
    }


    public Completable saveReadState(ReadState state) {
        return Completable.create(s -> {
            prefs.edit()
                    .putString(READ_BOOK_ID, state.bookId)
                    .putInt(READ_CHAPTER, state.chapter)
                    .putBoolean(READ_IS_START, state.isStart)
                    .putBoolean(READ_IS_END, state.isEnd)
                    .putInt(READ_SCROLL, state.scrollPos)
                    .apply();
            s.onComplete();
        });
    }

}
