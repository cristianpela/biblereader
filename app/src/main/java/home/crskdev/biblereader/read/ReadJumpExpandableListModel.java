package home.crskdev.biblereader.read;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crskdev.biblereader.util.MathExt;

/**
 * Created by criskey on 19/5/2017.
 */

public class ReadJumpExpandableListModel {

    private final List<Book> books;

    private final Map<Book, List<Chapter>> chapters;

    public ReadJumpExpandableListModel(List<Book> books, Map<Book, List<Chapter>> chapters) {
        this.books = new ArrayList<>(books);
        this.chapters = new HashMap<>(chapters);
    }

    public List<Book> getBooks() {
        return Collections.unmodifiableList(books);
    }

    public Map<Book, List<Chapter>> getChapters() {
        return Collections.unmodifiableMap(chapters);
    }

    public ReadJumpExpandableListModel expand(int position) {
        if (!MathExt.inRangeIncl(position, 0, books.size() - 1)) {
            return this;
        }
        ReadJumpExpandableListModel copy = copy();
        Book book = copy.books.get(position);
        book = new Book(book.id, book.title, true);
        copy.books.set(position, book);
        return copy;
    }

    public ReadJumpExpandableListModel expand(String bookId) {
        int position = getBookPosition(bookId);
        if (position == -1) {
            return this;
        }
        return expand(position);
    }

    public ReadJumpExpandableListModel collapseAll() {
        ReadJumpExpandableListModel copy = copy();
        for (int i = 0; i < copy.books.size(); i++) {
            Book book = copy.books.get(i);
            book = new Book(book.id, book.title, false);
            copy.books.set(i, book);
        }
        return copy;
    }

    public ReadJumpExpandableListModel collapse(int position) {
        if (!MathExt.inRangeIncl(position, 0, books.size() - 1)) {
            return this;
        }
        ReadJumpExpandableListModel copy = copy();
        Book book = copy.books.get(position);
        book = new Book(book.id, book.title, false);
        copy.books.set(position, book);
        return copy;
    }

    public List<Integer> getExpandedBookPositions() {
        ArrayList<Integer> positions = new ArrayList<>();
        for (int i = 0; i < books.size(); i++) {
            Book book = books.get(i);
            if (book.expanded)
                positions.add(i);
        }
        return positions;
    }

    private int getBookPosition(String id) {
        for (int i = 0; i < books.size(); i++) {
            Book b = books.get(i);
            if (b.id.equals(id))
                return i;
        }
        return -1;
    }

    @NonNull
    private synchronized ReadJumpExpandableListModel copy() {
        List<Book> newBooks = new ArrayList<>(books);
        Map<Book, List<Chapter>> newChapters = new HashMap<>(chapters);
        return new ReadJumpExpandableListModel(newBooks, newChapters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReadJumpExpandableListModel that = (ReadJumpExpandableListModel) o;

        if (that.getBooks().size() != getBooks().size())
            return false;

        for (int i = 0; i < books.size(); i++) {
            Book b1 = books.get(i);
            Book b2 = that.getBooks().get(i);
            if (b1.expanded != b2.expanded) return false;
        }

        return true;
    }


    public static class Book {

        public final String id;
        public final String title;
        public final boolean expanded;

        public Book(String id, String title, boolean expanded) {
            this.id = id;
            this.title = title;
            this.expanded = expanded;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Book book = (Book) o;

            return id != null ? id.equals(book.id) : book.id == null;

        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }

        @Override
        public String toString() {
            return title;
        }
    }

    public static class Chapter {
        public final int id;
        public final String text;

        public Chapter(int id, String text) {
            this.id = id;
            this.text = text;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Chapter chapter = (Chapter) o;

            return id == chapter.id;

        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public String toString() {
            return text;
        }
    }

}
