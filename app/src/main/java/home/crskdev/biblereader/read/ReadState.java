package home.crskdev.biblereader.read;

import android.support.annotation.NonNull;
import android.text.Spanned;
import android.text.SpannedString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.state.ResetableState;

/**
 * Created by criskey on 12/5/2017.
 */

public class ReadState implements ResetableState {

    public static final List<Content> NONE = Collections.unmodifiableList(new ArrayList<>());

    public static final ReadState DEFAULT = new ReadState(false, "GEN", 1, NONE, true, false, 0);

    public final String bookId;

    public final int chapter;

    public final List<Content> content;

    public final boolean isStart;

    public final boolean isEnd;

    public final boolean inProgress;

    public final int scrollPos;

    public ReadState(String bookId, int chapter, List<Content> content) {
        this(false, bookId, chapter, (content != null) ? Collections.unmodifiableList(content)
                : NONE, false, false, 0);
    }

    public ReadState(String bookId, int chapter) {
        this(bookId, chapter, NONE);
    }

    public ReadState(String bookId, int chapter, List<Content> content, boolean isStart, boolean isEnd, int scrollPos) {
        this(false, bookId, chapter, content, isStart, isEnd, scrollPos);
    }

    public ReadState(String bookId, int chapter, boolean isStart, boolean isEnd) {
        this(false, bookId, chapter, NONE, isStart, isEnd, 0);
    }

    public ReadState(String bookId, int chapter, boolean isStart, boolean isEnd, int scrollPosition) {
        this(false, bookId, chapter, NONE, isStart, isEnd, scrollPosition);
    }

    public ReadState(boolean inProgress, String bookId, int chapter,
                     List<Content> content, boolean isStart, boolean isEnd, int scrollPos) {
        this.inProgress = inProgress;
        this.bookId = bookId;
        this.chapter = chapter;
        this.content = content;
        this.isStart = isStart;
        this.isEnd = isEnd;
        this.scrollPos = scrollPos;
    }

    public static ReadState scrollPos(ReadState from, int scrollPos) {
        from = notNull(from);
        return new ReadState(from.inProgress, from.bookId, from.chapter, from.content, from.isStart, from.isEnd, scrollPos);
    }

    public static ReadState jump(Verset verset) {
        return new ReadState(verset.bookId, verset.chapterId).scrollPosition(verset.verset);
    }

    private static ReadState notNull(ReadState s) {
        return s == null ? DEFAULT : s;
    }

    /**
     * Note: don'v store this around, only use it for transient states
     * Provide an empty list when you're about to store ReadState into memory, oir
     *
     * @param content
     * @return
     */
    public ReadState content(@NonNull List<Content> content) {
        return new ReadState(false, bookId, chapter, content, isStart, isEnd, scrollPos);
    }

    public ReadState updateContentAt(int index, Verset favorite) {
        List<Content> update = new ArrayList<>(content);
        update.set(index, favorite);
        return content(update);
    }

    public ReadState scrollPosition(int position) {
        return new ReadState(inProgress, bookId, chapter, content, isStart, isEnd, position);
    }

    public ReadState noContent() {
        return new ReadState(false, bookId, chapter, NONE, isStart, isEnd, scrollPos);
    }

    public ReadState end() {
        return new ReadState(inProgress, bookId, chapter, content, false, true, scrollPos);
    }

    public ReadState start() {
        return new ReadState(inProgress, bookId, chapter, content, true, false, scrollPos);
    }

    public ReadState inProgress() {
        return new ReadState(true, bookId, chapter, content, isStart, isEnd, scrollPos);
    }

    public ReadState inIdle() {
        return new ReadState(false, bookId, chapter, content, isStart, isEnd, scrollPos);
    }

    @Override
    public ReadState reset() {
        return DEFAULT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReadState readState = (ReadState) o;

        if (chapter != readState.chapter) return false;
        if (isStart != readState.isStart) return false;
        if (isEnd != readState.isEnd) return false;
        if (inProgress != readState.inProgress) return false;
        if (scrollPos != readState.scrollPos) return false;
        if (bookId != null ? !bookId.equals(readState.bookId) : readState.bookId != null)
            return false;
        return content != null ? content.equals(readState.content) : readState.content == null;

    }

    @Override
    public int hashCode() {
        int result = bookId != null ? bookId.hashCode() : 0;
        result = 31 * result + chapter;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (isStart ? 1 : 0);
        result = 31 * result + (isEnd ? 1 : 0);
        result = 31 * result + (inProgress ? 1 : 0);
        result = 31 * result + scrollPos;
        return result;
    }


    @Override
    public String toString() {
        return "ReadState{" +
                "bookId='" + bookId + '\'' +
                ", chapterId=" + chapter +
                ", content='" + content.size() +
                ", isStart=" + isStart +
                ", isEnd=" + isEnd +
                ", inProgress=" + inProgress +
                ", scrollPos=" + scrollPos +
                '}';
    }
}
