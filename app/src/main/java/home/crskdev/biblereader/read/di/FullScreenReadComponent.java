package home.crskdev.biblereader.read.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerActivity;
import home.crskdev.biblereader.read.FullScreenActivity;

/**
 * Created by criskey on 29/5/2017.
 */
@PerActivity
@Subcomponent
public interface FullScreenReadComponent extends AndroidInjector<FullScreenActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FullScreenActivity> {
    }

}
