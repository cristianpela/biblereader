package home.crskdev.biblereader.read;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.biblereader.core.BibleInfo;
import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

import static home.crskdev.biblereader.util.RxUtils.scheduleFlowable;
import static home.crskdev.biblereader.util.RxUtils.scheduleSingle;


/**
 * Created by criskey on 17/5/2017.
 */
@PerFragment
public class ReadNavPresenter extends Presenter<ReadNavView> {

    private final BibleInfoLoader loader;

    private String chapterTranslated;

    @Inject
    public ReadNavPresenter(EventBus bus, StateContainer stateContainer, BibleInfoLoader loader, Scheduler uiScheduler, ResourceFinder res) {
        super(bus, stateContainer, uiScheduler);
        this.loader = loader;
        chapterTranslated = res.getString("nav_exp_list_item_chapter") + " ";
    }

    @Override
    protected void postAttach() {
        disposables.add(bus.toObservable().subscribe(ev -> {
            if (ev.group == GroupEventID.GROUP_READ && ev.id == EventID.READ_NAV_RESULT) {
                ReadState s = (ReadState) ev.data;
                view.onProgress(s.inProgress);
                if (!s.inProgress) {
                    if (s.isStart && s.isEnd) {
                        view.onStartReached();
                        view.onEndReached();
                    } else if (s.isStart) {
                        view.onStartReached();
                    } else if (s.isEnd) {
                        view.onEndReached();
                    } else {
                        view.onNavigation();
                    }
                }
            }
        }));
        disposables.add(stateContainer.subjectJumpListState
                .filter(jls -> jls != null)
                .distinctUntilChanged() // prevents stackoverflow; check view implementation #onInitExpandedJumpModel
                .subscribe(view::onInitExpandedJumpModel));
        //we expand the current reading book
        disposables.add(Flowable.combineLatest(stateContainer.subjectReadState.toFlowable(BackpressureStrategy.LATEST),
                getModel().toFlowable(), (rs, jls) -> jls.collapseAll().expand(rs.bookId))
                .compose(scheduleFlowable(uiScheduler))
                .subscribe(stateContainer.subjectJumpListState::onNext));
    }

    public void dispatchNext(Observable<Object> click) {
        disposables.add(click.debounce(100, TimeUnit.MILLISECONDS)
                .observeOn(uiScheduler)
                .subscribe(rs -> {
                    bus.dispatch(new Event<>(GroupEventID.GROUP_READ, EventID.READ_NAV_NEXT, null));
                }));
    }

    public void dispatchPrev(Observable<Object> click) {
        disposables.add(click.debounce(100, TimeUnit.MILLISECONDS)
                .observeOn(uiScheduler)
                .subscribe(__ -> {
                    bus.dispatch(new Event<>(GroupEventID.GROUP_READ, EventID.READ_NAV_PREV, null));
                }));
    }

    public void jumpRead(String bookId, int chapter) {
        bus.dispatch(new Event<>(GroupEventID.GROUP_READ, EventID.READ_NAV_JUMP, new ReadState(bookId, chapter)));
    }

    public void expand(int groupPosition) {
        ReadJumpExpandableListModel model = stateContainer.subjectJumpListState.getValue();
        stateContainer.subjectJumpListState.onNext(model.expand(groupPosition));
    }

    public void collapse(int groupPosition) {
        ReadJumpExpandableListModel model = stateContainer.subjectJumpListState.getValue();
        stateContainer.subjectJumpListState.onNext(model.collapse(groupPosition));
    }

    public void searchBooks(Observable<CharSequence> edit) {
        disposables.add(edit.debounce(500, TimeUnit.MILLISECONDS).skip(1)
                .switchMapSingle(in -> getModelContains(in.toString()).compose(scheduleSingle(uiScheduler)))
                .subscribe(stateContainer.subjectJumpListState::onNext));
    }

    private Single<ReadJumpExpandableListModel> getModel() {
        return Single.defer(() -> {
            ReadJumpExpandableListModel value = stateContainer.subjectJumpListState.getValue();
            String any = "";
            return value != null ? Single.just(value) : getModelContains(any);
        });
    }

    private Single<ReadJumpExpandableListModel> getModelContains(@NonNull String contains) {
        return loader.load()
                .map(bi -> {
                    List<ReadJumpExpandableListModel.Book> bkModel = new ArrayList<>();
                    Map<ReadJumpExpandableListModel.Book, List<ReadJumpExpandableListModel.Chapter>> chModel = new HashMap<>();
                    String sw = contains.trim();
                    Collection<BibleInfo.BookInfo> bookInfos = (sw.length() == 0) ? bi.getBookInfos() : bi.getBooksContains(sw);
                    for (BibleInfo.BookInfo b : bookInfos) {
                        ReadJumpExpandableListModel.Book mb = new ReadJumpExpandableListModel.Book(b.id, b.title, false);
                        bkModel.add(mb);
                        List<ReadJumpExpandableListModel.Chapter> chDetails = new ArrayList<>();
                        for (int i = 1; i <= b.noOfChapters; i++) {
                            chDetails.add(new ReadJumpExpandableListModel.Chapter(i, chapterTranslated + i));
                        }
                        chModel.put(mb, chDetails);
                    }
                    return new ReadJumpExpandableListModel(bkModel, chModel);
                });
    }
}
