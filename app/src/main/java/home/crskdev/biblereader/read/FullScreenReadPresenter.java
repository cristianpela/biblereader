package home.crskdev.biblereader.read;

import javax.inject.Inject;

import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.di.PerActivity;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.search.SearchService;
import home.crskdev.biblereader.util.HtmlContentRenderer;
import home.crskdev.biblereader.util.MathExt;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 29/5/2017.
 */
@PerActivity
public class FullScreenReadPresenter extends Presenter<FullScreenReadView> {

    private final HtmlContentRenderer renderer;
    private final SearchService searchService;
    private final ResourceFinder res;
    private float lastProd = 0f;

    @Inject
    public FullScreenReadPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler,
                                   HtmlContentRenderer renderer, SearchService searchService, ResourceFinder res) {
        super(bus, stateContainer, uiScheduler);
        this.renderer = renderer;
        this.searchService = searchService;
        this.res = res;
    }

    @Override
    protected void postAttach() {
        disposables.add(stateContainer.subjectReadState
                .filter(rs -> rs != null)
                .flatMap(rs -> Single.just(rs).compose(toDisplayableState()).toObservable())
                .observeOn(uiScheduler)
                .subscribe((rs) -> {
                    view.display(renderer.toSpanned(renderer.render(rs.content)));
                }));
    }

    public void scale(float textSize, float factor) {
        float prod = MathExt.clamp(textSize * factor, 10f, 30f);
        if (prod != lastProd) {
            view.onScale(prod);
        }
        lastProd = prod;
    }

    private SingleTransformer<ReadState, ReadState> toDisplayableState() {
        return up -> up
                .subscribeOn(Schedulers.io())
                .flatMap(rs -> searchService
                        .find(rs.bookId, new int[]{rs.chapter}, new int[]{})
                        .subscribeOn(Schedulers.io())
                        .toList()
                        .map(l -> rs.content(Content.Util.organize(l, null, res.getString("read_chapter_prefix")))));
    }
}
