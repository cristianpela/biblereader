package home.crskdev.biblereader.read.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.read.ReadViewFragment;
import home.crskdev.biblereader.search.SearchViewFragment;

/**
 * Created by criskey on 12/5/2017.
 */
@PerFragment
@Subcomponent
public interface ReadComponent extends AndroidInjector<ReadViewFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ReadViewFragment> {
    }
}
