package home.crskdev.biblereader.read;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.Chapter;
import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import home.crskdev.biblereader.util.ViewUtils;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 7/6/2017.
 */
public class ReadRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int BOOK_VIEW_TYPE = 0;
    static final int CHAPTER_VIEW_TYPE = 1;
    static final int VERSET_VIEW_TYPE = 2;
    static final int OTHER_VIEW_TYPE = 3;

    private List<Content> contents;

    private BehaviorSubject<Integer> favSelSubject;

    public ReadRecyclerAdapter() {
        contents = Collections.emptyList();
        favSelSubject = BehaviorSubject.create();
    }

    public void setContents(@NonNull List<Content> content) {
        this.contents = content;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        Content c = contents.get(position);
        if (c.getClass().equals(Book.class)) {
            return BOOK_VIEW_TYPE;
        }
        if (c.getClass().equals(Chapter.class)) {
            return CHAPTER_VIEW_TYPE;
        }
        if (c.getClass().equals(Verset.class)) {
            return VERSET_VIEW_TYPE;
        }
        return OTHER_VIEW_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        switch (viewType) {
            case BOOK_VIEW_TYPE:
                vh = new BookViewHolder(inflate(parent, R.layout.read_recy_book_item));
                break;
            case CHAPTER_VIEW_TYPE:
                vh = new ChapterViewHolder(inflate(parent, R.layout.read_recy_chapter_item));
                break;
            case VERSET_VIEW_TYPE:
                vh = new VersetViewHolder(inflate(parent, R.layout.read_recy_verset_item));
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case BOOK_VIEW_TYPE:
                ((BookViewHolder) holder)
                        .txtBook.setText(((Book) contents.get(position)).title);
                break;
            case CHAPTER_VIEW_TYPE:
                ((ChapterViewHolder) holder)
                        .txtChapter.setText(((Chapter) contents.get(position)).title);
                break;
            case VERSET_VIEW_TYPE:
                Verset verset = (Verset) contents.get(position);
                TextView txtVerset = ((VersetViewHolder) holder)
                        .txtVerset;
                txtVerset.setText(verset.content);
                int color = ContextCompat.getColor(holder.itemView.getContext(),
                        verset.favorite ? R.color.colorFavorite : R.color.colorBackground);
                txtVerset.setBackgroundColor(color);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    private View inflate(ViewGroup parent, @LayoutRes int layout) {
        return LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
    }

    public Observable<Integer> favoriteSelectionObservable() {
        return favSelSubject;
    }

    public void changeFavorite(int index) {
        notifyItemChanged(index);
    }

    static class BookViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_recy_book)
        TextView txtBook;

        public BookViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class ChapterViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_recy_chapter)
        TextView txtChapter;

        public ChapterViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class VersetViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_recy_verset)
        TextView txtVerset;

        public VersetViewHolder(View itemView) {
            super(itemView);
        }

    }
}
