package home.crskdev.biblereader.read.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.read.ReadNavViewFragment;

/**
 * Created by criskey on 17/5/2017.
 */
@PerFragment
@Subcomponent
public interface ReadNavComponent extends AndroidInjector<ReadNavViewFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<ReadNavViewFragment> {
    }
}
