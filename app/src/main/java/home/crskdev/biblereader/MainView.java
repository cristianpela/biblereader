package home.crskdev.biblereader;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 5/5/2017.
 */

public interface MainView extends IView {

    void onTabSelected(@MainState.Tab int tab);

    void onTabUnselected(int tab);

    void onWait(boolean wait);

    void onWaitInfo(String waitInfo);

    void onConfirmRestoreData();
}
