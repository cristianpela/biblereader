package home.crskdev.biblereader;

import android.support.v4.view.ViewPager;

import javax.inject.Inject;

/**
 * Created by criskey on 28/7/2017.
 */
public class TandemViewPagerManager {

    private ViewPager[] viewPagers = new ViewPager[]{};

    @Inject
    public TandemViewPagerManager() {

    }

    public void setViewPagers(ViewPager... viewPagers) {
        this.viewPagers = viewPagers;
        setOffscreenPageLimit(3);
    }

    public void setOffscreenPageLimit(int limit) {
        for (int i = 0; i < viewPagers.length; i++) {
            viewPagers[i].setOffscreenPageLimit(limit);
        }
    }

    public void setCurrentItem(int item) {
        for (int i = 0; i < viewPagers.length; i++) {
            viewPagers[i].setCurrentItem(item);
        }
    }

}
