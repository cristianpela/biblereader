package home.crskdev.biblereader.favorites;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.db.BaseRepository;
import home.crskdev.biblereader.core.db.BibleSQLiteOpenHelper;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.ImmLists;
import home.crskdev.biblereader.util.Pair;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.core.db.Contract.COL_BOOK_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_CHAPTER_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_DATE;
import static home.crskdev.biblereader.core.db.Contract.COL_FAVORITE;
import static home.crskdev.biblereader.core.db.Contract.COL_FAVTAG_FAV_BOOK_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_FAVTAG_FAV_CHAPTER_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_FAVTAG_FAV_VERSETS;
import static home.crskdev.biblereader.core.db.Contract.COL_FAVTAG_TAG_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_TAG_NAME;
import static home.crskdev.biblereader.core.db.Contract.COL_VERSET_CONTENT;
import static home.crskdev.biblereader.core.db.Contract.COL_VERSET_ID;
import static home.crskdev.biblereader.core.db.Contract.TABLE_FAVORITES_INFO;
import static home.crskdev.biblereader.core.db.Contract.TABLE_FAVORITES_TAG_INFO;
import static home.crskdev.biblereader.core.db.Contract.TABLE_TAG_INFO;
import static home.crskdev.biblereader.core.db.CursorMapper.NO_MAPPER;

/**
 * Created by criskey on 5/6/2017.
 */
@Singleton
public class FavoritesService extends BaseRepository {

    @Inject
    public FavoritesService(BibleSQLiteOpenHelper helper) {
        super(helper);
    }

    public Single<Verset> setFavorite(Verset verset) {
        return Single.fromCallable(() -> {
            ContentValues cv = toFavCv(verset);
            db().insertWithOnConflict(TABLE_FAVORITES_INFO, null, cv,
                    SQLiteDatabase.CONFLICT_REPLACE);
            return verset.date(cv.getAsLong(COL_DATE));
        });
    }

    public Single<Tag> saveTag(Tag tag) {
        return Single.fromCallable(() -> {
            long tagId = db().insertWithOnConflict(TABLE_TAG_INFO, null, toTagCv(tag), SQLiteDatabase.CONFLICT_REPLACE);
            return new Tag(tagId, tag.name);
        });
    }

    public Single<Boolean> existTag(Tag tag) {
        String idCheckToo = (tag.id != -1) ? " AND " + COL_ID + "!=" + tag.id : "";
        String sql = "SELECT " + COL_ID + " FROM " + TABLE_TAG_INFO
                + " WHERE " + COL_TAG_NAME + "='" + tag.name + "'" + idCheckToo;
        return rawQuery(sql, null, NO_MAPPER).map(l -> l.size() > 0);
    }

    public Single<Verset> setTag(Tag tag, Verset verset) {
        return Single.fromCallable(() -> {
            Verset vTag = verset;
            try {
                db().beginTransaction();
                //since we are in a transaction, we can'v use insertWithConflict,
                //because REPLACE algorithm makes the transaction to abort
                //so in this case make sure that if tag.id is != -1 then is valid one
                long tagId = (tag.id == -1)
                        ? db().insert(TABLE_TAG_INFO, null, toTagCv(tag))
                        : tag.id;
                ContentValues values = toFavTagCv(tagId, verset);
                db().insert(TABLE_FAVORITES_TAG_INFO, null, values);
                vTag = verset.tags(ImmLists.add(new Tag(tagId, tag.name), verset.tags));
                db().setTransactionSuccessful();
            } finally {
                db().endTransaction();
            }
            return vTag;
        });
    }

    public Single<List<Tag>> getAllTags() {
        String sql = "SELECT * FROM " + TABLE_TAG_INFO + " ORDER BY " + COL_TAG_NAME + " ASC";
        return rawQuery(sql, null, this::toTag);
    }

    public Completable removeTag(Tag tag) {
        return Completable.fromCallable(() -> {
            int result = db().delete(TABLE_TAG_INFO, COL_ID + "=?", new String[]{Long.toString(tag.id)});
            if (result == 0)
                throw new RuntimeException("Tag " + tag + " could not be deleted. Make sure its id is valid");
            return true;
        }).subscribeOn(Schedulers.io());
    }

    public Completable removeTags(List<Tag> tags) {
        return Completable.fromCallable(() -> {
            try {
                db().beginTransaction();
                for (Tag tag : tags) {
                    long tagId = tag.id;
                    String sql = "DELETE FROM " + TABLE_TAG_INFO
                            + " WHERE " + COL_ID + "=" + tagId;
                    db().execSQL(sql);
                }
                db().setTransactionSuccessful();
            } finally {
                db().endTransaction();
            }
            return true;
        }).subscribeOn(Schedulers.io());
    }

    public Completable removeTags(List<Tag> tags, Verset verset) {
        return Completable.fromCallable(() -> {
            try {
                db().beginTransaction();
                for (Tag tag : tags) {
                    long tagId = tag.id;
                    String bookId = verset.bookId;
                    int chapterId = verset.chapterId;
                    int versetId = verset.verset;
                    String sql = "DELETE FROM " + TABLE_FAVORITES_TAG_INFO
                            + " WHERE " + COL_FAVTAG_TAG_ID + "=" + tagId
                            + " AND " + COL_FAVTAG_FAV_BOOK_ID + "='" + bookId + "'"
                            + " AND " + COL_FAVTAG_FAV_CHAPTER_ID + "=" + chapterId
                            + " AND " + COL_FAVTAG_FAV_VERSETS + "=" + versetId;
                    db().execSQL(sql);
                }
                db().setTransactionSuccessful();
            } finally {
                db().endTransaction();
            }
            return true;
        }).subscribeOn(Schedulers.io());
    }

    public Completable editTags(List<Tag> tags) {
        return Completable.fromCallable(() -> {
            try {
                db().beginTransaction();
                for (int i = 0; i < tags.size(); i++) {
                    Tag tag = tags.get(i);
                    ContentValues cv = new ContentValues();
                    cv.put(COL_TAG_NAME, tag.name.toString());
                    db().update(TABLE_TAG_INFO, cv, COL_ID + "=?",
                            new String[]{Long.toString(tag.id)});
                }
                db().setTransactionSuccessful();
            } finally {
                db().endTransaction();
            }
            return true;
        }).subscribeOn(Schedulers.io());
    }

    public Single<Verset> getTags(Verset verset) {
        String sql = "SELECT v." + COL_ID + ", v." + COL_TAG_NAME + " FROM "
                + TABLE_TAG_INFO + " v"
                + " JOIN "
                + TABLE_FAVORITES_TAG_INFO + " ft ON ft." + COL_FAVTAG_TAG_ID + "=v." + COL_ID
                + " JOIN "
                + TABLE_FAVORITES_INFO + " f ON "
                + " f." + COL_BOOK_ID + "=ft." + COL_FAVTAG_FAV_BOOK_ID
                + " AND f." + COL_CHAPTER_ID + "=ft." + COL_FAVTAG_FAV_CHAPTER_ID
                + " AND f." + COL_VERSET_ID + "=ft." + COL_FAVTAG_FAV_VERSETS
                + " WHERE "
                + " f." + COL_BOOK_ID + "='" + verset.bookId + "'"
                + " AND f." + COL_CHAPTER_ID + "=" + verset.chapterId
                + " AND f." + COL_VERSET_ID + "=" + verset.verset
                + " ORDER BY v." + COL_TAG_NAME + " ASC";
        return rawQuery(sql, null, this::toTag).map(verset::tags);
    }

    public Single<Boolean> hasTag(Tag tag, Verset verset) {
        String sql = "SELECT " + COL_ID + " FROM " + TABLE_FAVORITES_TAG_INFO + " WHERE "
                + COL_FAVTAG_FAV_BOOK_ID + "='" + verset.bookId + "'"
                + " AND " + COL_FAVTAG_FAV_CHAPTER_ID + "=" + verset.chapterId
                + " AND " + COL_FAVTAG_FAV_VERSETS + "=" + verset.verset
                + " AND " + COL_FAVTAG_TAG_ID + "=" + tag.id;
        return rawQuery(sql, null, NO_MAPPER).map(l -> l.size() > 0);
    }

    public Single<Tag> findTagByName(String name) {
        String sql = "SELECT * FROM " + TABLE_TAG_INFO + " WHERE " + COL_TAG_NAME + "='" + name + "'";
        return rawQuery(sql, null, this::toTag).map(l -> l.get(0));
    }

    public Single<List<Verset>> getFavoritesWithTag(Tag tag, int size, long fromDate) {
        String sql = "SELECT * FROM " + TABLE_FAVORITES_INFO + " f"
                + " JOIN "
                + TABLE_FAVORITES_TAG_INFO + " ft ON "
                + " f." + COL_BOOK_ID + "=ft." + COL_FAVTAG_FAV_BOOK_ID
                + " AND f." + COL_CHAPTER_ID + "=ft." + COL_FAVTAG_FAV_CHAPTER_ID
                + " AND f." + COL_VERSET_ID + "=ft." + COL_FAVTAG_FAV_VERSETS
                + " JOIN "
                + TABLE_TAG_INFO + " v ON "
                + " v." + COL_ID + "=ft." + COL_FAVTAG_TAG_ID
                + " AND v." + COL_ID + "=" + tag.id
                + " WHERE "
                + COL_DATE + "<" + fromDate + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY " + COL_DATE + " DESC LIMIT " + size;
        return rawQuery(sql, null, this::toFav);
    }

    public Single<List<Verset>> getFavoritesWithTagUntil(Tag tag, int size, long untilDate) {
        String sql = "SELECT * FROM " + TABLE_FAVORITES_INFO + " f"
                + " JOIN "
                + TABLE_FAVORITES_TAG_INFO + " ft ON "
                + " f." + COL_BOOK_ID + "=ft." + COL_FAVTAG_FAV_BOOK_ID
                + " AND f." + COL_CHAPTER_ID + "=ft." + COL_FAVTAG_FAV_CHAPTER_ID
                + " AND f." + COL_VERSET_ID + "=ft." + COL_FAVTAG_FAV_VERSETS
                + " JOIN "
                + TABLE_TAG_INFO + " v ON "
                + " v." + COL_ID + "=ft." + COL_FAVTAG_TAG_ID
                + " AND v." + COL_ID + "=" + tag.id
                + " WHERE "
                + COL_DATE + ">=" + untilDate + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY " + COL_DATE + " DESC LIMIT " + size;
        return rawQuery(sql, null, this::toFav);
    }


    public Single<List<Verset>> getAllFavoritesWithTag(int size, long fromDate) {
        String cols = "f." + COL_BOOK_ID + ", f." + COL_CHAPTER_ID + ", f."
                + COL_VERSET_ID + ", f." + COL_FAVORITE
                + ", f." + COL_VERSET_CONTENT + ", f." + COL_DATE;
        String sql = "SELECT DISTINCT " + cols + " FROM " + TABLE_FAVORITES_INFO + " f"
                + " JOIN "
                + TABLE_FAVORITES_TAG_INFO + " ft ON "
                + " f." + COL_BOOK_ID + "=ft." + COL_FAVTAG_FAV_BOOK_ID
                + " AND f." + COL_CHAPTER_ID + "=ft." + COL_FAVTAG_FAV_CHAPTER_ID
                + " AND f." + COL_VERSET_ID + "=ft." + COL_FAVTAG_FAV_VERSETS
                + " JOIN "
                + TABLE_TAG_INFO + " v ON "
                + " v." + COL_ID + "=ft." + COL_FAVTAG_TAG_ID
                + " WHERE "
                + COL_DATE + "<" + fromDate + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY " + COL_DATE + " DESC LIMIT " + size;
        return rawQuery(sql, null, this::toFav);
    }

    public Single<List<Verset>> getAllFavoritesWithTagUntil(int size, long untilDate) {
        String cols = "f." + COL_BOOK_ID + ", f." + COL_CHAPTER_ID + ", f."
                + COL_VERSET_ID + ", f." + COL_FAVORITE
                + ", f." + COL_VERSET_CONTENT + ", f." + COL_DATE;
        String sql = "SELECT DISTINCT " + cols + " FROM " + TABLE_FAVORITES_INFO + " f"
                + " JOIN "
                + TABLE_FAVORITES_TAG_INFO + " ft ON "
                + " f." + COL_BOOK_ID + "=ft." + COL_FAVTAG_FAV_BOOK_ID
                + " AND f." + COL_CHAPTER_ID + "=ft." + COL_FAVTAG_FAV_CHAPTER_ID
                + " AND f." + COL_VERSET_ID + "=ft." + COL_FAVTAG_FAV_VERSETS
                + " JOIN "
                + TABLE_TAG_INFO + " v ON "
                + " v." + COL_ID + "=ft." + COL_FAVTAG_TAG_ID
                + " WHERE "
                + COL_DATE + ">=" + untilDate + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY " + COL_DATE + " DESC LIMIT " + size;
        return rawQuery(sql, null, this::toFav);
    }


    public Single<List<Verset>> getAllFavorites(int size, long fromDate) {
        return getFavorites(size, fromDate, null);
    }

    public Single<List<Verset>> getFavorites(int size, long fromDate, String byBookId) {
        String groupBy = byBookId != null ? " AND " + COL_BOOK_ID + "='" + byBookId + "'" : "";
        String sql = "SELECT * FROM " + TABLE_FAVORITES_INFO
                + " WHERE " + COL_DATE + "<" + fromDate + groupBy + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY "
                + COL_DATE + " DESC LIMIT " + size;
        return rawQuery(sql, null, this::toFav);
    }

    public Single<List<Verset>> getAllFavoritesUntil(long fromDate) {
        return getFavoritesUntil(fromDate, null);
    }

    public Single<List<Verset>> getFavoritesUntil(long fromDate, String byBookId) {
        String groupBy = byBookId != null ? " AND " + COL_BOOK_ID + "='" + byBookId + "'" : "";
        String sql = "SELECT * FROM " + TABLE_FAVORITES_INFO
                + " WHERE " + COL_DATE + ">=" + fromDate + groupBy + " AND " + COL_FAVORITE + "=1 " +
                " ORDER BY "
                + COL_DATE + " DESC";
        return rawQuery(sql, null, this::toFav);
    }

    public Single<List<Verset>> getFavoritesFromChapter(String bookId, int chapter, boolean ignoreContent) {
        String cols = !ignoreContent ? "*" : COL_BOOK_ID
                + "," + COL_CHAPTER_ID + "," + COL_VERSET_ID + "," + COL_FAVORITE + "," + COL_DATE;
        String sql = "SELECT " + cols + " FROM " + TABLE_FAVORITES_INFO
                + " WHERE " + COL_BOOK_ID + "='" + bookId + "' AND "
                + COL_CHAPTER_ID + "=" + chapter + " AND " + COL_FAVORITE + "=1";
        return rawQuery(sql, null, this::toFav);
    }

    /**
     * by passing a DTO list of versets (verset where which we don't know if these are favorites or not)
     * we return a list with valid favorites
     *
     * @param justDTOVersets
     * @return
     */
    public Single<List<Verset>> getValidFavorites(List<Verset> justDTOVersets) {
        StringBuilder builder = new StringBuilder("SELECT "
                + COL_BOOK_ID + ", "
                + COL_CHAPTER_ID + ", "
                + COL_VERSET_ID + ", "
                + COL_FAVORITE + ", "
                + COL_DATE
                + " FROM " + TABLE_FAVORITES_INFO + " WHERE ");
        for (int i = 0, s = justDTOVersets.size(); i < s; i++) {
            Verset v = justDTOVersets.get(i);
            String where = COL_BOOK_ID + "='" + v.bookId + "' AND "
                    + COL_VERSET_ID + "=" + v.verset + " AND "
                    + COL_CHAPTER_ID + "=" + v.chapterId;
            builder.append(where);
            if (i < s - 1) {
                builder.append(" OR ");
            }
        }
        String sql = builder.toString();
        return rawQuery(sql, null, this::toFav)
                .map(listDb -> Content.Util.joinFavorites(justDTOVersets, listDb));
    }

    public Single<List<Pair<String, Integer>>> getBookIdsWithFavorites() {
        String sql = "SELECT " + COL_BOOK_ID + ", COUNT(" + COL_VERSET_ID + ") FROM " +
                TABLE_FAVORITES_INFO
                + " WHERE " + COL_FAVORITE + "=1 GROUP BY " + COL_BOOK_ID + " ORDER BY " + COL_BOOK_ID;
        return rawQuery(sql, null, c -> {
            String title = c.getString(c.getColumnIndexOrThrow(COL_BOOK_ID));
            int count = c.getInt(1);
            return Pair.of(title, count);
        });
    }

    private Verset toFav(Cursor c) {
        int idx = c.getColumnIndex(COL_BOOK_ID);
        String bookId = idx != -1 ? c.getString(idx) : "";
        idx = c.getColumnIndex(COL_CHAPTER_ID);
        int chapter = idx != -1 ? c.getInt(idx) : idx;
        idx = c.getColumnIndex(COL_VERSET_ID);
        int verset = idx != -1 ? c.getInt(idx) : idx;
        idx = c.getColumnIndex(COL_VERSET_CONTENT);
        String content = idx != -1 ? c.getString(idx) : "";
        idx = c.getColumnIndex(COL_FAVORITE);
        boolean fav = idx != -1 && c.getInt(idx) == 1;
        idx = c.getColumnIndex(COL_DATE);
        long date = idx != -1 ? c.getLong(idx) : idx;
        return new Verset(bookId, null, chapter, verset, content, fav, date, Collections.emptyList());
    }

    private Tag toTag(Cursor c) {
        long id = c.getLong(c.getColumnIndex(COL_ID));
        String name = c.getString(c.getColumnIndex(COL_TAG_NAME));
        return new Tag(id, name);
    }

    private ContentValues toTagCv(Tag tag) {
        ContentValues cv = new ContentValues();
        if (tag.id != -1) {
            cv.put(COL_ID, tag.id);
        }
        cv.put(COL_TAG_NAME, tag.name.toString());
        return cv;
    }

    private ContentValues toFavTagCv(long tagId, Verset verset) {
        ContentValues cv = new ContentValues();
        cv.put(COL_FAVTAG_TAG_ID, tagId);
        cv.put(COL_FAVTAG_FAV_BOOK_ID, verset.bookId);
        cv.put(COL_FAVTAG_FAV_CHAPTER_ID, verset.chapterId);
        cv.put(COL_FAVTAG_FAV_VERSETS, verset.verset);
        return cv;
    }


    private ContentValues toFavCv(Verset verset) {
        ContentValues cv = new ContentValues();
        cv.put(COL_BOOK_ID, verset.bookId);
        cv.put(COL_CHAPTER_ID, verset.chapterId);
        cv.put(COL_VERSET_ID, verset.verset);
        cv.put(COL_VERSET_CONTENT, verset.content.toString());
        cv.put(COL_FAVORITE, verset.favorite ? 1 : 0);
        cv.put(COL_DATE, verset.date > 0 ? verset.date : System.currentTimeMillis());
        return cv;
    }
}
