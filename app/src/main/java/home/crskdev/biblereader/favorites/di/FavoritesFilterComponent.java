package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.filter.FavoritesFilterFragmentView;

/**
 * Created by criskey on 10/6/2017.
 */
@PerFragment
@Subcomponent
public interface FavoritesFilterComponent extends AndroidInjector<FavoritesFilterFragmentView> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FavoritesFilterFragmentView> {
    }

}
