package home.crskdev.biblereader.favorites.filter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 13/6/2017.
 */

public class BookTitlesWithFavoritesAdapter extends RecyclerView.Adapter<BookTitlesWithFavoritesAdapter.BookTitlesViewHolder> {

    private List<BookTitleWithFavorites> content;

    private BehaviorSubject<String> clickSubject;

    public BookTitlesWithFavoritesAdapter() {
        content = new ArrayList<>();
        clickSubject = BehaviorSubject.create();
    }

    @Override
    public BookTitlesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_title_w_fav_item, parent, false);
        return new BookTitlesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BookTitlesViewHolder holder, int position) {
        final BookTitleWithFavorites book = content.get(position);
        holder.txtTitle.setText(book.title);
        holder.txtCount.setText(String.valueOf(book.count));
        holder.itemView.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(), book.color));

        View.OnClickListener listener = (v) -> {
            clickSubject.onNext(book.title);
        };
        holder.txtTitle.setOnClickListener(listener);
        holder.txtCount.setOnClickListener(listener);
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public Observable<String> clickObservable() {
        return clickSubject;
    }

    public void setContent(BookTitleWithFavorites[] bookTitles) {
        content.clear();
        Collections.addAll(content, bookTitles);
        notifyDataSetChanged();
    }

    static class BookTitlesViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_book_title_w_fav)
        TextView txtTitle;

        @BindView(R.id.txt_book_fav_count_)
        TextView txtCount;

        BookTitlesViewHolder(View itemView) {
            super(itemView);

        }
    }
}
