package home.crskdev.biblereader.favorites.detail.tagtoolbar.di;

import dagger.Subcomponent;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.favorites.detail.tagtoolbar.TagToolbar;

/**
 * Created by criskey on 6/7/2017.
 */
@Subcomponent
@PerView
public interface TagToolbarComponent extends AndroidArgumentsInjector<TagToolbar> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidArgumentsInjector.Builder<TagToolbar> {}

}
