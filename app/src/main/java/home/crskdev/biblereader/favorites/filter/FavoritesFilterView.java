package home.crskdev.biblereader.favorites.filter;

import home.crskdev.biblereader.core.mvp.ChildHolderView;

/**
 * Created by criskey on 10/6/2017.
 */
public interface FavoritesFilterView extends ChildHolderView {

    String FRAGMENT_TAG_BOOK_FILTER = "FRAGMENT_TAG_BOOK_FILTER";
    String FRAGMENT_TAG_TAG_FILTER = "FRAGMENT_TAG_TAG_FILTER";

    void onLoadAutocompleteEntries(String[] entries);

    void collapseSheet();

    void checkMenu(String menuCheckFilter);

    void setFilterHint(String filterHint);

    void clearFilterFocus();

}
