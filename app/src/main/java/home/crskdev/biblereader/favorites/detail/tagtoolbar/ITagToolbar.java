package home.crskdev.biblereader.favorites.detail.tagtoolbar;

import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 6/7/2017.
 */

public interface ITagToolbar extends IView {

    void showDeleteTagDialog(Verset verset);

    void showEditTagDialog(Verset verset);

}
