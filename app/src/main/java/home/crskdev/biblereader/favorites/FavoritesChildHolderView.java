package home.crskdev.biblereader.favorites;

import android.support.annotation.NonNull;

import java.util.Map;

import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.ChildHolderView;
import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 17/6/2017.
 */

public interface FavoritesChildHolderView extends ChildHolderView {

    void addOrReplaceView(String tag, Class<? extends BaseFragment> fragClass, @NonNull Map<String, ?> args,
                          boolean addToBackStack);

    void backTo();
}
