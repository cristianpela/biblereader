package home.crskdev.biblereader.favorites.detail;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.TabEventDispatcher;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.Owner;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagOwnershipManager;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.RxUtils;
import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Created by criskey on 17/6/2017.
 */
@PerFragment
public class FavoriteDetailPresenter extends Presenter<FavoriteDetailView> {

    private static final String STRING_RES_TAG_ERR_FAV_HAS_IT = "tag_err_fav_has_it";

    private final FavoritesService favoritesService;

    private final TagOwnershipManager manager;
    private final TabEventDispatcher tabEventDispatcher;

    private Verset verset;

    @Inject
    public FavoriteDetailPresenter(EventBus bus, StateContainer stateContainer,
                                   Scheduler uiScheduler, FavoritesService favoritesService,
                                   TagOwnershipManager manager, TabEventDispatcher tabEventDispatcher) {
        super(bus, stateContainer, uiScheduler);
        this.favoritesService = favoritesService;
        this.manager = manager;
        this.tabEventDispatcher = tabEventDispatcher;
    }

    @Override
    protected void postAttach() {
        Map<String, ?> args = view.getArgs();
        List<Owner> owners = (List<Owner>) args.get(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID);
        manager.addOwners(owners);
        verset = (Verset) args.get(FavoriteDetailView.ARG_KEY_VERSET);
        view.displayVerset(verset.formalTitle(), verset.content);
    }

    @Override
    public void accept(Event event) {
        if (event.checkGroup(GroupEventID.GROUP_TAG)) {
            if (event.checkId(EventID.TAG_SELECT)) {
                TagWrapper tagW = (TagWrapper) event.data;
                if (manager.contains(tagW.owner)) {
                    Single<Verset> setTagSingle = favoritesService.hasTag(tagW.tag, verset)
                            .flatMap(has -> (has) ? Single.error(new Error()) : favoritesService
                                    .setTag(tagW.tag, verset))
                            .compose(RxUtils.scheduleSingle(uiScheduler));
                    disposables.add(setTagSingle.subscribe((v) -> {
                        dispatchOrders(manager.decideAddTag(tagW, true));
                    }, (e) -> {
                        bus.dispatch(new Event<>(EventID.ERROR,
                                new ResError(STRING_RES_TAG_ERR_FAV_HAS_IT, tagW.tag.name)));
                    }));
                }
            }
        }
    }

    private void dispatchOrders(Map<Integer, List<TagWrapper>> orders) {
        for (Integer key : orders.keySet()) {
            List<TagWrapper> tws = orders.get(key);
            for (TagWrapper tw : tws) {
                bus.dispatch(new Event<>(GroupEventID.GROUP_TAG, key, tw));
            }
        }
    }

    public void showReadTab() {
        view.onConfirmVersetToShowInReadTab(verset);
    }

    public void confirmVersetToShowInReadTab() {
        tabEventDispatcher.openReadTab(verset);
    }
}
