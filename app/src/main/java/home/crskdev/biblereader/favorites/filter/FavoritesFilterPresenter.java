package home.crskdev.biblereader.favorites.filter;

import android.support.annotation.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.biblereader.core.BibleInfo;
import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.FavoritesState;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.asserts.Asserts;
import home.crskdev.biblereader.util.ImmLists;
import home.crskdev.biblereader.util.Pair;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.biblereader.favorites.FavoritesState.ALL_BOOKS;
import static home.crskdev.biblereader.favorites.FavoritesState.ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.FILTER_MODE_TAG;
import static home.crskdev.biblereader.favorites.filter.FavoritesFilterView.FRAGMENT_TAG_BOOK_FILTER;
import static home.crskdev.biblereader.favorites.filter.FavoritesFilterView.FRAGMENT_TAG_TAG_FILTER;

/**
 * Created by criskey on 10/6/2017.
 */
@PerFragment
public class FavoritesFilterPresenter extends Presenter<FavoritesFilterView> {

    private final FavoritesService favoritesService;
    private final BibleInfoLoader loader;
    private final ResourceFinder res;

    @Inject
    public FavoritesFilterPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler,
                                    FavoritesService favoritesService, BibleInfoLoader loader,
                                    ResourceFinder res) {
        super(bus, stateContainer, uiScheduler);
        this.favoritesService = favoritesService;
        this.loader = loader;
        this.res = res;
    }

    @Override
    protected void postAttach() {
        //load tags from db;
        Single<String[]> tagTitles = favoritesService.getAllTags().map((tags) -> {
            Asserts.assertIsBackgroundThread();
            return extractTagTitles(tags);
        });

        //load titles from db
        Single<String[]> bookTitles = loader.load().flatMap(bi -> favoritesService
                .getBookIdsWithFavorites()
                .map(ids -> {
                    Asserts.assertIsBackgroundThread();
                    return extractTitles(bi, ids);
                }));

        //choose what to load based on current filter mode
        Observable<String[]> switchTo = stateContainer.subjectFavoriteState.distinctUntilChanged()
                .serialize().flatMap(s ->
                        (s.filterMode == FILTER_MODE_TAG)
                                ? tagTitles.toObservable()
                                : bookTitles.toObservable());

        //triggered when a new favorite or tag was added/removed -> then load from db
        Observable<String[]> busTrigger = bus.toObservable()
                .flatMap(e -> {
                    Observable<String[]> obs = Observable.never();
                    if (e.checkGroup(GroupEventID.GROUP_FAVORITE)
                            && (e.checkId(EventID.FAVORITE_CHANGED))) {
                        obs = bookTitles.toObservable();
                    } else if (e.check(GroupEventID.GROUP_TAG, EventID.TAG_RELOAD)) {
                        obs = tagTitles.toObservable();
                    }
                    return obs;
                });

        Observable<String[]> titlesObservable = switchTo.mergeWith(busTrigger).observeOn(uiScheduler);

        disposables.add(titlesObservable
                .subscribe((entries) -> {
                    view.onLoadAutocompleteEntries(entries);
                    int filterMode = stateContainer.subjectFavoriteState.getValue()
                            .filterMode;
                    if (filterMode == FILTER_MODE_TAG) {
                        showFilterTagPane();
                    } else {
                        showFilterBookPane();
                    }
                }));

        //filter hint disposable
        disposables.add(stateContainer.subjectFavoriteState.
                flatMap(s -> loader.load().map(b -> new Pair<>(s, b)).toObservable()).
                subscribe(pair -> {
                    FavoritesState state = pair.first;
                    BibleInfo bi = pair.second;
                    int filterMode = state.filterMode;
                    String selectedName = null;
                    if (filterMode == FILTER_MODE_TAG &&
                            state.filterTag != null &&
                            !state.filterTag.equals(ALL_TAGS)) {
                        selectedName = state.filterTag.name.toString();
                    } else if (filterMode == FILTER_MODE_BOOK &&
                            state.filterBookId != null &&
                            !state.filterBookId.equals(ALL_BOOKS)) {
                        selectedName = bi.getBookById(state.filterBookId).title;
                    }
                    view.setFilterHint(createFilterHint(filterMode, selectedName));
                }));
    }

    private String createFilterHint(int filterMode, @Nullable String selectedName) {
        String prefixTag = res.getString("edit_filter_prefix_tag");
        String prefixBook = res.getString("edit_filter_prefix_book");
        String prefix = (filterMode == FILTER_MODE_TAG) ? prefixTag : prefixBook;
        return prefix + ((selectedName != null) ? ": " + selectedName : "");
    }

    private String[] extractTitles(BibleInfo info, List<Pair<String, Integer>> ids) {
        String[] titles = new String[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            String id = ids.get(i).first;
            String title = info.getBookById(id).title;
            titles[i] = title;
        }
        return titles;
    }

    private String[] extractTagTitles(List<Tag> tags) {
        return ImmLists.map(tags, Tag::toString).toArray(new String[tags.size()]);
    }

    public void filter(Observable<String> filterSource, Observable<Object> resetClick) {
        final String NONE = "";
        Observable<String> clickMap = resetClick
                .debounce(300, TimeUnit.MILLISECONDS)
                .switchMap(__ -> Observable.just(NONE));
        Observable<Pair<Integer, String>> mergedClick = Observable.merge(filterSource, clickMap)
                .map(text -> {
                    FavoritesState value = stateContainer.subjectFavoriteState.getValue();
                    return new Pair<>(value.filterMode, text);
                })
                .observeOn(uiScheduler);  // cause debounce is called on computation thread

        Observable<Object> switchTo = mergedClick
                .flatMap(caseOf -> {
                    int fm = caseOf.first;
                    String selected = caseOf.second;
                    if (fm == FILTER_MODE_TAG) {
                        if (!selected.equals(NONE)) {
                            return favoritesService.findTagByName(selected).toObservable()
                                    .subscribeOn(Schedulers.io());
                        } else {
                            //this happens when "X" - reset button was pressed
                            return Observable.just(FavoritesState.ALL_TAGS);
                        }
                    } else {
                        return loader.load().map(bi -> bi.getBook(selected)).toObservable()
                                .subscribeOn(Schedulers.io());
                    }
                });

        disposables.add(switchTo
                .observeOn(uiScheduler)
                .subscribe(result -> {
                    FavoritesState value = stateContainer.subjectFavoriteState.getValue();
                    if (result.getClass().equals(BibleInfo.BookInfo.class)) {
                        BibleInfo.BookInfo book = (BibleInfo.BookInfo) result;
                        String id = !book.equals(BibleInfo.BookInfo.NO_BOOK) ? book.id :
                                FavoritesState.ALL_BOOKS;
                        stateContainer.subjectFavoriteState.onNext(value.bookId(id));
                    } else {
                        Tag tag = (Tag) result;
                        stateContainer.subjectFavoriteState.onNext(value.tag(tag));
                    }
                    //instruct the favorites screen holder to go back to
                    //favorites screen if we are in a detail fragment screen
                    bus.dispatch(Event.flag(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_BACK_TO));
                }));
    }

    public void showByBookFilter(Observable<Object> click) {
        disposables.add(click.subscribe(__ -> {
            FavoritesState value = stateContainer.subjectFavoriteState.getValue();
            stateContainer.subjectFavoriteState
                    .onNext(value.filterMode(FavoritesState.FILTER_MODE_BOOK));
        }));
    }

    private void showFilterBookPane() {
        view.addOrReplaceView(
                FRAGMENT_TAG_BOOK_FILTER,
                BookFilterFragment.class, Collections.emptyMap(),
                false
        );
        view.checkMenu(FRAGMENT_TAG_BOOK_FILTER);
    }

    public void showByTagFilter(Observable<Object> click) {
        disposables.add(click.subscribe(__ -> {
            FavoritesState value = stateContainer.subjectFavoriteState.getValue();
            stateContainer.subjectFavoriteState
                    .onNext(value.filterMode(FILTER_MODE_TAG));
        }));
    }

    private void showFilterTagPane() {
        view.addOrReplaceView(
                FRAGMENT_TAG_TAG_FILTER,
                TagFilterFragment.class, Collections.emptyMap(),
                false
        );
        view.checkMenu(FRAGMENT_TAG_TAG_FILTER);
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        if (event.checkGroup(GroupEventID.GROUP_FAVORITE)) {
            view.collapseSheet();
            view.clearFilterFocus();
        }
    }
}
