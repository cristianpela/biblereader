package home.crskdev.biblereader.favorites.filter;

import android.support.v7.widget.Toolbar;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjection;
import home.crskdev.biblereader.core.di.Arguments;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;

/**
 * Created by criskey on 29/6/2017.
 */
public class TagFilterFragment extends BaseFragment implements TagFilterView {

    @Inject
    TagFilterPresenter mPresenter;

    @BindView(R.id.recy_fav_filter_tag)
    TagsRecyclerView mRecylerView;

    @BindView(R.id.toolbar_fav_tag)
    Toolbar mToolbar;

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.fav_filter_tag_layout;
    }

    @Override
    public void onPostAttachPresenter() {
        AndroidArgumentsInjection.inject(this,
                Arguments.noArgs(mToolbar),
                Arguments.noArgs(mRecylerView));
    }

    @Override
    public Map<String, ?> getArgs() {
        Map<String, Object> args = new HashMap<>();
        args.put(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID, mRecylerView.getOwnerId());
        return args;
    }
}
