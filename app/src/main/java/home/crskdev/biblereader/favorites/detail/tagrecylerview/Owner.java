package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import home.crskdev.biblereader.favorites.detail.Identifiable;

/**
 * Created by criskey on 24/6/2017.
 */

public final class Owner implements Identifiable {

    public final boolean master;

    public final String id;

    public Owner(boolean master, Identifiable generator) {
        this.master = master;
        this.id = generator.getOwnerId();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Owner owner = (Owner) o;

        return id != null ? id.equals(owner.id) : owner.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String getOwnerId() {
        return id;
    }

    @Override
    public Identifiable getSource() {
        throw new UnsupportedOperationException();
    }
}
