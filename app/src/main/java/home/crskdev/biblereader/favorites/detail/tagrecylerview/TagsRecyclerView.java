package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.Args;
import home.crskdev.biblereader.core.mvp.MVPLifecycle;
import home.crskdev.biblereader.core.mvp.MVPLifecycleViewDelegate;
import home.crskdev.biblereader.favorites.detail.Identifiable;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.Pair;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;

/**
 * Created by criskey on 22/6/2017.
 */

public class TagsRecyclerView extends TapableRecyclerView implements MVPLifecycle, ITagsRecyclerView,
        Identifiable {

    static final int LINEAR_HORIZONTAL = 0;
    static final int LINEAR_VERTICAL = 1;
    static final int GRID = 2;
    @Inject
    TagsRecyclerPresenter mPresenter;
    @Inject
    @Args
    Map<String, Object> mArgs;
    //argument values for presenter
    private int mLayoutType;
    private int mActionFlags;
    private boolean mHasDefaultData;
    private boolean mInteractable;
    //############################
    private MVPLifecycleViewDelegate mLifecycle = MVPLifecycleViewDelegate.NO_OP;
    private Identifiable mOwnerIdGenerator = new Generate();

    public TagsRecyclerView(Context context) {
        super(context);
        init(null);
    }

    public TagsRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TagsRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        mLayoutType = LINEAR_HORIZONTAL;
        mInteractable = false;
        Context context = getContext();
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.TagsRecyclerView,
                    0, 0);
            try {
                mLayoutType = a.getInt(R.styleable.TagsRecyclerView_layout_type, LINEAR_HORIZONTAL);
                mHasDefaultData = a.getBoolean(R.styleable.TagsRecyclerView_has_default_data, false);
                mActionFlags = a.getInt(R.styleable.TagsRecyclerView_actions, ACTION_NONE);
                mInteractable = a.getBoolean(R.styleable.TagsRecyclerView_interactable, false);
            } finally {
                a.recycle();
            }
        }
        LayoutManager manager;
        switch (mLayoutType) {
            case LINEAR_VERTICAL:
                manager = new LinearLayoutManager(context, VERTICAL, false);
                break;
            case GRID:
                manager = new StaggeredGridLayoutManager(4, VERTICAL);
                break;
            default:
                manager = new LinearLayoutManager(context, HORIZONTAL, false);
        }
        setLayoutManager(manager);
        setAdapter(new TagsRecyclerAdapter(mLayoutType));
//        if (mLayoutType == LINEAR_VERTICAL)
//            addItemDecoration(new DividerItemDecoration(context, HORIZONTAL));
    }

    @Inject
    public void delegate() {
        mLifecycle = new MVPLifecycleViewDelegate(this, mPresenter);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        super.onMeasure(widthSpec, heightSpec);
        if (mLayoutType == GRID) {
            int width = MeasureSpec.getSize(widthSpec);
            int itemWidth = 150;
            if (width != 0) {
                int spans = width / itemWidth;
                if (spans > 0) {
                    ((StaggeredGridLayoutManager) getLayoutManager()).setSpanCount(spans);
                }
            }
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mLifecycle.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mLifecycle.onDetachedFromWindow();
    }

    @Override
    public void onDisplayTags(List<Tag> tags) {
        getAdapter().changeTags(tags);
    }

    @Override
    public TagsRecyclerAdapter getAdapter() {
        return (TagsRecyclerAdapter) super.getAdapter();
    }

    @Override
    public void onDeleteTag(Tag tag) {
        getAdapter().deleteTag(tag);
    }

    @Override
    public void onAddTag(Tag tag) {
        getAdapter().addTag(tag);
    }

    @Override
    public void onEditTag(Tag tag) {
        getAdapter().editTag(tag);
    }

    @Override
    public Map<String, ?> getArgs() {
        Map<String, Object> updatedArgs = new HashMap<>(mArgs);
        updatedArgs.put(ARG_KEY_ACTION_FLAGS, mActionFlags);
        updatedArgs.put(ARG_KEY_HAS_DEFAULT_DATA, mHasDefaultData);
        updatedArgs.put(ARG_KEY_INTERACTABLE, mInteractable);
        updatedArgs.put(ARG_KEY_OWNERSHIP_MANAGER_ID, getSource());
        return Collections.unmodifiableMap(updatedArgs);
    }

    @Override
    public void onPreAttachPresenter() {
    }

    @Override
    public void onPostAttachPresenter() {
        mPresenter.select(tapObservable().map(ev -> Pair.of(ev, getAdapter().getTag(ev.position))));
    }

    @Override
    public void onPreDetachPresenter() {
    }

    @Override
    public String getOwnerId() {
        return mOwnerIdGenerator.getOwnerId();
    }

    @Override
    public Identifiable getSource() {
        return new Owner(mInteractable, mOwnerIdGenerator);
    }

}
