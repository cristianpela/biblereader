package home.crskdev.biblereader.favorites;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 8/6/2017.
 */

public class FavoritesRecyclerAdapter extends RecyclerView.Adapter<FavoritesRecyclerAdapter.FavoritesViewHolder> {

    private List<ColoredVerset> mContent;

    private DateFormat mDateFormat;

    public FavoritesRecyclerAdapter() {
        mContent = new ArrayList<>();
        mDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss", Locale.getDefault());
    }

    public void addBatchedContent(List<ColoredVerset> batch) {
        if (!batch.isEmpty()) {
            mContent.addAll(batch);
            int start = this.mContent.isEmpty() ? 0 : this.mContent.size();
            notifyItemRangeInserted(start, batch.size());
        }
    }

    public void addContent(List<ColoredVerset> content) {
        mContent.clear();
        mContent.addAll(content);
        notifyDataSetChanged();
    }

    @Override
    public FavoritesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_recy_item, parent, false);
        return new FavoritesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoritesViewHolder holder, int position) {
        ColoredVerset cv = mContent.get(position);
        Verset verset = cv.verset;
        holder.txtContent.setText(verset.content);
        holder.txtSrc.setText(verset.bookId + ":" + verset.chapterId);
        holder.txtDate.setText(mDateFormat.format(verset.date));

        Context ctx = holder.card.getContext();
        holder.card.setCardBackgroundColor(ContextCompat.getColor(ctx, cv.colorResId));
    }

    @Override
    public int getItemCount() {
        return mContent.size();
    }


    public Verset getItem(int index) {
        return mContent.get(index).verset;
    }

    public void remove(int index) {
        mContent.remove(index);
        notifyItemRemoved(index);
    }

    static class FavoritesViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_fav_content)
        TextView txtContent;
        @BindView(R.id.txt_fav_src)
        TextView txtSrc;
        @BindView(R.id.txt_fav_date)
        TextView txtDate;
        @BindView(R.id.card_fav)
        CardView card;

        FavoritesViewHolder(View itemView) {
            super(itemView);
        }

    }
}
