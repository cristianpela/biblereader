package home.crskdev.biblereader.favorites;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.read.ReadState;
import home.crskdev.biblereader.search.SearchState;
import home.crskdev.biblereader.util.Pair;
import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.ObservableTransformer;

/**
 * Created by criskey on 16/7/2017.
 */
@Singleton
public final class FavoritesOpsMediator {

    private final EventBus bus;
    private final StateContainer stateContainer;

    @Inject
    public FavoritesOpsMediator(EventBus bus, StateContainer stateContainer) {
        this.bus = bus;
        this.stateContainer = stateContainer;
    }

    /**
     * Utility method to toggle a favorite verset. In: index verset to toggle - Out: index and toggled verset
     * <br/>
     * Gotcha:<br/>
     * In order to keep the list verset up to date we must defer it - by piggybacking a provider (as lazy value)<br/>
     * Usually these versets are from a state subject, so we need the most recent value of them.
     * <br/>
     * In this context, the usage of provider has nothing to do with dagger.
     *
     * @param svc     favorite service
     * @param versets lazy value
     * @return observable for a pair of index and toggled verset
     */
    public static <C extends Content>
    ObservableTransformer<Integer, Pair<Integer, Verset>> toggle(FavoritesService svc, Provider<List<C>> versets) {
        return up -> up.switchMap(index -> {
            Asserts.basicAssert(versets.get().size() > 0, "Empty content");
            Content content = versets.get().get(index);
            Asserts.basicAssert(content.getClass().equals(Verset.class), "This content must " +
                    " be instance of a Verset");
            return svc
                    .setFavorite(((Verset) content).toggleFavorite())
                    .map(v -> Pair.of(index, v)).toObservable();
        });
    }

    @Inject
    public void postInject() {
        bus.toObservable()
                .filter(ev -> ev.checkGroup(GroupEventID.GROUP_FAVORITE)
                        && ev.checkId(EventID.FAVORITE_CHANGED))
                .subscribe(ev -> {
                    Verset verset = (Verset) ev.data;
                    updateFavoriteState();
                    updateReadState(verset);
                    updateSearchState(verset);
                });
    }

    private void updateSearchState(Verset verset) {
        SearchState ss = stateContainer.subjectSearchState.getValue();
        List<Verset> foundVersets = ss.foundVersets;
        int ssIndex = Content.Util.indexOf(foundVersets, verset);
        if (ssIndex != -1) {
            foundVersets.set(ssIndex, verset);
            stateContainer.subjectSearchState.onNext(ss.foundVersets(foundVersets));
        }
    }

    private void updateReadState(Verset verset) {
        ReadState rs = stateContainer.subjectReadState.getValue();
        List<Content> content = rs.content;
        int rsIndex = Content.Util.indexOf(content, verset);
        if (rsIndex != -1) {
            content.set(rsIndex, verset);
            stateContainer.subjectReadState.onNext(rs.content(content));
        }
    }

    private void updateFavoriteState() {
        //refresh state;
        FavoritesState fs = stateContainer.subjectFavoriteState.getValue();
        stateContainer.subjectFavoriteState.onNext(fs);
    }
}
