package home.crskdev.biblereader.favorites.detail;

import android.widget.EditText;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjection;
import home.crskdev.biblereader.core.di.Arguments;
import home.crskdev.biblereader.core.mvp.BaseDialogFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;

/**
 * Created by criskey on 27/6/2017.
 */
public class EditTagDialog extends BaseDialogFragment implements EditTagView {

    @Inject
    EditTagPresenter mPresenter;

    @BindView(R.id.recy_edit_tag)
    TagsRecyclerView mRecyclerTagEdit;

    @BindView(R.id.recy_history_tag)
    TagsRecyclerView mRecyclerTagHistory;

    @BindView(R.id.edit_tag)
    EditText mEditTag;

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.tag_edit_dialog_layout;
    }

    @Override
    public void onPostAttachPresenter() {
        AndroidArgumentsInjection.inject(this,
                new Arguments<>(mRecyclerTagEdit, (Map<String, Object>) getArgs()),
                Arguments.noArgs(mRecyclerTagHistory)
        );
    }

    @Override
    public void onEdit(CharSequence tagName) {
        mEditTag.setText(tagName);
        mEditTag.selectAll();
    }

    @Override
    public void onClearEdit() {
        mEditTag.setText("");
    }

    @Override
    public void close() {
        dismiss();
    }

    @OnClick(R.id.btn_undo_tag)
    public void actionUndo() {
        mPresenter.undo();
    }

    @OnClick(R.id.btn_commit_tag)
    public void actionCommit() {
        mPresenter.commit();
    }

    @OnClick(R.id.btn_add_to_history_tag)
    public void actionEdit() {
        mPresenter.addToHistory(mEditTag.getText().toString());
    }

    @Override
    public Map<String, ?> getArgs() {
        Map<String, Object> args = new HashMap<>(super.getArgs());
        args.put(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID,
                Collections.unmodifiableList(Arrays.asList(
                        mRecyclerTagEdit.getSource(),
                        mRecyclerTagHistory.getSource()
                )));
        return args;
    }
}
