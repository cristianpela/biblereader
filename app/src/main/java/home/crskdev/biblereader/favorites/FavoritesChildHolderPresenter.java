package home.crskdev.biblereader.favorites;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailFragmentView;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailView;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;

/**
 * Created by criskey on 17/6/2017.
 */
@PerFragment
public class FavoritesChildHolderPresenter extends Presenter<FavoritesChildHolderView> {

    static final String FAVORITES_VIEW_FRAGMENT_TAG = "FavoritesViewFragment";
    static final String FAVORITES_DETAIL_VIEW_FRAGMENT_TAG = "FavoritesDetailViewFragment";

    @Inject
    public FavoritesChildHolderPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler) {
        super(bus, stateContainer, uiScheduler);
    }

    @Override
    protected void postAttach() {
        view.addOrReplaceView(FAVORITES_VIEW_FRAGMENT_TAG, FavoritesViewFragment.class,
                Collections.emptyMap(), false);
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        if (event.checkGroup(GroupEventID.GROUP_FAVORITE)) {
            if (event.checkId(EventID.FAVORITE_SELECT)) {
                Map<String, Object> args = new HashMap<>();
                args.put(FavoriteDetailView.ARG_KEY_VERSET, event.data);
                view.addOrReplaceView(FAVORITES_DETAIL_VIEW_FRAGMENT_TAG, FavoriteDetailFragmentView.class, args, true);

            }else if(event.checkId(EventID.FAVORITE_BACK_TO)){
                view.backTo();
            }
        }
    }
}
