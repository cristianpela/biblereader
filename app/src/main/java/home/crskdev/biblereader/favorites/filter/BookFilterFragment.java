package home.crskdev.biblereader.favorites.filter;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxAutoCompleteTextView;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;

/**
 * Created by criskey on 29/6/2017.
 */

public class BookFilterFragment extends BaseFragment implements BookFilterView {

    @Inject
    BookFilterPresenter presenter;

    @BindView(R.id.recy_books_w_fav)
    RecyclerView recyclerView;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.fav_filter_book_layout;
    }

    @Override
    public void onPreAttachPresenter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new BookTitlesWithFavoritesAdapter());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

    }

    @Override
    public void onPostAttachPresenter() {
        BookTitlesWithFavoritesAdapter adapter = (BookTitlesWithFavoritesAdapter) recyclerView.getAdapter();
        presenter.filter(adapter.clickObservable());
    }

    @Override
    public void onLoadBooks(BookTitleWithFavorites[] titles) {
        BookTitlesWithFavoritesAdapter adapter = (BookTitlesWithFavoritesAdapter) recyclerView.getAdapter();
        adapter.setContent(titles);
    }
}
