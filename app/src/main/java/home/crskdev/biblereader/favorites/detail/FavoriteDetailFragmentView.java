package home.crskdev.biblereader.favorites.detail;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjection;
import home.crskdev.biblereader.core.di.Arguments;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesAlerts;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 17/6/2017.
 */
public class FavoriteDetailFragmentView extends BaseFragment implements FavoriteDetailView {

    @Inject
    FavoriteDetailPresenter mPresenter;

    @BindView(R.id.txt_fav_detail_content)
    TextView mTxtDetail;

    @BindView(R.id.txt_fav_detail_title)
    TextView mTxtTitle;

    @BindView(R.id.toolbar_fav_tag_detail)
    Toolbar mToolbar;

    @BindView(R.id.recy_fav_detail_verset_tags)
    TagsRecyclerView mRecyclerVersetTags;

    @BindView(R.id.recy_fav_detail_all_tags)
    TagsRecyclerView mRecyclerAllTags;

    @Override
    public void onPreAttachPresenter() {
        super.onPreAttachPresenter();
        AndroidArgumentsInjection
                .inject(this,
                        new Arguments(mToolbar, getArgs()),
                        new Arguments(mRecyclerVersetTags, getArgs()),
                        Arguments.noArgs(mRecyclerAllTags)
                );
    }

    @Override
    public void onPostAttachPresenter() {
        mTxtDetail.setOnLongClickListener(v -> {
            mPresenter.showReadTab();
            return false;
        });
    }

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.fav_detail_layout;
    }

    @Override
    public void displayVerset(String title, CharSequence content) {
        mTxtDetail.setText(content);
        mTxtTitle.setText(title);
    }

    @Override
    public void onConfirmVersetToShowInReadTab(Verset verset) {
        FavoritesAlerts.favoriteConfirmShow(getContext(), verset,
                (d, w) -> mPresenter.confirmVersetToShowInReadTab()).create().show();
    }


    @Override
    public Map<String, ?> getArgs() {
        Map<String, Object> args = new HashMap<>(super.getArgs());
        args.put(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID,
                Collections.unmodifiableList(Arrays.asList(
                        mRecyclerAllTags.getSource(),
                        mRecyclerVersetTags.getSource()
                )));
        return args;
    }

}
