package home.crskdev.biblereader.favorites;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import home.crskdev.biblereader.core.state.ResetableState;
import home.crskdev.biblereader.favorites.detail.Tag;

import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_ALL_BOOKS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_TAG;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_ALL_BOOKS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_TAG;

/**
 * Created by criskey on 6/6/2017.
 */

public class FavoritesState implements ResetableState {

    // @formatter:off
    public static final String ALL_BOOKS = "";
    public static final Tag ALL_TAGS = Tag.NO_TAG;
    public static final long INIT_DATE = Long.MAX_VALUE;
    public static int FILTER_MODE_BOOK = 0;
    public static final FavoritesState DEFAULT_ALL_BOOKS = new FavoritesState(FILTER_MODE_BOOK,
            ALL_BOOKS, null, INIT_DATE, false);
    public static final FavoritesState DEFAULT = DEFAULT_ALL_BOOKS;
    public static int FILTER_MODE_TAG = 1;
    public static final FavoritesState DEFAULT_ALL_TAGS = new FavoritesState(FILTER_MODE_TAG,
            null, ALL_TAGS, INIT_DATE, false);

    public final int filterMode;
    public final String filterBookId;
    public final Tag filterTag;
    public final long lastDate;
    public final boolean batchMode;
    // @formatter:on

    public FavoritesState(int filterMode, String filterBookId, Tag filterTag, long lastDate, boolean batchMode) {
        this.filterMode = filterMode;
        this.filterBookId = filterBookId;
        this.filterTag = filterTag;
        this.lastDate = lastDate;
        this.batchMode = batchMode;
    }

    public FavoritesState bookId(String filterBookId) {
        return new FavoritesState(filterMode, filterBookId, null, INIT_DATE, false);
    }

    public FavoritesState lastDate(long lastDate) {
        return new FavoritesState(filterMode, filterBookId, filterTag, lastDate, batchMode);
    }

    public FavoritesState batchMode(boolean batchMode) {
        return new FavoritesState(filterMode, filterBookId, filterTag, lastDate, batchMode);
    }

    public FavoritesState tag(Tag tag) {
        return new FavoritesState(filterMode, null, tag, INIT_DATE, false);
    }

    public FavoritesState filterMode(int mode) {
        FavoritesState fs = new FavoritesState(mode, ALL_BOOKS, null, INIT_DATE, false);
        if (mode == FILTER_MODE_TAG) {
            fs = new FavoritesState(mode, null, ALL_TAGS, INIT_DATE, false);
        }
        return fs;
    }

    public FavoritesState nextPage(long date) {
        return lastDate(date).batchMode(true);
    }

    public FavoritesState setRestorePoint(long date) {
        return lastDate(date).batchMode(false);
    }

    public FavoritesState changeTag(Tag tag) {
        return tag(tag).batchMode(false);
    }

    public FavoritesState changeBook(String bookId) {
        return bookId(bookId).batchMode(false);
    }

    @Override
    public FavoritesState reset() {
        return DEFAULT;
    }

    @Case
    public int getStateCase() {
        if (batchMode || lastDate == INIT_DATE) {
            if (filterMode == FILTER_MODE_BOOK) {
                if (filterBookId != null) {
                    if (filterBookId.equals(ALL_BOOKS)) {
                        return BATCH_FILTER_ALL_BOOKS;
                    } else {
                        return BATCH_FILTER_BOOK;
                    }
                } else {
                    throw new IllegalStateException("Filter Book Mode: filterBookId is NULL");
                }
            } else {
                if (filterTag != null) {
                    if (filterTag.equals(ALL_TAGS)) {
                        return BATCH_FILTER_ALL_TAGS;
                    } else {
                        return BATCH_FILTER_TAG;
                    }
                } else {
                    throw new IllegalStateException("Filter Tag Mode: filterTag is NULL");
                }
            }
        } else {
            if (filterMode == FILTER_MODE_BOOK) {
                if (filterBookId != null) {
                    if (filterBookId.equals(ALL_BOOKS)) {
                        return RESUME_FILTER_ALL_BOOKS;
                    } else {
                        return RESUME_FILTER_BOOK;
                    }
                } else {
                    throw new IllegalStateException("Filter Book Mode: filterBookId is NULL");
                }
            } else {
                if (filterTag != null) {
                    if (filterTag.equals(ALL_TAGS)) {
                        return RESUME_FILTER_ALL_TAGS;
                    } else {
                        return RESUME_FILTER_TAG;
                    }
                } else {
                    throw new IllegalStateException("Filter Tag Mode: filterTag is NULL");
                }
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FavoritesState that = (FavoritesState) o;

        if (filterMode != that.filterMode) return false;
        if (lastDate != that.lastDate) return false;
        if (batchMode != that.batchMode) return false;
        if (filterBookId != null ? !filterBookId.equals(that.filterBookId) : that.filterBookId != null)
            return false;
        return filterTag != null ? filterTag.equals(that.filterTag) : that.filterTag == null;

    }

    @Override
    public int hashCode() {
        int result = filterBookId != null ? filterBookId.hashCode() : 0;
        result = 31 * result + (filterTag != null ? filterTag.hashCode() : 0);
        result = 31 * result + (int) (lastDate ^ (lastDate >>> 32));
        result = 31 * result + (batchMode ? 1 : 0);
        return result;
    }

    public boolean isFresh() {
        return (!batchMode && lastDate == INIT_DATE) || !batchMode;
    }

    @Override
    public String toString() {
        return "FavoritesState{" +
                "filterMode=" + filterMode +
                ", filterBookId='" + filterBookId + '\'' +
                ", filterTag=" + filterTag +
                ", lastDate=" + lastDate +
                ", batchMode=" + batchMode +
                '}';
    }

    @IntDef(value = {
            BATCH_FILTER_ALL_BOOKS,
            BATCH_FILTER_BOOK,
            RESUME_FILTER_ALL_BOOKS,
            RESUME_FILTER_BOOK,

            BATCH_FILTER_ALL_TAGS,
            BATCH_FILTER_TAG,
            RESUME_FILTER_TAG,
            RESUME_FILTER_ALL_TAGS
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface Case {
        /**
         * means that date>=0, batchMode=true, tag = null, bookId = ALL_BOOKS
         */
        int BATCH_FILTER_ALL_BOOKS = 1;

        /**
         * means that date>=0, batchMode=true, tag = null, bookId =valid
         */
        int BATCH_FILTER_BOOK = 2;

        /**
         * means that date>0, batchMode=false, tag = null, bookId =ALL_BOOKS
         */
        int RESUME_FILTER_ALL_BOOKS = 3;
        /**
         * means that date> 0, batchMode=false, tag = null, bookId =valid
         */
        int RESUME_FILTER_BOOK = 4;
        /**
         * means that date>=0, batchMode=true, tag != null, bookId =null
         */
        int BATCH_FILTER_TAG = 5;
        /**
         * means that date> 0, batchMode=true, tag != null, bookId =null
         */
        int RESUME_FILTER_TAG = 6;

        /**
         * means that date>=0, batchMode=true, tag = ALL_TAGS, bookId = null
         */
        int BATCH_FILTER_ALL_TAGS = 7;

        int RESUME_FILTER_ALL_TAGS = 8;

    }
}
