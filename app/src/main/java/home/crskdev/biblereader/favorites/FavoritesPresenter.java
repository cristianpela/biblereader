package home.crskdev.biblereader.favorites;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.TabEventDispatcher;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.bus.WaitEventDispatcher;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.RxUtils;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_ALL_BOOKS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.BATCH_FILTER_TAG;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_ALL_BOOKS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_ALL_TAGS;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_BOOK;
import static home.crskdev.biblereader.favorites.FavoritesState.Case.RESUME_FILTER_TAG;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.DOUBLE_TAP;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.LONG_TAP;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.MOVE;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.SINGLE_TAP;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.SWIPE_LEFT;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.SWIPE_RIGHT;

/**
 * Created by criskey on 4/6/2017.
 */
@PerFragment
public class FavoritesPresenter extends Presenter<FavoritesView> {

    static final int SIZE = 5;
    private static final String TAG = FavoritesPresenter.class.getSimpleName();
    private final FavoritesService svcFav;
    private final WaitEventDispatcher waitEventDispatcher;
    private final TabEventDispatcher tabEventDispatcher;
    private BehaviorSubject<FavoritesState> state;
    private long lastDate;
    private FavoriteColorPalette palette;

    @Inject
    public FavoritesPresenter(EventBus bus, StateContainer stateContainer,
                              Scheduler uiScheduler, FavoritesService svcFav,
                              FavoriteColorPalette palette,
                              WaitEventDispatcher waitEventDispatcher,
                              TabEventDispatcher tabEventDispatcher) {
        super(bus, stateContainer, uiScheduler);
        this.svcFav = svcFav;
        state = stateContainer.subjectFavoriteState;
        this.palette = palette;
        this.waitEventDispatcher = waitEventDispatcher;
        this.tabEventDispatcher = tabEventDispatcher;
    }

    @Override
    protected void postAttach() {
        Disposable disposable = state
                .compose(waitEventDispatcher.dispatchSubject(TAG, s -> caseOf(s).toObservable()))
                .subscribe(data -> {
                    FavoritesState currentState = state.getValue();
                    if (data.size() > 0) {
                        lastDate = data.get(data.size() - 1).date;
                    }
                    if (currentState.isFresh()) {
                        view.display(toColored(data));
                    } else {
                        view.displayBatch(toColored(data));
                    }
                });
        disposables.add(disposable);
    }

    @VisibleForTesting
    protected List<ColoredVerset> toColored(List<Verset> data) {
        List<ColoredVerset> coloredVersets = new ArrayList<>(data.size());
        for (Verset v : data) {
            coloredVersets.add(new ColoredVerset(palette.nextColor(v.bookId), v));
        }
        return coloredVersets;
    }

    @Override
    protected void postDetach() {
        state.onNext(state.getValue().setRestorePoint(lastDate));
    }

    private void removeFavorite(int index, Verset verset) {
        view.onConfirmRemoveFavorite(index, verset);
    }

    public void confirmRemoveFavorite(int index, Verset verset) {
        disposables.add(svcFav.setFavorite(verset.toggleFavorite())
                .compose(RxUtils.scheduleSingle(uiScheduler))
                .subscribe((v) -> {
                    view.onRemovedFromPersistance(index);
                    bus.dispatch(new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_CHANGED, v));
                }));
    }

    void nextPage() {
        state.onNext(state.getValue().nextPage(lastDate));
    }

    private void showSelected(Verset verset) {
        bus.dispatch(new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_SELECT, verset));
    }

    private void showReadTab(Verset verset) {
        tabEventDispatcher.openReadTab(verset);
    }

    private Single<List<Verset>> caseOf(FavoritesState state) {
        return Single.defer(() -> {
            int caseOf = state.getStateCase();
            switch (caseOf) {
                case BATCH_FILTER_ALL_BOOKS:
                    return svcFav.getAllFavorites(SIZE, state.lastDate);
                case RESUME_FILTER_ALL_BOOKS:
                    return svcFav.getAllFavoritesUntil(state.lastDate);
                case BATCH_FILTER_BOOK:
                    return svcFav.getFavorites(SIZE, state.lastDate, state.filterBookId);
                case RESUME_FILTER_BOOK:
                    return svcFav.getFavoritesUntil(state.lastDate, state.filterBookId);
                case BATCH_FILTER_TAG:
                    return svcFav.getFavoritesWithTag(state.filterTag, SIZE, state.lastDate);
                case RESUME_FILTER_TAG:
                    return svcFav.getFavoritesWithTagUntil(state.filterTag, SIZE, state.lastDate);
                case BATCH_FILTER_ALL_TAGS:
                    return svcFav.getAllFavoritesWithTag(SIZE, state.lastDate);
                case RESUME_FILTER_ALL_TAGS:
                    return svcFav.getAllFavoritesWithTagUntil(SIZE, state.lastDate);
                default:
                    throw new IllegalStateException("Bad Favorites ResetableState");
            }
        }).observeOn(uiScheduler);
    }

    public void processTap(Observable<TapableRecyclerView.TapEvent> tapEventObservable) {
        disposables.add(tapEventObservable
                .subscribe((ev) -> {
                    switch (ev.kind) {
                        case DOUBLE_TAP:
                            break;
                        case LONG_TAP:
                            view.onConfirmVersetToShowInReadTab((Verset) ev.extra);
                            break;
                        case MOVE:
                            break;
                        case SINGLE_TAP:
                            showSelected((Verset) ev.extra);
                            break;
                        case SWIPE_LEFT:
                            break;
                        case SWIPE_RIGHT:
                            removeFavorite(ev.position, (Verset) ev.extra);
                            break;
                    }
                }));
    }

    public void confirmVersetToShowInReadTab(Verset verset) {
        showReadTab(verset);
    }
}
