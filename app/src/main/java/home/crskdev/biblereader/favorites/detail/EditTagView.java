package home.crskdev.biblereader.favorites.detail;

/**
 * Created by criskey on 27/6/2017.
 */

public interface EditTagView extends CloseableView {

    void onEdit(CharSequence tagName);

    void onClearEdit();
}
