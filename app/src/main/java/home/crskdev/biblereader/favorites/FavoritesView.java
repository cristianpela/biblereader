package home.crskdev.biblereader.favorites;

import java.util.List;

import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 4/6/2017.
 */
public interface FavoritesView extends IView {

    void display(List<ColoredVerset> contents);

    void displayBatch(List<ColoredVerset> contents);

    void onRemovedFromPersistance(int index);

    void onConfirmRemoveFavorite(int index, Verset verset);

    void onConfirmVersetToShowInReadTab(Verset verset);
}
