package home.crskdev.biblereader.favorites.filter;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesState;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import io.reactivex.Scheduler;

/**
 * Created by criskey on 29/6/2017.
 */
@PerFragment
public class TagFilterPresenter extends Presenter<TagFilterView> {

    private String tagRecyclerOwnerId;

    @Inject
    public TagFilterPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler) {
        super(bus, stateContainer, uiScheduler);
    }

    @Override
    protected void postAttach() {
        tagRecyclerOwnerId = (String) view.getArgs().get(ITagsRecyclerView
                .ARG_KEY_OWNERSHIP_MANAGER_ID);
    }

    @Override
    public void accept(Event event) {
        if (event.check(GroupEventID.GROUP_TAG, EventID.TAG_SELECT)) {
            TagWrapper tw = (TagWrapper) event.data;
            if (tw.owner.equals(tagRecyclerOwnerId)) {
                //update state
                FavoritesState state = stateContainer.subjectFavoriteState.getValue();
                Tag tag = tw.tag;
                stateContainer.subjectFavoriteState.onNext(state.tag(tag));
                //instruct the favorites screen holder to go back to
                //favorites screen if we are in a detail fragment screen
                // and then collapse the filter bottom sheet
                bus.dispatch(Event.flag(GroupEventID.GROUP_FAVORITE,
                        EventID.FAVORITE_BACK_TO));
                bus.dispatch(Event.flag(GroupEventID.GROUP_FAVORITE,
                        EventID.FAVORITE_COLLAPSE_FILTER_SHEET));
            }


        }
    }
}
