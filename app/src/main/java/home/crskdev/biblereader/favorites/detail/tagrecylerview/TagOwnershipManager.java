package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.core.bus.EventID;

/**
 * Created by criskey on 24/6/2017.
 */
public class TagOwnershipManager {

    private List<Owner> owners;

    @Inject
    public TagOwnershipManager() {
        owners = new ArrayList<>();
    }

    public void addOwner(Owner owner) {
        owners.add(owner);
    }

    public void addOwners(List<Owner> owners) {
        this.owners.addAll(owners);
    }

    /**
     * sends add to all owners which are not master
     *
     * @param tw
     * @param isMasterRelevant
     * @return mutableMap with de tag event id and tagwrapper;
     */
    public Map<Integer, List<TagWrapper>> decideAddTag(TagWrapper tw, boolean isMasterRelevant) {
        Map<Integer, List<TagWrapper>> orders = new HashMap<>();
        List<TagWrapper> tws = new ArrayList<>();
        orders.put(EventID.TAG_ADD, tws);
        for (Owner o : owners) {
            TagWrapper newTw = new TagWrapper(o.id, tw.tag, tw.isAnon);
            if (isMasterRelevant) {
                if (!o.master) {
                    tws.add(newTw);
                }
            } else {
                tws.add(newTw);
            }
        }
        return orders;
    }

    /**
     * sends add to non-master and remove from master
     *
     * @param tw
     * @return mutableMap with de tag event id and tagwrapper;
     */
    public Map<Integer, List<TagWrapper>> decideDeleteTag(TagWrapper tw) {
        return decide(tw, EventID.TAG_DELETE, EventID.TAG_ADD);
    }

    /**
     * send undo actions for master in case of delete or id
     *
     * @param tw     tag wrapper
     * @param caseOf must be {@link EventID#TAG_DELETE} or {@link EventID#TAG_EDIT}
     * @return mutableMap with de tag event id and tagwrapper;
     */
    public Map<Integer, List<TagWrapper>> decideUndoTag(TagWrapper tw, @EventID int caseOf) {
        int masterAction;
        int slaveAction = EventID.TAG_DELETE;
        switch (caseOf) {
            case EventID.TAG_DELETE:
                masterAction = EventID.TAG_ADD;
                break;
            case EventID.TAG_EDIT:
                masterAction = EventID.TAG_EDIT;
                EditedTag edited = (EditedTag) tw.tag;
                tw = new TagWrapper(tw.owner, edited.undo(), tw.isAnon);
                break;
            default:
                throw new IllegalArgumentException("Action to undo must be a DELETE or EDIT");
        }
        return decide(tw, masterAction, slaveAction);
    }

    public Map<Integer, List<TagWrapper>> decideEditTag(TagWrapper tw) {
        if (!tw.tag.getClass().equals(EditedTag.class)) {
            throw new IllegalArgumentException("TagWrapper must wrap an EditedTag");
        }

        EditedTag editedTag = (EditedTag) tw.tag;

        @SuppressLint("UseSparseArrays")
        Map<Integer, List<TagWrapper>> orders = new HashMap<>();
        List<TagWrapper> twsSlave = new ArrayList<>();
        orders.put(EventID.TAG_ADD, twsSlave);
        List<TagWrapper> twsMaster = new ArrayList<>();
        orders.put(EventID.TAG_EDIT, twsMaster);
        for (Owner o : owners) {
            if (o.master) {
                twsMaster.add(new TagWrapper(o.id, editedTag.commit(), tw.isAnon));
            } else {
                twsSlave.add(new TagWrapper(o.id, editedTag, tw.isAnon));
            }
        }
        return orders;
    }

    @NonNull
    private Map<Integer, List<TagWrapper>> decide(TagWrapper tw, int masterAction, int slaveAction) {
        @SuppressLint("UseSparseArrays")
        Map<Integer, List<TagWrapper>> orders = new HashMap<>();
        List<TagWrapper> twsUndo = new ArrayList<>();
        orders.put(masterAction, twsUndo);
        List<TagWrapper> twsDel = new ArrayList<>();
        orders.put(slaveAction, twsDel);
        for (Owner o : owners) {
            TagWrapper newTw = new TagWrapper(o.id, tw.tag, tw.isAnon);
            if (o.master) {
                twsUndo.add(newTw);
            } else {
                twsDel.add(newTw);
            }
        }
        return orders;
    }


    public boolean contains(String ownerId) {
        for (Owner o : owners) {
            if (o.id.equals(ownerId)) {
                return true;
            }
        }
        return false;
    }
}
