package home.crskdev.biblereader.favorites.detail;

import android.content.Context;
import android.support.v4.view.ActionProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.jakewharton.rxbinding2.view.RxView;

import butterknife.BindView;
import butterknife.ButterKnife;
import home.crskdev.biblereader.R;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 21/6/2017.
 */

public class AddTagActionProvider extends ActionProvider {

    @BindView(R.id.edit_add_tag_action_provider)
    EditText editAddTag;

    @BindView(R.id.btn_add_tag_action_provider)
    ImageButton btnAddTag;

    private Observable<String> addTagObservable;

    public AddTagActionProvider(Context context) {
        super(context);
        addTagObservable = Observable.empty();
    }

    @Override
    public View onCreateActionView() {
        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.fav_detail_add_action_provider_layout, null);
        ButterKnife.bind(this, view);
        addTagObservable = RxView.clicks(btnAddTag).map(__ -> editAddTag.getText()
                .toString().trim().toLowerCase());
        return view;
    }

    public Observable<String> observable() {
        return addTagObservable;
    }

}
