package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.FavoritesChildFragmentHolder;

/**
 * Created by criskey on 17/6/2017.
 */
@PerFragment
@Subcomponent
public interface FavoritesChildHolderComponent extends AndroidInjector<FavoritesChildFragmentHolder> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FavoritesChildFragmentHolder> {
    }
}
