package home.crskdev.biblereader.favorites.detail;

import android.support.annotation.CallSuper;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.Owner;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagOwnershipManager;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import home.crskdev.biblereader.util.RxUtils;
import io.reactivex.Completable;
import io.reactivex.Scheduler;

/**
 * Created by criskey on 26/6/2017.
 */

public abstract class ActionTagPresenter<V extends CloseableView> extends Presenter<V> {

    protected FavoritesService favoritesService;

    protected TagOwnershipManager tagOwnershipManager;

    protected Deque<TagWrapper> history;

    protected Verset verset;

    public ActionTagPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler,
                              FavoritesService favoritesService,
                              TagOwnershipManager tagOwnershipManager) {
        super(bus, stateContainer, uiScheduler);
        this.favoritesService = favoritesService;
        this.tagOwnershipManager = tagOwnershipManager;
        this.history = new ArrayDeque<>();
    }

    @Override
    @CallSuper
    protected void postAttach() {
        Map<String, ?> args = view.getArgs();
        verset = (Verset) args.get(FavoriteDetailView.ARG_KEY_VERSET);
        tagOwnershipManager.addOwners((List<Owner>) args
                .get(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID));
    }

    protected void dispatchOrders(Map<Integer, List<TagWrapper>> orders) {
        for (Integer key : orders.keySet()) {
            List<TagWrapper> tws = orders.get(key);
            for (TagWrapper tw : tws) {
                bus.dispatch(new Event<>(GroupEventID.GROUP_TAG, key, tw));
            }
        }
    }

    public final void undo() {
        if (!history.isEmpty()) {
            TagWrapper tw = history.pop();
            dispatchOrders(tagOwnershipManager.decideUndoTag(tw, actionWhenUndo()));
            onUndo(tw);
        }
    }

    protected void onUndo(TagWrapper tw) {

    }

    public final void commit() {
        if (!history.isEmpty()) {
            disposables.add(commitCompletable()
                    .compose(RxUtils.scheduleCompletable(uiScheduler))
                    .subscribe(() -> {
                        view.close();
                        //notify all interested parties that new tag db was changed
                        // -> they should reload their tag lists
                        bus.dispatch(Event.flag(GroupEventID.GROUP_TAG, EventID.TAG_RELOAD));
                    }, (e) -> bus.dispatch(new Event<>(EventID.ERROR, e))));
        } else {
            view.close();
        }
    }

    Collection<TagWrapper> getHistory() {
        return Collections.unmodifiableCollection(history);
    }

    abstract int actionWhenUndo();

    abstract Completable commitCompletable();

}
