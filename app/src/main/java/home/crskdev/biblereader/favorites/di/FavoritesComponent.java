package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.FavoritesViewFragment;

/**
 * Created by criskey on 4/6/2017.
 */
@PerFragment
@Subcomponent
public interface FavoritesComponent extends AndroidInjector<FavoritesViewFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FavoritesViewFragment> {
    }
}
