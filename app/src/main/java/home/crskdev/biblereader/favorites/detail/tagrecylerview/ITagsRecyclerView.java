package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import java.util.List;

import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.favorites.detail.Tag;

/**
 * Created by criskey on 22/6/2017.
 */
public interface ITagsRecyclerView extends IView {

    String ARG_KEY_ACTION_FLAGS = "ARG_KEY_ACTION_FLAGS";
    String ARG_KEY_HAS_DEFAULT_DATA = "ARG_KEY_HAS_DEFAULT_DATA";
    String ARG_KEY_INTERACTABLE = "ARG_KEY_INTERACTABLE";
    String ARG_KEY_OWNERSHIP_MANAGER_ID = "ARG_KEY_OWNERSHIP_MANAGER_ID" ;

    int ACTION_NONE = 16;
    int ACTION_ADD = 1;
    int ACTION_DELETE = 2;
    int ACTION_EDIT = 4;
    int ACTION_UNDO = 8;
    int ACTION_ALL = 15;

    void onDisplayTags(List<Tag> tags);

    void onDeleteTag(Tag tag);

    void onAddTag(Tag tag);

    void onEditTag(Tag tag);
}
