package home.crskdev.biblereader.favorites;

import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.recyclerview.TapableExtraRecyclerView;

/**
 * Created by criskey on 12/5/2017.
 */
public class FavoritesViewFragment extends BaseFragment implements FavoritesView {

    @Inject
    FavoritesPresenter mPresenter;

    @BindView(R.id.recy_fav)
    TapableExtraRecyclerView mRecyclerView;

    @BindView(R.id.swipe_layout_fav)
    SwipeRefreshLayout mSwipeRefreshLayout;

    FavoritesRecyclerAdapter mAdapter;

    @Override
    public void onPreAttachPresenter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new FavoritesRecyclerAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setProgressBackgroundColorSchemeColor(
                ContextCompat.getColor(getContext(), R.color.colorAccent));
    }

    @Override
    public void onPostAttachPresenter() {
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mPresenter.nextPage();
        });
        mPresenter.processTap(mRecyclerView.tapObservable().map(ev -> ev
                .addExtra(mAdapter.getItem(ev.position))));
    }

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.fav_layout;
    }

    @Override
    public void display(List<ColoredVerset> contents) {
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.addContent(contents);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void displayBatch(List<ColoredVerset> contents) {
        mSwipeRefreshLayout.setRefreshing(false);
        mAdapter.addBatchedContent(contents);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void onRemovedFromPersistance(int index) {
        mAdapter.remove(index);
    }

    @Override
    public void onConfirmRemoveFavorite(int index, Verset verset) {
        FavoritesAlerts.favoriteConfirmDialogBuilder(getContext(), verset,
                (dialog, which) -> mPresenter.confirmRemoveFavorite(index, verset),
                (dialog, which) -> mAdapter.notifyItemChanged(index))
                .create().show();
    }

    @Override
    public void onConfirmVersetToShowInReadTab(Verset verset) {
        FavoritesAlerts.favoriteConfirmShow(getContext(), verset,
                (dialog, which) -> mPresenter.confirmVersetToShowInReadTab(verset))
                .create().show();
    }

}
