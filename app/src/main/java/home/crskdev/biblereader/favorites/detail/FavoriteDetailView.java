package home.crskdev.biblereader.favorites.detail;

import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 17/6/2017.
 */

public interface FavoriteDetailView extends IView {

    String ARG_KEY_VERSET = "KEY_VERSET";

    void displayVerset(String title, CharSequence content);

    void onConfirmVersetToShowInReadTab(Verset verset);
}
