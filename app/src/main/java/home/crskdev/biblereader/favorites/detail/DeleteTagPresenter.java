package home.crskdev.biblereader.favorites.detail;

import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagOwnershipManager;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import home.crskdev.biblereader.util.ImmLists;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;

/**
 * Created by criskey on 25/6/2017.
 */
@PerFragment
public class DeleteTagPresenter extends ActionTagPresenter<CloseableView> {

    @Inject
    public DeleteTagPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler,
                              TagOwnershipManager tagOwnershipManager, FavoritesService favoritesService) {
        super(bus, stateContainer, uiScheduler, favoritesService, tagOwnershipManager);
    }

    @Override
    int actionWhenUndo() {
        return EventID.TAG_DELETE;
    }

    @Override
    Completable commitCompletable() {
        List<Tag> tags = ImmLists.map(history, tw -> tw.tag);
        Completable removeVersetTags = favoritesService.removeTags(tags, verset);
        Completable removeTags = favoritesService.removeTags(tags);
        return (verset == null) ? removeTags: removeVersetTags;
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        if (event.checkGroup(GroupEventID.GROUP_TAG)) {
            if (event.checkId(EventID.TAG_SELECT)) {
                TagWrapper tw = (TagWrapper) event.data;
                if (tagOwnershipManager.contains(tw.owner)) {
                    history.push(tw);
                    dispatchOrders(tagOwnershipManager.decideDeleteTag(tw));
                }
            }
        }
    }
}
