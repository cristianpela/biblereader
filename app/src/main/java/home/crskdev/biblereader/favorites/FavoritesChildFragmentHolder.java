package home.crskdev.biblereader.favorites;

import javax.inject.Inject;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.ChildFragmentHolder;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;

/**
 * Created by criskey on 17/6/2017.
 */
public class FavoritesChildFragmentHolder extends ChildFragmentHolder implements FavoritesChildHolderView {

    @Inject
    FavoritesChildHolderPresenter presenter;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.child_fragment_holder_layout;
    }

    @Override
    public int childContainerId() {
        return R.id.child_fragment_holder;
    }
}
