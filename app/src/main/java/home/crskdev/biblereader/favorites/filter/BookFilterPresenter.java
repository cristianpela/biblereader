package home.crskdev.biblereader.favorites.filter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.biblereader.core.BibleInfo;
import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoriteColorPalette;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.FavoritesState;
import home.crskdev.biblereader.util.Pair;
import home.crskdev.biblereader.util.RxUtils;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;

/**
 * Created by criskey on 29/6/2017.
 */
@PerFragment
public class BookFilterPresenter extends Presenter<BookFilterView> {

    private final FavoriteColorPalette pallete;
    private final BibleInfoLoader loader;
    private final FavoritesService favoritesService;

    @Inject
    public BookFilterPresenter(EventBus bus, StateContainer stateContainer,
                               Scheduler uiScheduler, FavoriteColorPalette pallete,
                               BibleInfoLoader loader, FavoritesService favoritesService) {
        super(bus, stateContainer, uiScheduler);
        this.pallete = pallete;
        this.loader = loader;
        this.favoritesService = favoritesService;
    }

    @Override
    protected void postAttach() {
        //load from db
        Single<BookTitleWithFavorites[]> titleSingle = loader.load().flatMap(bi -> favoritesService
                .getBookIdsWithFavorites()
                .map(ids -> extractTitles(bi, ids))
                .compose(RxUtils.scheduleSingle(uiScheduler)));
        //triggered when a new favorite was added/removed -> then load from db
        Observable<Integer> busTriggerFav = bus.toObservable()
                .flatMap(e -> e.checkGroup(GroupEventID.GROUP_FAVORITE) ? Observable.just(1) : Observable.never());
        Observable<BookTitleWithFavorites[]> titlesObservable = Observable.just(1).concatWith(busTriggerFav)
                .flatMapSingle(__ -> titleSingle);
        disposables.add(titlesObservable
                .subscribe(view::onLoadBooks));
    }

    public void filter(Observable<String> filter) {
        Observable<String> debounced = filter.debounce(300, TimeUnit.MILLISECONDS);
        disposables.add(debounced
                .switchMapSingle(text -> loader.load().map(bi -> bi.getBook(text)))
                .observeOn(uiScheduler) // cause debounce is called on computation thread
                .subscribe(book -> {
                    String id = !book.equals(BibleInfo.BookInfo.NO_BOOK) ? book.id : FavoritesState.ALL_BOOKS;
                    stateContainer.subjectFavoriteState.onNext(stateContainer
                            .subjectFavoriteState.getValue().bookId(id));
                    //instruct the favorites screen holder to go back to
                    //favorites screen if we are in a detail fragment screen
                    // and then collapse the filter bottom sheet
                    bus.dispatch(Event.flag(GroupEventID.GROUP_FAVORITE,
                            EventID.FAVORITE_BACK_TO));
                    bus.dispatch(Event.flag(GroupEventID.GROUP_FAVORITE,
                            EventID.FAVORITE_COLLAPSE_FILTER_SHEET));
                }));
    }

    private BookTitleWithFavorites[] extractTitles(BibleInfo info, List<Pair<String, Integer>> ids) {
        BookTitleWithFavorites[] titles = new BookTitleWithFavorites[ids.size()];
        for (int i = 0; i < ids.size(); i++) {
            String id = ids.get(i).first;
            String title = info.getBookById(id).title;
            titles[i] = new BookTitleWithFavorites(id, title, pallete.nextColor(id), ids.get(i).second);
        }
        return titles;
    }
}
