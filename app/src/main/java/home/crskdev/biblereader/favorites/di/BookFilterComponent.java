package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.detail.DeleteTagDialog;
import home.crskdev.biblereader.favorites.filter.BookFilterFragment;

/**
 * Created by criskey on 29/6/2017.
 */
@PerFragment
@Subcomponent
public interface BookFilterComponent extends AndroidInjector<BookFilterFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BookFilterFragment> {
    }

}
