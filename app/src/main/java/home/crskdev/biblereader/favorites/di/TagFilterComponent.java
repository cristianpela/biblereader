package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.filter.TagFilterFragment;

/**
 * Created by criskey on 29/6/2017.
 */
@PerFragment
@Subcomponent
public interface TagFilterComponent extends AndroidInjector<TagFilterFragment> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<TagFilterFragment> {
    }
}