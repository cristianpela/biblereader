package home.crskdev.biblereader.favorites.detail;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 25/6/2017.
 */

public interface CloseableView extends IView {
    void close();
}
