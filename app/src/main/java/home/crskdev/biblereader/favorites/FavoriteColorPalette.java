package home.crskdev.biblereader.favorites;

import android.support.annotation.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.mvp.ResourceFinder;

/**
 * Created by criskey on 11/6/2017.
 */
@Singleton
public class FavoriteColorPalette {

    private Map<String, Integer> map;
    private int MAX = 6;
    private String colorPrefix = "color_fav_pallete_";
    private int pointer = 0;
    private ResourceFinder res;

    @Inject
    public FavoriteColorPalette(ResourceFinder res) {
        this.res = res;
        map = new HashMap();
    }

    @VisibleForTesting
    protected FavoriteColorPalette(ResourceFinder res, int max) {
        this(res);
        MAX = max;
    }

    public int nextColor(String bookId) {
        if (map.get(bookId) == null) {
            if (pointer++ == MAX) {
                pointer = 1;
            }
            map.put(bookId, res.getColorId(colorPrefix + pointer));
        }
        return map.get(bookId);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    String getColorPrefix() {
        return colorPrefix;
    }
}
