package home.crskdev.biblereader.favorites.detail.tagrecylerview.di;

import dagger.Subcomponent;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;

/**
 * Created by criskey on 22/6/2017.
 */
@PerView
@Subcomponent
public interface TagsRecyclerComponent extends AndroidArgumentsInjector<TagsRecyclerView> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidArgumentsInjector.Builder<TagsRecyclerView> {}
}
