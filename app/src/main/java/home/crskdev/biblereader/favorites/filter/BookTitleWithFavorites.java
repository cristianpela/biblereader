package home.crskdev.biblereader.favorites.filter;

import android.support.annotation.ColorRes;

/**
 * Created by criskey on 13/6/2017.
 */

public class BookTitleWithFavorites {

    final String id;

    final String title;

    @ColorRes
    final int color;

    final int count;

    public BookTitleWithFavorites(String id, String title, @ColorRes int color, int count) {
        this.id = id;
        this.title = title;
        this.color = color;
        this.count = count;
    }

    @Override
    public String toString() {
        return title;
    }
}
