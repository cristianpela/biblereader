package home.crskdev.biblereader.favorites.filter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxMenuItem;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxAutoCompleteTextView;

import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.ChildFragmentHolderDelegate;
import home.crskdev.biblereader.util.bottomsheet.BottomSheetFragment;
import home.crskdev.biblereader.MainState;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 10/6/2017.
 */

public class FavoritesFilterFragmentView extends BottomSheetFragment implements FavoritesFilterView {

    public static final String TAG = Integer.toString(MainState.FAVORITES_TAB);

    @Inject
    FavoritesFilterPresenter presenter;

    @BindView(R.id.toolbar_fav_filter)
    Toolbar toolbarFavFilter;

    @BindView(R.id.edit_fav_filter)
    AutoCompleteTextView editFavFilter;

    @BindView(R.id.btn_fav_reset)
    ImageButton btnFavReset;

    private ChildFragmentHolderDelegate cfhDelegate;
    private Menu mMenu;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.fav_filter_layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        cfhDelegate = new ChildFragmentHolderDelegate(this, getChildFragmentManager());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cfhDelegate.detach();
    }

    @Override
    public void onPreAttachPresenter() {
        toolbarFavFilter.inflateMenu(R.menu.menu_fav_filter);
    }

    @Override
    public void onPostAttachPresenter() {
        presenter.filter(RxAutoCompleteTextView.itemClickEvents(editFavFilter).map(
                e -> ((TextView) e.clickedView()).getText().toString())
                , RxView.clicks(btnFavReset));
        mMenu = toolbarFavFilter.getMenu();
        presenter.showByBookFilter(RxMenuItem.clicks(mMenu.findItem(R.id.action_fav_filter_by_book)));
        presenter.showByTagFilter(RxMenuItem.clicks(mMenu.findItem(R.id.action_fav_filter_by_tag)));
    }

    @Override
    public void onBottomSheetBehaviorInit() {
        getBehavior().setAllowDragging(false);
    }

    @Override
    public void onLoadAutocompleteEntries(String[] entries) {
        editFavFilter.setAdapter(new ArrayAdapter<>(getContext(),
                R.layout.dropdown_item_1line_custom, entries));
    }

    @Override
    public void collapseSheet() {
        getBehavior().setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void checkMenu(String menuCheckFilter) {
        if (menuCheckFilter.equals(FRAGMENT_TAG_BOOK_FILTER)) {
            mMenu.findItem(R.id.action_fav_filter_by_book).setChecked(true);
        } else {
            mMenu.findItem(R.id.action_fav_filter_by_tag).setChecked(true);
        }
    }

    @Override
    public void setFilterHint(String filterHint) {
        editFavFilter.setHint(filterHint);
    }

    @Override
    public void clearFilterFocus() {
        editFavFilter.setText("");
    }

    @Override
    public void addOrReplaceView(String tag, Class<? extends BaseFragment> fragClass,
                                 @NonNull Map<String, ?> args, boolean addToBackStack) {
        cfhDelegate.addOrReplaceView(tag, fragClass, args, addToBackStack);
    }

    @Override
    public void backTo() {

    }

    @Override
    public int childContainerId() {
        return R.id.fav_filter_container;
    }

}
