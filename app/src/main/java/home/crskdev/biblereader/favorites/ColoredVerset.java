package home.crskdev.biblereader.favorites;

import android.support.annotation.ColorRes;

import home.crskdev.biblereader.core.Verset;

/**
 * Created by criskey on 11/6/2017.
 */

public class ColoredVerset {

    @ColorRes
    public final int colorResId;

    public final Verset verset;

    public ColoredVerset(@ColorRes int colorResId, Verset verset) {
        this.colorResId = colorResId;
        this.verset = verset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ColoredVerset that = (ColoredVerset) o;

        if (colorResId != that.colorResId) return false;
        return verset != null ? verset.equals(that.verset) : that.verset == null;

    }

    @Override
    public int hashCode() {
        int result = colorResId;
        result = 31 * result + (verset != null ? verset.hashCode() : 0);
        return result;
    }
}
