package home.crskdev.biblereader.favorites.detail;

import android.support.annotation.NonNull;

import java.util.Comparator;

/**
 * Created by criskey on 15/6/2017.
 */
public class Tag implements Comparable<Tag> {

    public final static Tag NO_TAG = Tag.asDTO("");

    public final long id;

    public final CharSequence name;

    public Tag(long id, CharSequence name) {
        this.id = id;
        this.name = name;
    }

    public static Tag asDTO(CharSequence name) {
        return new Tag(-1L, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Tag)) return false;

        Tag tag = (Tag) o;

        return id == tag.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return name.toString();
    }

    @Override
    public int compareTo(@NonNull Tag o) {
        return name.toString().compareTo(o.name.toString());
    }
}
