package home.crskdev.biblereader.favorites.detail.tagtoolbar;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;

import com.jakewharton.rxbinding2.view.RxMenuItem;

import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.di.Args;
import home.crskdev.biblereader.core.mvp.MVPLifecycle;
import home.crskdev.biblereader.core.mvp.MVPLifecycleViewDelegate;
import home.crskdev.biblereader.favorites.detail.AddTagActionProvider;
import home.crskdev.biblereader.favorites.detail.DeleteTagDialog;
import home.crskdev.biblereader.favorites.detail.EditTagDialog;
import home.crskdev.biblereader.favorites.detail.Identifiable;

import static home.crskdev.biblereader.favorites.detail.FavoriteDetailView.ARG_KEY_VERSET;
import static home.crskdev.biblereader.util.ViewUtils.getActivity;

/**
 * Created by criskey on 6/7/2017.
 */
public class TagToolbar extends Toolbar implements MVPLifecycle, ITagToolbar, Identifiable {

    @Inject
    TagToolbarPresenter mPresenter;

    @Args
    @Inject
    Map<String, Object> mArgs;

    private MVPLifecycleViewDelegate mLifecycle = MVPLifecycleViewDelegate.NO_OP;
    private Identifiable mOwnerId = new Generate();

    public TagToolbar(Context context) {
        super(context);
        init();
    }

    public TagToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TagToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflateMenu(R.menu.menu_fav_details);
    }

    @Inject
    public void onPostInject() {
        mLifecycle = new MVPLifecycleViewDelegate(this, mPresenter);
    }

    @Override
    public void onAttachedToWindow() {
        mLifecycle.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mLifecycle.onDetachedFromWindow();
    }

    @Override
    public Map<String, ?> getArgs() {
        return mArgs;
    }

    @Override
    public void onPreAttachPresenter() {
    }

    @Override
    public void onPostAttachPresenter() {
        Menu menu = getMenu();
        AddTagActionProvider addTagActionProvider = (AddTagActionProvider) MenuItemCompat
                .getActionProvider(menu.findItem(R.id.action_fav_detail_new_tag));
        mPresenter.addTag(addTagActionProvider.observable());
        mPresenter.showDeleteTagDialog(RxMenuItem.clicks(menu.findItem(R.id.action_fav_detail_delete)));
        mPresenter.showEditTagDialog(RxMenuItem.clicks(menu.findItem(R.id.action_fav_detail_edit)));
    }

    @Override
    public void onPreDetachPresenter() {
    }

    @Override
    public String getOwnerId() {
        return mOwnerId.getOwnerId();
    }

    @Override
    public Identifiable getSource() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void showDeleteTagDialog(Verset verset) {
        FragmentManager manager = getActivity(this).getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_KEY_VERSET, verset);
        DeleteTagDialog dialog = DeleteTagDialog.newInstance(bundle,
                getVersetTitle(verset, R.string.dialog_tag_delete_title),
                DeleteTagDialog.class);
        dialog.show(manager, "Delete Tags");
    }

    @Override
    public void showEditTagDialog(Verset verset) {
        FragmentManager manager = getActivity(this).getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_KEY_VERSET, verset);
        EditTagDialog dialog = EditTagDialog.newInstance(bundle,
                getVersetTitle(verset, R.string.dialog_tag_edit_title),
                EditTagDialog.class);
        dialog.show(manager, "Edit Tags");
    }

    private String getVersetTitle(Verset verset, int titleStringId) {
        return getResources().getString(titleStringId,
                verset == null ? "" : verset.formalTitle());
    }

}
