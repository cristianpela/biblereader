package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.detail.DeleteTagDialog;

/**
 * Created by criskey on 25/6/2017.
 */
@PerFragment
@Subcomponent
public interface DeleteTagComponent extends AndroidInjector<DeleteTagDialog> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<DeleteTagDialog> {
    }
}
