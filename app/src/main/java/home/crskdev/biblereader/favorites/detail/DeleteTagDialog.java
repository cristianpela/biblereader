package home.crskdev.biblereader.favorites.detail;

import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjection;
import home.crskdev.biblereader.core.di.Arguments;
import home.crskdev.biblereader.core.mvp.BaseDialogFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 25/6/2017.
 */
public class DeleteTagDialog extends BaseDialogFragment implements CloseableView {

    @Inject
    DeleteTagPresenter mPresenter;

    @BindView(R.id.recy_delete_tag)
    TagsRecyclerView mRecyclerTagDelete;

    @BindView(R.id.recy_history_tag)
    TagsRecyclerView mRecyclerTagHistory;

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.tag_delete_dialog_layout;
    }

    @Override
    public void onPostAttachPresenter() {
        AndroidArgumentsInjection.inject(this,
                new Arguments<>(mRecyclerTagDelete, (Map<String, Object>) getArgs()),
                Arguments.noArgs(mRecyclerTagHistory)
        );
    }

    @Override
    public void close() {
        dismiss();
    }

    @OnClick(R.id.btn_undo_tag)
    public void actionUndo() {
        mPresenter.undo();
    }

    @OnClick(R.id.btn_commit_tag)
    public void actionCommit() {
        mPresenter.commit();
    }

    @Override
    public Map<String, ?> getArgs() {
        Map<String, Object> args = new HashMap<>(super.getArgs());
        args.put(ITagsRecyclerView.ARG_KEY_OWNERSHIP_MANAGER_ID,
                Collections.unmodifiableList(Arrays.asList(
                        mRecyclerTagDelete.getSource(),
                        mRecyclerTagHistory.getSource()
                )));
        return args;
    }
}
