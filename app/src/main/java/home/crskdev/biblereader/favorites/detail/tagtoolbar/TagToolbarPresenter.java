package home.crskdev.biblereader.favorites.detail.tagtoolbar;

import android.text.TextUtils;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailView;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.ResError;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 6/7/2017.
 */
@PerView
public class TagToolbarPresenter extends Presenter<ITagToolbar> {

    private static final String STRING_RES_TAG_ERR_FAV_HAS_IT = "tag_err_fav_has_it";

    private FavoritesService favoritesService;
    private Verset verset;

    @Inject
    public TagToolbarPresenter(EventBus bus, StateContainer stateContainer,
                               Scheduler uiScheduler, FavoritesService favoritesService) {
        super(bus, stateContainer, uiScheduler);
        this.favoritesService = favoritesService;
    }

    @Override
    protected void postAttach() {
        verset = (Verset) view.getArgs().get(FavoriteDetailView.ARG_KEY_VERSET);
    }

    public void addTag(Observable<String> addTagObservable) {

        SingleTransformer<Tag, Tag> setVersetTag = upstream -> upstream.flatMap(tag -> {
            //if verset is null just pass the tag down the stream
            if(verset == null)
                return Single.just(tag);
            return favoritesService
                    .hasTag(tag, verset)
                    .flatMap(has -> has
                            ? Single.error(new Error(tag.name.toString()))
                            : favoritesService.setTag(tag, verset)).map(v -> tag);
        });

        Observable<Tag> addTag = addTagObservable
                .debounce(300, TimeUnit.MILLISECONDS)
                .filter(tagName -> !TextUtils.isEmpty(tagName))
                .switchMap(tagName -> favoritesService.existTag(Tag.asDTO(tagName))
                        .flatMap(exist -> (exist)
                                ? Single.error(new Error(tagName))
                                : favoritesService.saveTag(Tag.asDTO(tagName))
                                .compose(setVersetTag))
                        .toObservable()
                        .onErrorResumeNext(Observable.just(Tag.asDTO(tagName)))
                        .subscribeOn(Schedulers.io()))
                .observeOn(uiScheduler);

        disposables.add(addTag.subscribe(tag -> {
            if (tag.id != -1) {
                //notify all interested parties that new tag db was changed
                // -> they should reload their tag lists
                bus.dispatch(Event.flag(GroupEventID.GROUP_TAG, EventID.TAG_RELOAD));
            } else {
                bus.dispatch(new Event<>(EventID.ERROR, new ResError(STRING_RES_TAG_ERR_FAV_HAS_IT, tag.name)));
            }
        }));
    }

    public void showDeleteTagDialog(Observable<Object> clicks) {
        Disposable disposable = clicks.debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(uiScheduler)
                .subscribe(__ -> {
                    view.showDeleteTagDialog(verset);
                });
        disposables.add(disposable);
    }

    public void showEditTagDialog(Observable<Object> clicks) {
        Disposable disposable = clicks.debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(uiScheduler)
                .subscribe(__ -> {
                    view.showEditTagDialog(verset);
                });
        disposables.add(disposable);
    }
}
