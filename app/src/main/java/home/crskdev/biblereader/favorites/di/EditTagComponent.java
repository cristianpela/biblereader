package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.detail.EditTagDialog;

/**
 * Created by criskey on 27/6/2017.
 */
@PerFragment
@Subcomponent
public interface EditTagComponent extends AndroidInjector<EditTagDialog> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<EditTagDialog> {
    }
}
