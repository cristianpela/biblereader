package home.crskdev.biblereader.favorites;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 31/7/2017.
 */
public final class FavoritesAlerts {

    public static AlertDialog.Builder favoriteConfirmDialogBuilder(
            Context context,
            @NonNull Verset verset,
            @Nullable DialogInterface.OnClickListener onConfirm,
            @Nullable DialogInterface.OnClickListener onDismiss) {
        String formalTitle = verset.formalTitle();
        String msg = (!verset.favorite)
                ? context.getString(R.string.dialog_fav_add_msg, formalTitle)
                : context.getString(R.string.dialog_fav_remove_msg, formalTitle);
        return ViewUtils.getAlertBuilder(context, onConfirm, onDismiss, msg,
                R.string.dialog_btn_save, R.string.dialog_btn_cancel);
    }

    public static AlertDialog.Builder favoriteConfirmDialogBuilder(
            Context context,
            @NonNull Verset verset,
            @Nullable DialogInterface.OnClickListener onConfirm) {
        return favoriteConfirmDialogBuilder(context, verset, onConfirm, null);
    }

    public static AlertDialog.Builder favoriteConfirmShow(
            Context context,
            @NonNull Verset verset,
            @Nullable DialogInterface.OnClickListener onConfirm,
            @Nullable DialogInterface.OnClickListener onDismiss) {
        String msg = context.getString(R.string.dialog_fav_show_msg, verset.formalTitle());
        return ViewUtils.getAlertBuilder(context, onConfirm, onDismiss, msg,
                R.string.dialog_btn_ok, R.string.dialog_btn_no);
    }

    public static AlertDialog.Builder favoriteConfirmShow(
            Context context,
            @NonNull Verset verset,
            @Nullable DialogInterface.OnClickListener onConfirm) {
        return favoriteConfirmShow(context, verset, onConfirm, null);
    }


}
