package home.crskdev.biblereader.favorites.detail;

import android.support.annotation.VisibleForTesting;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.EditedTag;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagOwnershipManager;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagWrapper;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.RxUtils;
import io.reactivex.Completable;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;

/**
 * Created by criskey on 26/6/2017.
 */
@PerFragment
public class EditTagPresenter extends ActionTagPresenter<EditTagView> {

    private final ResourceFinder res;
    private TagWrapper editTw;

    @Inject
    public EditTagPresenter(EventBus bus, StateContainer stateContainer,
                            Scheduler uiScheduler, FavoritesService favoritesService,
                            TagOwnershipManager tagOwnershipManager, ResourceFinder res) {
        super(bus, stateContainer, uiScheduler, favoritesService, tagOwnershipManager);
        this.res = res;
    }

    @Override
    Completable commitCompletable() {
        List<Tag> latestEdits = new ArrayList<>();
        for (TagWrapper tw : history) {
            EditedTag tag = (EditedTag) tw.tag;
            if (!latestEdits.contains(tag)) {
                latestEdits.add(tag.commit());
            }
        }
        return favoritesService.editTags(latestEdits);
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        if (event.checkGroup(GroupEventID.GROUP_TAG)) {
            if (event.checkId(EventID.TAG_SELECT)) {
                editTw = (TagWrapper) event.data;
                if (tagOwnershipManager.contains(editTw.owner)) {
                    view.onEdit(editTw.tag.name);
                }
            }
        }
    }

    public void addToHistory(@NonNull String edited) {
        if (editTw != null && edited.trim().length() > 0) {
            if (!edited.equalsIgnoreCase(editTw.tag.name.toString())) {
                String newName = edited.toLowerCase().trim();
                EditedTag editedTag = EditedTag.from(editTw.tag, newName);
                disposables.add(favoritesService
                        .existTag(editedTag.commit())
                        .compose(RxUtils.scheduleSingle(uiScheduler))
                        .subscribe((exists) -> {
                            if (!exists) {
                                TagWrapper tw = decorateEditedTag(editedTag, editTw);
                                history.push(tw);
                                dispatchOrders(tagOwnershipManager.decideEditTag(tw));
                                editTw = null; // reset to prevent duplicate entries in history
                                view.onClearEdit();
                            } else {
                                bus.dispatch(new Event(EventID.ERROR,
                                        new ResError("dialog_tag_edit_error_input_duplicate_db")));
                                view.onEdit(editedTag.undo().name);
                            }
                        }));
            } else {
                bus.dispatch(new Event(EventID.ERROR, new ResError("dialog_tag_edit_error_input_duplicate")));
            }
        } else {
            bus.dispatch(new Event(EventID.ERROR, new ResError("dialog_tag_edit_error_input_select")));
        }
    }

    /**
     * redecorates an editTag. Mock this method in your tests
     */
    @VisibleForTesting
    @android.support.annotation.NonNull
    TagWrapper decorateEditedTag(EditedTag editedTag, TagWrapper editTw) {
        return new TagWrapper(editTw.owner, editedTag.redecorate((name, newName) -> {
            final String delim = "<->";
            final String toDecorate = name + delim + newName;
            final int delimIndex = toDecorate.indexOf(delim);
            SpannableString ss = new SpannableString(toDecorate);
            ss.setSpan(new ForegroundColorSpan(res.getColor("colorError")),
                    0, delimIndex,
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ss.setSpan(new ForegroundColorSpan(res.getColor("color_fav_pallete_7")),
                    delimIndex + delim.length(), toDecorate.length(),
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            return ss;
        }));
    }

    @Override
    protected void onUndo(TagWrapper tw) {
        //on undo, for user convenience, we want to fill back edit text with the popped history item
        //but in order the edit text to be valid
        //we must to simulate a selection event from tag list
        //this way we keep edit text and editTw in sync
        Tag tag = ((EditedTag) tw.tag).undo();
        bus.dispatch(new Event<>(GroupEventID.GROUP_TAG, EventID.TAG_SELECT,
                new TagWrapper(tw.owner, tag)));
    }

    @Override
    int actionWhenUndo() {
        return EventID.TAG_EDIT;
    }
}
