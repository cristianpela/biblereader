package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import home.crskdev.biblereader.favorites.detail.Tag;

/**
 * Created by criskey on 25/6/2017.
 */

public class EditedTag extends Tag {

    public static final String DELIM = "->";

    public final CharSequence newName;

    private final CharSequence oldName;

    public EditedTag(long id, CharSequence name, CharSequence newName) {
        super(id, name + DELIM + newName);
        this.newName = newName;
        this.oldName = name;
    }

    public EditedTag(long id, CharSequence name, CharSequence newName, Decorator decorator) {
        super(id, decorator.decorate(name, newName));
        this.newName = newName;
        this.oldName = name;
    }

    public static EditedTag from(Tag tag, CharSequence newName) {
        return new EditedTag(tag.id, tag.name, newName);
    }

    public EditedTag redecorate(Decorator decorator) {
        return new EditedTag(id, undo().name, newName, decorator);
    }

    public EditedTag edit(CharSequence newName) {
        return new EditedTag(id, this.newName, newName);
    }

    public Tag commit() {
        return new Tag(id, newName);
    }

    public Tag undo() {
        return new Tag(id, oldName);
    }


    public interface Decorator {
        CharSequence decorate(CharSequence name, CharSequence newName);
    }

}
