package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 22/6/2017.
 */
class TagsRecyclerAdapter extends RecyclerView.Adapter<TagsRecyclerAdapter.TagsViewHolder> {

    private List<Tag> mTags;

    private int mLayoutType;

    public TagsRecyclerAdapter(int layoutType) {
        mLayoutType = layoutType;
        mTags = new ArrayList<>();
        setHasStableIds(true);
    }

    @Override
    public TagsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (mLayoutType) {
            case TagsRecyclerView.LINEAR_HORIZONTAL:
                layout = R.layout.recycler_tag_item_horizontal;
                break;
            case TagsRecyclerView.GRID:
                layout = R.layout.recycler_tag_item_grid;
                break;
            default:
                layout = R.layout.recycler_tag_item_vertical;
        }
        View view = LayoutInflater.from(parent.getContext())
                .inflate(layout, parent, false);
        return new TagsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TagsViewHolder holder, int position) {
        Tag tag = mTags.get(position);
        holder.text.setText(tag.name);
        if (mLayoutType == TagsRecyclerView.GRID) {
            holder.text.setTextSize(16.0f);
        }
    }

    @Override
    public long getItemId(int position) {
        return mTags.get(position).id;
    }

    Tag getTag(int index){
        return mTags.get(index);
    }

    void changeTags(List<Tag> tags) {
        mTags.clear();
        mTags.addAll(tags);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    public void addTag(Tag tag) {
        mTags.add(tag);
        Collections.sort(mTags);
        notifyDataSetChanged();
    }

    public void deleteTag(Tag tag) {
        int index = mTags.indexOf(tag);
        mTags.remove(index);
        notifyItemRemoved(index);
    }

    public void editTag(Tag tag) {
        int index = mTags.indexOf(tag);
        mTags.set(index, tag);
        Collections.sort(mTags);
        notifyDataSetChanged();
    }

    static class TagsViewHolder extends ButterKnifeViewHolder {

        @BindView(R.id.txt_recy_tag_item)
        TextView text;

        TagsViewHolder(View itemView) {
            super(itemView);
        }
    }
}
