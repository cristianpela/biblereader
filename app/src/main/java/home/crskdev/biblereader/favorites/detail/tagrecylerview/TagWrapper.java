package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import home.crskdev.biblereader.favorites.detail.Tag;

/**
 * Created by criskey on 23/6/2017.
 */
public final class TagWrapper {

    public final String owner;

    public final Tag tag;

    /**
     * this means that some interested presenters might use this tag regardless of owner managers decisions
     */
    public final boolean isAnon;

    public TagWrapper(String owner, Tag tag, boolean isAnon) {
        this.owner = owner;
        this.tag = tag;
        this.isAnon = isAnon;
    }

    public TagWrapper(String owner, Tag tag) {
       this(owner, tag, false);
    }


    public static TagWrapper anon(Tag tag) {
        return new TagWrapper("", tag, true);
    }
}
