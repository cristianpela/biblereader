package home.crskdev.biblereader.favorites.detail.tagrecylerview;

import java.util.Map;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailView;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.util.Pair;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;

import static home.crskdev.biblereader.favorites.detail.tagrecylerview.ITagsRecyclerView.*;
import static home.crskdev.biblereader.util.RxUtils.scheduleSingle;

/**
 * Created by criskey on 22/6/2017.
 */
@PerView
public class TagsRecyclerPresenter extends Presenter<ITagsRecyclerView> {

    private FavoritesService favoriteService;

    private Verset verset;

    private Owner owner;

    private int actionFlags;

    private boolean mHasDefaultData;

    @Inject
    public TagsRecyclerPresenter(EventBus bus, StateContainer stateContainer,
                                 Scheduler uiScheduler, FavoritesService favoriteService) {
        super(bus, stateContainer, uiScheduler);
        this.favoriteService = favoriteService;
    }

    public String getId() {
        return owner.id;
    }

    @Override
    protected void postAttach() {
        Map<String, ?> args = view.getArgs();
        verset = (Verset) args.get(FavoriteDetailView.ARG_KEY_VERSET);
        actionFlags = (Integer) args.get(ARG_KEY_ACTION_FLAGS);
        mHasDefaultData = (Boolean) args.get(ARG_KEY_HAS_DEFAULT_DATA);
        owner = ((Owner) args.get(ARG_KEY_OWNERSHIP_MANAGER_ID));
        load();
    }

    private void load() {
        if (mHasDefaultData) {
            fetchTagsBy();
        }
    }

    private void fetchTagsBy() {
        Single<Verset> caseOf = (this.verset == null)
                ? favoriteService.getAllTags().map(tags -> Verset.DEFAULT.tags(tags))
                : favoriteService.getTags(this.verset);
        Single<Verset> getTagSingle = caseOf.compose(scheduleSingle(uiScheduler));
        disposables.add(getTagSingle.subscribe(v -> {
            view.onDisplayTags(v.tags);
            if (this.verset != null) {
                this.verset = this.verset.tags(v.tags);
            }
        }));
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        if (event.checkGroup(GroupEventID.GROUP_TAG) &&
                !event.checkId(EventID.TAG_SELECT)) {
            if (event.checkId(EventID.TAG_RELOAD)) {
                load();
                return;
            }
            //if action flag none is present do nothing regardless of other flags
            if ((actionFlags & ACTION_NONE) == ACTION_NONE) {
                return;
            }
            int bitFlag = event.toBitFlag();
            TagWrapper tagWrapper = (TagWrapper) event.data;
            boolean isOwner = tagWrapper.owner.equals(owner.id);
            if (isOwner) {//process data only from other presenters
                if ((actionFlags & bitFlag) == ACTION_DELETE) {
                    view.onDeleteTag(tagWrapper.tag);
                }
                if ((actionFlags & bitFlag) == ACTION_ADD) {
                    view.onAddTag(tagWrapper.tag);
                }
                if ((actionFlags & bitFlag) == ACTION_EDIT) {
                    view.onEditTag(tagWrapper.tag);
                }
            }
        }
    }

    public void select(Observable<Pair<TapableRecyclerView.TapEvent, Tag>> observable) {
        if (!owner.master) {
            observable.subscribe().dispose();
        } else {
            disposables.add(observable
                    .filter(pair -> pair.first.kind == TapableRecyclerView.TapKind.SINGLE_TAP)
                    .subscribe(pair -> {
                        bus.dispatch(new Event<>(GroupEventID.GROUP_TAG,
                                EventID.TAG_SELECT, new TagWrapper(owner.id, pair.second)));
                    }));
        }
    }
}
