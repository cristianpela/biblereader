package home.crskdev.biblereader.favorites.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailFragmentView;

/**
 * Created by criskey on 17/6/2017.
 */
@PerFragment
@Subcomponent
public interface FavoriteDetailComponent extends AndroidInjector<FavoriteDetailFragmentView>{
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<FavoriteDetailFragmentView> {
    }
}
