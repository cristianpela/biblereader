package home.crskdev.biblereader.core.di;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.Map;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;
import dagger.multibindings.Multibinds;
import home.crskdev.biblereader.MainActivity;
import home.crskdev.biblereader.MainComponent;
import home.crskdev.biblereader.favorites.FavoritesChildFragmentHolder;
import home.crskdev.biblereader.favorites.detail.tagtoolbar.TagToolbar;
import home.crskdev.biblereader.favorites.detail.tagtoolbar.di.TagToolbarComponent;
import home.crskdev.biblereader.favorites.di.BookFilterComponent;
import home.crskdev.biblereader.favorites.di.TagFilterComponent;
import home.crskdev.biblereader.favorites.filter.BookFilterFragment;
import home.crskdev.biblereader.favorites.filter.FavoritesFilterFragmentView;
import home.crskdev.biblereader.favorites.detail.DeleteTagDialog;
import home.crskdev.biblereader.favorites.detail.EditTagDialog;
import home.crskdev.biblereader.favorites.detail.FavoriteDetailFragmentView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.TagsRecyclerView;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.di.TagsRecyclerComponent;
import home.crskdev.biblereader.favorites.di.DeleteTagComponent;
import home.crskdev.biblereader.favorites.di.EditTagComponent;
import home.crskdev.biblereader.favorites.di.FavoriteDetailComponent;
import home.crskdev.biblereader.favorites.di.FavoritesChildHolderComponent;
import home.crskdev.biblereader.favorites.di.FavoritesComponent;
import home.crskdev.biblereader.favorites.di.FavoritesFilterComponent;
import home.crskdev.biblereader.favorites.filter.TagFilterFragment;
import home.crskdev.biblereader.init.InitActivity;
import home.crskdev.biblereader.init.di.InitComponent;
import home.crskdev.biblereader.favorites.FavoritesViewFragment;
import home.crskdev.biblereader.quote.QuoteTextView;
import home.crskdev.biblereader.quote.di.QuoteComponent;
import home.crskdev.biblereader.read.FullScreenActivity;
import home.crskdev.biblereader.read.ReadNavViewFragment;
import home.crskdev.biblereader.read.ReadViewFragment;
import home.crskdev.biblereader.read.di.FullScreenReadComponent;
import home.crskdev.biblereader.read.di.ReadComponent;
import home.crskdev.biblereader.read.di.ReadNavComponent;
import home.crskdev.biblereader.search.SearchBookTitleDialog;
import home.crskdev.biblereader.search.SearchResultViewFragment;
import home.crskdev.biblereader.search.SearchViewFragment;
import home.crskdev.biblereader.search.di.SearchBookTitleComponent;
import home.crskdev.biblereader.search.di.SearchComponent;
import home.crskdev.biblereader.search.di.SearchResultComponent;

/**
 * Created by criskey on 4/5/2017.
 */
@Module(subcomponents = {
        //search components
        SearchComponent.class,
        SearchResultComponent.class,
        SearchBookTitleComponent.class,

        //favorites components
        FavoritesComponent.class,
        FavoriteDetailComponent.class,
        FavoritesChildHolderComponent.class,

        //filter components
        FavoritesFilterComponent.class,
        BookFilterComponent.class,
        TagFilterComponent.class,

        //tag components
        TagToolbarComponent.class,
        TagsRecyclerComponent.class,
        DeleteTagComponent.class,
        EditTagComponent.class,

        QuoteComponent.class,

        ReadComponent.class,
        ReadNavComponent.class,

        FullScreenReadComponent.class,

        InitComponent.class,

        MainComponent.class})
public abstract class BindingModule {

    @Binds
    @IntoMap
    @ActivityKey(InitActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindInitActivityInjectorFactory(InitComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindBibleScrollActivityInjectorFactory(MainComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(FullScreenActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindBibleFullScreenReadActivityInjectorFactory(FullScreenReadComponent.Builder builder);

    //#############FAVORITES FILTER COMPONENTS###############

    @Binds
    @IntoMap
    @FragmentKey(SearchViewFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindSearchFragmentInjectorFactory(SearchComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SearchResultViewFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindSearchResultFragmentInjectorFactory(SearchResultComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(SearchBookTitleDialog.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindSearchBookTitleDialogInjectorFactory(SearchBookTitleComponent.Builder builder);


    //#############FAVORITES FILTER COMPONENTS###############
    @Binds
    @IntoMap
    @FragmentKey(FavoritesFilterFragmentView.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindFavoriteFilterFragmentInjectorFactory(FavoritesFilterComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(BookFilterFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindBookFilterFragmentInjectorFactory(BookFilterComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(TagFilterFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindTagFilterFragmentInjectorFactory(TagFilterComponent.Builder builder);
    //######################################################


    @Binds
    @IntoMap
    @FragmentKey(FavoritesViewFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindFavoriteFragmentInjectorFactory(FavoritesComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(FavoriteDetailFragmentView.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindFavoriteDetailFragmentInjectorFactory(FavoriteDetailComponent.Builder builder);

    //#############TAG COMPONENTS###############
    @Binds
    @IntoMap
    @ViewKey(TagToolbar.class)
    abstract AndroidArgumentsInjector.Factory<? extends View>
    bindTagToolbarViewInjectorFactory(TagToolbarComponent.Builder builder);

    @Binds
    @IntoMap
    @ViewKey(TagsRecyclerView.class)
    abstract AndroidArgumentsInjector.Factory<? extends View>
    bindTagsRecyclerViewInjectorFactory(TagsRecyclerComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(DeleteTagDialog.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindDeleteTagDialogInjectorFactory(DeleteTagComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(EditTagDialog.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindEditTagDialogInjectorFactory(EditTagComponent.Builder builder);
    //######################################################

    @Binds
    @IntoMap
    @FragmentKey(FavoritesChildFragmentHolder.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindFavoritesChildHolderInjectorFactory(FavoritesChildHolderComponent.Builder builder);


    @Binds
    @IntoMap
    @ViewKey(QuoteTextView.class)
    abstract AndroidArgumentsInjector.Factory<? extends View>
    bindQuoteCustomViewInjectorFactory(QuoteComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(ReadViewFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindReadFragmentInjectorFactory(ReadComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(ReadNavViewFragment.class)
    abstract AndroidInjector.Factory<? extends Fragment>
    bindReadNavFragmentInjectorFactory(ReadNavComponent.Builder builder);


    @Multibinds
    abstract Map<Class<? extends android.app.Fragment>, AndroidInjector.Factory<? extends android.app.Fragment>>
    bindNativeFragments();


//    @PerFragment
//    @ContributesAndroidInjector(modules = {SearchModule.class})
//    abstract SearchViewFragment contributeSearchFragmentInjector();

//    @PerActivity
//    @ContributesAndroidInjector
//    abstract MainActivity contributeBibleScrollActivityInjector();

}
