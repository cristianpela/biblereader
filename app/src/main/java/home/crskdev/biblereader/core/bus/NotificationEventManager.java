package home.crskdev.biblereader.core.bus;

import android.content.Context;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.ViewUtils;
import home.crskdev.biblereader.util.asserts.Asserts;

/**
 * Display error and notification messages
 * Created by criskey on 12/7/2017.
 */
@Singleton
public class NotificationEventManager {

    private final EventBus bus;
    private final Context context;
    private final ResourceFinder res;

    @Inject
    public NotificationEventManager(EventBus bus, Context context, ResourceFinder res) {
        this.bus = bus;
        this.context = context;
        this.res = res;
    }

    @Inject
    public void postInject() {
        bus.toObservable().filter(e -> e.checkIdInGroupOther(EventID.ERROR))
                .subscribe(e -> {
                    Asserts.assertIsOnMainThread();
                    Asserts.basicAssert(e.data instanceof Throwable, "Error provided was a "
                            + e.data.getClass().getSimpleName() + " instead of Throwable");

                    Throwable throwable = (Throwable) e.data;
                    String msg;
                    if (throwable.getClass().equals(ResError.class)) {
                        ResError resError = (ResError) throwable;
                        msg = res.getString(resError.resourceKey, resError.args);
                    } else {
                        msg = throwable.getMessage();
                    }
                    ViewUtils.styledToast(context, msg, Toast.LENGTH_SHORT);

                });
        bus.toObservable().filter(e -> e.checkIdInGroupOther(EventID.NOTIFICATION))
                .subscribe(ev -> {
                    Asserts.assertIsOnMainThread();
                    String notification = res.getString((String) ev.data);
                    ViewUtils.styledToast(context, notification, Toast.LENGTH_SHORT);
                });
    }
}
