package home.crskdev.biblereader.core;

/**
 * Created by criskey on 30/4/2017.
 */

public interface BibleXMLTags {

    String BOOK_TAG = "book";

    String TITLE_TAG = "h";

    String CHAPTER_TAG = "c";

    String VERSET_TAG = "v";

}
