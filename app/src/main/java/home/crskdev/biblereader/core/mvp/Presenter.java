package home.crskdev.biblereader.core.mvp;

import android.support.annotation.CallSuper;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by criskey on 12/5/2017.
 */

public class Presenter<V extends IView> implements Consumer<Event<?>> {

    protected CompositeDisposable disposables;
    protected V view;
    protected StateContainer stateContainer;
    protected Scheduler uiScheduler;

    protected EventBus bus;

    public Presenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler) {
        this.bus = bus;
        this.stateContainer = stateContainer;
        this.uiScheduler = uiScheduler;
        disposables = new CompositeDisposable();
    }

    @CallSuper
    public void attach(V view) {
        this.view = view;
        disposables.add(bus.subscribe(this));
        postAttach();
    }

    @CallSuper
    public void detach() {
        disposables.clear();
        view = null;
        postDetach();
    }

    /**
     * Bus listener
     *
     * @param event
     * @throws Exception
     */
    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
    }

    protected void postAttach() {
    }

    protected void postDetach() {

    }

}
