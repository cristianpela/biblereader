package home.crskdev.biblereader.core.di;

import android.support.annotation.VisibleForTesting;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.disposables.Disposable;

/**
 * Created by criskey on 11/7/2017.
 */
@Singleton
public class GlobalDisposablesManager {

    private Map<String, Disposable> disposables;

    @Inject
    public GlobalDisposablesManager() {
        disposables = new HashMap<>();
    }

    public void add(String key, Disposable disposable) {
        Asserts.assertIsOnMainThread();
        //try dispose first
        dispose(key);
        disposables.put(key, disposable);
    }

    public void dispose(String key) {
        Asserts.assertIsOnMainThread();
        Disposable disposable = disposables.get(key);
        if (disposable != null) {
            if (!disposable.isDisposed())
                disposable.dispose();
            disposables.remove(key);
        }
    }

    @VisibleForTesting
    Map<String, Disposable> getDisposables() {
        return Collections.unmodifiableMap(disposables);
    }
}
