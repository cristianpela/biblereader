package home.crskdev.biblereader.core.bus;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.util.asserts.Asserts;
import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;
import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.subjects.Subject;

/**
 * Watcher of reactive streams lifecycle which sends wait and completed wait events to application's bus<br/>
 * Note: NEVER use wrap methods on subjects(only in case you are sure that the subject will be completed)
 * or hot observables. Just use {@link WaitEventDispatcher#dispatchSubject} <br/>
 * Created by criskey on 14/7/2017.
 */
@Singleton
public final class WaitEventDispatcher {

    private static final String TAG = "WEvD";

    /**
     * Tracks the sources implicated in a wait event session.<br/>
     * Each source has an unique id<br/>
     * Entering is done onSubscribe - Exiting is done on any stream's terminal lifecycle phase
     * (onComplete, onError, onDispose etc...)</br>
     * In the end the data structure MUST be empty!
     */
    protected ConcurrentHashMap<String, Boolean> sharedWaitState;

    private EventBus bus;

    @Inject
    public WaitEventDispatcher(EventBus bus) {
        this.bus = bus;
        this.sharedWaitState = new ConcurrentHashMap<>();
    }

    /**
     * Wraps a stream source and send waiting events to applicatiob bus,
     * according to source lifecycle<br/>
     *
     * @param src flags from where wrap was called (for logging purposes)
     * @return same source
     */
    public <T> ObservableTransformer<T, T> dispatchObservable(final String src) {
        final String id = UUID.randomUUID().toString();
        return up -> up
                .doOnSubscribe(d -> addWaitState(id, src, "subscribe"))
                .doOnDispose(() -> removeWaitState(id, src, "dispose"))
                .doOnEach(n -> {
                    if (n.isOnComplete()) {
                        removeWaitState(id, src, "complete");
                    } else if (n.isOnError()) {
                        removeWaitState(id, src, "error");
                    }
                });
    }


    /**
     * Wraps a subject source and send waiting events to applicatiob bus,
     * according to source lifecycle<br/>
     * The wait event are sent before and after injected observable
     *
     * @param src flags from where wrap was called (for logging purposes)
     * @return same source
     */
    public <In, Out> ObservableTransformer<In, Out> dispatchSubject(final String src, Function<In,
            Observable<Out>> injected) {
        final String id = UUID.randomUUID().toString();
        return up -> up
                .doOnNext(__ -> addWaitState(id, src, "next"))
                .flatMap((t) -> injected.apply(t))
                        .doOnNext(__ -> removeWaitState(id, src, "next"))
                .doFinally(() -> removeWaitState(id, src, "next"));
    }

    /**
     * Wraps a stream source and send waiting events to applicatiob bus,
     * according to source lifecycle<br/>
     *
     * @param src flags from where wrap was called (for logging purposes)
     * @return same source
     */
    public <T> FlowableTransformer<T, T> dispatchFlowable(final String src) {
        final String id = UUID.randomUUID().toString();
        return up -> up
                .doOnSubscribe(d -> addWaitState(id, src, "subscribe"))
                .doFinally(() -> removeWaitState(id, src, "finally"))
                .doOnCancel(() -> removeWaitState(id, src, "cancel"))
                .doOnTerminate(() -> removeWaitState(id, src, "terminate"))
                .doOnComplete(() -> removeWaitState(id, src, "complete"))
                .doOnEach(n -> {
                    if (n.isOnComplete()) {
                        removeWaitState(id, src, "complete");
                    } else if (n.isOnError()) {
                        removeWaitState(id, src, "error");
                    }
                });
    }

    /**
     * Wraps a stream source and send waiting events to applicatiob bus,
     * according to source lifecycle<br/>
     *
     * @param src flags from where wrap was called (for logging purposes)
     * @return same source
     */
    public <T> SingleTransformer<T, T> dispatchSingle(String src) {
        final String id = UUID.randomUUID().toString();
        return up -> up
                .doOnSubscribe(d -> addWaitState(id, src, "subscribe"))
                .doFinally(() -> removeWaitState(id, src, "finally"));
    }

    /**
     * Wraps a stream source and send waiting events to applicatiob bus,
     * according to source lifecycle<br/>
     *
     * @param src flags from where wrap was called (for logging purposes)
     * @return same source
     */
    public CompletableTransformer dispatchCompletable(String src) {
        final String id = UUID.randomUUID().toString();
        return up -> up
                .doOnSubscribe(d -> addWaitState(id, src, "subscribe"))
                .doFinally(() -> removeWaitState(id, src, "finally"));
    }

    private void addWaitState(String id, String src, String lifeCycle) {
        Boolean added = sharedWaitState.putIfAbsent(id, true);
        if (added == null) {
            System.out.println(TAG + "-" + src + "(" + id + "): On " + lifeCycle + ": Send WAIT Event " + true);
            bus.dispatch(new Event<>(EventID.WAIT, true));
        }
    }

    private void removeWaitState(String id, String src, String lifeCycle) {
        Boolean hasState = sharedWaitState.remove(id);
        if (hasState != null && hasState) {
            System.out.println(TAG + "-" + src + "(" + id + "): On " + lifeCycle + ": Send WAIT Event " + false);
            bus.dispatch(new Event<>(EventID.WAIT, false));
        }
    }
}
