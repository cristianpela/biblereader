package home.crskdev.biblereader.core.state;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.MainState;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.favorites.FavoritesState;
import home.crskdev.biblereader.init.InitState;
import home.crskdev.biblereader.quote.QuoteState;
import home.crskdev.biblereader.read.ReadJumpExpandableListModel;
import home.crskdev.biblereader.read.ReadState;
import home.crskdev.biblereader.search.SearchState;
import io.reactivex.subjects.BehaviorSubject;

@Singleton
public class StateContainer {

    public final BehaviorSubject<InitState> subjectInitState =
            BehaviorSubject.createDefault(InitState.DEFAULT);

    public final BehaviorSubject<SearchState> subjectSearchState =
            BehaviorSubject.createDefault(SearchState.DEFAULT);

    public final BehaviorSubject<MainState> subjectMainState =
            BehaviorSubject.createDefault(MainState.DEFAULT);

    public final BehaviorSubject<QuoteState> subjectQuoteState =
            BehaviorSubject.createDefault(QuoteState.DEFAULT);

    public final BehaviorSubject<ReadState> subjectReadState =
            BehaviorSubject.createDefault(ReadState.DEFAULT);

    public final BehaviorSubject<FavoritesState> subjectFavoriteState =
            BehaviorSubject.createDefault(FavoritesState.DEFAULT);

    public final BehaviorSubject<ReadJumpExpandableListModel> subjectJumpListState =
            BehaviorSubject.create();

    private List<BehaviorSubject<? extends ResetableState>> states;

    @Inject
    public StateContainer(EventBus bus) {
        states = Arrays.asList(
                subjectSearchState,
                subjectFavoriteState,
                subjectReadState
        );
        bus.toObservable()
                .filter(ev -> ev.checkIdInGroupOther(EventID.RESET))
                .subscribe(__ -> {
                    for (BehaviorSubject<? extends ResetableState> state : states) {
                        state.onNext(state.getValue().reset());
                    }
                });

    }

}
