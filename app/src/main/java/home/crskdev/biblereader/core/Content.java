package home.crskdev.biblereader.core;

import junit.framework.Assert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.util.MathExt;
import home.crskdev.biblereader.util.asserts.Asserts;

/**
 * Created by criskey on 6/6/2017.
 */

public interface Content extends Serializable {

    String formalTitle();

    final class Util {

        private Util() {
        }

        public static <C extends Content> int indexOf(List<C> list, C content) {
            int i = 0;
            for (C c : list) {
                if (c.equals(content)) {
                    return i;
                }
                i++;
            }
            return -1;
        }

        public static List<Content> organize(List<Verset> versets, String bookTitlePrefix, String chapterTitlePrefix) {
            //npe guard;
            String bkPfx = (bookTitlePrefix == null) ? "" : bookTitlePrefix + " ";
            String chPfx = (chapterTitlePrefix == null) ? "" : chapterTitlePrefix + " ";
            sort(versets);
            //the actual organization in structure of grouping like BookInfo[Chapters[Versets...]...]
            List<Content> list = new ArrayList<>();
            boolean newBook = true, newCh = true;
            for (int i = 0, s = versets.size(); i < s; i++) {
                Verset current = versets.get(i);
                if (newBook) {
                    String title = (current.bookTitle == null) ? "" : current.bookTitle;
                    list.add(new Book(current.bookId, bkPfx + title));
                    newBook = false;
                }
                if (newCh) {
                    list.add(new Chapter(current.chapterId, current.bookId, chPfx + current.chapterId));
                    newCh = false;
                }
                if (i + 1 < s) {
                    Verset next = versets.get(i + 1);
                    newBook = !next.bookId.equals(current.bookId);
                    newCh = next.chapterId != current.chapterId;
                }
                list.add(current.content(current.verset + "." + current.content));
            }
            return list;
        }

        /**
         * Sort versets to book -> chapter -> verset order priority
         *
         * @param versets
         */
        public static void sort(List<Verset> versets) {
            Collections.sort(versets, (o1, o2) -> {
                int cheq = MathExt.compareInt(o1.chapterId, o2.chapterId);
                int vseq = MathExt.compareInt(o1.verset, o2.verset);
                int bkeq = o1.bookId.compareTo(o2.bookId);
                int r;
                if ((r = bkeq) == 0)
                    if ((r = cheq) == 0)
                        r = vseq;
                return r;
            });
        }

        public static List<Content> organize(List<Verset> versets) {
            return organize(versets, null, null);
        }

        /**
         * utility method that make base versets to have corresponded transformer verset be favorite
         * or not favorite
         *
         * @param base
         * @param transformer
         * @return
         */
        public static List<Verset> joinFavorites(List<Verset> base, List<Verset> transformer) {
            List<Verset> out = new ArrayList<>();
            Asserts.basicAssert(base.size() >= transformer.size(),
                    "Base size must be at least equals or bigger than transformer's size");
            for (int i = 0, s = base.size(); i < s; i++) {
                Verset bv = base.get(i);
                for (int j = 0; j < transformer.size(); j++) {
                    Verset tv = transformer.get(j);
                    if (bv.equals(tv)) {
                        bv = bv.setFavorite(tv.favorite, tv.date);
                        break;
                    }
                }
                out.add(bv);
            }
            return out;
        }
    }

}
