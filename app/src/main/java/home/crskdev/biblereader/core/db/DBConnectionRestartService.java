package home.crskdev.biblereader.core.db;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 4/8/2017.
 */
public class DBConnectionRestartService {

    private BibleSQLiteOpenHelper helper;

    @Inject
    public DBConnectionRestartService(BibleSQLiteOpenHelper helper) {
        this.helper = helper;
    }

    public Completable restart() {
        return Completable.create((c) -> {
            helper.close();
            c.onComplete();
        }).subscribeOn(Schedulers.io());
    }
}
