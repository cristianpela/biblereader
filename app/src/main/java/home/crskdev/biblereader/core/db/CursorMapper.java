package home.crskdev.biblereader.core.db;

import android.database.Cursor;

/**
 * Created by criskey on 12/6/2017.
 */

public interface CursorMapper<T> {

    CursorMapper<Boolean> NO_MAPPER = (c) -> false;

    T map(Cursor cursor);

}
