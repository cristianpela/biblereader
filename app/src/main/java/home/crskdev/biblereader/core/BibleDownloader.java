package home.crskdev.biblereader.core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import home.crskdev.biblereader.init.StreamedInitState;
import home.crskdev.biblereader.util.MathExt;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

import static android.os.Build.*;

/**
 * Created by criskey on 30/4/2017.
 */
@Singleton
public class BibleDownloader {

    public static final String STRING_RES_PREPARE_DOWNLOADING = "download_prepare";

    public static final String STRING_RES_INTERRUPT_DOWNLOADING = "download_interrupt";

    public static String FILE_NAME = "ron-rccv.usfx.xml";

    public static String REMOTE_URL = "https://github.com/churchio/open-bibles/raw/master/ron-rccv.usfx.xml";

    private String link;

    private File outFile;

    @Inject
    public BibleDownloader(@Named("download-url") String link, @Named("bible-path") File outFile) {
        this.link = link;
        this.outFile = outFile;
    }

    public Flowable<StreamedInitState> download() {
        return Flowable.create(s -> {
            if (outFile.exists()) {
                s.onComplete();
                return;
            }
            InputStream in = null;
            OutputStream out = null;
            s.onNext(new StreamedInitState.ProgressSubStepMax(100, STRING_RES_PREPARE_DOWNLOADING));
            try {
                URL url = new URL(link);
                URLConnection conn = url.openConnection();
                conn.connect();
                long fileSize = 4697599L; // hardcoded length because the real size is larger than getContentLength
//              conn.getContentLength();
//                if (VERSION.SDK_INT >= VERSION_CODES.N) {
//                    fileSize = conn.getContentLengthLong();
//                }
                in = new BufferedInputStream(url.openStream());
                out = new FileOutputStream(outFile);
                byte buffer[] = new byte[1024];
                int bytesRead;
                long totalBytesRead = 0L;
                int lastNormal = -1;
                while ((bytesRead = in.read(buffer)) != -1) {
                    if (s.isCancelled()) {
                        outFile.delete();
                        throw new Error(STRING_RES_INTERRUPT_DOWNLOADING);
                    } else {
                        out.write(buffer, 0, bytesRead);
                        totalBytesRead += bytesRead;
                        int normal = MathExt.normalize100(totalBytesRead, fileSize);
                        if (normal > lastNormal) {
                            s.onNext(new StreamedInitState.SubTick(normal, normal + "%"));
                        }
                        lastNormal = normal;
                    }
                }
                s.onComplete();
            } catch (Exception ex) {
                s.onError(ex);
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception ex) {
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (Exception ex) {
                    }
                }
            }
        }, BackpressureStrategy.LATEST);
    }

}
