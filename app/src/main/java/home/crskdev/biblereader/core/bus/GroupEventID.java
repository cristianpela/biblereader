package home.crskdev.biblereader.core.bus;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_FAVORITE;
import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_OTHER;
import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_READ;
import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_SEARCH;
import static home.crskdev.biblereader.core.bus.GroupEventID.GROUP_TAG;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by criskey on 18/5/2017.
 */
@Retention(SOURCE)
@IntDef({GROUP_SEARCH, GROUP_OTHER, GROUP_READ, GROUP_FAVORITE, GROUP_TAG})
public @interface GroupEventID {
    int GROUP_SEARCH = 100;
    int GROUP_READ = 200;
    int GROUP_FAVORITE = 300;
    int GROUP_TAG = 400;
    int GROUP_OTHER = 600;
}
