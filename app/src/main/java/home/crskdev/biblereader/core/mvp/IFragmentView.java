package home.crskdev.biblereader.core.mvp;

/**
 * Created by criskey on 18/7/2017.
 */
public interface IFragmentView extends IView {
    /**
     * Tells the client if fragment state was saved. Doing ops with fragment manager
     * after saving will throw exception <br/>
     * <code>“IllegalStateException: Can not perform this action after onSaveInstanceState”</code>
     * <br/>
     * Thus is mandatory for a check.
     * ex:<br/> try to pop stack during back pressing only if this return false
     * @return check if fragment instance is saved
     */
    boolean isInstanceSaved();

}
