package home.crskdev.biblereader.core.mvp;

import android.util.Log;
import android.view.View;

/**
 * Helper to delegate mvp cycle event for Android View
 * Created by criskey on 13/5/2017.
 */
public class MVPLifecycleViewDelegate<V extends IView & MVPLifecycle> {

    //prevents npe in android studios visual preview
    public static final MVPLifecycleViewDelegate NO_OP = new MVPLifecycleViewDelegate(null, null);
    private static final String TAG = MVPLifecycleViewDelegate.class.getSimpleName();
    private String noOpWarning = "It looks like you are calling a stub of MVPLifecycleViewDelegate (NO_OP)." +
            " Are you sure you want to?\n In order to use this class correctly, you must create the instance" +
            " using method injection in the injected View (see Dagger 2 method injection). DON'T create an instance in" +
            " View's constructors";

    private V v;

    private Presenter presenter;


    public MVPLifecycleViewDelegate(V v, Presenter presenter) {
        this.v = v;
        this.presenter = presenter;
    }

    /**
     * Delegate mehtod for {@link View#onAttachedToWindow()}
     * make sure to call super in v - implementation
     */
    public void onAttachedToWindow() {
        //null check for android studio visual preview
        if (presenter != null) {
            v.onPreAttachPresenter();
            presenter.attach(v);
            v.onPostAttachPresenter();
        } else {
            Log.w(TAG, noOpWarning);
        }
    }

    /**
     * Delegate mehtod for {@link View#onDetachedFromWindow()}
     * make sure to call super in v - implementation
     */
    public void onDetachedFromWindow() {
        //null check for android studio visual preview
        if (presenter != null) {
            v.onPreDetachPresenter();
            presenter.detach();
        } else {
            Log.w(TAG, noOpWarning);
        }
        // deref to avoid leaks
        v = null;
    }
}
