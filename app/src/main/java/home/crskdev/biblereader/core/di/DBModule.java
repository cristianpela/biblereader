package home.crskdev.biblereader.core.di;

import android.database.sqlite.SQLiteDatabase;

import dagger.Module;
import dagger.Provides;
import home.crskdev.biblereader.core.db.BibleSQLiteOpenHelper;

/**
 * Created by criskey on 7/5/2017.
 */

@Module
public class DBModule {
//
//    @Singleton
//    @Provides
//    public SqlBrite provideSqlBriteDB() {
//        return new SqlBrite.Builder().build();
//    }

//    @Singleton
//    @Provides
//    public BriteDatabase provideBriteDB(BibleSQLiteOpenHelper helper) {
//        return new SqlBrite.Builder().build().wrapDatabaseHelper(helper, Schedulers.io());
//    }

    /**
     * Provides a database connection. Note: this should not be singleton, we might reset
     * connection, so we always want to get an up to date {@link SQLiteDatabase} instance
     *
     * @param helper
     * @return
     */
    @Provides
    public SQLiteDatabase provideNativeDatabase(BibleSQLiteOpenHelper helper) {
        return helper.getWritableDatabase();
    }
}
