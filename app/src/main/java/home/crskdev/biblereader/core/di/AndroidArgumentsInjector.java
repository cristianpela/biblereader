package home.crskdev.biblereader.core.di;

import java.util.Map;

import dagger.BindsInstance;

/**
 * Created by criskey on 20/6/2017.
 */
public interface AndroidArgumentsInjector<V>{

    void inject(V instance);

    void injectArgs(Map<String, Object> args);

    interface Factory<V> {
        AndroidArgumentsInjector<V> create(V instance, Map<String, Object> args);
    }

    abstract class Builder<V> implements AndroidArgumentsInjector.Factory<V> {

        @Override
        public AndroidArgumentsInjector<V> create(V instance, Map<String, Object> args) {
            seedInstance(instance);
            seedArgs(args);
            return build();
        }

        @BindsInstance
        public abstract void seedArgs(@Args Map<String, Object> args);

        @BindsInstance
        public abstract void seedInstance(V instance);

        public abstract AndroidArgumentsInjector<V> build();
    }

}
