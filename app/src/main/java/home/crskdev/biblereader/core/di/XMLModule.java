package home.crskdev.biblereader.core.di;

import com.fasterxml.aalto.stax.InputFactoryImpl;

import org.codehaus.stax2.XMLInputFactory2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by criskey on 30/4/2017.
 */

@Module
public class XMLModule {

    public XMLModule() {}

    @Provides
    @Singleton
    public XMLInputFactory2 provideXMLInputFactory2() {
        XMLInputFactory2 factory = new InputFactoryImpl();
        factory.configureForLowMemUsage();
        return factory;
    }

}
