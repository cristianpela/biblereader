package home.crskdev.biblereader.core.mvp;

import java.util.List;

/**
 * Created by criskey on 19/7/2017.
 */

public interface IListDisplayView<T> extends IView {

    void onDisplay(List<T> items);

    void onSelect(int index);

    void onConfirmToggleFavorite(T item, int index);

}
