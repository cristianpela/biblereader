package home.crskdev.biblereader.core.di;

import android.view.View;

import java.lang.annotation.Target;

import dagger.MapKey;

import static java.lang.annotation.ElementType.METHOD;

/**
 * Created by criskey on 4/6/2017.
 */
@MapKey
@Target(METHOD)
public @interface ViewKey {
    Class<? extends View> value();
}
