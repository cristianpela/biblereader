package home.crskdev.biblereader.core.mvp;

import android.content.Context;
import android.support.annotation.ArrayRes;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.lang.reflect.Field;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.R;

/**
 * Adapter for context resource ids)}
 * Created by criskey on 28/5/2017.
 */
@Singleton
public class ResourceFinder {

    private static final int NO_RESOURCE = 0;

    private Context context;

    @Inject
    public ResourceFinder(Context context) {
        this.context = context;
    }

    @ColorRes
    public int getColorId(String name) {
        return getId(name, R.color.class);
    }

    @StringRes
    public int getStringId(String string) {
        return getId(string, R.string.class);
    }

    @ArrayRes
    public int getArrayId(String array) {
        return getId(array, R.array.class);
    }

    public int getColor(String name) {
        int id = getColorId(name);
        return ContextCompat.getColor(context, id);
    }

    public String getString(String resName) {
        int id = getStringId(resName);
        return (id == NO_RESOURCE) ? resName : context.getString(id);
    }

    public String getString(String resName, Object... formatArgs) {
        int id = getStringId(resName);
        return (id == NO_RESOURCE) ? resName : context.getString(id, formatArgs);
    }

    public String[] getStringArray(String resName) {
        int id = getArrayId(resName);
        return (id == NO_RESOURCE) ? new String[]{resName} : context.getResources().getStringArray(id);
    }

    private int getId(String name, Class type) {
        if (name == null) {
            Log.w("Resource Finder", "Resource key name is null");
            return NO_RESOURCE;
        }
        int id = NO_RESOURCE;
        try {
            Field field = type.getField(name);
            id = field.getInt(null);
        } catch (Exception e) {
            Log.w("Resource Finder", "Couldn't find the resource key, falling back to: " + name);
        }
        return id;
    }


}
