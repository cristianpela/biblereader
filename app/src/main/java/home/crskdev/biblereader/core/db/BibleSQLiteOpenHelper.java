package home.crskdev.biblereader.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

import static home.crskdev.biblereader.core.db.Contract.*;
import static home.crskdev.biblereader.core.db.Contract.Exec.dropTableSQ;

/**
 * Created by criskey on 10/5/2017.
 */

@Singleton
public class BibleSQLiteOpenHelper extends SQLiteOpenHelper {

    @Inject
    public BibleSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public BibleSQLiteOpenHelper(Context context, String name) {
        super(context, name, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ENABLE_FOREIGN_KEY_SUPPORT);
        //todo re-enable when you whant to recreate whole schema
        db.execSQL(CREATE_BOOK_TABLE_SQ);
        db.execSQL(CREATE_CHAPTER_TABLE_SQ);
        db.execSQL(CREATE_FAVORITES_TABLE_SQ);
        db.execSQL(CREATE_TAG_TABLE_SQ);
        db.execSQL(CREATE_FAVORITES_TAG_TABLE_SQ);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //todo re-enable when you whant to recreate whole schema
        db.execSQL(dropTableSQ(TABLE_BOOK_INFO));
        db.execSQL(dropTableSQ(TABLE_CHAPTER_INFO));
        db.execSQL(dropTableSQ(TABLE_FAVORITES_INFO));
        db.execSQL(dropTableSQ(TABLE_TAG_INFO));
        db.execSQL(dropTableSQ(TABLE_FAVORITES_TAG_INFO));
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
