package home.crskdev.biblereader.core.bus;

/**
 * Created by criskey on 2/5/2017.
 */
public class Event<T> {

    public final int id;

    public final int group;

    public final T data;

    public Event(@GroupEventID int group, @EventID int id, T data) {
        this.group = group;
        this.id = id;
        this.data = data;
    }

    public Event(@EventID int id, T data) {
        this(GroupEventID.GROUP_OTHER, id, data);
    }

    public static Event<Boolean> flag(@GroupEventID int group, @EventID int id, boolean flag) {
        return new Event<>(group, id, flag);
    }

    public static Event<Boolean> flag(@EventID int id, boolean flag) {
        return new Event<>(id, flag);
    }

    public static Event<Boolean> flag(@EventID int id) {
        return new Event<>(id, true);
    }

    public static Event<Boolean> flag(@GroupEventID int group, @EventID int id) {
        return new Event<>(group, id, true);
    }

    public boolean check(@GroupEventID int group, @EventID int id) {
        return id == this.id && group == this.group;
    }

    public boolean checkId(@EventID int id) {
        return id == this.id;
    }

    public boolean checkIdInGroupOther(@EventID int id) {
        return id == this.id && group == GroupEventID.GROUP_OTHER;
    }

    public int toBitFlag() {
        return id - group;
    }

    public boolean checkGroup(@GroupEventID int groupId) {
        return group == groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event<?> event = (Event<?>) o;

        if (id != event.id) return false;
        if (group != event.group) return false;
        return data != null ? data.equals(event.data) : event.data == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + group;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", data=" + data +
                '}';
    }
}
