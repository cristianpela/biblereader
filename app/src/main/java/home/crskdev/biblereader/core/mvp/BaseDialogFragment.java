package home.crskdev.biblereader.core.mvp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.DispatchingArgumentsAndroidInjector;
import home.crskdev.biblereader.core.di.HasArgumentsInjector;
import home.crskdev.biblereader.favorites.detail.CloseableView;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 25/6/2017.
 */

public abstract class BaseDialogFragment extends AppCompatDialogFragment implements MVPSetup,
        MVPLifecycle, HasSupportFragmentInjector, HasArgumentsInjector, CloseableView {

    private static final String ARG_KEY_TITLE = "ARG_KEY_TITLE";

    @Inject
    DispatchingAndroidInjector<Fragment> childFragmentInjector;

    @Inject
    DispatchingArgumentsAndroidInjector<View> viewInjector;


    ViewUtils.SoftKeyboardWatcher softKeyboardWatcher;

    public static <D extends BaseDialogFragment> D newInstance(@NonNull Bundle args,
                                                               String title, Class<D> clazz) {
        D dialog = null;
        //noinspection TryWithIdenticalCatches
        try {
            dialog = clazz.newInstance();
            args.putString(ARG_KEY_TITLE, title);
            dialog.setArguments(args);
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return dialog;
    }

    public static <D extends BaseDialogFragment> D newInstance(Class<D> clazz) {
        return newInstance(new Bundle(), "", clazz);
    }


    public static <D extends BaseDialogFragment> D newInstance(String title, Class<D> clazz) {
        return newInstance(new Bundle(), title, clazz);
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog);
        return new TouchAwareDialog(getContext(), getTheme());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //doesn't pick the theme
        //this is a bug: https://stackoverflow.com/questions/32784009/styling-custom-dialog-fragment-not-working
        //https://issuetracker.google.com/issues/37042151#comment23
        // View inflate = inflater.inflate(layoutId(), container);
        //apparently we have to use activity's inflator to inflate the view with the right theme
        View inflate = getActivity().getLayoutInflater().inflate(layoutId(), container);
        softKeyboardWatcher = new ViewUtils.SoftKeyboardWatcher(inflate, getContext());
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setTitle(getArguments().getString(ARG_KEY_TITLE));
        attachPresenter();
    }


    @Override
    public void onDestroyView() {
        softKeyboardWatcher.disconnect();
        detachPresenter();
        super.onDestroyView();
    }

    public void attachPresenter() {
        onPreAttachPresenter();
        setupPresenter().attach(setupView());
        onPostAttachPresenter();
    }

    public void detachPresenter() {
        onPreDetachPresenter();
        setupPresenter().detach();
    }

    /**
     * init here anything that is usually done on onCreate (add toolbar, set adapters etc)
     */
    @Override
    public void onPreAttachPresenter() {
    }

    /**
     * init here anything to be initialized after presenter was plugged with view
     */
    @Override
    public void onPostAttachPresenter() {
    }

    @Override
    public void onPreDetachPresenter() {
    }

    @Override
    public Map<String, ?> getArgs() {
        Bundle bundle = getArguments();
        return (bundle == null) ? Collections.emptyMap() : ViewUtils.toMap(bundle);
    }

    @Override
    public void close() {
        getDialog().dismiss();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return childFragmentInjector;
    }

    @Override
    public AndroidArgumentsInjector<View> argumentsInjector() {
        return viewInjector;
    }

    private class TouchAwareDialog extends AppCompatDialog {

        TouchAwareDialog(Context context, int themeResId) {
            super(context, themeResId);
        }

        @Override
        public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
            softKeyboardWatcher.overrideDispatchTouchEvent(ev);
            return super.dispatchTouchEvent(ev);
        }
    }

}
