package home.crskdev.biblereader.core;

import android.support.annotation.VisibleForTesting;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.favorites.detail.Tag;

/**
 * Created by criskey on 5/6/2017.
 */

public class Verset implements Content {

    public static Verset DEFAULT = Verset.asDTO(null, -1, -1, null, false);

    //composite pk;
    public final String bookId;
    public final int chapterId;
    public final int verset;

    public final String bookTitle;

    public final CharSequence content;

    public final boolean favorite;

    public final long date;

    public final List<Tag> tags;

    public Verset(String bookId, String bookTitle, int chapterId, int verset, CharSequence content,
                  boolean favorite, long date, List<Tag> tags) {
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.chapterId = chapterId;
        this.verset = verset;
        this.content = content;
        this.favorite = favorite;
        this.date = date;
        this.tags = tags;
    }

    public static Verset asDTO(String bookId, int chapter, int verset, CharSequence content, boolean favorite) {
        return new Verset(bookId, null, chapter, verset, content, favorite, -1, Collections.emptyList());
    }
    public static Verset asDTO(String bookId, String bookTitle, int chapter, int verset, CharSequence content) {
        return new Verset(bookId, bookTitle, chapter, verset, content, false, -1, Collections.emptyList());
    }

    public static Verset asDTO(String bookId, String bookTitle, int chapter, int verset, CharSequence content, boolean favorite) {
        return new Verset(bookId, bookTitle, chapter, verset, content, favorite, -1, Collections.emptyList());
    }

    public Verset setFavorite(boolean favorite) {
        return setFavorite(favorite, System.currentTimeMillis());
    }

    public Verset toggleFavorite() {
        return setFavorite(!favorite);
    }

    public Verset date(long date) {
        return new Verset(bookId, bookTitle, chapterId, verset, content, favorite, date, tags);
    }

    public Verset content(CharSequence content) {
        return new Verset(bookId, bookTitle, chapterId, verset, content, favorite, date, tags);
    }

    public Verset tags(List<Tag> tags) {
        return new Verset(bookId, bookTitle, chapterId, verset, content, favorite, date,
                Collections.unmodifiableList(tags));
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    public Verset setFavorite(boolean favorite, long date) {
        return new Verset(this.bookId, bookTitle, this.chapterId, this.verset, this.content,
                favorite, date, tags);
    }

    public Verset setFavorite(long id, boolean favorite, long date) {
        return new Verset(this.bookId, bookTitle, this.chapterId, this.verset, this.content,
                favorite, date, tags);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Verset verset1 = (Verset) o;

        if (chapterId != verset1.chapterId) return false;
        if (verset != verset1.verset) return false;
        return bookId != null ? bookId.equals(verset1.bookId) : verset1.bookId == null;

    }

    @Override
    public int hashCode() {
        int result = bookId != null ? bookId.hashCode() : 0;
        result = 31 * result + chapterId;
        result = 31 * result + verset;
        return result;
    }

    @Override
    public String formalTitle() {
        return bookId + " " + chapterId + ":" + verset;
    }

    @Override
    public String toString() {
        return "Verset{" +
                "ID ='" + bookId + "-" + chapterId + "-" + verset +
                ", content='" + content + '\'' +
                ", favorite=" + favorite +
                ", tags =" + tags +
                ", date=" + date +
                '}';
    }
}
