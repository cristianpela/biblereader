package home.crskdev.biblereader.core.mvp;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import java.util.Map;

import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 29/6/2017.
 */

public class ChildFragmentHolderDelegate implements ChildHolderView {

    private ChildHolderView holder;

    private FragmentManager manager;

    public ChildFragmentHolderDelegate(ChildHolderView holder, FragmentManager manager) {
        this.holder = holder;
        this.manager = manager;
    }

    @Override
    public void addOrReplaceView(String tag, Class<? extends BaseFragment> fragClass, @NonNull Map<String, ?> args, boolean addToBackStack) {
        Fragment fragment = manager.findFragmentByTag(tag);
        FragmentTransaction tx = manager.beginTransaction();
        if (fragment != null && !fragment.isAdded()) {
            tx.show(fragment);
        } else {
            manager.popBackStackImmediate();
            fragment = newFragment(fragClass);
            if (addToBackStack) {
                tx.addToBackStack(null);
            }
            tx.replace(holder.childContainerId(), fragment, tag);
        }
        if (!args.isEmpty())
            fragment.setArguments(ViewUtils.toBundle(args));
        tx.commitAllowingStateLoss();
    }

    @Override
    public void backTo() {
        manager.popBackStack();
    }


    @Override
    public boolean isInstanceSaved() { throw new UnsupportedOperationException(); }

    @Override
    public int childContainerId() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Map<String, ?> getArgs() {
        throw new UnsupportedOperationException();
    }

    private Fragment newFragment(Class<? extends BaseFragment> fragClass) {
        Fragment fragment = null;
        try {
            fragment = fragClass.newInstance();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    public BaseActivity.BackPressedHandler getBackPressedHandler() {
        return () -> {
            if (holder.isInstanceSaved())
                return false;
            boolean hasBackStack = manager.getBackStackEntryCount() > 0;
            if (hasBackStack)
                manager.popBackStack();
            return hasBackStack;
        };
    }

    public void detach() {
        holder = null;
        manager = null;
    }
}
