package home.crskdev.biblereader.core.mvp;

import java.util.Map;

/**
 * Created by criskey on 12/5/2017.
 */

public interface IView {

    Map<String, ?> getArgs();

}
