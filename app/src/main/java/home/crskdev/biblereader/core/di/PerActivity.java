package home.crskdev.biblereader.core.di;

import javax.inject.Scope;

/**
 * Created by criskey on 2/5/2017.
 */
@Scope
public @interface PerActivity {
}
