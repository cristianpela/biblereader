package home.crskdev.biblereader.core.di;

import java.util.Collections;
import java.util.Map;

/**
 * Created by criskey on 20/6/2017.
 */

public class Arguments<V> {

    public final V v;

    public final Map<String, Object> args;

    public Arguments(V v, Map<String, Object> args) {
        this.v = v;
        this.args = Collections.unmodifiableMap(args);
    }

    public static <V> Arguments<V> noArgs(V v) {
        return new Arguments<>(v, Collections.emptyMap());
    }
}
