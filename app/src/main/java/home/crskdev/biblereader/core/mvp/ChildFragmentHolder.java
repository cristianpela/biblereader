package home.crskdev.biblereader.core.mvp;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by criskey on 29/6/2017.
 */
public abstract class ChildFragmentHolder extends BaseFragment implements ChildHolderView {

    private ChildFragmentHolderDelegate delegate;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        delegate = new ChildFragmentHolderDelegate(this, getChildFragmentManager());
    }

    @Override
    @CallSuper
    public void onPostAttachPresenter() {
        setBackPressedHandler(delegate.getBackPressedHandler());
    }

    @Override
    public void addOrReplaceView(String tag, Class<? extends BaseFragment> fragClass,
                                 @NonNull Map<String, ?> args, boolean addToBackStack) {
        delegate.addOrReplaceView(tag, fragClass, args, addToBackStack);
    }

    @Override
    public void backTo() {
        delegate.backTo();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        delegate.detach();
    }
}
