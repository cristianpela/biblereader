package home.crskdev.biblereader.core.di;

import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * Created by criskey on 20/6/2017.
 */
public class DispatchingArgumentsAndroidInjector<V> implements AndroidArgumentsInjector<V> {

    private static final String TAG = DispatchingArgumentsAndroidInjector.class.getSimpleName();

    private final Map<Class<? extends V>, Provider<Factory<? extends V>>>
            injectorFactories;

    private WeakReference<V> instanceRef;

    @Inject
    public DispatchingArgumentsAndroidInjector(Map<Class<? extends V>, Provider<Factory<? extends V>>>
                                                       injectorFactories) {
        this.injectorFactories = injectorFactories;
    }

    @Override
    public void inject(V instance) {
        instanceRef = new WeakReference<V>(instance);
        String name = instance.getClass().getSimpleName();
        Log.w(TAG, "Inject() for " + name + " was called. Make sure you call injectArgs() next...");
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    @Override
    public void injectArgs(Map<String, Object> args) {
        V instance = instanceRef.get();
        if (instance != null) {
            Provider<Factory<? extends V>> factoryProvider = injectorFactories.get(instance.getClass());
            if (factoryProvider == null) {
                String name = instance.getClass().getSimpleName();
                throw new RuntimeException("Factory provider for " + name + " was" +
                        " not found. Make sure that " + name + " is added to multibinding mutableMap!");
            }
            AndroidArgumentsInjector.Factory<V> factory = (AndroidArgumentsInjector.Factory<V>)
                    factoryProvider.get();
            AndroidArgumentsInjector<V> injector = factory.create(instance, args);
            injector.inject(instance);
            injector.injectArgs(args);
            instanceRef.clear();
        } else {
            throw new RuntimeException("Must called inject first/Or maybe instance was gc collected already");
        }
    }
}
