package home.crskdev.biblereader.core.di;

import javax.inject.Scope;

/**
 * Created by criskey on 4/6/2017.
 */
@Scope
public @interface PerView {
}
