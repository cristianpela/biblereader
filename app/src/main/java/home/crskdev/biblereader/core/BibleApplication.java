package home.crskdev.biblereader.core;

import android.app.Activity;
import android.app.Application;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import home.crskdev.biblereader.MemoryLevelEvent;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.di.AppComponent;
import home.crskdev.biblereader.core.di.AppModule;
import home.crskdev.biblereader.core.di.DaggerAppComponent;

/**
 * Created by criskey on 30/4/2017.
 */
public class BibleApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;

    @Inject
    EventBus bus;

    @Override
    public void onCreate() {
        super.onCreate();
        AppComponent build = DaggerAppComponent.builder()
                .appModule(new AppModule(getApplicationContext(), BibleDownloader.REMOTE_URL))
                .build();
        build.inject(this);
        build.initErrorEventManager();
        build.initFavoritesOpsMediator();
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        bus.dispatch(new MemoryLevelEvent(level));
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        //memory critical
        bus.dispatch(new MemoryLevelEvent(15));
    }
}


