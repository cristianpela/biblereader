package home.crskdev.biblereader.core;

import android.support.annotation.NonNull;

/**
 * Created by criskey on 6/6/2017.
 */

public class Book implements Content {

    public static final String NO_ID = "";
    private static final String NO_TITLE = "";

    public static final Book ALL_BOOKS = new Book(NO_ID, NO_TITLE);

    public final String id;

    public final String title;

    public Book(@NonNull String id, @NonNull String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;
        return id.equals(book.id);

    }

    @Override
    public String formalTitle() {
        if (id.equals(NO_ID))
            return "--.--";
        return "(" + id + ") " + title;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "BookInfo{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
