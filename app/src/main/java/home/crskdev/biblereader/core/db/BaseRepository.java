package home.crskdev.biblereader.core.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 12/6/2017.
 */
public class BaseRepository {

    private BibleSQLiteOpenHelper helper;

    public BaseRepository(BibleSQLiteOpenHelper helper) {
        this.helper = helper;
    }

    public <T> Single<List<T>> rawQuery(@NonNull String query, @Nullable String[] selectionArgs, @NonNull CursorMapper<T> mapper) {
        return Single.fromCallable(() -> {
            List<T> list = new ArrayList<>();
            Cursor c = db().rawQuery(query, selectionArgs);
            Cursor ic = new ImmutableCursor(c);
            c.moveToFirst();
            while (!c.isAfterLast()) {
                list.add(mapper.map(ic));
                c.moveToNext();
            }
            c.close();
            return Collections.unmodifiableList(list);
        }).subscribeOn(Schedulers.io());
    }

    protected SQLiteDatabase db(){
        return helper.getWritableDatabase();
    }


}
