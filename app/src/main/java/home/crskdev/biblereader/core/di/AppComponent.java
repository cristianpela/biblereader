package home.crskdev.biblereader.core.di;

import javax.inject.Singleton;

import dagger.Component;
import home.crskdev.biblereader.core.BibleApplication;
import home.crskdev.biblereader.core.bus.NotificationEventManager;
import home.crskdev.biblereader.favorites.FavoritesOpsMediator;

/**
 * Created by criskey on 30/4/2017.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(BibleApplication application);

    NotificationEventManager initErrorEventManager();

    FavoritesOpsMediator initFavoritesOpsMediator();

}
