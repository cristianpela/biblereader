package home.crskdev.biblereader.core.mvp;

/**
 * Created by criskey on 23/6/2017.
 */

public interface MVPLifecycle {
    /**
     * init here anything that is usually done on onCreate (add toolbar, set adapters etc)
     */
    void onPreAttachPresenter();

    /**
     * init here anything to be initialized after presenter was plugged with view
     */
    void onPostAttachPresenter();

    void onPreDetachPresenter();

}
