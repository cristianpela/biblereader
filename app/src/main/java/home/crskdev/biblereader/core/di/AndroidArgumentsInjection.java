package home.crskdev.biblereader.core.di;

import android.view.View;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by criskey on 4/6/2017.
 */
public final class AndroidArgumentsInjection {

    public static void inject(HasArgumentsInjector hasInjector, Arguments<View> args) {
        checkNotNull(args, "args");
        checkNotNull(args.v, "view");
        checkNotNull(hasInjector, "hasInjector");
        AndroidArgumentsInjector<View> viewAndroidInjector = hasInjector.argumentsInjector();
        injectArguments(viewAndroidInjector, args);
    }

    public static void inject(HasArgumentsInjector hasInjector, View view) {
        inject(hasInjector, Arguments.noArgs(view));
    }

    public static void inject(HasArgumentsInjector hasInjector, Arguments... args) {
        checkNotNull(args, "args");
        checkNotNull(hasInjector, "hasInjector");
        AndroidArgumentsInjector<View> viewAndroidInjector = hasInjector.argumentsInjector();
        for (Arguments arg : args) {
            injectArguments(viewAndroidInjector, arg);
        }
    }

    private static void injectArguments(AndroidArgumentsInjector<View> viewAndroidInjector, Arguments<View> args) {
        viewAndroidInjector.inject(args.v);
        viewAndroidInjector.injectArgs(args.args);
    }

}
