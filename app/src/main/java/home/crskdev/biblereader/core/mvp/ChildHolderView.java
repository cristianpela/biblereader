package home.crskdev.biblereader.core.mvp;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by criskey on 29/6/2017.
 */

public interface ChildHolderView extends IFragmentView {

    void addOrReplaceView(String tag, Class<? extends BaseFragment> fragClass, @NonNull Map<String, ?> args,
                          boolean addToBackStack);

    void backTo();

    @IdRes
    int childContainerId();

}
