package home.crskdev.biblereader.core.mvp;

import android.support.annotation.IdRes;

/**
 * Created by criskey on 13/5/2017.
 */

public interface MVPSetup {

    int NO_CONTENT = -1;

    Presenter setupPresenter();

    IView setupView();

    int layoutId();
}
