package home.crskdev.biblereader.core.bus;

import android.support.annotation.VisibleForTesting;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by criskey on 12/7/2017.
 */
@Singleton
public class WaitEventQueue {

    public static final int WAIT_SESSION_START = 0;
    public static final int WAIT_SESSION_END = 1;
    public static final int WAIT_SESSION_INFO = 2;

    private EventBus bus;

    private PublishSubject<WaitState> stateSubject;

    private Queue<Boolean> sessionQueue;

    /**
     * normally would use a ReplaySubject, but we only want to replay events from this session
     * as from WAIT_SESSION_START to WAIT_SESSION_END
     * after session has ended we clear sessionReplay
     * replaying is done using startWith operator;
     * </br>
     * see {@link WaitEventQueue#toObservable()}
     */
    private List<WaitState> sessionReplay;

    @Inject
    public WaitEventQueue(EventBus bus) {
        this.bus = bus;
        stateSubject = PublishSubject.create();
        sessionQueue = new ArrayDeque<>();
        sessionReplay = new ArrayList<>();
    }


    @Inject
    public void postInject() {
        bus.toObservable()
                .filter(e -> e.checkIdInGroupOther(EventID.WAIT))
                .subscribe(this::decide);
    }

    /**
     * Pushing when we have a waiting event and polling when a wait event is "done".
     * In the end the queue must be empty... any extra "done" waiting event after queue was empty,
     * must trigger a state exception
     * <br/>
     *
     * @param e wait event
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    void decide(Event<?> e) {
        Boolean isWaiting = (Boolean) e.data;
        if (isWaiting) {
            boolean wasEmpty = sessionQueue.isEmpty();
            sessionQueue.add(true);
            WaitState ws;
            if (wasEmpty) {
                ws = new WaitState(WAIT_SESSION_START, true);
                stateSubject.onNext(ws);
                sessionReplay.add(ws);
            }
            ws = new WaitState(WAIT_SESSION_INFO, sessionQueue.size());
            stateSubject.onNext(ws);
            sessionReplay.add(ws);
        } else {
            if (sessionQueue.isEmpty()) {
                throw new IllegalStateException("No more waiting states in queue");
            }
            sessionQueue.poll();
            if (sessionQueue.isEmpty()) {//is empty
                stateSubject.onNext(new WaitState(WAIT_SESSION_END, true));
                sessionReplay.clear();
            } else {
                WaitState ws = new WaitState(WAIT_SESSION_INFO, sessionQueue.size());
                stateSubject.onNext(ws);
                sessionReplay.add(ws);
            }
        }
    }

    public Observable<WaitState> toObservable() {
        return stateSubject.startWith(sessionReplay);
    }

    public static class WaitState {

        public final int type;

        public final Object data;

        public WaitState(int type, Object data) {
            this.type = type;
            this.data = data;
        }
    }
}
