package home.crskdev.biblereader.core;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.db.BaseRepository;
import home.crskdev.biblereader.core.db.BibleSQLiteOpenHelper;
import home.crskdev.biblereader.init.StreamedInitState;
import home.crskdev.biblereader.util.MathExt;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;

import static home.crskdev.biblereader.core.db.Contract.COL_BOOK_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_BOOK_TITLE;
import static home.crskdev.biblereader.core.db.Contract.COL_CHAPTERS;
import static home.crskdev.biblereader.core.db.Contract.COL_CHAPTER_ID;
import static home.crskdev.biblereader.core.db.Contract.COL_VERSET_ID;
import static home.crskdev.biblereader.core.db.Contract.INSERT_BOOK_SQ_PREP;
import static home.crskdev.biblereader.core.db.Contract.INSERT_CHAPTER_SQ_PREP;
import static home.crskdev.biblereader.core.db.Contract.SELECT_BOOKS_SQ;

/**
 * Created by criskey on 30/4/2017.
 */
@Singleton
public class BibleInfoLoader extends BaseRepository {

    public static final String STRING_RES_INFO_LOADER_SAVE = "info_loader_save";
    public static final String STRING_RES_INFO_LOADER_SAVE_DONE = "info_loader_save_done";
    public static final String STRING_RES_INFO_LOADER_INIT_DONE = "info_loader_init_done";

    private static final String INITIALIZED = "initialized";
    private SharedPreferences prefs;
    
    private BibleInfoMapper mapper;

    @Inject
    public BibleInfoLoader(SharedPreferences prefs, BibleSQLiteOpenHelper helper, BibleInfoMapper mapper) {
        super(helper);
        //TODO remove nativeDB when sqlbrite support rxjava 2;
        this.prefs = prefs;
        this.mapper = mapper;
    }

    public Single<BibleInfo> load() {
        //TODO addOrReplaceView with sqlbrite impl when it supports rxjava 2
        //todo refactor these make use of BaseRepository
        return Single.fromCallable(() -> {
            List<BibleInfo.BookInfo> bookInfos = new ArrayList<>();
            Cursor c = db().rawQuery(SELECT_BOOKS_SQ, null);
            c.moveToFirst();
            HashMap<Integer, Integer> chapters = new HashMap<>();
            while (!c.isAfterLast()) {
                String bookId = c.getString(c.getColumnIndexOrThrow(COL_BOOK_ID));
                String bookTitle = c.getString(c.getColumnIndexOrThrow(COL_BOOK_TITLE));
                int chapterId = c.getInt(c.getColumnIndexOrThrow(COL_CHAPTER_ID));
                int noOfChapters = c.getInt(c.getColumnIndexOrThrow(COL_CHAPTERS));
                int noOfVersets = c.getInt(c.getColumnIndexOrThrow(COL_VERSET_ID));
                chapters.put(chapterId, noOfVersets);
                c.moveToNext();
                if (!c.isAfterLast()) {
                    String nextBookId = c.getString(c.getColumnIndexOrThrow(COL_BOOK_ID));
                    if (!bookId.equals(nextBookId)) {
                        bookInfos.add(new BibleInfo.BookInfo(bookId, bookTitle, noOfChapters, chapters));
                        chapters = new HashMap<>();
                    }
                }
            }
            c.close();
            return new BibleInfo(bookInfos);
        }).cache();
    }

    public Single<Boolean> isInitialized() {
        return Single.defer(() -> Single.just(prefs.getBoolean(INITIALIZED, false)));
    }

    public Flowable<StreamedInitState> save() {
        //TODO addOrReplaceView with sqlbrite impl when it supports rxjava 2
        return Single
                .defer(() -> Single.just(mapper.toBibleInfo()))
                .toFlowable()
                .flatMap(bibleInfo -> Flowable.create((s) -> {
                    s.onNext(new StreamedInitState.ProgressSubStepMax(100, STRING_RES_INFO_LOADER_SAVE));
                    try {
                        db().beginTransaction();
                        SQLiteStatement bookPrep = db().compileStatement(INSERT_BOOK_SQ_PREP);
                        SQLiteStatement chPrep = db().compileStatement(INSERT_CHAPTER_SQ_PREP);
                        Collection<BibleInfo.BookInfo> bookInfos = bibleInfo.getBookInfos();
                        int count = 1;
                        for (BibleInfo.BookInfo bookInfo : bookInfos) {
                            bookPrep.bindString(1, bookInfo.id);
                            bookPrep.bindString(2, bookInfo.title);
                            bookPrep.bindLong(3, bookInfo.noOfChapters);
                            bookPrep.executeInsert();
                            for (int chId : bookInfo.getChapterIds()) {
                                chPrep.bindLong(1, chId);
                                chPrep.bindString(2, bookInfo.id);
                                chPrep.bindLong(3, bookInfo.getVersetsOf(chId));
                                chPrep.executeInsert();
                            }
                            int normal = MathExt.normalize100(count++, bookInfos.size());
                            s.onNext(new StreamedInitState.SubTick(normal, bookInfo.title));
                        }
                        s.onNext(new StreamedInitState.SubTick(100, STRING_RES_INFO_LOADER_SAVE_DONE));
                        chPrep.close();
                        bookPrep.close();
                        db().setTransactionSuccessful();

                        prefs.edit().putBoolean(INITIALIZED, true).apply();
                        s.onNext(new StreamedInitState.Wait(true, STRING_RES_INFO_LOADER_INIT_DONE));

                        s.onComplete();
                    } catch (Exception ex) {
                        s.onError(ex);
                    } finally {
                        db().endTransaction();
                    }
                }, BackpressureStrategy.LATEST));


    }

}
