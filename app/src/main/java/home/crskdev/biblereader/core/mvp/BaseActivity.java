package home.crskdev.biblereader.core.mvp;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.DispatchingArgumentsAndroidInjector;
import home.crskdev.biblereader.core.di.HasArgumentsInjector;
import home.crskdev.biblereader.util.ViewUtils;
/**
 * Created by criskey on 13/5/2017.
 */
public abstract class BaseActivity extends DaggerAppCompatActivity implements MVPSetup,
        MVPLifecycle,
        IView, HasArgumentsInjector {

    public static final String INTENT_BUNDLE_KEY = "INTENT_BUNDLE_KEY";

    @Inject
    DispatchingArgumentsAndroidInjector<View> viewInjector;

    private BackPressedHandler mBackHandler;

    private ViewUtils.SoftKeyboardWatcher softKeyboardWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int layout = layoutId();
        if (layout != NO_CONTENT) {
            setContentView(layout);
        } else {
            setTheme(android.R.style.Theme_NoDisplay);
        }
        ButterKnife.bind(this);
        softKeyboardWatcher = new ViewUtils.SoftKeyboardWatcher(this);
        onPreAttachPresenter();
        setupPresenter().attach(setupView());
        onPostAttachPresenter();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        softKeyboardWatcher.overrideDispatchTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    public void setBackPressedHandler(BackPressedHandler handler) {
        this.mBackHandler = handler;
    }

    @Override
    protected void onDestroy() {
        softKeyboardWatcher.disconnect();
        onPreDetachPresenter();
        setupPresenter().detach();
        mBackHandler = null;
        super.onDestroy();
    }

    /**
     * init here anything that is usually done on onCreate (add toolbar, set adapters etc)
     */
    @Override
    public void onPreAttachPresenter() {
    }

    /**
     * init here anything to be initialized after presenter was plugged with view
     */
    @Override
    public void onPostAttachPresenter() {
    }

    @Override
    public void onPreDetachPresenter() {
    }

    @Override
    public void onBackPressed() {
        if (mBackHandler == null || !mBackHandler.onBackPressedHandled()) {
            super.onBackPressed();
        }
    }

    @Override
    public Map<String, ?> getArgs() {
        Bundle bundle = getIntent().getBundleExtra(INTENT_BUNDLE_KEY);
        return (bundle == null) ? Collections.emptyMap() : ViewUtils.toMap(bundle);
    }

    @Override
    public AndroidArgumentsInjector<View> argumentsInjector() {
        return viewInjector;
    }

    public interface BackPressedHandler {
        boolean onBackPressedHandled();
    }
}
