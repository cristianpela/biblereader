package home.crskdev.biblereader.core.di;

import android.view.View;

/**
 * Created by criskey on 4/6/2017.
 */

public interface HasArgumentsInjector {

    AndroidArgumentsInjector<View> argumentsInjector();
}