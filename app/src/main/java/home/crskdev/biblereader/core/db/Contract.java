package home.crskdev.biblereader.core.db;

/**
 * Created by criskey on 10/5/2017.
 */

public interface Contract {

    String DB_NAME = "bible.db";

    int DB_VERSION = 9;

    String TABLE_BOOK_INFO = "table_book_info";

    String TABLE_CHAPTER_INFO = "table_chapter_info";

    String TABLE_FAVORITES_INFO = "table_favorites_info";

    String TABLE_TAG_INFO = "table_tag_info";

    String TABLE_FAVORITES_TAG_INFO = "table_favorites_tag_info";

    String COL_ID = "_id";

    String COL_BOOK_ID = "book_id";

    String COL_CHAPTER_ID = "chapter_id";

    String COL_BOOK_TITLE = "title";

    String COL_CHAPTERS = "noOfChapters";

    String COL_VERSET_ID = "versets";

    String COL_DATE = "date";

    String COL_FAVORITE = "fav";

    String COL_TAG_NAME = "tag";

    String COL_FAVTAG_TAG_ID = "fav_tag_tagid";

    String COL_FAVTAG_FAV_BOOK_ID = "fav_tag_fav_book_id";

    String COL_FAVTAG_FAV_CHAPTER_ID = "fav_tag_fav_chapter_id";

    String COL_FAVTAG_FAV_VERSETS = "fav_tag_fav_versets";

    String COL_VERSET_CONTENT = "verset_content";

    //scripts

    String ENABLE_FOREIGN_KEY_SUPPORT = "PRAGMA foreign_keys = ON;";

    String CREATE_BOOK_TABLE_SQ = "CREATE TABLE IF NOT EXISTS " + TABLE_BOOK_INFO + "(" +
            COL_BOOK_ID + " TEXT PRIMARY KEY NOT NULL, " +
            COL_BOOK_TITLE + " TEXT NOT NULL, " +
            COL_CHAPTERS + " INTEGER NOT NULL);";


    String INSERT_BOOK_SQ_PREP = "INSERT INTO " + TABLE_BOOK_INFO + " VALUES(?,?,?)";

    String CREATE_CHAPTER_TABLE_SQ = "CREATE TABLE IF NOT EXISTS " + TABLE_CHAPTER_INFO + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_CHAPTER_ID + " INTEGER NOT NULL, " +
            COL_BOOK_ID + " TEXT NOT NULL, " +
            COL_VERSET_ID + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + COL_BOOK_ID + ") REFERENCES " + TABLE_BOOK_INFO + "(" + COL_BOOK_ID + "));";


    String CREATE_FAVORITES_TABLE_SQ = "CREATE TABLE IF NOT EXISTS " + TABLE_FAVORITES_INFO + "(" +
            COL_CHAPTER_ID + " INTEGER NOT NULL, " +
            COL_BOOK_ID + " TEXT NOT NULL, " +
            COL_VERSET_ID + " INTEGER NOT NULL, " +
            COL_VERSET_CONTENT + " TEXT NOT NULL, " +
            COL_FAVORITE + " BOOLEAN NOT NULL CHECK (" + COL_FAVORITE + " IN (0,1)), " +
            COL_DATE + " INTEGER NOT NULL, " +
            "PRIMARY KEY (" + COL_BOOK_ID + "," + COL_CHAPTER_ID + "," + COL_VERSET_ID + "));";


    String CREATE_TAG_TABLE_SQ = "CREATE TABLE IF NOT EXISTS " + TABLE_TAG_INFO + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_TAG_NAME + " TEXT NOT NULL UNIQUE CHECK(LENGTH(" + COL_TAG_NAME + ")<=20));";

    String CREATE_FAVORITES_TAG_TABLE_SQ = "CREATE TABLE IF NOT EXISTS " + TABLE_FAVORITES_TAG_INFO + "(" +
            COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COL_FAVTAG_TAG_ID + " INTEGER NOT NULL, " +
            COL_FAVTAG_FAV_CHAPTER_ID + " INTEGER NOT NULL, " +
            COL_FAVTAG_FAV_BOOK_ID + " TEXT NOT NULL, " +
            COL_FAVTAG_FAV_VERSETS + " INTEGER NOT NULL, " +
            "FOREIGN KEY(" + COL_FAVTAG_FAV_CHAPTER_ID + ","
            + COL_FAVTAG_FAV_BOOK_ID + ","
            + COL_FAVTAG_FAV_VERSETS + ") REFERENCES " + TABLE_FAVORITES_INFO + "("
            + COL_CHAPTER_ID + ","
            + COL_BOOK_ID + ","
            + COL_VERSET_ID + "), " +
            "FOREIGN KEY (" + COL_FAVTAG_TAG_ID + ") REFERENCES " + TABLE_TAG_INFO + "(" + COL_ID + ") ON DELETE CASCADE );";


    String INSERT_CHAPTER_SQ_PREP = "INSERT INTO " + TABLE_CHAPTER_INFO + "(" +
            COL_CHAPTER_ID + "," + COL_BOOK_ID + "," + COL_VERSET_ID + ") VALUES(?,?,?)";

    String SELECT_BOOKS_SQ = "SELECT tb.*, tc." + COL_CHAPTER_ID + ", tc." + COL_VERSET_ID + " FROM "
            + TABLE_BOOK_INFO + " AS tb, "
            + TABLE_CHAPTER_INFO + " AS tc WHERE tb." + COL_BOOK_ID + "=tc." + COL_BOOK_ID;


    class Exec {
        static String dropTableSQ(String tableName) {
            return "DROP TABLE IF EXISTS " + tableName;
        }
    }

}
