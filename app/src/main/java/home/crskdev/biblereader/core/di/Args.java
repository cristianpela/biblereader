package home.crskdev.biblereader.core.di;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by criskey on 20/6/2017.
 */
@Qualifier
@Retention(RUNTIME)
public @interface Args {
}
