package home.crskdev.biblereader.core.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.DispatchingArgumentsAndroidInjector;
import home.crskdev.biblereader.core.di.HasArgumentsInjector;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 12/5/2017.
 */
public abstract class BaseFragment extends DaggerFragment implements MVPSetup, MVPLifecycle,
        IFragmentView, HasArgumentsInjector {

    @Inject
    DispatchingArgumentsAndroidInjector<View> viewInjector;

    private boolean instanceSaved;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        instanceSaved = false;
        View view = inflater.inflate(layoutId(), container, false);
        ButterKnife.bind(this, view);
        attachPresenter();
        return view;
    }

    @Override
    public void onResume() {
        instanceSaved = false;
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        instanceSaved = true;
        detachPresenter();
        super.onDestroyView();
    }

    @Override
    @CallSuper
    public void onSaveInstanceState(Bundle outState) {
        instanceSaved = true;
    }

    public void attachPresenter() {
        onPreAttachPresenter();
        setupPresenter().attach(setupView());
        onPostAttachPresenter();
    }

    public void detachPresenter() {
        onPreDetachPresenter();
        setupPresenter().detach();
    }

    /**
     * init here anything that is usually done on onCreate (add toolbar, set adapters etc)
     */
    @Override
    public void onPreAttachPresenter() {
    }

    /**
     * init here anything to be initialized after presenter was plugged with view
     */
    @Override
    public void onPostAttachPresenter() {
    }

    @Override
    public void onPreDetachPresenter() {
    }

    protected void setBackPressedHandler(BaseActivity.BackPressedHandler handler) {
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.setBackPressedHandler(handler);
        } else {
            throw new IllegalArgumentException("In order to support backpressed handler, " +
                    "the fragment must be attached to a BaseActivity inherit instance");
        }
    }

    @Override
    public boolean isInstanceSaved() {
        return instanceSaved;
    }

    @Override
    public Map<String, ?> getArgs() {
        Bundle bundle = getArguments();
        return (bundle == null) ? Collections.emptyMap() : ViewUtils.toMap(bundle);
    }

    @Override
    public AndroidArgumentsInjector<View> argumentsInjector() {
        return viewInjector;
    }

}
