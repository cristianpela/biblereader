package home.crskdev.biblereader.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import home.crskdev.biblereader.util.TextUtils;

/**
 * Created by criskey on 30/4/2017.
 */

public class BibleInfo {

    private Collection<BookInfo> bookInfos;

    //use it for tests only
    public BibleInfo(Collection<BookInfo> bookInfos) {
        this.bookInfos = bookInfos;
    }

    public Collection<BookInfo> getBookInfos() {
        return Collections.unmodifiableCollection(bookInfos);
    }

    public BookInfo getBookStartWith(String startsWith) {
        for (BookInfo bookInfo : bookInfos) {
            if (TextUtils.startsWithIgnoreDiacritics(bookInfo.title.toLowerCase(),
                    startsWith.toLowerCase()))
                return bookInfo;
        }
        return BookInfo.NO_BOOK;
    }


    public List<BookInfo> getBooksContains(String contains) {
        List<BookInfo> filtered = new ArrayList<>();
        for (BookInfo bookInfo : bookInfos) {
            if (TextUtils.containsIgnoreDiacritics(bookInfo.title.toLowerCase(),
                    contains.toLowerCase()))
                filtered.add(bookInfo);
        }
        return filtered;
    }

    public BookInfo getBook(String bookName) {
        for (BookInfo bookInfo : bookInfos) {
            if (TextUtils.equalsIgnoreDiacriticsAndCase(bookInfo.title, bookName))
                return bookInfo;
        }
        return BookInfo.NO_BOOK;
    }

    public boolean hasBook(String bookName) {
        return !getBook(bookName).equals(BookInfo.NO_BOOK);
    }

    public BookInfo getBookById(String id) {
        for (BookInfo bookInfo : bookInfos) {
            if (bookInfo.id.equals(id))
                return bookInfo;
        }
        return BookInfo.NO_BOOK;
    }


    public Collection<String> getBookNames() {
        List<String> names = new ArrayList<>(bookInfos.size());
        for (BookInfo bookInfo : bookInfos) {
            names.add(bookInfo.title);
        }
        return Collections.unmodifiableCollection(names);
    }


    public static class BookInfo {

        public static final BookInfo NO_BOOK = new BookInfo(null, null, -1, null);

        public final String id;

        public final String title;

        public final int noOfChapters;

        private final HashMap<Integer, Integer> chapters;

        public BookInfo(String id, String title, int noOfChapters, HashMap<Integer, Integer> chapters) {
            this.id = id;
            this.title = title;
            this.noOfChapters = noOfChapters;
            this.chapters = chapters;
        }

        public int getVersetsOf(int chapter) {
            return chapters.get(chapter);
        }

        public Set<Integer> getChapterIds() {
            SortedSet<Integer> set = new TreeSet<>(chapters.keySet());
            return Collections.unmodifiableSet(set);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            BookInfo bookInfo = (BookInfo) o;

            if (noOfChapters != bookInfo.noOfChapters) return false;
            if (!id.equals(bookInfo.id)) return false;
            if (!title.equals(bookInfo.title)) return false;
            return chapters.equals(bookInfo.chapters);

        }

        @Override
        public int hashCode() {
            int result = id.hashCode();
            result = 31 * result + title.hashCode();
            result = 31 * result + noOfChapters;
            result = 31 * result + chapters.hashCode();
            return result;
        }

        public static class Builder {

            private String id;
            private String title;
            private HashMap<Integer, Integer> versets;

            private int currChapter;

            public Builder() {
                versets = new HashMap<>();
            }

            public Builder setId(String id) {
                this.id = id;
                return this;
            }

            public Builder setTitle(String title) {
                this.title = title;
                return this;
            }

            public Builder setChapters(int chapter) {
                this.versets.put(chapter, 0);
                this.currChapter = chapter;
                return this;
            }

            public Builder addVerset(int chapter, int verset) {
                this.versets.put(chapter, verset);
                return this;
            }

            public Builder incCurrChapterVerset() {
                int verset = this.versets.get(currChapter);
                addVerset(currChapter, verset + 1);
                return this;
            }

            public Builder incCurrChapterVersetBy(int times) {
                for (int i = 0; i < times; i++) {
                    incCurrChapterVerset();
                }
                return this;
            }

            public BookInfo createBook() {
                return new BookInfo(id, title, currChapter, versets);
            }
        }
    }
}
