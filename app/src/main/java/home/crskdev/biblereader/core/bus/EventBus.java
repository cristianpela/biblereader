package home.crskdev.biblereader.core.bus;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.util.asserts.Asserts;
import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by criskey on 2/5/2017.
 */
@Singleton
public final class EventBus {

    protected PublishSubject<Event<?>> subject;

    private MainThreadDispatcher mainThreadDispatcher;

    @Inject
    public EventBus(MainThreadDispatcher mainThreadDispatcher) {
        this.mainThreadDispatcher = mainThreadDispatcher;
        subject = PublishSubject.create();
    }

    public Disposable subscribe(Consumer<Event<?>> consumer) {
        return subject.subscribe(consumer);
    }

    public void dispatch(Event<?> event) {
        if (mainThreadDispatcher.isMainThread()) {
            subject.onNext(event);
        } else {
            mainThreadDispatcher.dispatch(() -> subject.onNext(event));
        }
    }

    public Observable<Event<?>> toObservable() {
        return subject;
    }

    //todo: merge this into toObservable by default
    public Observable<Event<?>> toThreadSafeObservable() {
        return subject.serialize();
    }
}
