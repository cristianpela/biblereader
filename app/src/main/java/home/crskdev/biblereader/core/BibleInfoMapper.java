package home.crskdev.biblereader.core;

import org.codehaus.stax2.XMLInputFactory2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.namespace.QName;

import edu.usf.cutr.javax.xml.stream.XMLStreamException;
import edu.usf.cutr.javax.xml.stream.XMLStreamReader;
import home.crskdev.biblereader.core.BibleInfo.BookInfo.Builder;

import static edu.usf.cutr.javax.xml.stream.XMLStreamConstants.START_ELEMENT;

/**
 * Created by criskey on 30/4/2017.
 */
@Singleton
public class BibleInfoMapper {

    private XMLInputFactory2 input;

    private File file;

    @Inject
    public BibleInfoMapper(XMLInputFactory2 input, @Named("bible-path")File file) {
        this.input = input;
        this.file = file;
    }

    public BibleInfo toBibleInfo() throws XMLStreamException, FileNotFoundException {
        XMLStreamReader reader = input.createXMLStreamReader(new FileInputStream(file));
        Builder bookBuilder = null;
        Collection<BibleInfo.BookInfo> bookInfos = new ArrayList<>();
        while (reader.hasNext()) {
            int ev = reader.getEventType();
            switch (ev) {
                case START_ELEMENT:
                    if (elEq(reader.getName(), BibleXMLTags.BOOK_TAG)) {
                        bookBuilder = refreshBuilder(bookBuilder, bookInfos);
                        bookBuilder.setId(reader.getAttributeValue(0));
                    } else if (elEq(reader.getName(), BibleXMLTags.TITLE_TAG)) {
                        bookBuilder.setTitle(reader.getElementText());
                    } else if (elEq(reader.getName(), BibleXMLTags.CHAPTER_TAG)) {
                        bookBuilder.setChapters(Integer.parseInt(reader.getAttributeValue(0)));
                    } else if (elEq(reader.getName(), BibleXMLTags.VERSET_TAG)) {
                        bookBuilder.incCurrChapterVerset();
                    }
                    break;
            }
            reader.next();
        }
        return new BibleInfo(bookInfos);
    }

    private boolean elEq(QName name, String expected) {
        return name.toString().equals(expected);
    }

    private Builder refreshBuilder(Builder bookBuilder, Collection<BibleInfo.BookInfo> bookInfos) {
        if (bookBuilder != null) {
            bookInfos.add(bookBuilder.createBook());
        }
        return new Builder();
    }
}
