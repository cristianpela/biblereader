package home.crskdev.biblereader.core.db;

import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

/**
 * Created by criskey on 12/6/2017.
 */

class ImmutableCursor implements Cursor {
    
    private Cursor delegate;

    ImmutableCursor(Cursor delegate) {
        this.delegate = delegate;
    }

    @Override
    public int getColumnIndex(String columnName) {
        return delegate.getColumnIndex(columnName);
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        return delegate.getColumnIndexOrThrow(columnName);
    }

    @Override
    public short getShort(int columnIndex) {
        return delegate.getShort(columnIndex);
    }

    @Override
    public int getInt(int columnIndex) {
        return delegate.getInt(columnIndex);
    }

    @Override
    public long getLong(int columnIndex) {
        return delegate.getLong(columnIndex);
    }

    @Override
    public float getFloat(int columnIndex) {
        return delegate.getFloat(columnIndex);
    }

    @Override
    public double getDouble(int columnIndex) {
        return delegate.getDouble(columnIndex);
    }

    @Override
    public int getType(int columnIndex) {
        return delegate.getType(columnIndex);
    }

    @Override
    public boolean isNull(int columnIndex) {
        return delegate.isNull(columnIndex);
    }

    @Override
    public void deactivate() {

    }

    @Override
    public boolean requery() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public void close() {

    }

    @Override
    public boolean isClosed() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public void registerContentObserver(ContentObserver observer) {

    }

    @Override
    public void unregisterContentObserver(ContentObserver observer) {

    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void setNotificationUri(ContentResolver cr, Uri uri) {

    }

    @Override
    public Uri getNotificationUri() {
        return null;
    }

    @Override
    public boolean getWantsAllOnMoveCalls() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public void setExtras(Bundle extras) {

    }

    @Override
    public Bundle getExtras() {
        return null;
    }

    @Override
    public Bundle respond(Bundle extras) {
        return null;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return delegate.getColumnName(columnIndex);
    }

    @Override
    public String[] getColumnNames() {
        return delegate.getColumnNames();
    }

    @Override
    public int getColumnCount() {
        return delegate.getColumnCount();
    }

    @Override
    public byte[] getBlob(int columnIndex) {
        return delegate.getBlob(columnIndex);
    }

    @Override
    public String getString(int columnIndex) {
        return delegate.getString(columnIndex);
    }

    @Override
    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
        throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public int getCount() {
        return delegate.getCount();
    }

    @Override
    public int getPosition() {
        return delegate.getPosition();
    }

    @Override
    public boolean move(int offset) {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean moveToPosition(int position) {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean moveToFirst() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean moveToLast() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean moveToNext() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean moveToPrevious() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean isFirst() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean isLast() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean isBeforeFirst() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }

    @Override
    public boolean isAfterLast() {
         throw new UnsupportedOperationException("Unsupported method. Cursor is immutable");
    }
}
