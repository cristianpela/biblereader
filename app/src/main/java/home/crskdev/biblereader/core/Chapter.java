package home.crskdev.biblereader.core;

/**
 * Created by criskey on 6/6/2017.
 */

public class Chapter implements Content {

    public final int id;

    public final String bookId;

    public final String title;

    public Chapter(int id, String bookId, String title) {
        this.id = id;
        this.bookId = bookId;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Chapter chapter = (Chapter) o;

        if (id != chapter.id) return false;
        return bookId != null ? bookId.equals(chapter.bookId) : chapter.bookId == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (bookId != null ? bookId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Chapter{" +
                "id=" + id +
                ", bookId='" + bookId + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    @Override
    public String formalTitle() {
        throw new UnsupportedOperationException();
    }
}


