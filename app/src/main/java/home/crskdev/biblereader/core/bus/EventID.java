package home.crskdev.biblereader.core.bus;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static home.crskdev.biblereader.core.bus.EventID.*;
import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by criskey on 2/5/2017.
 */
@Retention(SOURCE)
@IntDef({
        SEARCH_RESULT,
        SEARCH_ERROR,
        SEARCH_RESULT_IN_PROGRESS,

        READ_NAV_JUMP,
        READ_NAV_NEXT,
        READ_NAV_PREV,
        READ_NAV_RESULT,
        READ_NAV_JUMP_DIALOG,

        FAVORITE_CHANGED,
        FAVORITE_SELECT,
        FAVORITE_BACK_TO,
        FAVORITE_COLLAPSE_FILTER_SHEET,

        TAG_ADD,
        TAG_EDIT,
        TAG_DELETE,
        TAG_SELECT,
        TAG_UNDO,
        TAG_NONE,
        TAG_RELOAD,
        TAG_DIALOG_DELETE,
        TAG_DIALOG_EDIT,

        MEMORY_LEVEL,

        WAIT,
        ERROR,
        TAB,
        NOTIFICATION,
        RESET
})
public @interface EventID {

    int SEARCH_RESULT = 100;
    int SEARCH_ERROR = 101;
    int SEARCH_RESULT_IN_PROGRESS = 102;

    int READ_NAV_NEXT = 200;
    int READ_NAV_PREV = 201;
    int READ_NAV_JUMP = 202;
    int READ_NAV_RESULT = 204;
    int READ_NAV_JUMP_DIALOG = 208;

    int FAVORITE_CHANGED = 300;
    int FAVORITE_SELECT = 302;
    int FAVORITE_BACK_TO = 304;
    int FAVORITE_COLLAPSE_FILTER_SHEET = 308;

    int TAG_NONE = 400;
    int TAG_ADD = 401;
    int TAG_DELETE = 402;
    int TAG_EDIT = 404;
    int TAG_UNDO = 408;
    int TAG_SELECT = 416;
    int TAG_RELOAD = 465;
    int TAG_DIALOG_DELETE = 466;
    int TAG_DIALOG_EDIT = 467;

    int MEMORY_LEVEL = 500;

    int WAIT = 600;
    int ERROR = 601;
    int TAB = 602;
    int NOTIFICATION = 603;
    int RESET = 604;
}
