package home.crskdev.biblereader.core.state;

/**
 * Created by criskey on 4/8/2017.
 */
public interface ResetableState {

    <R extends ResetableState> R reset();

}
