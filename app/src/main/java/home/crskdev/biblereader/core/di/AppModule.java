package home.crskdev.biblereader.core.di;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import home.crskdev.biblereader.core.BibleDownloader;
import home.crskdev.biblereader.core.db.Contract;
import home.crskdev.biblereader.util.plafadapters.AndroidMainThreadDispatcher;
import home.crskdev.biblereader.util.plafadapters.MainThreadDispatcher;
import home.crskdev.biblereader.util.plafadapters.SparseArrayFactory;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by criskey on 30/4/2017.
 */

@Module(includes = {XMLModule.class, BindingModule.class, DBModule.class, RxModule.class})
public class AppModule {

    private Context context;

    private String downloadURL;

    public AppModule(Context context, String downloadURL) {
        this.context = context;
        this.downloadURL = downloadURL;

    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    public SharedPreferences providePreferences() {
        return context.getSharedPreferences("bible_info", Context.MODE_PRIVATE);
    }

    @Provides
    @Named("download-url")
    public String provideDownloadUrl() {
        return downloadURL;
    }

    @Provides
    @Singleton
    @Named("bible-path")
    public File provideDownloadPath(Context context) {
        return new File(context.getFilesDir(), BibleDownloader.FILE_NAME);
    }

    @Provides
    @Singleton
    @Named("db-path")
    public File provideDatabasePath(Context context) {
        return context.getDatabasePath(Contract.DB_NAME);
    }

    @Provides
    @Singleton
    public Scheduler provideMainScheduler() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    public SparseArrayFactory provideSparseArrayFactory() {
        return new SparseArrayFactory.Android();
    }

    @Provides
    @Singleton
    public MainThreadDispatcher provideMainThreadDispatcher() {
        return new AndroidMainThreadDispatcher();
    }
}
