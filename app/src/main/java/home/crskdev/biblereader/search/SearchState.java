package home.crskdev.biblereader.search;

import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.state.ResetableState;

/**
 * Created by criskey on 9/7/2017.
 */
public class SearchState implements ResetableState {

    public static final SearchState DEFAULT = new SearchState(false, Book.NO_ID, null,
            Collections.emptyList());

    public final boolean inProgress;

    public final String phrase;

    public final String bookId;

    public final List<Verset> foundVersets;


    public SearchState(boolean inProgress, @NonNull String bookId, String phrase, List<Verset> foundVersets) {
        this.inProgress = inProgress;
        this.phrase = phrase;
        this.bookId = bookId;
        this.foundVersets = foundVersets;
    }

    public SearchState inProgress(boolean inProgress) {
        return new SearchState(inProgress, bookId, phrase, foundVersets);
    }

    public SearchState bookId(String bookId) {
        return new SearchState(inProgress, bookId, phrase, foundVersets);
    }

    public SearchState phrase(String phrase) {
        return new SearchState(inProgress, bookId, phrase, foundVersets);
    }

    public SearchState foundVersets(List<Verset> versets) {
        return new SearchState(inProgress, bookId, phrase, versets);
    }

    @Override
    public SearchState reset() {
        return DEFAULT;
    }
}
