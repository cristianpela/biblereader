package home.crskdev.biblereader.search;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import home.crskdev.biblereader.util.ViewUtils;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 10/7/2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultVH> {

    private List<Verset> versets;

    public SearchResultAdapter() {
        versets = Collections.emptyList();
    }

    @Override
    public SearchResultVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fav_recy_item, parent, false);
        return new SearchResultVH(view);
    }

    @Override
    public void onBindViewHolder(SearchResultVH holder, int position) {
        Verset v = versets.get(position);
        holder.txtContent.setText(v.content);
        holder.txtSrc.setText(v.formalTitle());
        int color = ContextCompat.getColor(holder.itemView.getContext(),
                v.favorite ? R.color.colorFavorite : R.color.colorBackground);
        holder.card.setCardBackgroundColor(color);
    }

    @Override
    public int getItemCount() {
        return versets.size();
    }

    public void setSearchResults(List<Verset> versets) {
        this.versets = versets;
        notifyDataSetChanged();
    }

    static class SearchResultVH extends ButterKnifeViewHolder {

        @BindView(R.id.txt_fav_content)
        TextView txtContent;
        @BindView(R.id.txt_fav_src)
        TextView txtSrc;
        @BindView(R.id.card_fav)
        CardView card;

        SearchResultVH(View itemView) {
            super(itemView);
        }
    }
}
