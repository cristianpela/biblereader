package home.crskdev.biblereader.search;

import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesAlerts;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;

/**
 * Created by criskey on 12/5/2017.
 */
public class SearchResultViewFragment extends BaseFragment implements SearchResultView {

    @BindView(R.id.recy_search_result)
    TapableRecyclerView recyclerView;

    @Inject
    SearchResultPresenter presenter;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.search_result_layout;
    }

    @Override
    public void onPreAttachPresenter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new SearchResultAdapter());
    }

    @Override
    public void onPostAttachPresenter() {
        presenter.processTap(recyclerView.tapObservable());
    }

    @Override
    public void onDisplay(List<Verset> items) {
        ((SearchResultAdapter) recyclerView.getAdapter()).setSearchResults(items);
    }

    @Override
    public void onSelect(int index) {
        recyclerView.getAdapter().notifyItemChanged(index);
    }

    @Override
    public void onConfirmToggleFavorite(Verset item, int index) {
        FavoritesAlerts.favoriteConfirmDialogBuilder(getContext(), item, (dialog, which) -> {
            presenter.confirmFavoriteToggle(index);
        }, null).create().show();
    }

    @Override
    public void onChooseTabToShow(Verset verset, String[] choices) {
        new AlertDialog.Builder(new ContextThemeWrapper(getContext(), R.style.AppTheme_Dialog))
                .setSingleChoiceItems(choices, -1, (dialog, which) -> {
                    dialog.dismiss();
                    presenter.confirmTabToShow(verset, which);
                })
                .setCancelable(true)
                .create()
                .show();
    }

    @Override
    public void onConfirmVersetToShow(Verset verset) {
        FavoritesAlerts.favoriteConfirmShow(getContext(), verset, (d, w) ->
                presenter.confirmVersetToShow(verset)).create().show();
    }
}
