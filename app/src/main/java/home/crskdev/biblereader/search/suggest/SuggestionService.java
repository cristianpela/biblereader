package home.crskdev.biblereader.search.suggest;

import android.support.annotation.WorkerThread;

import org.codehaus.stax2.XMLInputFactory2;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import edu.usf.cutr.javax.xml.stream.XMLStreamException;
import edu.usf.cutr.javax.xml.stream.XMLStreamReader;
import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableTransformer;
import io.reactivex.schedulers.Schedulers;

import static edu.usf.cutr.javax.xml.stream.XMLStreamConstants.START_ELEMENT;

/**
 * Created by criskey on 29/7/2017.
 */
public class SuggestionService {

    private static final String BASE_URL = "http://google.com/complete/search?output=toolbar&hl=ro&q=";
    private XMLInputFactory2 input;
    private SuggestionURLEncoder urlEncoder;

    @Inject
    public SuggestionService(XMLInputFactory2 input) {
        this(input, q -> {
            try {
                String encodedQ = URLEncoder.encode(q, "UTF-8");
                return new URL(BASE_URL + encodedQ);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        });
    }

    public SuggestionService(XMLInputFactory2 input, SuggestionURLEncoder urlEncoder) {
        this.input = input;
        this.urlEncoder = urlEncoder;
    }

    public Flowable<CharSequence> fetch(String q) {
        return Flowable.create(e -> fetch(e, urlEncoder.encodeUrl(q)), BackpressureStrategy.LATEST);
    }

    private void fetch(FlowableEmitter<CharSequence> emitter, URL url)
            throws IOException, XMLStreamException {
        Asserts.assertIsBackgroundThread();
        XMLStreamReader reader = input.createXMLStreamReader(url.openStream());
        while (!emitter.isCancelled() && reader.hasNext()) {
            int ev = reader.getEventType();
            switch (ev) {
                case START_ELEMENT:
                    String elName = reader.getName().toString();
                    if (elName.equals("suggestion")) {
                        emitter.onNext(reader.getAttributeValue(0));
                    }
                    break;
                default:
                    break;
            }
            reader.next();
        }
        reader.close();
        emitter.onComplete();
    }

    @WorkerThread
    public FlowableTransformer<CharSequence, CharSequence> debounceFetch(long millis) {
        return up -> up.debounce(millis, TimeUnit.MILLISECONDS)
                .switchMap(c -> fetch(c.toString())
                        .defaultIfEmpty("")
                        .onErrorReturnItem("")
                        .subscribeOn(Schedulers.io()));
    }

    interface SuggestionURLEncoder {
        URL encodeUrl(String q);
    }
}
