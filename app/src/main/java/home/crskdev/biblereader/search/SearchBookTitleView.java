package home.crskdev.biblereader.search;

import java.util.List;

import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.favorites.detail.CloseableView;

/**
 * Created by criskey on 11/7/2017.
 */

public interface SearchBookTitleView extends CloseableView {

    void onLoadBookTitles(int indexOfSelect, List<Book> books);

    void onFilterBookTitles(int indexOfSelect, List<Book> filteredBooks);

    void onSelection(String title);
}
