package home.crskdev.biblereader.search.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.search.SearchResultViewFragment;

/**
 * Created by criskey on 12/5/2017.
 */
@PerFragment
@Subcomponent
public interface SearchResultComponent extends AndroidInjector<SearchResultViewFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SearchResultViewFragment> {}
}
