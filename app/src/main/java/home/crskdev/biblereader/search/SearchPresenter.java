package home.crskdev.biblereader.search;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.WaitEventDispatcher;
import home.crskdev.biblereader.core.di.GlobalDisposablesManager;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.search.SearchService.HighlightPhraseVerset;
import home.crskdev.biblereader.util.Data;
import home.crskdev.biblereader.util.ImmLists;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.RxUtils;
import home.crskdev.biblereader.util.TextUtils;
import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 2/5/2017.
 */
@PerFragment
public class SearchPresenter extends Presenter<SearchView> {

    private static final String TAG = SearchPresenter.class.getSimpleName();

    private static final String PHRASE_SEARCH_DISPOSABLE_KEY = "PHRASE_SEARCH_DISPOSABLE_KEY";
    private final FavoritesService favoriteService;
    private final GlobalDisposablesManager globalDisposables;
    private final WaitEventDispatcher waitEventDispatcher;

    private SearchService searchService;

    private String changedBookId;
    private String changedPhrase = "";

    @Inject
    public SearchPresenter(SearchService searchService,
                           FavoritesService favoriteService,
                           Scheduler uiScheduler,
                           StateContainer stateContainer,
                           GlobalDisposablesManager globalDisposables,
                           EventBus bus,
                           WaitEventDispatcher waitEventDispatcher) {
        super(bus, stateContainer, uiScheduler);
        this.searchService = searchService;
        this.favoriteService = favoriteService;
        this.globalDisposables = globalDisposables;
        this.waitEventDispatcher = waitEventDispatcher;
    }

    @Override
    protected void postAttach() {
        disposables.add(stateContainer.subjectSearchState
                .subscribe(state -> {
                    view.onEnableSearch(!state.inProgress);
                    view.onCurrentSearch(state.bookId.equals(Book.NO_ID) ? "..." : state.bookId
                            , state.phrase);
                    changedBookId = state.bookId;
                    changedPhrase = state.phrase;
                }));
    }

    @Override
    protected void postDetach() {
        SearchState value = stateContainer.subjectSearchState.getValue();
        stateContainer.subjectSearchState.onNext(value
                .bookId(changedBookId)
                .phrase(changedPhrase)
        );
    }

    public void changePhrase(Observable<CharSequence> changePhraseObs) {
        Observable<CharSequence> dataObservable = changePhraseObs
                .skip(1)
                .compose(RxUtils.defaultDebounce())
                .observeOn(uiScheduler);
        disposables.add(dataObservable.subscribe(phrase -> {
            changedPhrase = phrase.toString();
        }));
    }

    public void showBooksDialog() {
        view.onShowBooksDialog();
    }

    public void cancel() {
        globalDisposables.dispose(PHRASE_SEARCH_DISPOSABLE_KEY);
    }

    public void search() {

        FlowableTransformer<String, HighlightPhraseVerset> foundVersetsTransform = up ->
                up.flatMap(phrase -> searchService.findPhrase(changedBookId, phrase));

        SingleTransformer<List<HighlightPhraseVerset>, List<Verset>> spannedListVersetTransform = up ->
                up.flatMap(highlights -> {
                    Asserts.assertIsBackgroundThread();
                    //we have no results
                    if (highlights.isEmpty()) {
                        return Single.just(Collections.emptyList());
                    }
                    List<Verset> justDTOVersets = ImmLists.map(highlights, h -> h.verset);
                    //we need to pass this list to favoriteService and return a list with the same versets
                    //but now these versets are for sure favorites or not.
                    Single<List<Verset>> validFavoritesSingle = favoriteService.getValidFavorites(justDTOVersets);
                    //now transform these valid favorites to spannable string content based on the
                    //original highlights list retrieved from search service
                    return validFavoritesSingle.map(validVersets -> {
                        //we must have same size
                        Asserts.basicAssert(highlights.size() == validVersets.size(),
                                "highlight versets size must be the same with valid versets size");
                        //for efficiency we use a mutable map - because when we (un)favorite a verset
                        // we won't recreate the whole list - list might be big
                        return ImmLists.mutableMap(validVersets, (v, index) -> {
                            HighlightPhraseVerset hv = highlights.get(index);
                            //we must have 1:1 matching
                            Asserts.basicAssert(hv.verset.equals(v), hv + " is not the same with " + v);
                            Spannable spannedContent = new SpannableString(v.content);
                            //set spans based on indexes
                            for (int hvIndex : hv.indexes) {
                                spannedContent.setSpan(
                                        new ForegroundColorSpan(Color.GREEN),
                                        hvIndex,//start of span
                                        hv.offset(hvIndex),// end of span
                                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                                );
                            }
                            return v.content(spannedContent);
                        });
                    });
                });

        Observable<Data<?>> searchObs = Observable.defer(() -> Observable.just(changedPhrase))
                .flatMap(phrase -> {
                    if (TextUtils.isEmpty(phrase)) {
                        return Data.observableError(new ResError("search_err_empty"));
                    } else if (phrase.length() <= 3)
                        return Data.observableError(new ResError("search_err_length"));
                    Single<Data<SearchState>> findSingle = Flowable.just(phrase)
                            .compose(foundVersetsTransform)
                            .toList()
                            .compose(spannedListVersetTransform)
                            .map(versets -> Data.of(new SearchState(true, changedBookId, phrase, versets)))
                            .subscribeOn(Schedulers.io());
                    SearchState wait = new SearchState(true,
                            changedBookId, null, Collections.emptyList());
                    return Data.observableOf(wait).concatWith(findSingle.toObservable());
                }).observeOn(uiScheduler)
                .compose(waitEventDispatcher.dispatchObservable(TAG));

        Disposable disposable = searchObs
                //see https://github.com/Reactive-Extensions/RxJS/issues/841
                // when disposed, onComplete might not be called,
                // that's why we side effect "finally"
                .doFinally(() -> {
                    Asserts.assertIsOnMainThread();
                    stateContainer.subjectSearchState.onNext(stateContainer
                            .subjectSearchState.getValue().inProgress(false));
                })
                .subscribeWith(new DisposableObserver<Data<?>>() {
                    @Override
                    public void onNext(@NonNull Data<?> data) {
                        Asserts.assertIsOnMainThread();
                        int caseOf = data.caseOf();
                        switch (caseOf) {
                            case Data.DATA_TYPE:
                                SearchState state = (SearchState) data.data;
                                stateContainer.subjectSearchState.onNext(state);
                                break;
                            case Data.ERROR_TYPE:
                                bus.dispatch(new Event<>(EventID.ERROR, data.data));
                                break;
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        bus.dispatch(new Event<>(EventID.ERROR, e));
                    }

                    @Override
                    public void onComplete() {
                    }
                });
        //since this an expensive operation, we don't add to local composite disposables
        //but to global disposables allowing the user to dispose them whenever they want;
        globalDisposables.add(PHRASE_SEARCH_DISPOSABLE_KEY, disposable);
    }


}
