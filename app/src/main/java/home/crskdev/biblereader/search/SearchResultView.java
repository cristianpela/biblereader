package home.crskdev.biblereader.search;

import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.mvp.IListDisplayView;

/**
 * Created by criskey on 12/5/2017.
 */

public interface SearchResultView extends IListDisplayView<Verset> {

    void onChooseTabToShow(Verset verset, String[] choices);

    void onConfirmVersetToShow(Verset verset);
}
