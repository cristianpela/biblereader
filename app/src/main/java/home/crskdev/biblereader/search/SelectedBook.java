package home.crskdev.biblereader.search;

import home.crskdev.biblereader.core.Book;

/**
 * Created by criskey on 11/7/2017.
 */

class SelectedBook extends Book {

    boolean selected;

    SelectedBook(boolean selected, String id, String title) {
        super(id, title);
        this.selected = selected;
    }

    static SelectedBook from(boolean selected, Book book) {
        return new SelectedBook(selected, book.id, book.title);
    }
}
