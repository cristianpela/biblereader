package home.crskdev.biblereader.search;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.core.BibleInfo;
import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.ImmLists;
import home.crskdev.biblereader.util.Pair;
import home.crskdev.biblereader.util.RxUtils;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

/**
 * Created by criskey on 11/7/2017.
 */
@PerFragment
public class SearchBookTitlePresenter extends Presenter<SearchBookTitleView> {

    private static final int NO_INDEX = -1;

    private final BibleInfoLoader loader;

    private List<Book> originalBooks;

    private List<Book> filteredBooks;

    private Book selectedBook;

    @Inject
    public SearchBookTitlePresenter(EventBus bus, StateContainer stateContainer,
                                    Scheduler uiScheduler,
                                    BibleInfoLoader loader) {
        super(bus, stateContainer, uiScheduler);
        this.loader = loader;
    }

    @Override
    protected void postAttach() {
        Single<Pair<Integer, List<Book>>> singleBooks = loader.load().map(bibleInfo -> {
            Collection<BibleInfo.BookInfo> bookInfos = bibleInfo.getBookInfos();
            List<Book> books = ImmLists.mutableMap(bookInfos,
                    bi -> new Book(bi.id, bi.title));
            books.add(0, Book.ALL_BOOKS);
            String bookId = stateContainer.subjectSearchState.getValue().bookId;
            int indexOfSelect = ImmLists.reduce(books, NO_INDEX,
                    (acc, e, index) -> (e.id.equals(bookId)) ? index : acc);
            return Pair.of(indexOfSelect, Collections.unmodifiableList(books));
        });
        disposables.add(singleBooks.subscribe(pair -> {
            originalBooks = filteredBooks = pair.second;
            int selectedIndex = pair.first;
            selectedBook = originalBooks.get(selectedIndex);
            view.onLoadBookTitles(selectedIndex, originalBooks);
            view.onSelection(selectedBook.formalTitle());
        }));
    }

    public void setIndexSelection(Observable<Integer> selectObservable) {
        disposables.add(selectObservable.subscribe(index -> {
            selectedBook = filteredBooks.get(index);
            view.onSelection(selectedBook.formalTitle());
        }));
    }

    public void setFilter(Observable<CharSequence> filterObservable) {
        Disposable disposable = filterObservable.compose(RxUtils.defaultDebounce())
                .switchMap(filter -> {
                    String flt = filter.toString().trim().toLowerCase();
                    List<Book> filtered = ImmLists.Chain.from(originalBooks)
                            .filter(b -> b.title.toLowerCase().contains(flt))
                            .ifEmptyThen(originalBooks).get();
                    int indexOfSelect = ImmLists.reduce(filtered, NO_INDEX,
                            (acc, e, index) -> (e.id.equals(selectedBook.id)) ? index : acc);
                    return Observable.just(Pair.of(indexOfSelect, filtered));
                })
                .observeOn(uiScheduler)
                .subscribe(pair -> {
                    int selectedIndex = pair.first;
                    filteredBooks = pair.second;
                    view.onFilterBookTitles(selectedIndex, filteredBooks);
                });
        disposables.add(disposable);
    }

    public void save() {
        SearchState value = stateContainer.subjectSearchState.getValue();
        stateContainer.subjectSearchState.onNext(value.bookId(selectedBook.id));
        view.close();
    }
}
