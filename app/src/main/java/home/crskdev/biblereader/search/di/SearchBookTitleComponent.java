package home.crskdev.biblereader.search.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.search.SearchBookTitleDialog;

/**
 * Created by criskey on 11/7/2017.
 */
@PerFragment
@Subcomponent
public interface SearchBookTitleComponent extends AndroidInjector<SearchBookTitleDialog> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SearchBookTitleDialog> {
    }
}
