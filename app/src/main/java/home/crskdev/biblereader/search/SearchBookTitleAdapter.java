package home.crskdev.biblereader.search;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.List;

import butterknife.BindView;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.util.recyclerview.ButterKnifeViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 11/7/2017.
 */
public class SearchBookTitleAdapter extends RecyclerView.Adapter<SearchBookTitleAdapter.SearchBookTitleVH> {

    private List<Book> books;

    private int lastChecked;

    private BehaviorSubject<Integer> selectSubject;

    public SearchBookTitleAdapter(int indexOfSelect, List<Book> books) {
        this.books = books;
        lastChecked = indexOfSelect;
        selectSubject = BehaviorSubject.create();
    }

    @Override
    public SearchBookTitleVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_book_title_recy_item, parent, false);
        return new SearchBookTitleVH(view);
    }

    @Override
    public void onBindViewHolder(SearchBookTitleVH holder, int position) {
        Book book = books.get(position);
        holder.radioButton.setChecked(lastChecked == position);
        holder.radioButton.setText(book.formalTitle());
        holder.radioButton.setTag(book.id);
        holder.radioButton.setOnClickListener(v -> {
            lastChecked = holder.getAdapterPosition();
            selectSubject.onNext(lastChecked);
            notifyItemRangeChanged(0, books.size());
        });
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public Observable<Integer> selectObservable() {
        return selectSubject;
    }

    public void setFilteredBooks(int indexOfSelect, List<Book> filteredBooks) {
        this.books = filteredBooks;
        lastChecked = indexOfSelect;
        notifyDataSetChanged();
    }

    static class SearchBookTitleVH extends ButterKnifeViewHolder {

        @BindView(R.id.radio_book_title)
        RadioButton radioButton;

        public SearchBookTitleVH(View itemView) {
            super(itemView);
        }
    }
}
