package home.crskdev.biblereader.search;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.codehaus.stax2.XMLInputFactory2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.namespace.QName;

import edu.usf.cutr.javax.xml.stream.XMLStreamException;
import edu.usf.cutr.javax.xml.stream.XMLStreamReader;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.BibleXMLTags;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.TextUtils;
import home.crskdev.biblereader.util.asserts.Asserts;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;

import static edu.usf.cutr.javax.xml.stream.XMLStreamConstants.CHARACTERS;
import static edu.usf.cutr.javax.xml.stream.XMLStreamConstants.START_ELEMENT;

/**
 * Created by criskey on 30/4/2017.
 */
@Singleton
public class SearchService {

    private XMLInputFactory2 input;

    private File file;

    @Inject
    public SearchService(XMLInputFactory2 input, @Named("bible-path")File file) {
        this.input = input;
        this.file = file;
    }

    /**
     * Rules of searching: <br/>
     * <ul>
     * <li>If noOfChapters length =0  we get all the noOfChapters + versets in the book</li>
     * <li>If noOfChapters length =1 and versets length = 0 we get all the versets in book's chapterId</li>
     * <li>If noOfChapters length =1 and versets length > 0 we get the versets in book's chapterId filtered by versets array</li>
     * <li>If noOfChapters length >1  we get the noOfChapters + versets filtered by noOfChapters array regardless of versets array filter</li>
     * <p>
     * </ul>
     *
     * @param bookId
     * @param chapters
     * @param versets
     * @return a stream of versets
     */
    public Flowable<Verset> find(String bookId, int[] chapters, int[] versets) {
        return Flowable.create(e -> {
            //we make sure current thread is not the main one
            Asserts.assertIsBackgroundThread();
            find(bookId, chapters, versets, e);
        }, BackpressureStrategy.BUFFER);
    }

    /**
     * Find a phrase in a book or whole bible if bookId is empty(null or "")
     *
     * @param bookId
     * @param phrase
     * @return a stream with versets contain the phrase, and indexes were the phrase appear in
     */
    public Flowable<HighlightPhraseVerset> findPhrase(@Nullable String bookId, @NonNull String phrase) {
        return Flowable.create(e -> {
            //we make sure current thread is not the main one
            Asserts.assertIsBackgroundThread();
            findPhrase(bookId, phrase, e);
        }, BackpressureStrategy.BUFFER);
    }


    private void findPhrase(String bookId, String phrase, FlowableEmitter<HighlightPhraseVerset> emitter)
            throws XMLStreamException, FileNotFoundException {
        XMLStreamReader reader = input.createXMLStreamReader(new FileInputStream(file));

        phrase = TextUtils.stripDiacritis(phrase.trim().toLowerCase());

        String bookTitle = null;
        int chapterId = Integer.MIN_VALUE;
        int versetId = 1;

        int resultCount = 0;

        //if bookId is empty (null or ""), search phrase through all books
        boolean allBooks = TextUtils.isEmpty(bookId);

        boolean foundBook = allBooks;
        boolean foundChapter = false;
        while (reader.hasNext()) {
            if (emitter.isCancelled()) {
                break;
            }
            int ev = reader.getEventType();
            if (ev == START_ELEMENT) {
                //check if the bookId was previously found (assuming allBooks is false)
                boolean wasFoundBook = foundBook;
                if (elEq(reader.getName(), BibleXMLTags.BOOK_TAG)) {
                    String currBookId = reader.getAttributeValue(0);
                    if (!allBooks) {
                        foundBook = currBookId.equals(bookId);
                    } else {
                        bookId = currBookId;
                    }
                }
                if (!allBooks && wasFoundBook && !foundBook) {
                    //we don't search the whole bible if the bookId was already found
                    //(assuming allBooks is false)
                    break;
                }
                if (foundBook) {
                    if (elEq(reader.getName(), BibleXMLTags.TITLE_TAG)) {
                        bookTitle = reader.getElementText();
                    } else if (elEq(reader.getName(), BibleXMLTags.CHAPTER_TAG)) {
                        chapterId = Integer.parseInt(reader.getAttributeValue(0));
                        versetId = 1; //reset versetId;
                        foundChapter = true;
                    }
                }
            } else if (ev == CHARACTERS && foundBook && foundChapter) {
                //we found verset content
                final String content = reader.getText().trim();
                if (!TextUtils.isEmpty(content)) {
                    //find the indexes of phrase in current verset
                    List<Integer> indexes = TextUtils
                            .multipleIndexOf(TextUtils.stripDiacritis(content.toLowerCase()), phrase);
                    if (!indexes.isEmpty()) {
                        Verset verset = Verset.asDTO(bookId, bookTitle, chapterId, versetId, content);
                        emitter.onNext(new HighlightPhraseVerset(phrase, indexes, verset));
                        resultCount++;
                    }
                    versetId++;
                }
            }
            reader.next();
        }
        if (resultCount == 0 && !emitter.isCancelled()) {
            emitter.onError(new ResError("search_err_no_result", phrase));
        } else {
            emitter.onComplete();
        }
    }

    private void find(String bookId, int[] chapters, int[] versets, FlowableEmitter<Verset> emitter)
            throws XMLStreamException, FileNotFoundException {

        XMLStreamReader reader = input.createXMLStreamReader(new FileInputStream(file));

        boolean foundBook = false;
        boolean foundChapter = false;

        String bookTitle = null;
        int chapterId = 0;
        int versetId = 1;

        while (reader.hasNext()) {
            if (emitter.isCancelled()) {
                break;
            }
            int ev = reader.getEventType();
            if (ev == START_ELEMENT) {
                boolean wasFoundBook = foundBook;
                if (elEq(reader.getName(), BibleXMLTags.BOOK_TAG)) {
                    foundBook = reader.getAttributeValue(0).equals(bookId);
                }
                if (wasFoundBook && !foundBook) {
                    // we are on another bookTitle then the found, so we break cause we are interested just in bookId
                    break;
                }
                if (foundBook) {
                    if (elEq(reader.getName(), BibleXMLTags.TITLE_TAG)) {
                        bookTitle = reader.getElementText();
                    } else if (elEq(reader.getName(), BibleXMLTags.CHAPTER_TAG)) {
                        final int c = Integer.parseInt(reader.getAttributeValue(0));
                        //if there are no chapterId to filter (length ==0) then we get all
                        //else we match with isInArray method
                        foundChapter = chapters.length == 0 || isInArray(c, chapters);
                        if (foundChapter) {
                            chapterId = c;
                            versetId = 1;
                        }
                    }
                }
            } else if (ev == CHARACTERS && foundBook && foundChapter) {
                final String content = reader.getText().trim();
                if (!TextUtils.isEmpty(content)) {
                    //if there are more than 1 noOfChapters, then pass the verset regardless of versets array
                    //same case if there is one chapterId but no versets
                    if (chapters.length > 1 || (chapters.length == 1 && versets.length == 0) || isInArray(versetId, versets)) {
                        emitter.onNext(Verset.asDTO(bookId, bookTitle, chapterId, versetId, content));
                    }
                    versetId++;
                }
            }
            reader.next();
        }
        emitter.onComplete();
    }

    private boolean isInArray(int elem, int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == elem)
                return true;
        }
        return false;
    }

    private boolean elEq(QName name, String expected) {
        return name.toString().equals(expected);
    }


    public static class HighlightPhraseVerset {

        public final String phrase;

        public final List<Integer> indexes;

        public final Verset verset;

        public HighlightPhraseVerset(String phrase, List<Integer> indexes, Verset verset) {
            this.phrase = phrase;
            this.indexes = indexes;
            this.verset = verset;
        }

        /**
         * return the "end" of the highlight based on current element from {@link HighlightPhraseVerset#indexes}
         *
         * @param element
         * @return
         */
        public int offset(int element) {
            return element + phrase.length();
        }

        @Override
        public String toString() {
            return "HighlightPhraseVerset{" +
                    "phrase='" + phrase + '\'' +
                    ", indexes=" + indexes +
                    ", verset=" + verset +
                    '}';
        }

    }

}
