package home.crskdev.biblereader.search;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 2/5/2017.
 */

public interface SearchView extends IView {

    void onCurrentSearch(String bookId, String phrase);

    void onError(String error);

    void onEnableSearch(boolean enabled);

    void onShowBooksDialog();
}
