package home.crskdev.biblereader.search.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.search.SearchViewFragment;

/**
 * Created by criskey on 2/5/2017.
 */

@PerFragment
@Subcomponent(modules = SearchModule.class)
public interface SearchComponent extends AndroidInjector<SearchViewFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<SearchViewFragment> {
    }
}
