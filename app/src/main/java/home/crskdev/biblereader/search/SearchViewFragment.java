package home.crskdev.biblereader.search;

import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.biblereader.MainState;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.BaseDialogFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.util.bottomsheet.BottomSheetFragment;
import home.crskdev.biblereader.util.ViewUtils;

/**
 * Created by criskey on 2/5/2017.
 */

public class SearchViewFragment extends BottomSheetFragment implements SearchView {

    public static String TAG = Integer.toString(MainState.SEARCH_RESULT_TAB);

    @BindView(R.id.edit_pharse_search)
    EditText editPhraseSearch;

    @BindView(R.id.btn_phrase_search)
    ImageButton btnSearch;

    @BindView(R.id.btn_phrase_search_cancel)
    ImageButton btnSearchCancel;

    @BindView(R.id.btn_phrase_search_dialog)
    Button btnSearchDialog;

    @Inject
    SearchPresenter presenter;

    @Override
    public void onPostAttachPresenter() {
        presenter.changePhrase(RxTextView.textChanges(editPhraseSearch));
    }

    @Override
    public void onCurrentSearch(String bookId, String phrase) {
        editPhraseSearch.setText(phrase);
        btnSearchDialog.setText(bookId);
    }

    @Override
    public void onBottomSheetBehaviorInit() {
        getBehavior().setAllowDragging(false);
    }

    @Override
    public void onError(String error) {
        ViewUtils.styledToast(getContext(), error, Toast.LENGTH_SHORT);
    }

    @Override
    public void onEnableSearch(boolean enabled) {
        btnSearchDialog.setEnabled(enabled);
        editPhraseSearch.setEnabled(enabled);

        int visibility = (enabled) ? View.VISIBLE : View.GONE;
        btnSearch.setVisibility(visibility);
        btnSearchCancel.setVisibility(Math.abs(visibility - View.GONE));
    }

    @Override
    public void onShowBooksDialog() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        SearchBookTitleDialog dialog = BaseDialogFragment.newInstance(SearchBookTitleDialog.class);
        dialog.show(manager, "Search Book Title Dialog");
    }

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.search_layout;
    }

    @OnClick(R.id.btn_phrase_search)
    public void onActionSearch() {
        presenter.search();
    }

    @OnClick(R.id.btn_phrase_search_cancel)
    public void onActionSearchCancel() {
        presenter.cancel();
    }

    @OnClick(R.id.btn_phrase_search_dialog)
    public void onActionDialogSearch() {
        presenter.showBooksDialog();
    }

}
