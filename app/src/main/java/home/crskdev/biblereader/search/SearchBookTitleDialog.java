package home.crskdev.biblereader.search;

import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.mvp.BaseDialogFragment;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;

/**
 * Created by criskey on 11/7/2017.
 */
public class SearchBookTitleDialog extends BaseDialogFragment implements SearchBookTitleView {

    @Inject
    SearchBookTitlePresenter mPresenter;

    @BindView(R.id.recy_book_titles)
    RecyclerView mRecyclerView;

    @BindView(R.id.edit_search_filter_book_title)
    EditText mEditFilter;

    @Override
    public Presenter setupPresenter() {
        return mPresenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public void onPreAttachPresenter() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(
                getContext(),
                LinearLayoutManager.VERTICAL,
                false
        ));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.HORIZONTAL));
    }

    @Override
    public void onPostAttachPresenter() {
        mPresenter.setFilter(RxTextView.textChanges(mEditFilter));
    }

    @Override
    public int layoutId() {
        return R.layout.search_book_title_dialog_layout;
    }

    @Override
    public void onLoadBookTitles(int indexOfSelect, List<Book> books) {
        SearchBookTitleAdapter adapter = new SearchBookTitleAdapter(indexOfSelect, books);
        mRecyclerView.setAdapter(adapter);
        mPresenter.setIndexSelection(adapter.selectObservable());
    }

    @Override
    public void onFilterBookTitles(int indexOfSelect, List<Book> filteredBooks) {
        SearchBookTitleAdapter adapter = (SearchBookTitleAdapter) mRecyclerView.getAdapter();
        adapter.setFilteredBooks(indexOfSelect, filteredBooks);
    }

    @Override
    public void onSelection(String title) {
        getDialog().setTitle(title);
    }

    @OnClick(R.id.btn_seach_book_title_save)
    public void onActionSave() {
        mPresenter.save();
    }
}
