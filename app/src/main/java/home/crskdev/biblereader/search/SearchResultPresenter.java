package home.crskdev.biblereader.search;

import javax.inject.Inject;

import home.crskdev.biblereader.TabEventDispatcher;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.core.di.PerFragment;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import home.crskdev.biblereader.favorites.FavoritesOpsMediator;
import home.crskdev.biblereader.favorites.FavoritesService;
import home.crskdev.biblereader.util.RxUtils;
import home.crskdev.biblereader.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.DOUBLE_TAP;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.LONG_TAP;
import static home.crskdev.biblereader.util.recyclerview.TapableRecyclerView.TapKind.SINGLE_TAP;

//import home.crskdev.biblereader.R;

/**
 * Created by criskey on 12/5/2017.
 */
@PerFragment
public class SearchResultPresenter extends Presenter<SearchResultView> {

    private final FavoritesService favoriteService;
    private final TabEventDispatcher tabEventDispatcher;

    private String[] tabTitles;
    private DispatchTabCommand[] dispatchTabCommands;

    @Inject
    public SearchResultPresenter(EventBus bus, StateContainer stateContainer,
                                 Scheduler uiScheduler,
                                 FavoritesService favoritesService,
                                 ResourceFinder res,
                                 TabEventDispatcher tabEventDispatcher) {
        super(bus, stateContainer, uiScheduler);
        this.favoriteService = favoritesService;
        this.tabEventDispatcher = tabEventDispatcher;

        //todo: we need only Favorite and Read tab titles - find a way to get them in scalable fashion
        String[] allTabTitles = res.getStringArray("tab_titles");
        int favTabIndex = 0;
        int readTabIndex = 2;
        this.tabTitles = new String[]{allTabTitles[favTabIndex], allTabTitles[readTabIndex]};

        dispatchTabCommands = new DispatchTabCommand[]{
                //favorite tab
                TabEventDispatcher::openFavoriteTab,
                //read tab
                TabEventDispatcher::openReadTab

        };
    }

    @Override
    protected void postAttach() {
        disposables.add(stateContainer.subjectSearchState
                .filter(s -> s != null)
                .subscribe(s -> {
                    view.onDisplay(s.foundVersets);
                }));
    }

    void processTap(Observable<TapableRecyclerView.TapEvent> tapObservable) {
        disposables.add(tapObservable
                .subscribe(ev -> {
                    Verset verset = stateContainer.subjectSearchState.getValue()
                            .foundVersets.get(ev.position);
                    switch (ev.kind) {
                        case DOUBLE_TAP:
                            view.onConfirmToggleFavorite(verset, ev.position);
                            break;
                        case LONG_TAP:
                            if (verset.favorite) {
                                view.onChooseTabToShow(verset, tabTitles);
                            } else {
                                view.onConfirmVersetToShow(verset);
                            }
                            break;
                        case SINGLE_TAP:
                            break;
                    }
                })
        );
    }

    void confirmFavoriteToggle(int index) {
        Disposable disposable = Observable.just(index)
                .compose(FavoritesOpsMediator.toggle(favoriteService, () -> stateContainer.subjectSearchState
                        .getValue().foundVersets))
                .compose(RxUtils.scheduleObservable(uiScheduler))
                .subscribe(pair -> {
                    bus.dispatch( new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_CHANGED,
                            pair.second));
                    view.onSelect(pair.first);
                });
        disposables.add(disposable);
    }

    public void confirmTabToShow(Verset verset, int selectedIndex) {
        dispatchTabCommands[selectedIndex].dispatch(tabEventDispatcher, verset);
    }

    public void confirmVersetToShow(Verset verset) {
        dispatchTabCommands[1].dispatch(tabEventDispatcher, verset);
    }


    private interface DispatchTabCommand {
        void dispatch(TabEventDispatcher ted, Verset verset);
    }
}
