package home.crskdev.biblereader.util.bottomsheet;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by criskey on 7/6/2017.
 */
//todo remove me?
public class BottomSheetNestedScrollView extends NestedScrollView {

    public BottomSheetNestedScrollView(Context context) {
        super(context);
    }

    public BottomSheetNestedScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BottomSheetNestedScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.onTouchEvent(ev);
    }
}
