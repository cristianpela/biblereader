package home.crskdev.biblereader.util.plafadapters;

/**
 * Created by criskey on 14/7/2017.
 */

public interface MainThreadDispatcher {

    void dispatch(Runnable run);

    boolean isMainThread();
}
