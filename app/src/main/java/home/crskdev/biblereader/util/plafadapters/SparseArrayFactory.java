package home.crskdev.biblereader.util.plafadapters;

import java.util.Map;

/**
 * Created by criskey on 14/7/2017.
 */
public interface SparseArrayFactory {

    <V> Map<Integer, V> create();

    class Android<V> implements SparseArrayFactory {

        @Override
        public <V> Map<Integer, V> create() {
            return new SparseArrayAdapted<>();
        }
    }

    class JavaMap implements SparseArrayFactory {

        @Override
        public <V> Map<Integer, V> create() {
            return new SparseArrayFake<V>();
        }
    }

}
