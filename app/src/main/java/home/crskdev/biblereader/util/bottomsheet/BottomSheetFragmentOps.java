package home.crskdev.biblereader.util.bottomsheet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import home.crskdev.biblereader.util.HideOnScrollBottomReachedBehavior;

/**
 * Created by criskey on 29/6/2017.
 */

public interface BottomSheetFragmentOps {

    View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState);

    void onViewCreated(View view, @Nullable Bundle savedInstanceState);

    void onBottomSheetBehaviorInit();

    void onSheetSlide(View bottomSheet, float slideOffset);

    void onSheetStateChanged(View bottomSheet, int newState);

    void setUserVisibleHint(boolean isVisibleToUser);

    HideOnScrollBottomReachedBehavior getBehavior();

}
