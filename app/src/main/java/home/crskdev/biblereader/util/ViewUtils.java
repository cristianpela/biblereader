package home.crskdev.biblereader.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.StyleRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.util.asserts.Asserts;

/**
 * Created by criskey on 15/6/2017.
 */

public final class ViewUtils {

    private ViewUtils() {
        throw new RuntimeException("No instance allowed");
    }

    public static Bundle toBundle(Map<String, ?> args) {
        Bundle bundle = new Bundle();
        for (Map.Entry<String, ?> entry : args.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            Class<?> clazz = value.getClass();
            if (clazz.equals(Integer.class)) {
                bundle.putInt(key, (Integer) value);
            } else if (clazz.equals(Long.class)) {
                bundle.putLong(key, (Long) value);
            } else if (clazz.equals(Double.class)) {
                bundle.putDouble(key, (Double) value);
            } else if (clazz.equals(Float.class)) {
                bundle.putFloat(key, (Float) value);
            } else if (clazz.equals(Short.class)) {
                bundle.putShort(key, (Short) value);
            } else if (clazz.equals(Long.class)) {
                bundle.putString(key, (String) value);
            } else if (Parcelable.class.isAssignableFrom(clazz)) {
                bundle.putParcelable(key, (Parcelable) value);
            } else if (Serializable.class.isAssignableFrom(clazz)) {
                bundle.putSerializable(key, (Serializable) value);
            } else {
                throw new IllegalArgumentException("Unsupported argument type conversion to Bundle: " + clazz);
            }
        }
        return bundle;
    }

    public static Map<String, ?> toMap(Bundle bundle) {
        if (bundle == null) {
            return Collections.emptyMap();
        }
        Map<String, Object> args = new HashMap<>();
        for (String key : bundle.keySet()) {
            args.put(key, bundle.get(key));
        }
        return args;
    }

    public static void styledToast(Context context, CharSequence text, int duration) {
        Toast toast = Toast.makeText(context, text, duration);
        toast.getView().setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        toast.show();
    }

    @NonNull
    public static AppCompatActivity getActivity(@NonNull View view) {
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof AppCompatActivity) {
                return (AppCompatActivity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        throw new IllegalStateException("View " + view + " is not attached to an activity");
    }

    public static float dp(int value, Context ctx) {
        return complexUnit(value, ctx, TypedValue.COMPLEX_UNIT_DIP);
    }

    public static float sp(int value, Context ctx) {
        return complexUnit(value, ctx, TypedValue.COMPLEX_UNIT_SP);
    }

    public static float complexUnit(int value, Context ctx, int complexUnit) {
        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(complexUnit, value, metrics);
    }

    public static AlertDialog.Builder getAlertBuilder(Context context,
                                                      @Nullable DialogInterface.OnClickListener onConfirm,
                                                      String msg) {
        return getAlertBuilder(context, onConfirm, null, msg,
                R.string.dialog_btn_ok, R.string.dialog_btn_no);
    }

    public static AlertDialog.Builder getAlertBuilder(Context context,
                                                      @Nullable DialogInterface.OnClickListener onConfirm,
                                                      @Nullable DialogInterface.OnClickListener onDismiss,
                                                      String msg,
                                                      @StringRes int positiveBtnId,
                                                      @StringRes int negativeBtnId) {
        //todo nicer alert dialog
        ContextThemeWrapper themeWrapper = new ContextThemeWrapper(context, R.style.AppTheme_Dialog);

        DialogInterface.OnClickListener _onDismiss = (onDismiss == null) ? (dialog, which) -> {
        } : onDismiss;

        String dialogTitle = context.getString(R.string.dialog_title_confirm);
        TextView txtTitle = new TextView(themeWrapper);
        txtTitle.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                (int) dp(100, context)
        ));
        txtTitle.setText(dialogTitle);
//        txtTitle.setBackgroundColor(R.color.colorPrimary);
//        txtTitle.setTextColor(android.R.color.white);
//        txtTitle.setGravity(Gravity.START);
        return new AlertDialog.Builder(themeWrapper)
                .setCustomTitle(txtTitle)
                .setMessage(msg)
                .setPositiveButton(positiveBtnId, onConfirm)
                .setNegativeButton(negativeBtnId, _onDismiss)
                .setOnCancelListener(dialog -> _onDismiss.onClick(dialog, -1));
    }

    /**
     * Watcher that globally watches focus changes and removes softkeyboard from an edittext
     * when pressing "enter" or when that edittext losses its focus (by pressing somewhere outside on screen)
     */
    public static class SoftKeyboardWatcher implements ViewTreeObserver.OnGlobalFocusChangeListener {

        private View root;
        private Context context;
        private EditText currentEditText;
        private OnItemClickListenerWrapper onItemClickListenerWrap;

        /**
         * Use this constructor in onCreate activity
         *
         * @param activity
         */
        public SoftKeyboardWatcher(Activity activity) {
            //todo: little bit hacky make sure is not null?
            this(((ViewGroup) ((ViewGroup) activity.getWindow().getDecorView()
                    .getRootView()).getChildAt(0)).getChildAt(1), activity);
        }

        /**
         * use this constructor in onCreateView in dialog fragments
         *
         * @param root    inflated view
         * @param context
         */
        public SoftKeyboardWatcher(View root, Context context) {
            this.root = root;
            this.context = context;
            onItemClickListenerWrap = new OnItemClickListenerWrapper();
            root.getViewTreeObserver().addOnGlobalFocusChangeListener(this);
        }

        public void disconnect() {
            root.getViewTreeObserver().removeOnGlobalFocusChangeListener(this);
            root = null;
            context = null;
            if (currentEditText != null) {
                currentEditText.setOnEditorActionListener(null);
                currentEditText = null;
            }
            onItemClickListenerWrap = null;
        }

        @Override
        public void onGlobalFocusChanged(View oldFocus, View newFocus) {
            if (newFocus instanceof EditText && newFocus != currentEditText) {
                currentEditText = (EditText) newFocus;
                //hide the keyboard when we hit done, next, enter
                currentEditText.setOnEditorActionListener((v, actionId, event) -> {
                    if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                        hideSoftKeyboard();
                    }
                    return false;
                });
                if (currentEditText instanceof AutoCompleteTextView) {
                    //hide the keyboard when click on a drop down item and preserve
                    //the original listener
                    onItemClickListenerWrap.originalListener = ((AutoCompleteTextView) currentEditText)
                            .getOnItemClickListener();
                    ((AutoCompleteTextView) currentEditText)
                            .setOnItemClickListener(onItemClickListenerWrap);
                }
            } else if (currentEditText != null) {
                hideSoftKeyboard();
                if (currentEditText instanceof AutoCompleteTextView) {
                    //we re-give back the original listener
                    ((AutoCompleteTextView) currentEditText)
                            .setOnItemClickListener(onItemClickListenerWrap.originalListener);
                    //we don't want to keep this ref around
                    onItemClickListenerWrap.originalListener = null;
                }
                currentEditText.setOnEditorActionListener(null);
                currentEditText = null;
            }
        }

        /**
         * Delegate method which hides the keyboard if user is touching the screen outside
         * of edit text's bounds.<br/>
         * Condition to work: edit text must have focus (thus showing soft keyboard)<br/>
         * Note: one must call this in activity's {@link Activity#dispatchTouchEvent(MotionEvent)}
         * <br/>
         * <br/>
         * <code>
         * public boolean dispatchTouchEvent(MotionEvent ev) {<br/>
         * softKeyboardWatcher.overrideDispatchTouchEvent(ev);<br/>
         * return super.dispatchTouchEvent(ev);<br/>
         * }<br/>
         * </code>
         * <p>
         * In the case of fragment dialog you must override
         * <pre>Dialog onCreateDialog(Bundle savedInstanceState)</pre>
         * with a custom Dialog which handles <pre>dispatchTouchEvent(MotionEvent ev)</pre>
         *
         * @param ev touch event or move event
         */
        public void overrideDispatchTouchEvent(MotionEvent ev) {
            if (currentEditText != null
                    && (ev.getAction() == MotionEvent.ACTION_UP
                    || ev.getAction() == MotionEvent.ACTION_MOVE)) {
                Rect hitRect = new Rect();
                currentEditText.getHitRect(hitRect);
                if (!hitRect.contains((int) ev.getX(), (int) ev.getY())) {
                    hideSoftKeyboard();
                }
            }
        }

        private void hideSoftKeyboard() {
            Asserts.basicAssert(currentEditText != null,
                    "Make sure currentEditText is init. (current is null)");
            InputMethodManager in = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(currentEditText
                            .getApplicationWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }

        /**
         * If the {@link AutoCompleteTextView} has registered a listener, we wrap it in our listener
         * and re-give it back when we loosing focus
         */
        private class OnItemClickListenerWrapper implements AdapterView.OnItemClickListener {

            AdapterView.OnItemClickListener originalListener;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyboard();
                if (originalListener != null)
                    originalListener.onItemClick(parent, view, position, id);
            }
        }

    }

}
