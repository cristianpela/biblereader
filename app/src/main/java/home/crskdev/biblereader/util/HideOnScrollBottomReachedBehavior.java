package home.crskdev.biblereader.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by criskey on 20/5/2017.
 */
public class HideOnScrollBottomReachedBehavior extends BottomSheetBehavior {

    private boolean mAllowDragging = true;

    private CompositeBottomSheetCallback compoundCallback;

    public HideOnScrollBottomReachedBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    public void setAllowDragging(boolean allowDragging) {
        mAllowDragging = allowDragging;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        if (getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            child.setTranslationY(Math.min(-dependency.getTop(), getPeekHeight()));
        }
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(CoordinatorLayout parent, View child, MotionEvent event) {
        if (!mAllowDragging) {
            return false;
        }
        return super.onInterceptTouchEvent(parent, child, event);
    }

    @Override
    public void setBottomSheetCallback(BottomSheetCallback callback) {
        if (compoundCallback == null) {
            compoundCallback = new CompositeBottomSheetCallback();
            super.setBottomSheetCallback(compoundCallback);
        }
        compoundCallback.callbacks.add(callback);
    }

    public void removeBottomSheetCallback() {
        if (compoundCallback != null) {
            compoundCallback.callbacks.clear();
        }
    }

    /**
     * we need to compound listeners in the case when behavior container is a viewpager with bottomsheets
     */
    private static class CompositeBottomSheetCallback extends BottomSheetCallback {

        List<BottomSheetCallback> callbacks;

        public CompositeBottomSheetCallback() {
            callbacks = new ArrayList<>();
        }

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            for (BottomSheetCallback callback : callbacks) {
                callback.onStateChanged(bottomSheet, newState);
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            //todo poor performance?
            for (BottomSheetCallback callback : callbacks) {
                callback.onSlide(bottomSheet, slideOffset);
            }
        }
    }

}