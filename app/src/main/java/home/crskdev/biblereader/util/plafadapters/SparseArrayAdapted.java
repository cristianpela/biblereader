package home.crskdev.biblereader.util.plafadapters;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by criskey on 14/7/2017.
 */
public class SparseArrayAdapted<V> implements Map<Integer, V> {

    private SparseArray<V> map;

    public SparseArrayAdapted() {
        map = new SparseArray<>();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return map.indexOfKey((Integer) key) >= 0;
    }

    @Override
    public boolean containsValue(Object value) {
        return map.indexOfValue((V) value) >= 0;
    }

    @Override
    public V get(Object key) {
        return map.get((Integer) key);
    }

    @Override
    public V put(Integer key, V value) {
        map.put(key, value);
        return value;
    }

    @Override
    public V remove(Object key) {
        V v = map.get((Integer) key);
        map.remove((Integer) key);
        return v;
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        map.clear();
    }

    @NonNull
    @Override
    public Set<Integer> keySet() {
        int size = size();
        Set<Integer> keys = new HashSet<>(size);
        for (int i = 0; i < size; i++) {
            keys.add(map.keyAt(i));
        }
        return Collections.unmodifiableSet(keys);
    }

    @NonNull
    @Override
    public Collection<V> values() {
        int size = size();
        List<V> values = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            values.add(map.valueAt(i));
        }
        return Collections.unmodifiableList(values);
    }

    @NonNull
    @Override
    public Set<Entry<Integer, V>> entrySet() {
        int size = size();
        Set<Entry<Integer, V>> entries = new HashSet<>(size);
        for (int i = 0; i < size; i++) {
            final int index = i;
            entries.add(new Entry<Integer, V>() {
                @Override
                public Integer getKey() {
                    return map.keyAt(index);
                }

                @Override
                public V getValue() {
                    return map.valueAt(index);
                }

                @Override
                public V setValue(V value) {
                    throw new UnsupportedOperationException();
                }
            });
        }
        return Collections.unmodifiableSet(entries);
    }
}
