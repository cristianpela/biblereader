package home.crskdev.biblereader.util;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/**
 * Created by criskey on 15/6/2017.
 */

public final class ImmLists {

    public static <T> List<T> add(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.add(element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> addAll(Collection<T> elements, Collection<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.addAll(elements);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> remove(int index, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.remove(index);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> remove(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.remove(findIndex(element, out));
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> set(int index, T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.set(index, element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> set(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.set(findIndex(element, out), element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> asMutable(List<T> list) {
        return new ArrayList<T>(list);
    }

    public static <T> List<T> clear() {
        return new ArrayList<>();
    }


    public static <Out, In> List<Out> mutableMap(@NonNull Collection<In> list, Function<In, Out> func) {
        List<Out> mapped = new ArrayList<Out>(list.size());
        for (In f : list) {
            try {
                mapped.add(func.apply(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapped;
    }

    public static <In, Out> List<Out> mutableMap(@NonNull Collection<In> list, BiFunction<In, Integer, Out> func) {
        List<Out> mapped = new ArrayList<Out>(list.size());
        int index = 0;
        for (In f : list) {
            try {
                mapped.add(func.apply(f, index));
            } catch (Exception e) {
                e.printStackTrace();
            }
            index++;
        }
        return mapped;
    }


    public static <Out, In> List<Out> map(@NonNull Collection<In> list, Function<In, Out> func) {
        return Collections.unmodifiableList(mutableMap(list, func));
    }

    public static <Out, In> List<Out> map(@NonNull Collection<In> list, BiFunction<In, Integer, Out> func) {
        return Collections.unmodifiableList(mutableMap(list, func));
    }

    public static <Out, In> List<Out> mutableMap(@NonNull In[] objects, Function<In, Out> func) {
        List<Out> mapped = new ArrayList<Out>(objects.length);
        for (In f : objects) {
            try {
                mapped.add(func.apply(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapped;
    }

    public static <Out, In> List<Out> mutableMap(@NonNull In[] objects, BiFunction<In, Integer, Out> func) {
        List<Out> mapped = new ArrayList<Out>(objects.length);
        int index = 0;
        for (In f : objects) {
            try {
                mapped.add(func.apply(f, index));
            } catch (Exception e) {
                e.printStackTrace();
            }
            index++;
        }
        return mapped;
    }

    public static <Out, In> List<Out> map(@NonNull In[] objects, Function<In, Out> func) {
        return Collections.unmodifiableList(mutableMap(objects, func));
    }

    public static <Out, In> List<Out> map(@NonNull In[] objects, BiFunction<In, Integer, Out> func) {
        return Collections.unmodifiableList(mutableMap(objects, func));
    }

    public static <T> List<T> filter(@NonNull Collection<T> list, Predicate<T> predicate) {
        List<T> out = new ArrayList<T>();
        try {
            for (T item : list) {
                if (predicate.test(item))
                    out.add(item);
            }
        } catch (Exception e) {
            //
        }
        return Collections.unmodifiableList(out);
    }

    public static <In, Out> Out reduce(@NonNull Collection<In> list,
                                       final @NonNull Out init, TriFunction<Out, In, Integer, Out> func) {
        Out acc = init;
        int index = 0;
        for (In elem : list) {
            //accumulator
            acc = func.apply(acc, elem, index);
            index++;
        }
        return acc;
    }

    private static <T> int findIndex(T element, ArrayList<T> out) {
        int index = -1;
        for (int i = 0; i < out.size(); i++) {
            if (out.get(i).equals(element)) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            throw new RuntimeException("Element " + element + " is not part of the source collection");
        }
        return index;
    }

    public static <T> Chain<T> of(List<T> src) {
        return new Chain<>(src);
    }

    public static interface TriFunction<In1, In2, In3, Out> {

        Out apply(In1 in1, In2 in2, In3 in3);

    }

    public static class Chain<T> {

        private List<T> list;

        private Chain(List<T> list) {
            this.list = new ArrayList<>(list);
        }

        public static <T> Chain<T> from(List<T> list) {
            return new Chain<>(list);
        }

        public Chain<T> add(T element) {
            list = ImmLists.add(element, list);
            return this;
        }

        public Chain<T> addAll(Collection<T> elements) {
            list = ImmLists.addAll(elements, list);
            return this;
        }

        public Chain<T> remove(int index) {
            list = ImmLists.remove(index, list);
            return this;
        }


        public Chain<T> remove(T element) {
            list = ImmLists.remove(element, list);
            return this;
        }

        public Chain<T> set(int index, T element) {
            list = ImmLists.set(index, element, list);
            return this;
        }

        public Chain<T> set(T element) {
            list = ImmLists.set(element, list);
            return this;
        }

        public Chain<T> clear() {
            list = ImmLists.clear();
            return this;
        }

        public Chain<T> filter(Predicate<T> predicate) {
            list = ImmLists.filter(list, predicate);
            return this;
        }

        public Chain<T> ifEmptyThen(List<T> fallBackList) {
            list = (list.isEmpty()) ? new ArrayList<T>(fallBackList) : list;
            return this;
        }

        public List<T> get() {
            return Collections.unmodifiableList(list);
        }

    }

}
