package home.crskdev.biblereader.util;

import java.util.List;

import home.crskdev.biblereader.read.ReadJumpExpandableListModel;

/**
 * Created by criskey on 14/5/2017.
 */

public final class MathExt {

    private MathExt() {
    }

    public static int normalize(long number, long boundMin, long boundMax) {
        return (int) ((number - boundMin) / (boundMax - boundMin));
    }

    public static int normalize100(float number, float boundMax) {
        return (int) Math.floor((number / boundMax) * 100);
    }

    /**
     * for compat reasons {@link Integer#compare(int, int)} is available from API 19+
     *
     * @param x
     * @param y
     * @return
     */
    public static int compareInt(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }


    public static boolean inRangeIncl(int val, int min, int max) {
        return val >= min && val <= max;
    }

    public static float clamp(float value, float min, float max) {
        return Math.max(min, Math.min(max, value));
    }
}
