package home.crskdev.biblereader.util;

/**
 * Error which can wrap a key to strings.xml resource instead of a hardcoded message
 * Created by criskey on 17/7/2017.
 */
public class ResError extends Error {

    public final Object[] args;

    public final String resourceKey;

    public ResError(String resourceKey, Object... args) {
        super(resourceKey);
        this.resourceKey = resourceKey;
        this.args = args;
    }
}
