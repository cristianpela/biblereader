package home.crskdev.biblereader.util.plafadapters;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by criskey on 14/7/2017.
 */

public class AndroidMainThreadDispatcher implements MainThreadDispatcher {

    private Handler handler;

    public AndroidMainThreadDispatcher() {
        handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void dispatch(Runnable run) {
        handler.post(run);
    }

    @Override
    public boolean isMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
