package home.crskdev.biblereader.util;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

import java.util.List;

import javax.inject.Inject;

import home.crskdev.biblereader.core.Book;
import home.crskdev.biblereader.core.Chapter;
import home.crskdev.biblereader.core.Content;
import home.crskdev.biblereader.core.Verset;

/**
 * Created by criskey on 15/5/2017.
 */
public class HtmlContentRenderer {

    @Inject
    public HtmlContentRenderer() {
    }

    public String render(List<Content> contents) {
        StringBuilder b = new StringBuilder();
        for (int i = 0, s = contents.size(); i < s; i++) {
            Content current = contents.get(i);
            if (current.getClass().equals(Book.class)) {
                b.append(tag("h1"))
                        .append(((Book) current).title)
                        .append(endTag("h1"));
            } else if (current.getClass().equals(Chapter.class)) {
                b.append(tag("p"))
                        .append(tag("h3"))
                        .append(((Chapter) current).title)
                        .append(endTag("h3"));
            } else if (current.getClass().equals(Verset.class)) {
                b.append(tag("p")).append(((Verset) current).content)
                        .append(endTag("p"));
            }
            if (i == s - 1) { //last
                b.append(endTag("p"));
            } else if (i + 1 < s) {
                Content next = contents.get(i + 1);
                if (next.getClass().equals(Chapter.class) && current.getClass().equals(Verset.class)) {
                    b.append(endTag("p"));
                }
            }

        }
        return b.toString();
    }

    public Spanned toSpanned(String rendered) {
        Spanned html;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            html = Html.fromHtml(rendered, Html.FROM_HTML_MODE_COMPACT);
        } else {
            html = Html.fromHtml(rendered);
        }
        return html;
    }

    private String tag(String name) {
        return "<" + name + ">";
    }

    private String endTag(String name) {
        return "</" + name + ">";
    }

}
