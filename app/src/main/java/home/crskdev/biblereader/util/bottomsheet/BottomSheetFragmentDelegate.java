package home.crskdev.biblereader.util.bottomsheet;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import home.crskdev.biblereader.R;
import home.crskdev.biblereader.util.HideOnScrollBottomReachedBehavior;

/**
 * Created by criskey on 29/6/2017.
 */

public class BottomSheetFragmentDelegate implements BottomSheetFragmentOps {

    private BottomSheetFragment fragment;

    private HideOnScrollBottomReachedBehavior bottomSheetBehavior;

    private ImageButton btnExpCol;

    public BottomSheetFragmentDelegate(BottomSheetFragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (!(container instanceof ViewSmuggler)) {
            throw new IllegalArgumentException("In order to pass the inflated view to delegator, container" +
                    " must be a ViewGroupDelegateAdapter (as just a carrier forced to follow the API)");
        }
        View view = ((ViewSmuggler) container).view;
        this.btnExpCol = (ImageButton) view.findViewById(R.id.btn_expand_collapse);
        if (btnExpCol != null) {
            btnExpCol.setOnClickListener(v -> actionExpandCollapse());
        }
        return view;
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        ViewGroup container = (ViewGroup) view.getParent();
        if (container != null) {
            bottomSheetBehavior = (HideOnScrollBottomReachedBehavior) BottomSheetBehavior.from(container);
            bottomSheetBehavior.setPeekHeight(48);
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (btnExpCol != null) {
                            switch (newState) {
                                case BottomSheetBehavior.STATE_EXPANDED:
                                    btnExpCol.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
                                    break;
                                case BottomSheetBehavior.STATE_COLLAPSED:
                                    btnExpCol.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
                                    break;
                            }
                        }
                        fragment.onSheetStateChanged(bottomSheet, newState);
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        fragment.onSheetSlide(bottomSheet, slideOffset);
                    }
                });
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            fragment.onBottomSheetBehaviorInit();
        } else {
            throw new IllegalStateException("Container is null");
        }
    }

    private boolean actionExpandCollapse() {
        int state = bottomSheetBehavior.getState();
        if (state == BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else if (state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        return true;
    }

    @Override
    public void onBottomSheetBehaviorInit() {

    }

    @Override
    public void onSheetSlide(View bottomSheet, float slideOffset) {

    }

    @Override
    public void onSheetStateChanged(View bottomSheet, int newState) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (bottomSheetBehavior != null)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public HideOnScrollBottomReachedBehavior getBehavior() {
        return bottomSheetBehavior;
    }

    public void detach() {
        bottomSheetBehavior.removeBottomSheetCallback();
        bottomSheetBehavior = null;
        fragment = null;
        btnExpCol = null;
    }


    /**
     * Adapter class to pass the inflated view from fragment's on createView
     * to delegator onCreateView
     */
    @SuppressLint("ViewConstructor")
    static class ViewSmuggler extends ViewGroup {

        final View view;

        public ViewSmuggler(@NonNull View view) {
            super(view.getContext());
            this.view = view;
        }

        @Override
        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            throw new UnsupportedOperationException();
        }
    }
}
