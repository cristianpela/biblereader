package home.crskdev.biblereader.util;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.TimeUnit;

/**
 * Created by criskey on 12/5/2017.
 */

public class Delayer {

    private Handler initDelay;

    private Runnable cb;

    public Delayer() {
        initDelay = new Handler(Looper.getMainLooper());
    }

    public void delay(Runnable cb, long millis) {
        this.cb = cb;
        initDelay.postDelayed(this.cb, millis);
    }

    public void cancel() {
        initDelay.removeCallbacks(cb);
    }

    public void cancel(Runnable onCancel) {
        cancel();
        if (cb != null) {
            onCancel.run();
        }
    }

}
