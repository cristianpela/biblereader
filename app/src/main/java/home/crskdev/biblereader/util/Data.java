package home.crskdev.biblereader.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Wrapper class for some data
 * Created by criskey on 10/7/2017.
 */
public class Data<T> {

    public static final int DATA_TYPE = 0;

    public static final int ERROR_TYPE = 1;

    public static final int WAIT_TYPE = 2;

    public final T data;

    public Data(T data) {
        this.data = data;
    }

    public static <T> Observable<Data<T>> observableOf(T data) {
        return Data.of(data).toObservable();
    }

    public static <T> Observable<Data<T>> observableError(Throwable error) {
        return Data.error(error).toObservable();
    }

    public static <T> Data<T> of(T data) {
        return new Data(data);
    }

    public static ErrorData error(Throwable error) {
        return new ErrorData(error);
    }

    public static WaitData justWait(boolean wait) {
        return new WaitData(wait);
    }

    public static ErrorData error(String errorMessage) {
        return new ErrorData(new Error(errorMessage));
    }

    public Observable<Data<T>> toObservable() {
        return Observable.just(this);
    }

    public Flowable<Data<T>> toFlowable() {
        return Flowable.just(this);
    }

    public Single<Data<T>> toSingle() {
        return Single.just(this);
    }

    @Type
    public int caseOf() {
        Class<? extends Data> clazz = getClass();
        if (clazz.equals(Data.class)) {
            return DATA_TYPE;
        } else if (clazz.equals(ErrorData.class)) {
            return ERROR_TYPE;
        } else if (clazz.equals(WaitData.class)) {
            return WAIT_TYPE;
        }
        throw new IllegalStateException("Unknown data type");
    }

    @IntDef(value = {DATA_TYPE, ERROR_TYPE, WAIT_TYPE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Type {
    }

    public static class ErrorData<E extends Throwable> extends Data<E> {
        public ErrorData(E error) {
            super(error);
        }
    }

    public static class WaitData extends Data<Boolean> {

        public WaitData(Boolean data) {
            super(data);
        }
    }

}
