package home.crskdev.biblereader.util;

import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.CompletableTransformer;
import io.reactivex.FlowableTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 18/5/2017.
 */

public class RxUtils {

    public static final long DEFAULT_DEBOUNCE_VALUE = 300;

    public static <T> SingleTransformer<T, T> scheduleSingle(Scheduler uiScheduler) {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    public static CompletableTransformer scheduleCompletable(Scheduler uiScheduler) {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    public static <T> FlowableTransformer<T, T> scheduleFlowable(Scheduler uiScheduler) {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    public static <T> ObservableTransformer<T, T> scheduleObservable(Scheduler uiScheduler) {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    public static <T> ObservableTransformer<T, T> defaultDebounce() {
        return up -> up.debounce(DEFAULT_DEBOUNCE_VALUE, TimeUnit.MILLISECONDS);
    }

}
