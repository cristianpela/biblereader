package home.crskdev.biblereader.util.recyclerview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;

/**
 * Extension of {@link TapableRecyclerView} which adds swipe and move behaviour
 * Created by criskey on 24/7/2017.
 */
public class TapableExtraRecyclerView extends TapableRecyclerView {
    
    public TapableExtraRecyclerView(Context context) {
        super(context);
        init();
    }

    public TapableExtraRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TapableExtraRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchCallback());
        itemTouchHelper.attachToRecyclerView(this);
    }

    private class ItemTouchCallback extends ItemTouchHelper.Callback{

        @Override
        public int getMovementFlags(RecyclerView recyclerView, ViewHolder viewHolder) {
            return makeMovementFlags(ItemTouchHelper.ACTION_STATE_SWIPE, ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(ViewHolder viewHolder, int direction) {
            int index = viewHolder.getAdapterPosition();
            int type = viewHolder.getItemViewType();
            tapSubject.onNext(new TapEvent(TapKind.SWIPE_RIGHT, index, type));
        }
    }
}
