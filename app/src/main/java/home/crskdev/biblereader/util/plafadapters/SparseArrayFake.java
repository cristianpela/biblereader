package home.crskdev.biblereader.util.plafadapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by criskey on 14/7/2017.
 */

public class SparseArrayFake<V> implements Map<Integer, V> {

    private Map<Integer, V> map;

    @SuppressLint("UseSparseArrays")
    public SparseArrayFake() {
        map = new HashMap<>();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return map.get(key);
    }

    @Override
    public V put(Integer key, V value) {
        return map.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return map.remove(key);
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends V> m) {
        map.putAll(m);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @NonNull
    @Override
    public Set<Integer> keySet() {
        return map.keySet();
    }

    @NonNull
    @Override
    public Collection<V> values() {
        return map.values();
    }

    @NonNull
    @Override
    public Set<Entry<Integer, V>> entrySet() {
        return map.entrySet();
    }
}
