package home.crskdev.biblereader.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by criskey on 4/7/2017.
 */
//todo Make this works... or just leave it this way is not that important
public class StrippedDiacriticsArrayAdapter<T> extends ArrayAdapter<T> {

    private Filter mFilter;

    public StrippedDiacriticsArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public StrippedDiacriticsArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public StrippedDiacriticsArrayAdapter(Context context, int resource, T[] objects) {
        super(context, resource, objects);
    }

    public StrippedDiacriticsArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public StrippedDiacriticsArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
    }

    public StrippedDiacriticsArrayAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new StrippedDiacriticsFilter();
        }
        return mFilter;
    }

    private class StrippedDiacriticsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            List<T> newValues = new ArrayList<>();

            if (prefix == null || prefix.length() == 0) {
                for (int i = 0, c = getCount(); i < c; i++) {
                    newValues.add(getItem(i));
                }
            } else {
                final String prefixString = TextUtils.stripDiacritis(prefix.toString().toLowerCase());
                for (int i = 0, c = getCount(); i < c; i++) {
                    T value = getItem(i);
                    assert value != null;
                    String valueText = TextUtils.stripDiacritis(value.toString().toLowerCase());
                    if (valueText.startsWith(prefixString)) {
                        newValues.add(value);
                    } else {
                        final String[] words = valueText.split(" ");
                        for (String word : words) {
                            word = TextUtils.stripDiacritis(word);
                            if (word.startsWith(prefixString)) {
                                newValues.add(value);
                                break;
                            }
                        }
                    }
                }
            }
            results.values = newValues;
            results.count = newValues.size();
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

}
