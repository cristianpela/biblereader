package home.crskdev.biblereader.util;

import android.support.annotation.Nullable;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by criskey on 4/7/2017.
 */
public final class TextUtils {

    private static Pattern diacriticsPattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");

    private TextUtils() {
        throw new UnsupportedOperationException();
    }

    public static boolean equalsIgnoreDiacritics(@Nullable String s1, @Nullable String s2) {
        return !(s1 == null || s2 == null) && stripDiacritis(s1).equals(stripDiacritis(s2));
    }

    public static boolean equalsIgnoreDiacriticsAndCase(@Nullable String s1, @Nullable String s2) {
        return !(s1 == null || s2 == null) && stripDiacritis(s1).equalsIgnoreCase(stripDiacritis(s2));
    }

    public static boolean containsIgnoreDiacritics(@Nullable String source, @Nullable String piece) {
        return !(source == null || piece == null) && stripDiacritis(source).contains(stripDiacritis(piece));
    }

    public static boolean startsWithIgnoreDiacritics(@Nullable String source, @Nullable String piece) {
        return !(source == null || piece == null) && stripDiacritis(source).startsWith(stripDiacritis(piece));
    }

    public static int indexOfIgnoreDiacritics(@Nullable String source, @Nullable String piece) {
        if (source == null || piece == null) {
            return -1;
        }
        return stripDiacritis(source).indexOf(stripDiacritis(piece));
    }

    public static String stripDiacritis(String s) {
        String normalized = Normalizer.normalize(s, Normalizer.Form.NFD);
        return diacriticsPattern.matcher(normalized).replaceAll("");
    }

    public static boolean isEmpty(@Nullable String s) {
        return s == null || s.trim().length() == 0;
    }

    /**
     * Return a list with indexes of piece in source. Note this is not stripping diactritics before searching
     *
     * @param source
     * @param piece
     * @return
     */
    public static List<Integer> multipleIndexOf(String source, String piece) {
        List<Integer> indexes = new ArrayList<>();
        int index = source.indexOf(piece);
        while (index >= 0) {
            indexes.add(index);
            index = source.indexOf(piece, index + 1);
        }
        return Collections.unmodifiableList(indexes);
    }

}
