package home.crskdev.biblereader.util.bottomsheet;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import home.crskdev.biblereader.core.mvp.BaseFragment;
import home.crskdev.biblereader.util.HideOnScrollBottomReachedBehavior;

/**
 * Created by criskey on 17/5/2017.
 */
public abstract class BottomSheetFragment extends BaseFragment implements BottomSheetFragmentOps {

    private BottomSheetFragmentDelegate delegate;

    @Override
    @CallSuper
    public void onAttach(Context context) {
        super.onAttach(context);
        delegate = new BottomSheetFragmentDelegate(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return delegate.onCreateView(inflater, new BottomSheetFragmentDelegate.ViewSmuggler(view),
                savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        delegate.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (delegate != null) {
            delegate.setUserVisibleHint(isVisibleToUser);
        }
    }

    @Override
    public void onBottomSheetBehaviorInit() {
    }

    @Override
    public void onSheetSlide(View bottomSheet, float slideOffset) {
    }

    @Override
    public HideOnScrollBottomReachedBehavior getBehavior() {
        return delegate.getBehavior();
    }

    //todo make main activity to suport multiple handler put'em in a stack maybe?
    @Override
    public void onSheetStateChanged(View bottomSheet, int newState) {
        if (newState == BottomSheetBehavior.STATE_EXPANDED) {
//            setBackPressedHandler(() -> {
//                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    return true;
//                }
//                return false;
//            });
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        delegate.detach();
    }

}
