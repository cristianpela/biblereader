package home.crskdev.biblereader;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by criskey on 10/5/2017.
 */

public class MainState {

    public static final MainState DEFAULT = MainState.tab(MainState.READ_TAB);

    public static final int FAVORITES_TAB = 0;
    public static final int SEARCH_RESULT_TAB = 1;
    public static final int READ_TAB = 2;

    public final int tab;

    public MainState(int tab) {
        this.tab = tab;
    }

    public static MainState tab(int tab) {
        return new MainState(tab);
    }

    @Retention(SOURCE)
    @IntDef({FAVORITES_TAB, SEARCH_RESULT_TAB, READ_TAB})
    @interface Tab {
    }

}
