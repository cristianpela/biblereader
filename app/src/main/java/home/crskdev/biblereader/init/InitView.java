package home.crskdev.biblereader.init;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 13/5/2017.
 */

public interface InitView extends IView {

    void onWait(String message);

    void onPrepareSteps(int max, String message);

    void onTickStep(int tick, String message);

    void onPrepareSubStep(int max, String message);

    void onTickSubStep(int tick, String message);

    void onError(String error);

    void redirect();

}
