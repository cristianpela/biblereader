package home.crskdev.biblereader.init.di;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerActivity;
import home.crskdev.biblereader.init.InitActivity;

/**
 * Created by criskey on 13/5/2017.
 */
@PerActivity
@Subcomponent
public interface InitComponent extends AndroidInjector<InitActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<InitActivity> {
    }

}
