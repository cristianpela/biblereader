package home.crskdev.biblereader.init;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.biblereader.MainActivity;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.mvp.BaseActivity;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;

/**
 * Created by criskey on 13/5/2017.
 */

public class InitActivity extends BaseActivity implements InitView {

    @Inject
    InitPresenter presenter;

    @BindView(R.id.txt_init_info)
    TextView txtInfo;

    @BindView(R.id.txt_init_prog_info)
    TextView txtProg;

    @BindView(R.id.txt_init_error)
    TextView txtError;

    @BindView(R.id.prog_init_step)
    ProgressBar progStep;

    @BindView(R.id.prog_init_sub_step)
    ProgressBar progSubStep;

    @BindView(R.id.layout_init_wait)
    LinearLayout layoutWait;

    @BindView(R.id.layout_init_retry)
    LinearLayout layoutRetry;

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.init_layout;
    }

    @Override
    public void onPreAttachPresenter() {
        int color = ContextCompat.getColor(this, R.color.colorLoading);
        progStep.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        progSubStep.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }


    @Override
    public void onWait(String message) {
        layoutWait.setVisibility(View.VISIBLE);
        layoutRetry.setVisibility(View.GONE);
        txtInfo.setText(message);
    }

    @Override
    public void onPrepareSteps(int max, String message) {
        progStep.setVisibility(View.VISIBLE);
        progStep.setMax(max);
        txtInfo.setText(message);
    }

    @Override
    public void onTickStep(int tick, String message) {
        progStep.setProgress(tick);
        txtInfo.setText(message);
    }

    @Override
    public void onPrepareSubStep(int max, String message) {
        progSubStep.setVisibility(View.VISIBLE);
        progSubStep.setMax(max);
        txtProg.setVisibility(View.VISIBLE);
        txtProg.setText(message);
    }

    @Override
    public void onTickSubStep(int tick, String message) {
        progSubStep.setProgress(tick);
        txtProg.setText(message);
    }

    @Override
    public void onError(String error) {
        layoutWait.setVisibility(View.GONE);
        layoutRetry.setVisibility(View.VISIBLE);
        txtError.setText(error);
    }

    @Override
    public void redirect() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    @OnClick(R.id.btn_init_retry)
    void actionRetry() {
        presenter.retry();
    }


}
