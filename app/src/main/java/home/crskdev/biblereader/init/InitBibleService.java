package home.crskdev.biblereader.init;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.biblereader.core.BibleDownloader;
import home.crskdev.biblereader.core.BibleInfoLoader;
import io.reactivex.Flowable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by criskey on 30/4/2017.
 */

//TODO better scalable step observing. Right now stepping is too tight coupled
// Idea: Create a publisher for StreamedInitState and then side observe
    // services on methods like: doOnSubscribe, doOnNext, doOnComplete
@Singleton
public class InitBibleService {

    private static final String STRING_RES_INIT_DOWNLOAD = "init_download";
    private static final String STRING_RES_INIT = "init_initialization";

    private BibleInfoLoader bibleInfoLoader;

    private BibleDownloader bibleDownloader;

   // private PublishSubject<StreamedInitState> stateSubject = PublishSubject.create();

    @Inject
    public InitBibleService(BibleInfoLoader bibleInfoLoader, BibleDownloader bibleDownloader) {
        this.bibleInfoLoader = bibleInfoLoader;
        this.bibleDownloader = bibleDownloader;
    }

    public Flowable<StreamedInitState> check() {
        Flowable<Boolean> isInitFLow = bibleInfoLoader.isInitialized().toFlowable();
        return isInitFLow.flatMap(init -> {
            Flowable<StreamedInitState> loadFlow = bibleInfoLoader.load()
                    .toFlowable().flatMap(b -> Flowable.just(new StreamedInitState.Checked(true)));
            if (!init) {
                Flowable<StreamedInitState> downloadFlow = bibleDownloader.download()
                        .doOnSubscribe(d -> {})
                        .startWith(new StreamedInitState.Tick(1, STRING_RES_INIT_DOWNLOAD));
                Flowable<StreamedInitState> saveFlow = bibleInfoLoader.save()
                        .startWith(new StreamedInitState.Tick(2, BibleInfoLoader.STRING_RES_INFO_LOADER_SAVE));
                return Flowable.concatArray(downloadFlow, saveFlow, loadFlow)
                        .startWith(new StreamedInitState.ProgressStepMax(2, STRING_RES_INIT));
            } else {
                return loadFlow;
            }
        });
    }
}
