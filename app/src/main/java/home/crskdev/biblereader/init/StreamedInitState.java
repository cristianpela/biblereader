package home.crskdev.biblereader.init;

/**
 * Created by criskey on 13/5/2017.
 */

public interface StreamedInitState {

    class Info implements StreamedInitState {
        final String message;

        public Info(String message) {
            this.message = message;
        }
    }

    final class Error extends Info {
        public Error(String message) {
            super(message);
        }
    }

    final class Wait extends Info {
        final boolean wait;

        public Wait(boolean wait, String message) {
            super(message);
            this.wait = wait;
        }
    }

    class ProgressStepMax extends Info {
        final int max;

        public ProgressStepMax(int max, String message) {
            super(message);
            this.max = max;
        }
    }

    final class ProgressSubStepMax extends ProgressStepMax {
        public ProgressSubStepMax(int max, String message) {
            super(max, message);
        }
    }

    class Tick extends Info {
        final int tick;

        public Tick(int tick, String message) {
            super(message);
            this.tick = tick;
        }
    }

    final class SubTick extends Tick {
        public SubTick(int tick, String message) {
            super(tick, message);
        }
    }

    final class Checked implements StreamedInitState {
        final boolean checked;

        public Checked(boolean checked) {
            this.checked = checked;
        }
    }


}
