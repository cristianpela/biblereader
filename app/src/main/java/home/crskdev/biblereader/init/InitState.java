package home.crskdev.biblereader.init;

/**
 * Created by criskey on 13/5/2017.
 */

public final class InitState  {

    public static InitState DEFAULT = new InitState.Builder().create();

    public final boolean checked;
    public final boolean wait;
    public final String error;

    public final String info;

    public final int stepMax;
    public final int stepTick;

    public final String progInfo;
    public final int subStepTick;
    public final int subStepMax;

    public InitState(boolean checked, boolean wait, String error, String info, int stepMax,
                     int stepTick, String progInfo, int subStepTick, int subStepMax) {
        this.checked = checked;
        this.wait = wait;
        this.error = error;
        this.stepMax = stepMax;
        this.stepTick = stepTick;
        this.progInfo = progInfo;
        this.subStepTick = subStepTick;
        this.subStepMax = subStepMax;
        this.info = info;
    }

    public boolean isUnitializedAndNotInProgress() {
        return !checked && !wait;
    }

    public static InitState waitFor(InitState from, StreamedInitState.Wait wait) {
        return new Builder(from)
                .wait(wait.wait)
                .error(null)
                .info(wait.message)
                .create();
    }

    public static InitState progressSteps(InitState from, StreamedInitState.ProgressStepMax max) {
        return new Builder(from)
                .stepMax(max.max)
                .info(max.message)
                .create();
    }

    public static InitState progressTick(InitState from, StreamedInitState.Tick tick) {
        return new Builder(from).stepTick(tick.tick).info(tick.message).create();
    }

    public static InitState progressSubTick(InitState from, StreamedInitState.SubTick tick) {
        return new Builder(from).subStepTick(tick.tick).progInfo(tick.message).create();
    }

    public static InitState progressSubStep(InitState from, StreamedInitState.ProgressStepMax max) {
        return new Builder(from)
                .subStepMax(max.max)
                .progInfo(max.message)
                .create();
    }

    public static InitState checked() {
        return new Builder(DEFAULT).checked(true).create();
    }

    public static InitState error(String error) {
        return new Builder(DEFAULT).error(error).create();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InitState initState = (InitState) o;

        if (checked != initState.checked) return false;
        if (wait != initState.wait) return false;
        if (stepMax != initState.stepMax) return false;
        if (stepTick != initState.stepTick) return false;
        if (subStepTick != initState.subStepTick) return false;
        if (subStepMax != initState.subStepMax) return false;
        if (error != null ? !error.equals(initState.error) : initState.error != null) return false;
        return progInfo != null ? progInfo.equals(initState.progInfo) : initState.progInfo == null;

    }

    @Override
    public int hashCode() {
        int result = (checked ? 1 : 0);
        result = 31 * result + (wait ? 1 : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        result = 31 * result + stepMax;
        result = 31 * result + stepTick;
        result = 31 * result + (progInfo != null ? progInfo.hashCode() : 0);
        result = 31 * result + subStepTick;
        result = 31 * result + subStepMax;
        return result;
    }

    public static class Builder {

        private boolean checked;
        private boolean wait;
        private String error;
        private String info;
        private int stepMax;
        private int stepTick;
        private String progInfo;
        private int subStepTick;
        private int subStepMax;

        public Builder() {

        }

        public Builder(InitState from) {
            checked = from.checked;
            wait = from.wait;
            error = from.error;
            info = from.info;
            stepMax = from.stepMax;
            stepTick = from.stepTick;
            progInfo = from.progInfo;
            subStepMax = from.subStepMax;
            subStepTick = from.subStepTick;
        }

        public Builder checked(boolean checked) {
            this.checked = checked;
            return this;
        }

        public Builder wait(boolean wait) {
            this.wait = wait;
            return this;
        }

        public Builder info(String info) {
            this.info = info;
            return this;
        }

        public Builder error(String error) {
            this.error = error;
            return this;
        }

        public Builder stepMax(int stepMax) {
            this.stepMax = stepMax;
            return this;
        }

        public Builder stepTick(int stepTick) {
            this.stepTick = stepTick;
            return this;
        }

        public Builder progInfo(String progInfo) {
            this.progInfo = progInfo;
            return this;
        }

        public Builder subStepTick(int subStepTick) {
            this.subStepTick = subStepTick;
            return this;
        }

        public Builder subStepMax(int subStepMax) {
            this.subStepMax = subStepMax;
            return this;
        }

        public InitState create() {
            return new InitState(checked, wait, error, info,
                    stepMax, stepTick, progInfo, subStepTick, subStepMax);
        }
    }
}
