package home.crskdev.biblereader.init;

import javax.inject.Inject;

import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.di.PerActivity;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.core.mvp.ResourceFinder;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 13/5/2017.
 */
@PerActivity
public class InitPresenter extends Presenter<InitView> {

    private static final String STRING_RES_INIT_WAIT = "init_wait";
    private final ResourceFinder res;
    private BehaviorSubject<InitState> state;
    private InitBibleService initService;

    @Inject
    public InitPresenter(EventBus bus, StateContainer stateContainer, Scheduler uiScheduler, InitBibleService initService,
                         ResourceFinder res) {
        super(bus, stateContainer, uiScheduler);
        state = stateContainer.subjectInitState;
        this.initService = initService;
        this.res = res;
        checkService();
    }

    private void checkService() {
        if (state.getValue().isUnitializedAndNotInProgress())
            initService
                    .check().startWith(new StreamedInitState.Wait(true, STRING_RES_INIT_WAIT))
                    .subscribeOn(Schedulers.io())
                    .observeOn(uiScheduler)
                    .subscribe(
                            sis -> {
                                if (sis.getClass().equals(StreamedInitState.Wait.class)) {
                                    state.onNext(InitState.waitFor(state.getValue(), (StreamedInitState.Wait) sis));
                                } else if (sis.getClass().equals(StreamedInitState.ProgressStepMax.class)) {
                                    state.onNext(InitState.progressSteps(state.getValue(), (StreamedInitState.ProgressStepMax) sis));
                                } else if (sis.getClass().equals(StreamedInitState.ProgressSubStepMax.class)) {
                                    state.onNext(InitState.progressSubStep(state.getValue(), (StreamedInitState.ProgressStepMax) sis));
                                } else if (sis.getClass().equals(StreamedInitState.Tick.class)) {
                                    state.onNext(InitState.progressTick(state.getValue(), (StreamedInitState.Tick) sis));
                                } else if (sis.getClass().equals(StreamedInitState.SubTick.class)) {
                                    state.onNext(InitState.progressSubTick(state.getValue(), (StreamedInitState.SubTick) sis));
                                } else if (sis.getClass().equals(StreamedInitState.Checked.class)) {
                                    state.onNext(InitState.checked());
                                }
                            },
                            e -> {
                                state.onNext(InitState.error(e.getMessage()));
                            }
                    );
    }

    @Override
    protected void postAttach() {
        disposables.add(state.subscribe(s -> {
            if (s.checked) {
                view.redirect();
            } else {
                if (s.wait) {
                    view.onWait(res.getString(s.info));
                }
                if (s.stepMax > 0) {
                    view.onPrepareSteps(s.stepMax, res.getString(s.info));
                }
                if (s.stepTick > 0) {
                    view.onTickStep(s.stepTick, res.getString(s.info));
                }
                if (s.subStepMax > 0) {
                    view.onPrepareSubStep(s.subStepMax, res.getString(s.progInfo));
                }
                if (s.subStepTick > 0) {
                    view.onTickSubStep(s.subStepTick, res.getString(s.progInfo));
                }
                if (s.error != null) {
                    view.onError(res.getString(s.error));
                }
            }

        }));
    }

    public void retry() {
        checkService();
    }
}
