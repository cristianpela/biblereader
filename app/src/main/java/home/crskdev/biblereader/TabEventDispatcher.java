package home.crskdev.biblereader;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;
import home.crskdev.biblereader.read.ReadState;

import static home.crskdev.biblereader.MainState.READ_TAB;

/**
 * Utility class which help sending specific tab related events,
 * thus keeping the app "d.r.y" <br/>
 * Created by criskey on 26/7/2017.
 */
public class TabEventDispatcher {

    private EventBus bus;

    @Inject
    public TabEventDispatcher(EventBus bus) {
        this.bus = bus;
    }

    public void openReadTab(@NonNull Verset versetToPoint) {
        bus.dispatch(new Event<>(GroupEventID.GROUP_OTHER, EventID.TAB, MainState.READ_TAB));
        bus.dispatch(new Event<>(GroupEventID.GROUP_READ, EventID.READ_NAV_JUMP,
                ReadState.jump(versetToPoint)));
    }

    public void openReadTabJumpDialog() {
        bus.dispatch(new Event<>(GroupEventID.GROUP_OTHER, EventID.TAB, MainState.READ_TAB));
        bus.dispatch(Event.flag(GroupEventID.GROUP_READ, EventID.READ_NAV_JUMP_DIALOG));
    }

    /**
     * opens the favorite tab, if versetToPoint is not null then opens the verset's detailed page
     *
     * @param versetToPoint
     */
    public void openFavoriteTab(@Nullable Verset versetToPoint) {
        bus.dispatch(new Event<>(GroupEventID.GROUP_OTHER, EventID.TAB, MainState.FAVORITES_TAB));
        if (versetToPoint != null)
            bus.dispatch(new Event<>(GroupEventID.GROUP_FAVORITE, EventID.FAVORITE_SELECT, versetToPoint));
    }

    public void openSearchTab() {
        bus.dispatch(new Event<>(GroupEventID.GROUP_OTHER, EventID.TAB, MainState.SEARCH_RESULT_TAB));
    }
}
