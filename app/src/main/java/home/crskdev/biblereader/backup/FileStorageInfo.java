package home.crskdev.biblereader.backup;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;

import javax.inject.Inject;

/**
 * Created by criskey on 31/7/2017.
 */
public class FileStorageInfo {

    private String documentsDirName;

    private Context context;

    @Inject
    public FileStorageInfo(Context context) {
        this.context = context;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            documentsDirName = Environment.DIRECTORY_DOCUMENTS;
        } else {
            documentsDirName = "Documents";
        }
    }

    public File getLocalStorageDirectory(){
        return context.getFilesDir();
    }

    public File getLocalDatabaseDirectory(){
        return new File(getLocalStorageDirectory().getParent(), "databases");
    }

    public File getExternalStorageDirectory() throws FileNotFoundException {
        File dir;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            dir = Environment.getExternalStoragePublicDirectory(documentsDirName);
        } else {
            dir = new File(Environment.getExternalStorageDirectory(), documentsDirName);
        }
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new FileNotFoundException("Unable to create documents directory");
            }
        }
        return dir;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

}
