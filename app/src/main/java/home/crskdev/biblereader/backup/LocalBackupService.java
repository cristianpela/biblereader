package home.crskdev.biblereader.backup;

import android.support.annotation.VisibleForTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.inject.Inject;
import javax.inject.Named;

import home.crskdev.biblereader.util.ResError;
import home.crskdev.biblereader.util.asserts.Asserts;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 31/7/2017.
 */
//todo make this service general... right know is hardcoded to bible_reader
public class LocalBackupService {

    private final File dbFile;
    private final File bookFile;
    private final FileStorageInfo info;

    @Inject
    public LocalBackupService(@Named("db-path") File dbFile, @Named("bible-path") File bookFile,
                              FileStorageInfo info) {
        this.dbFile = dbFile;
        this.bookFile = bookFile;
        this.info = info;
    }

    public Completable backup() {
        return Completable.create(s -> {
            Asserts.assertIsBackgroundThread();
            if (info.isExternalStorageWritable()) {
                copy(dbFile, backupDest(dbFile));
                copy(bookFile, backupDest(bookFile));
                s.onComplete();
            } else {
                s.onError(new ResError("local_backup_svc_error_backup"));
            }
        }).subscribeOn(Schedulers.io());
    }

    public Completable restore() {
        return Completable.create(s -> {
            Asserts.assertIsBackgroundThread();
            if (info.isExternalStorageWritable()) {
                dbFile.delete();
                bookFile.delete();
                File dbBackup = backupDest(dbFile);
                copy(dbBackup, restoreSource(dbBackup, info.getLocalDatabaseDirectory()));
                File bibleBackup = backupDest(bookFile);
                copy(bibleBackup, restoreSource(bibleBackup, info.getLocalStorageDirectory()));
                s.onComplete();
            } else {
                s.onError(new ResError("local_backup_svc_error_restore"));
            }
        }).subscribeOn(Schedulers.io());
    }

    private File backupDest(File source) throws FileNotFoundException {
        return new File(backupDir(), source.getName());
    }

    private File restoreSource(File toBakup, File restoreDir) throws FileNotFoundException {
        File backup = new File(backupDir(), toBakup.getName());
        return new File(restoreDir, backup.getName());
    }

    @VisibleForTesting
    File backupDir() throws FileNotFoundException {
        File dir = new File(info.getExternalStorageDirectory(), "bible_reader_backup");
        if (!dir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            dir.mkdir();
        }
        return dir;
    }


    private void copy(File source, File dest) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } finally {
            sourceChannel.close();
            destChannel.close();
        }
    }

}
