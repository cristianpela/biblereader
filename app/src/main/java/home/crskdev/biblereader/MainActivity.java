package home.crskdev.biblereader;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.biblereader.core.mvp.BaseActivity;
import home.crskdev.biblereader.core.mvp.IView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.favorites.FavoritesChildFragmentHolder;
import home.crskdev.biblereader.favorites.filter.FavoritesFilterFragmentView;
import home.crskdev.biblereader.read.FullScreenActivity;
import home.crskdev.biblereader.read.ReadNavViewFragment;
import home.crskdev.biblereader.read.ReadViewFragment;
import home.crskdev.biblereader.search.SearchResultViewFragment;
import home.crskdev.biblereader.search.SearchViewFragment;
import home.crskdev.biblereader.util.ViewUtils;

public class MainActivity extends BaseActivity implements MainView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

//    @BindView(R.id.custom_quote_view)
//    QuoteTextView txtQuote;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.viewpager_bottom)
    ViewPager viewPagerBottom;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.toolbar_progress_bar)
    ProgressBar progressWait;

    @Inject
    MainPresenter presenter;

    @Inject
    TandemViewPagerManager tandemViewPagerManager;

    ValueAnimator waitCountAnimator;

    String waitInfo, appTitle;

    @Override
    public void onPreAttachPresenter() {
        // AndroidArgumentsInjection.inject(this, txtQuote);
        setSupportActionBar(toolbar);

        viewPager.setAdapter(new MainPageAdapter(this, getSupportFragmentManager(),
                new Fragment[]{
                        new FavoritesChildFragmentHolder(),
                        new SearchResultViewFragment(),
                        new ReadViewFragment()
                }));
        tabLayout.setupWithViewPager(viewPager);

        viewPagerBottom.setAdapter(new MainPageAdapter(this, getSupportFragmentManager(),
                new Fragment[]{
                        new FavoritesFilterFragmentView(),
                        new SearchViewFragment(),
                        new ReadNavViewFragment()
                }));

        tandemViewPagerManager.setViewPagers(viewPager, viewPagerBottom);

        progressWait.getIndeterminateDrawable().setColorFilter(ContextCompat
                .getColor(this, R.color.colorLoading), PorterDuff.Mode.SRC_IN);

        appTitle = getString(R.string.app_name);
        waitCountAnimator = ValueAnimator.ofFloat(0f, 0.65f).setDuration(500);
        waitCountAnimator.addUpdateListener(animation -> {
            SpannableString animStyledTitle = new SpannableString(appTitle + " " + waitInfo);
            int length = animStyledTitle.length();
            animStyledTitle.setSpan(new StyleSpan(Typeface.ITALIC), 0, length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            Float value = (Float) animation.getAnimatedValue();
            animStyledTitle.setSpan(new RelativeSizeSpan(value),
                    appTitle.length(), length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            //noinspection ConstantConditions
            getSupportActionBar().setTitle(animStyledTitle);
        });
    }

    @Override
    public void onPostAttachPresenter() {
        tabLayout.addOnTabSelectedListener(presenter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bible_scroll, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            presenter.changeTab(MainState.SEARCH_RESULT_TAB);
            return true;
        } else if (id == R.id.action_full_screen) {
            startActivity(new Intent(this, FullScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            return true;
        } else if (id == R.id.action_read_jump_chapter) {
            presenter.jumpToChapter();
            return true;
        }else if(id == R.id.action_backup){
            presenter.backupData();
            return true;
        }else if(id == R.id.action_restore){
            presenter.confirmRestoreData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(@MainState.Tab int tab) {
        tabLayout.getTabAt(tab).select();
        tandemViewPagerManager.setCurrentItem(tab);
        // bottomSheetFactory.show(R.id.bottom_sheet_container, Integer.toString(tab), getSupportFragmentManager());
    }

    @Override
    public void onTabUnselected(int position) {
        //  bottomSheetFactory.hideCurrent(getSupportFragmentManager());
    }

    @Override
    public void onWait(boolean wait) {
        progressWait.setVisibility(wait ? View.VISIBLE : View.GONE);
        if (!wait) {
            if (waitCountAnimator != null && waitCountAnimator.isStarted()) {
                waitCountAnimator.cancel();
            }
            getSupportActionBar().setTitle(appTitle);
        }

    }

    @Override
    public void onWaitInfo(String waitInfo) {
        this.waitInfo = waitInfo;
        if (waitCountAnimator != null && waitCountAnimator.isStarted()) {
            waitCountAnimator.cancel();
        }
        waitCountAnimator.start();
    }

    @Override
    public void onConfirmRestoreData() {
        ViewUtils.getAlertBuilder(this, (d, w) -> {
            presenter.restoreData();
        }, getString(R.string.dialog_data_restore_msg))
        .create()
        .show();
    }

    @Override
    public Presenter setupPresenter() {
        return presenter;
    }

    @Override
    public void onPreDetachPresenter() {
        if (waitCountAnimator != null && waitCountAnimator.isStarted()) {
            waitCountAnimator.cancel();
        }
    }

    @Override
    public IView setupView() {
        return this;
    }

    @Override
    public int layoutId() {
        return R.layout.activity_bible_scroll;
    }

}
