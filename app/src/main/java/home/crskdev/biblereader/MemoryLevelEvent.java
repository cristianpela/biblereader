package home.crskdev.biblereader;

import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.GroupEventID;

/**
 * Created by criskey on 19/6/2017.
 */

public class MemoryLevelEvent extends Event<Integer> {

    public MemoryLevelEvent(int level) {
        super(GroupEventID.GROUP_OTHER, EventID.MEMORY_LEVEL, level);
    }
}
