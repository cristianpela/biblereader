package home.crskdev.biblereader;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import home.crskdev.biblereader.favorites.FavoritesChildFragmentHolder;
import home.crskdev.biblereader.read.ReadViewFragment;
import home.crskdev.biblereader.search.SearchResultViewFragment;

/**
 * Created by criskey on 12/5/2017.
 */
public class MainPageAdapter extends FragmentPagerAdapter {

    private final Context context;
    private Fragment[] fragments;

    public MainPageAdapter(Context context, FragmentManager fm, Fragment[] fragments) {
        super(fm);
        this.context = context;
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getResources().getStringArray(R.array.tab_titles)[position];
    }
}
