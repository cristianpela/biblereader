package home.crskdev.biblereader;

import android.support.design.widget.TabLayout;

import javax.inject.Inject;

import home.crskdev.biblereader.backup.LocalBackupService;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.Event;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.bus.EventID;
import home.crskdev.biblereader.core.bus.WaitEventDispatcher;
import home.crskdev.biblereader.core.bus.WaitEventQueue;
import home.crskdev.biblereader.core.db.DBConnectionRestartService;
import home.crskdev.biblereader.core.di.PerActivity;
import home.crskdev.biblereader.core.mvp.Presenter;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 5/5/2017.
 */
@PerActivity
public class MainPresenter extends Presenter<MainView> implements TabLayout.OnTabSelectedListener {

    public static final String TAG = MainPresenter.class.getSimpleName();

    private BehaviorSubject<MainState> state;
    private WaitEventQueue waitEventQueue;
    private WaitEventDispatcher waitEventDispatcher;
    private TabEventDispatcher tabEventDispatcher;
    private LocalBackupService backupService;
    private DBConnectionRestartService dbRestartService;

    @Inject
    public MainPresenter(EventBus bus, StateContainer stateContainer,
                         WaitEventQueue waitEventQueue,
                         WaitEventDispatcher waitEventDispatcher,
                         Scheduler uiScheduler,
                         TabEventDispatcher tabEventDispatcher,
                         LocalBackupService backupService,
                         DBConnectionRestartService dbRestartService) {
        super(bus, stateContainer, uiScheduler);
        state = stateContainer.subjectMainState;
        this.waitEventQueue = waitEventQueue;
        this.waitEventDispatcher = waitEventDispatcher;
        this.tabEventDispatcher = tabEventDispatcher;
        this.backupService = backupService;
        this.dbRestartService = dbRestartService;
    }

    @Override
    protected void postAttach() {
        disposables.add(state.subscribe((s) -> {
            view.onTabSelected(s.tab);
        }));
        disposables.add(waitEventQueue
                .toObservable()
                .subscribe(we -> {
                    int type = we.type;
                    switch (type) {
                        case WaitEventQueue.WAIT_SESSION_START:
                            view.onWait(true);
                            break;
                        case WaitEventQueue.WAIT_SESSION_END:
                            view.onWait(false);
                            break;
                        case WaitEventQueue.WAIT_SESSION_INFO:
                            view.onWaitInfo(we.data.toString());
                            break;
                    }
                }));
    }

    @Override
    public void accept(@NonNull Event<?> event) throws Exception {
        switch (event.id) {
            case EventID.TAB: {
                changeTab((Integer) event.data);
                break;
            }
        }
    }

    public void changeTab(@MainState.Tab int tab) {
        if (state.getValue().tab != tab) {
            state.onNext(MainState.tab(tab));
        }
    }

    public void backupData() {
        backupService.backup()
                .compose(waitEventDispatcher.dispatchCompletable(TAG))
                .observeOn(uiScheduler)
                .subscribe(
                        () -> bus.dispatch(new Event<>(EventID.NOTIFICATION, "notif_backup_complete")),
                        (e) -> bus.dispatch(new Event<>(EventID.ERROR, e))
                );
    }

    public void confirmRestoreData() {
        view.onConfirmRestoreData();
    }

    public void restoreData() {
        backupService.restore()
                .andThen(dbRestartService.restart())
                .compose(waitEventDispatcher.dispatchCompletable(TAG))
                .observeOn(uiScheduler)
                .subscribe(
                        () -> {
                            bus.dispatch(new Event<>(EventID.NOTIFICATION, "notif_restore_complete"));
                            bus.dispatch(Event.flag(EventID.RESET));
                        },
                        (e) -> bus.dispatch(new Event<>(EventID.ERROR, e))
                );
    }

    public void jumpToChapter() {
        tabEventDispatcher.openReadTabJumpDialog();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        //noinspection WrongConstant
        changeTab(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        view.onTabUnselected(tab.getPosition());
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }
}
