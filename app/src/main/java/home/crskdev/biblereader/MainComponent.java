package home.crskdev.biblereader;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import home.crskdev.biblereader.core.di.PerActivity;

/**
 * Created by criskey on 4/5/2017.
 */
@PerActivity
@Subcomponent
public interface MainComponent extends AndroidInjector<MainActivity> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<MainActivity> {
    }
}
