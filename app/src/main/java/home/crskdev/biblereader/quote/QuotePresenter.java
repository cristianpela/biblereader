package home.crskdev.biblereader.quote;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.biblereader.core.BibleInfoLoader;
import home.crskdev.biblereader.core.state.StateContainer;
import home.crskdev.biblereader.core.bus.EventBus;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.core.mvp.Presenter;
import home.crskdev.biblereader.search.SearchService;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 12/5/2017.
 */
@PerView
public class QuotePresenter extends Presenter<QuoteView> {

    private static final String PROVERBS_ID = "PRO";

    //TODO fix elipsize to styledToast popup only if text is bigger than that
    private static final int MIN_LENGTH_TO_ELIPSIZE = Integer.MAX_VALUE;

    private final BibleInfoLoader loader;
    private final SearchService searchService;

    private Random random = new Random();

    private boolean isPopupShown;

    @Inject
    public QuotePresenter(EventBus bus, StateContainer stateContainer,
                          BibleInfoLoader loader, Scheduler uiScheduler, SearchService searchService) {
        super(bus, stateContainer, uiScheduler);
        this.loader = loader;
        this.searchService = searchService;
        disposables.add(Single.defer(() -> {
            QuoteState value = getValue();
            return value.content != null ? Single.just(value.inIdle()) : reloadQuote();
        }).toObservable().startWith(getValue().inProgress()).subscribe((t) -> {
            //bus.dispatch(new Event<>(EventID.WAIT, t.inProgress));
            stateContainer.subjectQuoteState.onNext(t);
        }));
    }

    @Override
    protected void postAttach() {
        disposables.add(stateContainer.subjectQuoteState.filter(q -> q.content != null).subscribe(view::display));
    }

    @Override
    protected void postDetach() {
        stateContainer.subjectQuoteState.onNext(getValue().inIdle());
    }

    private QuoteState getValue() {
        return stateContainer.subjectQuoteState.getValue();
    }

    private Single<QuoteState> reloadQuote() {
        return loader.load()
                .subscribeOn(Schedulers.io())
                .map(bi -> bi.getBookById(PROVERBS_ID))
                .flatMap(b -> {
                    int ch = random.nextInt(b.noOfChapters) + 1;
                    int verset = random.nextInt(b.getVersetsOf(ch)) + 1;
                    return searchService.find(PROVERBS_ID, new int[]{ch}, new int[]{verset})
                            .toList()
                            .map(l -> l.get(0))
                            .map(r -> new QuoteState(PROVERBS_ID, r.chapterId, r.verset, r.content.toString()))
                            .subscribeOn(Schedulers.io());
                })
                .observeOn(uiScheduler);
    }

    public void reloadQuote(Observable<Object> click) {
        disposables.add(click
                .debounce(500, TimeUnit.MILLISECONDS)
                .switchMap(__ -> reloadQuote().toObservable().startWith(getValue().inProgress()))
                .observeOn(uiScheduler)
                .subscribe((t) -> {
                    //bus.dispatch(new Event<>(EventID.WAIT, t.inProgress));
                    stateContainer.subjectQuoteState.onNext(t);
                }));
    }

    private String getContent() {
        QuoteState quote = stateContainer.subjectQuoteState.getValue();
        return PROVERBS_ID + " " + quote.chapter + ":" + quote.verset + "-" + quote.content;
    }

    public void showPopup() {
        String content = getContent();
        if (!isPopupShown && content.length() <= MIN_LENGTH_TO_ELIPSIZE) {
            view.onShowPopup(content);
            isPopupShown = true;
        }
    }

    public void hidePopup() {
        isPopupShown = false;
        view.onHidePopup();
    }
}
