package home.crskdev.biblereader.quote.di;

import dagger.Subcomponent;
import home.crskdev.biblereader.core.di.AndroidArgumentsInjector;
import home.crskdev.biblereader.core.di.PerView;
import home.crskdev.biblereader.quote.QuoteTextView;

/**
 * Created by criskey on 4/6/2017.
 */
@PerView
@Subcomponent
public interface QuoteComponent extends AndroidArgumentsInjector<QuoteTextView> {

    @Subcomponent.Builder
    abstract class Builder extends AndroidArgumentsInjector.Builder<QuoteTextView> {}
}
