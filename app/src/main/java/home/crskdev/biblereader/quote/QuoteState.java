package home.crskdev.biblereader.quote;

/**
 * Created by criskey on 12/5/2017.
 */

public final class QuoteState {

    public static final QuoteState DEFAULT = new QuoteState(null, -1, -1, null);

    public final boolean inProgress;

    public final String bookId;

    public final int chapter;

    public final int verset;

    public final String content;

    public QuoteState(boolean inProgress, String bookId, int chapter, int verset, String content) {
        this.inProgress = inProgress;
        this.bookId = bookId;
        this.chapter = chapter;
        this.verset = verset;
        this.content = content;
    }

    public QuoteState(String bookId, int chapter, int verset, String content) {
        this(false, bookId, chapter, verset, content);
    }

    public QuoteState inProgress() {
        return new QuoteState(true, bookId, chapter, verset, content);
    }

    public QuoteState inIdle() {
        return new QuoteState(bookId, chapter, verset, content);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuoteState that = (QuoteState) o;

        if (inProgress != that.inProgress) return false;
        if (chapter != that.chapter) return false;
        if (verset != that.verset) return false;
        if (bookId != null ? !bookId.equals(that.bookId) : that.bookId != null) return false;
        return content != null ? content.equals(that.content) : that.content == null;

    }

    @Override
    public int hashCode() {
        int result = (inProgress ? 1 : 0);
        result = 31 * result + (bookId != null ? bookId.hashCode() : 0);
        result = 31 * result + chapter;
        result = 31 * result + verset;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
