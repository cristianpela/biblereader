package home.crskdev.biblereader.quote;

import home.crskdev.biblereader.core.mvp.IView;

/**
 * Created by criskey on 12/5/2017.
 */

public interface QuoteView extends IView {

    void display(QuoteState quote);

    void onShowPopup(String content);

    void onHidePopup();

}
