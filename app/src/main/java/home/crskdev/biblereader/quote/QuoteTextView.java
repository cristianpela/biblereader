package home.crskdev.biblereader.quote;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.internal.Preconditions;
import home.crskdev.biblereader.R;
import home.crskdev.biblereader.core.di.Args;
import home.crskdev.biblereader.core.di.Arguments;
import home.crskdev.biblereader.core.mvp.MVPLifecycle;
import home.crskdev.biblereader.core.mvp.MVPLifecycleViewDelegate;

/**
 * Created by criskey on 4/6/2017.
 */
public class QuoteTextView extends AppCompatTextView implements QuoteView, MVPLifecycle {

    @Inject
    QuotePresenter presenter;

    @Inject
    @Args
    Map<String, Object> args;

    private PopupWindow popup;

    private TextView txtPopup;

    private MVPLifecycleViewDelegate mLifecycle = MVPLifecycleViewDelegate.NO_OP;

    public QuoteTextView(Context context) {
        super(context);
    }

    public QuoteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public QuoteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Inject
    public void delegate() {
        mLifecycle = new MVPLifecycleViewDelegate(this, presenter);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        mLifecycle.onAttachedToWindow();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mLifecycle.onDetachedFromWindow();
    }

    @Override
    public void onShowPopup(String content) {
        if (popup == null) {
            View card = LayoutInflater.from(getContext()).inflate(R.layout.simple_card, null);
            txtPopup = ((TextView) card.findViewById(R.id.txt_simple_card));
            popup = new PopupWindow(card,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            if (Build.VERSION.SDK_INT >= 21) {
                popup.setElevation(5.0f);
            }
            popup.setOutsideTouchable(true);
            popup.setFocusable(true);
            popup.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            popup.setTouchInterceptor((v1, event) -> {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    presenter.hidePopup();
                    return true;
                }
                return false;
            });
        }
        txtPopup.setText(content);
        popup.showAsDropDown(this);
    }

    @Override
    public void onHidePopup() {
        Preconditions.checkNotNull(popup);
        popup.dismiss();
    }

    @Override
    public void display(QuoteState quote) {
        setText(quote.bookId + " " + quote.chapter + ":" + quote.verset + "-" + quote.content);
    }


    @Override
    public Map<String, ?> getArgs() {
        return args;
    }

    @Override
    public void onPreAttachPresenter() {

    }

    @Override
    public void onPostAttachPresenter() {
        addListeners();
    }

    @Override
    public void onPreDetachPresenter() {

    }

    private void addListeners() {
        setOnClickListener(v -> presenter.showPopup());
        presenter.reloadQuote(RxView.longClicks(this));
    }

}
