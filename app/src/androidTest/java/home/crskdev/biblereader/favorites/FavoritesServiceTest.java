package home.crskdev.biblereader.favorites;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.test.espresso.core.deps.guava.base.Function;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import home.crskdev.biblereader.AndroidBaseTest;
import home.crskdev.biblereader.DBPrepareTest;
import home.crskdev.biblereader.core.Verset;
import home.crskdev.biblereader.core.db.BibleSQLiteOpenHelper;
import home.crskdev.biblereader.favorites.detail.Tag;
import home.crskdev.biblereader.favorites.detail.tagrecylerview.EditedTag;
import home.crskdev.biblereader.util.Pair;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by criskey on 5/6/2017.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class FavoritesServiceTest extends DBPrepareTest {

    FavoritesService svc;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        svc = new FavoritesService(dbHelper);
    }

    @After
    public void finish() {
        db.close();
    }

    @Test
    public void insertAndUpdateAFavorite() throws Exception {
        Verset fav = Verset.asDTO("BOOK1", 1, 3, "Verset", true);

        TestObserver<Verset> test = svc
                .setFavorite(fav)
                .flatMapObservable(f ->
                        Observable.just(f)
                                .concatWith(
                                        svc.setFavorite(f.setFavorite(false)).toObservable()))
                .test();

        test.assertNoErrors();
        assertEquals(2, test.valueCount());
        test.assertValueAt(0, f -> f.favorite && f.date > 0);
        test.assertValueAt(1, f -> !f.favorite && f.date > 0);

        svc.setFavorite(fav.setFavorite(1, true, -1)).subscribe();

        TestObserver<List<Verset>> test1 = svc.getAllFavorites(5, FavoritesState.INIT_DATE).test();
        assertEquals(1, test1.values().get(0).size());
        // print(test1.values().get(0));

    }

    @Test
    public void getFavoritesPagination() throws Exception {
        TestObserver<List<Verset>> test = Single.concat(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 1, "Verset1", true)),//0
                svc.setFavorite(Verset.asDTO("BOOK2", 2, 2, "Verset2", true)),//1
                svc.setFavorite(Verset.asDTO("BOOK3", 3, 3, "Verset3", true)),//2
                svc.setFavorite(Verset.asDTO("BOOK4", 4, 4, "Verset4", true)),//3
                svc.setFavorite(Verset.asDTO("BOOK5", 5, 5, "Verset5", true)))//4
        ).toList()
                .flatMapObservable(l ->
                        Observable.concat(
                                svc.getAllFavorites(2, FavoritesState.INIT_DATE).toObservable(),
                                svc.getAllFavorites(2, l.get(3).date).toObservable(),
                                svc.getAllFavorites(2, l.get(1).date).toObservable(),
                                svc.getAllFavorites(2, l.get(0).date).toObservable()
                        )).test();
        test.assertNoErrors();
        assertEquals(4, test.valueCount());

        List<Verset> page1 = test.values().get(0);
        assertEquals(2, page1.size());


        List<Verset> page2 = test.values().get(1);
        assertEquals(2, page2.size());


        List<Verset> page3 = test.values().get(2);
        assertEquals(1, page3.size());


        List<Verset> page4 = test.values().get(3);
        assertEquals(0, page4.size());
    }

    @Test
    public void getFavoritesPaginationGroupBy() throws Exception {

        TestObserver<List<Verset>> test = Single.concat(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 1, "Verset1", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 2, 2, "Verset2", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 3, 3, "Verset3", true)),
                svc.setFavorite(Verset.asDTO("BOOK4", 4, 4, "Verset4", true)),
                svc.setFavorite(Verset.asDTO("BOOK5", 5, 5, "Verset5", true)))
        ).toList()
                .flatMapObservable(l ->
                        Observable.concat(
                                svc.getFavorites(2, FavoritesState.INIT_DATE, "BOOK1").toObservable(),
                                svc.getFavorites(2, l.get(1).date, "BOOK1").toObservable(),
                                svc.getFavorites(2, l.get(0).date, "BOOK1").toObservable()
                        )).test();
        test.assertNoErrors();
        assertEquals(3, test.valueCount());

        List<Verset> page1 = test.values().get(0);
        assertEquals(2, page1.size());

        List<Verset> page2 = test.values().get(1);
        assertEquals(1, page2.size());

        List<Verset> page3 = test.values().get(2);
        assertEquals(0, page3.size());

    }

    @Test
    public void getFavoritesUntil() throws Exception {
        TestObserver<List<Verset>> test = Single.merge(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)),
                svc.setFavorite(Verset.asDTO("BOOK2", 1, 3, "Verset3", false).date(2)),
                svc.setFavorite(Verset.asDTO("BOOK3", 1, 7, "Verset7", true).date(3)),
                svc.setFavorite(Verset.asDTO("BOOK4", 1, 5, "Verset5", false).date(4)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 9, "Verset9", true).date(5)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 71, "Verset71", true).date(6)),
                svc.setFavorite(Verset.asDTO("BOOK5", 2, 11, "Verset11", true).date(7)))
        ).toList().flatMap(l -> svc.getAllFavoritesUntil(5)).test();
        test.assertNoErrors();
        List<Verset> page1 = test.values().get(0);
        assertEquals(3, page1.size());
        assertEquals(5, page1.get(page1.size() - 1).date);


        TestObserver<List<Verset>> test1 = svc.getFavoritesUntil(5, "BOOK1").test();
        page1 = test1.values().get(0);
        assertEquals(2, page1.size());
    }

    @Test
    public void getFavoritesFromChapter() throws Exception {
        TestObserver<List<Verset>> test = Single.merge(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 1, "Verset1", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 3, "Verset3", false)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 7, "Verset7", true)),
                svc.setFavorite(Verset.asDTO("BOOK4", 1, 5, "Verset5", false)),
                svc.setFavorite(Verset.asDTO("BOOK5", 2, 1, "Verset1", true)))
        ).toList().flatMap(l -> svc.getFavoritesFromChapter("BOOK1", 1, false)).test();
        test.assertNoErrors();
        List<Verset> page1 = test.values().get(0);
        assertEquals(2, page1.size());
        assertEquals("Verset1", page1.get(0).content);
    }

    @Test
    public void getFavoritesFromChapterIgnoreContent() throws Exception {
        TestObserver<List<Verset>> test = Single.merge(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 1, "Verset1", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 3, "Verset3", false)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 7, "Verset7", true)),
                svc.setFavorite(Verset.asDTO("BOOK4", 1, 5, "Verset5", false)),
                svc.setFavorite(Verset.asDTO("BOOK5", 2, 1, "Verset1", true)))
        ).toList().flatMap(l -> svc.getFavoritesFromChapter("BOOK1", 1, true)).test();
        test.assertNoErrors();
        List<Verset> page1 = test.values().get(0);
        assertEquals(2, page1.size());
        assertEquals("", page1.get(0).content);
    }

    @Test
    public void getTitlesDistinct() throws Exception {

        Flowable<Verset> insert = Single.merge(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK5", 1, 1, "Verset1", true)),
                svc.setFavorite(Verset.asDTO("BOOK4", 1, 5, "Verset5", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 3, "Verset3", true)),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 7, "Verset7", true)),
                svc.setFavorite(Verset.asDTO("BOOK5", 2, 1, "Verset1", true)))
        );

        TestObserver<List<Pair<String, Integer>>> test = insert.toList().flatMap(__ -> svc.getBookIdsWithFavorites()).test();
        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        List<Pair<String, Integer>> ids = test.values().get(0);
        assertEquals(3, ids.size());
        assertEquals(Arrays.asList("BOOK1", "BOOK4", "BOOK5"),
                Arrays.asList(ids.get(0).first, ids.get(1).first, ids.get(2).first));
        assertEquals(Arrays.asList(2, 1, 2),
                Arrays.asList(ids.get(0).second, ids.get(1).second, ids.get(2).second));
    }


    @Test
    public void createTag() throws Exception {
        TestObserver<Verset> test = svc.setFavorite(Verset.asDTO("BOOK5", 1, 1, "Verset1", true))
                .flatMap(v -> Observable.merge(Arrays.asList(
                        svc.setTag(Tag.asDTO("FOO"), v).toObservable(),
                        svc.setTag(Tag.asDTO("BOO"), v).toObservable(),
                        svc.setTag(Tag.asDTO("VAR"), v).toObservable()
                        )).collectInto(new ArrayList<Tag>(), (acc, c) -> {
                            acc.addAll(c.tags);
                        }).map(v::tags)
                ).test();
        test.assertNoErrors();
        Verset verset = test.values().get(0);
        assertEquals(3, verset.tags.size());
        assertEquals("FOO", verset.tags.get(0).name);
        assertEquals("BOO", verset.tags.get(1).name);
        assertEquals("VAR", verset.tags.get(2).name);
    }

    @Test
    public void getTags() throws Exception {
        //create a favorite and add 3 tags for it
        Verset v1 = Verset.asDTO("BOOK5", 1, 1, "Verset1", true);
        Verset v2 = Verset.asDTO("BOOK1", 3, 2, "Verset2", true);
        Flowable<Verset> getTags = insertAndGetTags(v1, v2)
                .andThen(svc.getTags(v1).concatWith(svc.getTags(v2)));

        TestSubscriber<Verset> test = getTags.test();

        test.assertNoErrors();
        assertEquals(2, test.valueCount());
        Verset verset1 = test.values().get(0);
        Verset verset2 = test.values().get(1);

        assertEquals(3, verset1.tags.size());
        assertEquals("BOO", verset1.tags.get(0).name);
        assertEquals("FOO", verset1.tags.get(1).name);
        assertEquals("VAR", verset1.tags.get(2).name);

        assertEquals(3, verset2.tags.size());
        assertEquals("FOO", verset2.tags.get(0).name);
        assertEquals("LOO", verset2.tags.get(1).name);
        assertEquals("VAR", verset2.tags.get(2).name);
    }

    @Test
    public void insertTagsAndRemoveOnCascadeDelete() throws Exception {
        Verset v1 = Verset.asDTO("BOOK5", 1, 1, "Verset1", true);
        Verset v2 = Verset.asDTO("BOOK1", 3, 2, "Verset2", true);
        TestSubscriber<Verset> test = insertAndGetTags(v1, v2)
                .andThen(svc.removeTag(new Tag(1, "FOO")))
                .andThen(svc.getTags(v1).concatWith(svc.getTags(v2))).test();


        test.assertNoErrors();
        assertEquals(2, test.valueCount());
        Verset verset1 = test.values().get(0);
        Verset verset2 = test.values().get(1);

        assertEquals(2, verset1.tags.size());
        assertEquals("BOO", verset1.tags.get(0).name);
        assertEquals("VAR", verset1.tags.get(1).name);

        assertEquals(2, verset2.tags.size());
        assertEquals("LOO", verset2.tags.get(0).name);
        assertEquals("VAR", verset2.tags.get(1).name);

    }

    @Test
    public void insertAndUpdateTag() throws Exception {
        TestObserver<Tag> test = svc.saveTag(Tag.asDTO("FOO"))
                .flatMap(t -> svc.saveTag(new Tag(t.id, "BAR")))
                .test();
        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        Tag tag = test.values().get(0);
        assertEquals("BAR", tag.name);
    }

    @Test
    public void existTag() throws Exception {
        Tag foo = Tag.asDTO("FOO");
        Observable<Tag> batch = Observable.merge(Arrays.asList(
                svc.saveTag(foo).toObservable(),
                svc.saveTag(Tag.asDTO("VOO")).toObservable(),
                svc.saveTag(Tag.asDTO("LOO")).toObservable(),
                svc.saveTag(Tag.asDTO("BOO")).toObservable()
        ));
        TestObserver<List<Boolean>> test = Completable.fromObservable(batch).andThen(
                svc.existTag(EditedTag.from(foo, "VOO").commit()).toObservable()
                        .concatWith(svc.existTag(Tag.asDTO("FOO")).toObservable())
                        .concatWith(svc.existTag(new Tag(2, "XXX")).toObservable())
                        .concatWith(svc.existTag(Tag.asDTO("XXX")).toObservable())
        ).toList().test();
        test.assertNoErrors();
        assertEquals(Arrays.asList(true, true, false, false), test.values().get(0));

    }

    @Test
    public void getAllTagsInOrderASC() throws Exception {
        Observable<Tag> batch = Observable.merge(Arrays.asList(
                svc.saveTag(Tag.asDTO("FOO")).toObservable(),
                svc.saveTag(Tag.asDTO("VOO")).toObservable(),
                svc.saveTag(Tag.asDTO("LOO")).toObservable(),
                svc.saveTag(Tag.asDTO("BOO")).toObservable()
        ));
        TestObserver<List<Tag>> test = Completable
                .fromObservable(batch).andThen(svc.getAllTags()).test();
        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        List<CharSequence> tags = mapList(test.values().get(0), (tag) -> tag.name);
        assertEquals(Arrays.asList("BOO", "FOO", "LOO", "VOO"), tags);
    }

    @Test
    public void getFavoritesWithTag() throws Exception {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Verset v1 = Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)
                .tags(Arrays.asList(tags.get(0), tags.get(1), tags.get(2)));
        Verset v2 = Verset.asDTO("BOOK2", 3, 2, "Verset2", true).date(2)
                .tags(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)));
        Verset v3 = Verset.asDTO("BOOK3", 3, 2, "Verset2", true).date(3)
                .tags(Collections.singletonList(tags.get(0)));
        Verset v4 = Verset.asDTO("BOOK4", 3, 2, "Verset2", true).date(4)
                .tags(Collections.singletonList(tags.get(0)));

        TestObserver<List<Verset>> test = insertAndGetTags(Arrays.asList(v1, v2, v3, v4), tags)
                .andThen(svc.getFavoritesWithTag(new Tag(1, "FOO"), 2, FavoritesState.INIT_DATE)).test();
        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        List<Verset> versets = test.values().get(0);
        assertEquals(Arrays.asList("BOOK4", "BOOK3"), Arrays.asList(
                versets.get(0).bookId,
                versets.get(1).bookId
        ));
    }

    @Test
    public void getAllFavoritesWithTags() throws Exception {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Verset v1 = Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)
                .tags(Arrays.asList(tags.get(0), tags.get(1), tags.get(2)));
        Verset v2 = Verset.asDTO("BOOK2", 3, 2, "Verset2", true).date(2)
                .tags(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)));
        Verset v3 = Verset.asDTO("BOOK3", 3, 2, "Verset2", true).date(3)
                .tags(Collections.singletonList(tags.get(0)));
        Verset v4 = Verset.asDTO("BOOK4", 3, 2, "Verset2", true).date(4)
                .tags(Collections.singletonList(tags.get(0)));

        TestObserver<List<Verset>> test = insertAndGetTags(Arrays.asList(v1, v2, v3, v4), tags)
                .andThen(svc.getAllFavoritesWithTag(10, FavoritesState.INIT_DATE)).test();

        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        List<Verset> versets = test.values().get(0);
        assertEquals(Arrays.asList("BOOK4", "BOOK3", "BOOK2", "BOOK1"), mapList(versets, (v) -> v.bookId));

    }

    @Test
    public void getValidFavorites() throws Exception {

        List<Verset> justDTOVersets = Arrays.asList(
                //these are saved in db
                Verset.asDTO("BOOK5", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK4", 1, 5, "Verset5", false),
                Verset.asDTO("BOOK1", 1, 3, "Verset3", false),

                //these are not
                Verset.asDTO("BOOK10", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK11", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK12", 1, 1, "Verset1", false),

                //these are saved in db
                Verset.asDTO("BOOK1", 1, 7, "Verset7", false),
                Verset.asDTO("BOOK5", 2, 1, "Verset1", false),

                //these are not
                Verset.asDTO("BOOK13", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK14", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK15", 1, 1, "Verset1", false),
                Verset.asDTO("BOOK16", 1, 1, "Verset1", false)
        );
        TestObserver<List<Verset>> test = Completable.fromObservable(Observable.merge(Arrays.asList(
                svc.setFavorite(Verset.asDTO("BOOK5", 1, 1, "Verset1", true)).toObservable(),
                svc.setFavorite(Verset.asDTO("BOOK4", 1, 5, "Verset5", true)).toObservable(),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 3, "Verset3", true)).toObservable(),
                svc.setFavorite(Verset.asDTO("BOOK1", 1, 7, "Verset7", true)).toObservable(),
                svc.setFavorite(Verset.asDTO("BOOK5", 2, 1, "Verset1", true)).toObservable())
        )).andThen(svc.getValidFavorites(justDTOVersets)).test();

        List<Verset> validVersets = test.values().get(0);


        assertEquals(justDTOVersets.size(), validVersets.size());

        assertTrue(validVersets.get(0).favorite);
        assertTrue(validVersets.get(1).favorite);
        assertTrue(validVersets.get(2).favorite);

        assertFalse(validVersets.get(3).favorite);
        assertFalse(validVersets.get(4).favorite);
        assertFalse(validVersets.get(5).favorite);

        assertTrue(validVersets.get(6).favorite);
        assertTrue(validVersets.get(7).favorite);

        assertFalse(validVersets.get(8).favorite);
        assertFalse(validVersets.get(9).favorite);
        assertFalse(validVersets.get(10).favorite);
        assertFalse(validVersets.get(11).favorite);

    }

    @Test
    public void getAllFavoritesWithTagsUntil() throws Exception {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Verset v1 = Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)
                .tags(Arrays.asList(tags.get(0), tags.get(1), tags.get(2)));
        Verset v2 = Verset.asDTO("BOOK2", 3, 2, "Verset2", true).date(2)
                .tags(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)));
        Verset v3 = Verset.asDTO("BOOK3", 3, 2, "Verset2", true).date(3)
                .tags(Collections.singletonList(tags.get(0)));
        Verset v4 = Verset.asDTO("BOOK4", 3, 2, "Verset2", true).date(4)
                .tags(Collections.singletonList(tags.get(0)));

        TestObserver<List<Verset>> test = insertAndGetTags(Arrays.asList(v1, v2, v3, v4), tags)
                .andThen(svc.getAllFavoritesWithTagUntil(10, 2)).test();

        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        List<Verset> versets = test.values().get(0);
        assertEquals(Arrays.asList("BOOK4", "BOOK3", "BOOK2"), mapList(versets, (v) -> v.bookId));

    }

    @Test
    public void checkIfFavoriteHasTag() throws Exception {
        List<Tag> tags = Collections.singletonList(
                new Tag(1, "FOO"));
        Verset v1 = Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)
                .tags(Collections.singletonList(tags.get(0)));
        TestObserver<Boolean> test = insertAndGetTags(Collections.singletonList(v1), tags).andThen(
                svc.hasTag(tags.get(0), v1)).test();
        test.assertNoErrors();
        assertEquals(1, test.valueCount());
        assertEquals(true, test.values().get(0));
    }


    @Test
    public void removeVersetTags() throws Exception {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Verset v1 = Verset.asDTO("BOOK1", 1, 1, "Verset1", true).date(1)
                .tags(Arrays.asList(tags.get(0), tags.get(1), tags.get(2)));
        Verset v2 = Verset.asDTO("BOOK2", 3, 2, "Verset2", true).date(2)
                .tags(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)));
        TestObserver<List<Tag>> test = insertAndGetTags(Arrays.asList(v1, v2), tags)
                .andThen(svc.removeTags(Arrays.asList(tags.get(0), tags.get(1)), v1))
                .andThen(Observable.concat(
                        svc.getTags(v1).map(v -> v.tags).toObservable(),
                        svc.getTags(v2).map(v -> v.tags).toObservable())
                ).test();
        test.assertNoErrors();
        assertEquals(Collections.singletonList(tags.get(2)), test.values().get(0));
        assertEquals(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)), test.values().get(1));
    }

    @Test
    public void editTags() throws Exception {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Observable<Tag> batchTags = Observable
                .merge(mapList(tags, tag -> svc.saveTag(Tag.asDTO(tag.name)).toObservable()));
        TestObserver<List<Tag>> test = Completable.fromObservable(batchTags).andThen(svc.editTags(Arrays.asList(
                EditedTag.from(tags.get(0), "FOOUpdate").commit(),
                EditedTag.from(tags.get(2), "VARUpdate").commit()
        ))).andThen(svc.getAllTags()).test();
        test.assertNoErrors();
        //asc order
        assertEquals(Arrays.asList("BOO", "FOOUpdate", "LOO", "VARUpdate"),
                mapList(test.values().get(0), t -> t.name));

    }

    private Completable insertAndGetTags(List<Verset> versets, List<Tag> tags) {
        Observable<Tag> batchTags = Observable
                .merge(mapList(tags, tag -> svc.saveTag(Tag.asDTO(tag.name)).toObservable()));
        Observable<Verset> batchVersets = Observable
                .concat(mapList(versets, v -> svc.setFavorite(v).toObservable()));
        List<Observable<Verset>> tagVersetObs = new ArrayList<>();
        for (Verset verset : versets) {
            List<Tag> vTags = verset.tags;
            for (Tag vTag : vTags) {
                tagVersetObs.add(svc.setTag(vTag, verset).toObservable());
            }
        }
        Observable<Verset> batchTagVersets = Observable.concat(tagVersetObs);
        return Completable.fromObservable(batchTags)
                .andThen(Completable.fromObservable(batchVersets))
                .andThen(Completable.fromObservable(batchTagVersets));
    }

    private Completable insertAndGetTags(final Verset v1, final Verset v2) {
        List<Tag> tags = Arrays.asList(
                new Tag(1, "FOO"),
                new Tag(2, "BOO"),
                new Tag(3, "VAR"),
                new Tag(4, "LOO")
        );
        Verset v11 = v1.date(1).tags(Arrays.asList(tags.get(0), tags.get(1), tags.get(2)));
        Verset v22 = v2.date(2).tags(Arrays.asList(tags.get(0), tags.get(3), tags.get(2)));
        return insertAndGetTags(Arrays.asList(v11, v22), tags);
    }

    private <T> void print(T data) {
        System.out.println(data);
    }

    private <T> void printList(Collection<T> c) {
        for (T t : c) {
            System.out.println(t);
        }

    }

    private <T, F> List<T> mapList(@NonNull Collection<F> list, Function<F, T> func) {
        List<T> mapped = new ArrayList<T>(list.size());
        for (F f : list) {
            mapped.add(func.apply(f));
        }
        return mapped;
    }
}