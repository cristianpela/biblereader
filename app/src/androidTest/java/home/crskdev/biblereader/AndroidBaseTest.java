package home.crskdev.biblereader;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;

import org.junit.Before;

import java.util.Collection;

import home.crskdev.biblereader.util.asserts.Asserts;
import home.crskdev.biblereader.util.asserts.ThreadingAsserts;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 8/7/2017.
 */

public class AndroidBaseTest {

    static {
        Asserts.threadingAsserts = new ThreadingAsserts() {
            @Override
            public boolean assertIsOnMainThread() {
                return true;
            }

            @Override
            public boolean assertIsBackgroundThread() {
                return true;
            }
        };
    }

    protected Context context;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        context = InstrumentationRegistry.getTargetContext();
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
    }

    <T> void printList(Collection<T> list){
        for (T t : list) {
            System.out.println(t);
        }
    }


}
