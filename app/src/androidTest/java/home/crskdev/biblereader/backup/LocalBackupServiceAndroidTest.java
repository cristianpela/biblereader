package home.crskdev.biblereader.backup;

import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import home.crskdev.biblereader.AndroidBaseTest;
import home.crskdev.biblereader.core.BibleDownloader;
import home.crskdev.biblereader.core.db.Contract;

import static org.junit.Assert.*;

/**
 * Created by criskey on 2/8/2017.
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class LocalBackupServiceAndroidTest extends AndroidBaseTest {

    LocalBackupService svc;

    File dbFile;
    File bookFile;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        dbFile = context.getDatabasePath(Contract.DB_NAME);
        bookFile = createBookFile();
        svc = new LocalBackupService(dbFile, bookFile, new FileStorageInfo(context));
    }

    @After
    public void tearDown() throws Exception {
        deleteFiles(svc.backupDir());
    }

    @Test
    public void backupAndRestore() throws Exception {
        final long beforeDbModif = dbFile.lastModified();
        final long beforeBookModif = bookFile.lastModified();
        svc.backup().andThen(svc.restore()).subscribe(() -> {
            assertTrue(dbFile.lastModified() > beforeDbModif
                    && bookFile.lastModified() > beforeBookModif);
        });
    }

    private File createBookFile() throws IOException {
        File f =  new File(context.getFilesDir(), "book_file.txt");
        String content ="Book file content";
        write(f, content);
        return f;
    }

    private void write(File f, String content) throws IOException {
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(f), "utf-8"));
        writer.write(content);
        writer.close();
    }
    public void deleteFiles(File dir) {
        //lvl 1 delete - no recursive
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                f.delete();
            }
        }
    }
}