package home.crskdev.biblereader;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.CallSuper;
import android.support.test.filters.MediumTest;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.runner.RunWith;

import home.crskdev.biblereader.core.db.BibleSQLiteOpenHelper;

/**
 * Created by criskey on 2/8/2017.
 */
public class DBPrepareTest extends AndroidBaseTest {

    protected SQLiteDatabase db;
    protected BibleSQLiteOpenHelper dbHelper;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();
        dbHelper = new BibleSQLiteOpenHelper(context, "test_bible.db");
        try {
            db = dbHelper.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        dbHelper.onUpgrade(db, db.getVersion(), db.getVersion() + 1);
    }


}
